/**
 * @ingroup HELP
 *
 * @{
 */

/**********************************************************************/
/*! @file math_utils.c
 * @brief mathematical operations (mul, div, sine, cosine, ...)
 *
 * @details
 * encapsulated, easy-to-use math functionalities<br>
 * module prefix: math_
 *
 * @author       JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <math_utils.h>

#include <debug.h>

/*-------------------
   local defines
   -------------------*/

#define SINE_COMPUTATION_TYPE    INTERPOLATION_WITH_SLOPE_LUT
#define ARCSINE_COMPUTATION_TYPE INTERPOLATION_WITH_SLOPE_LUT

#define NO_INTERPOLATION                     1
#define INTERPOLATION_WITH_SLOPE_LUT         ( NO_INTERPOLATION + 1 )             /* PRQA S 3214 */ /* justification: circumvent warning messages due to conditional compilation */
#define INTERPOLATION_WITH_SLOPE_CALCULATION ( INTERPOLATION_WITH_SLOPE_LUT + 1 ) /* PRQA S 3214 */ /* justification: circumvent warning messages due to conditional compilation */


#define MATH_SINE_LUT_LENGTH_BITS 6u
#define MATH_SINE_LUT_LENGTH      ((u16) 1 << MATH_SINE_LUT_LENGTH_BITS )
#define MATH_SINE_LUT_IDX_MSK     ( 0x3F )                                        /* PRQA S 3214 */ /* justification: circumvent warning messages due to conditional compilation */

#define MATH_ARCTAN_LUT_LENGTH_BITS 8u
#define MATH_ARCTAN_LUT_LENGTH      (((u16) 1 << MATH_ARCTAN_LUT_LENGTH_BITS ) + 1u )

/* -------------------
   local declarations
   ------------------- */

/* PRQA S 3218 ++ */ /* justification: tables would decrease readability if they were included in functions */

/**
   @var math_svm_value_LUT
   @brief lookup table for SVM waveform
 */
static const s16 math_svm_value_LUT[ MATH_SINE_LUT_LENGTH ] =
{
  0, 4817, 9588, 14267, 18809, 23169, 25450, 26558, 27410, 27998, 28316, 28361,
  28134, 27635, 26871, 25847, 24575, 25847, 26871, 27635, 28134, 28361, 28316,
  27998, 27410, 26558, 25450, 23169, 18809, 14267, 9588, 4817, 0, -4817, -9588,
  -14267, -18809, -23169, -25450, -26558, -27410, -27998, -28316, -28361, -28134,
  -27635, -26871, -25847, -24575, -25847, -26871, -27635, -28134, -28361, -28316,
  -27998, -27410, -26558, -25450, -23169, -18809, -14267, -9588, -4817
};

/**
   @var math_svm_gradient_LUT
   @brief lookup table for SVM waveform gradient
 */
static const s16 math_svm_gradient_LUT[ MATH_SINE_LUT_LENGTH ] =
{
  1204, 1192, 1169, 1135, 1090, 570, 276, 212, 146, 79, 11, -56, -124, -191, -255,
  -318, 318, 255, 191, 124, 56, -11, -79, -146, -212, -276, -570, -1090, -1135,
  -1169, -1192, -1204, -1204, -1192, -1169, -1135, -1090, -570, -276, -212, -146,
  -79, -11, 56, 124, 191, 255, 318, -318, -255, -191, -124, -56, 11, 79, 146, 212,
  276, 570, 1090, 1135, 1169, 1192, 1204
};

/**
   @var math_arcsine_value_LUT
   @brief lookup table for arcsine waveform
 */
static const s16 math_arcsine_value_LUT[ 64 ] =
{
  0, 162, 326, 489, 652, 815, 979, 1143, 1307, 1471, 1636, 1801, 1967, 2133, 2300
  , 2467, 2635, 2804, 2973, 3143, 3315, 3487, 3660, 3834, 4009, 4185, 4363, 4542,
  4723, 4905, 5088, 5274, 5461, 5650, 5841, 6035, 6231, 6429, 6630, 6834, 7041,
  7252, 7466, 7684, 7906, 8133, 8365, 8602, 8845, 9095, 9352, 9617, 9892, 10177,
  10474, 10785, 11112, 11460, 11831, 12233, 12676, 13177, 13769, 14537
};

/**
   @var math_arcsine_gradient_LUT
   @brief lookup table for arcsine waveform gradient
 */
static const s16 math_arcsine_gradient_LUT[ 64 ] =
{
  162, 164, 163, 163, 163, 164, 164, 164, 164, 165, 165, 166, 166, 167, 167, 168,
  169, 169, 170, 172, 172, 173, 174, 175, 176, 178, 179, 181, 182, 183, 186, 187,
  189, 191, 194, 196, 198, 201, 204, 207, 211, 214, 218, 222, 227, 232, 237, 243,
  250, 257, 265, 275, 285, 297, 311, 327, 348, 371, 402, 443, 501, 592, 768, 1847
};

/**
   @var math_sine_value_LUT
   @brief lookup table for sine waveform
 */
static const s16 math_sine_value_LUT[ MATH_SINE_LUT_LENGTH ] =
{
  0, 3207, 6384, 9499, 12523, 15426, 18181, 20761, 23140, 25297, 27210, 28861,
  30234, 31316, 32097, 32568, 32726, 32568, 32097, 31316, 30234, 28861, 27210,
  25297, 23140, 20761, 18181, 15426, 12523, 9499, 6384, 3207, 0, -3207, -6384,
  -9499, -12523, -15426, -18181, -20761, -23140, -25297, -27210, -28861, -30234,
  -31316, -32097, -32568, -32726, -32568, -32097, -31316, -30234, -28861, -27210,
  -25297, -23140, -20761, -18181, -15426, -12523, -9499, -6384, -3207
};

/**
   @var math_sine_gradient_LUT
   @brief lookup table for sine waveform gradient
 */
static const s16 math_sine_gradient_LUT[ MATH_SINE_LUT_LENGTH ] =
{
  801, 794, 778, 756, 725, 688, 645, 594, 539, 478, 412, 343, 270, 195, 117,
  39, -40, -118, -196, -271, -344, -413, -479, -540, -595, -645, -689, -726,
  -756, -779, -795, -802, -802, -795, -779, -756, -726, -689, -645, -595, -540,
  -479, -413, -344, -271, -196, -118, -40, 39, 117, 195, 270, 343, 412, 478,
  539, 594, 645, 688, 725, 756, 778, 794, 801
};

/**
   @var arctan_LUT
   @brief lookup table for arctan waveform
 */
static const u16 arctan_LUT[ (u16) MATH_ARCTAN_LUT_LENGTH ] =
{
  0u, 40u, 81u, 122u, 162u, 203u, 244u, 285u, 325u, 366u, 407u, 447u, 488u,
  529u, 569u, 610u, 651u, 691u, 732u, 772u, 813u, 853u, 894u, 934u, 974u,
  1015u, 1055u, 1096u, 1136u, 1176u, 1216u, 1256u, 1297u, 1337u, 1377u, 1417u,
  1457u, 1497u, 1537u, 1576u, 1616u, 1656u, 1696u, 1735u, 1775u, 1814u, 1854u,
  1893u, 1933u, 1972u, 2011u, 2051u, 2090u, 2129u, 2168u, 2207u, 2246u, 2285u,
  2323u, 2362u, 2401u, 2439u, 2478u, 2516u, 2555u, 2593u, 2631u, 2669u, 2707u,
  2746u, 2783u, 2821u, 2859u, 2897u, 2934u, 2972u, 3010u, 3047u, 3084u, 3121u,
  3159u, 3196u, 3233u, 3270u, 3306u, 3343u, 3380u, 3416u, 3453u, 3489u, 3526u,
  3562u, 3598u, 3634u, 3670u, 3706u, 3742u, 3777u, 3813u, 3848u, 3884u, 3919u,
  3954u, 3989u, 4024u, 4059u, 4094u, 4129u, 4163u, 4198u, 4232u, 4267u, 4301u,
  4335u, 4369u, 4403u, 4437u, 4471u, 4504u, 4538u, 4571u, 4605u, 4638u, 4671u,
  4704u, 4737u, 4770u, 4803u, 4835u, 4868u, 4900u, 4933u, 4965u, 4997u, 5029u,
  5061u, 5093u, 5125u, 5156u, 5188u, 5219u, 5251u, 5282u, 5313u, 5344u, 5375u,
  5406u, 5436u, 5467u, 5497u, 5528u, 5558u, 5588u, 5618u, 5648u, 5678u, 5708u,
  5737u, 5767u, 5796u, 5826u, 5855u, 5884u, 5913u, 5942u, 5971u, 6000u, 6028u,
  6057u, 6085u, 6114u, 6142u, 6170u, 6198u, 6226u, 6254u, 6281u, 6309u, 6337u,
  6364u, 6391u, 6419u, 6446u, 6473u, 6500u, 6526u, 6553u, 6580u, 6606u, 6633u,
  6659u, 6685u, 6711u, 6737u, 6763u, 6789u, 6815u, 6841u, 6866u, 6892u, 6917u,
  6942u, 6967u, 6992u, 7017u, 7042u, 7067u, 7092u, 7116u, 7141u, 7165u, 7189u,
  7214u, 7238u, 7262u, 7286u, 7310u, 7333u, 7357u, 7381u, 7404u, 7427u, 7451u,
  7474u, 7497u, 7520u, 7543u, 7566u, 7589u, 7611u, 7634u, 7656u, 7679u, 7701u,
  7723u, 7746u, 7768u, 7790u, 7811u, 7833u, 7855u, 7877u, 7898u, 7920u, 7941u,
  7962u, 7984u, 8005u, 8026u, 8047u, 8068u, 8089u, 8109u, 8130u, 8150u, 8171u,
  8191u
};

/* PRQA S 3218 -- */ /* justification: tables would decrease readability if they were included in functions */

/*! @brief compute SVM(alpha)
 *
 * @details
 * compute space vector modulation waveform from a combination of LUT and interpolation<br>
 * argument 'alpha'' represents the angle with 0x0000 being 0 degrees and MATH_ANGLE_MAX being 360 degrees<br>
 * the result (i.e. return value) is S16_MIN ... S16_MAX
 */
INLINE s16 math_svm_waveform_unsafe ( const u16 alpha )
{
  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SINE_CALC_DURATION )
    dbg_pin3_set();
  #endif

  s16 loc_return_value = 0;

  #if ( SINE_COMPUTATION_TYPE == NO_INTERPOLATION )
    /* without interpolation */
    loc_return_value = math_svm_value_LUT[ (u16) ( alpha ) >> ( 16u - MATH_SINE_LUT_LENGTH_BITS ) ];

  #elif ( SINE_COMPUTATION_TYPE == INTERPOLATION_WITH_SLOPE_LUT )
    /* with interpolation and slope lookup table */
    u16 idx  = alpha >> ( 16u - MATH_SINE_LUT_LENGTH_BITS );
    s16 sinL = math_svm_value_LUT[ idx ];

    math_mulS16_unsafe((s16) (u16) ( alpha & ( 0xFFFFu >> MATH_SINE_LUT_LENGTH_BITS )), math_svm_gradient_LUT[ idx ] );
    math_mul_shiftright_result( 8u );
    loc_return_value = (s16) math_mul_get_result( False ) + sinL;

  #elif ( SINE_COMPUTATION_TYPE == INTERPOLATION_WITH_SLOPE_CALCULATION )
    /* with interpolation and slope calculation */
    u16 idx  = alpha >> ( 16 - MATH_SINE_LUT_LENGTH_BITS );
    s16 sinL = math_svm_sine_value_LUT[ idx ];
    s16 sinH = math_svm_sine_value_LUT[ ( idx + 1 ) & MATH_SINE_LUT_IDX_MSK ];
    s16 m    = ( sinH - sinL ) >> ( 16 - MATH_SINE_LUT_LENGTH_BITS - 8 );

    math_mulS16_unsafe( alpha & ( 0xFFFF >> MATH_SINE_LUT_LENGTH_BITS ), m );
    math_mul_shiftright_result( 8 );
    loc_return_value = math_mul_get_result( False ) + sinL;

  #else
  #error "ERROR! Missing type for computation of sine/cosine!";
  #endif

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SINE_CALC_DURATION )
    dbg_pin3_clear();
  #endif

  return loc_return_value;
}

/*! @brief compute sine(alpha)
 *
 * @details
 * compute sine(alpha) from a combination of LUT and interpolation<br>
 * argument 'alpha'' represents x with 0x0000 being 0 degrees and MATH_ANGLE_MAX being 360 degrees<br>
 * the result (i.e. return value) is S16_MIN ... S16_MAX<br>
 */
INLINE s16 math_sine_unsafe ( const u16 alpha )
{
  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SINE_CALC_DURATION )
    dbg_pin3_set();
  #endif

  s16 loc_return_value = 0;

  #if ( SINE_COMPUTATION_TYPE == NO_INTERPOLATION )
    /* without interpolation */
    loc_return_value = math_sine_value_LUT[ (u16) ( alpha ) >> ( 16u - MATH_SINE_LUT_LENGTH_BITS ) ];

  #elif ( SINE_COMPUTATION_TYPE == INTERPOLATION_WITH_SLOPE_LUT )
    /* with interpolation and slope lookup table */
    u16 idx  = alpha >> ( 16u - MATH_SINE_LUT_LENGTH_BITS );
    s16 sinL = math_sine_value_LUT[ idx ];

    math_mulS16_unsafe((s16) (u16) ( alpha & ( 0xFFFFu >> MATH_SINE_LUT_LENGTH_BITS )), math_sine_gradient_LUT[ idx ] );
    math_mul_shiftright_result( 8u );
    loc_return_value = (s16) math_mul_get_result( False ) + sinL;

  #elif ( SINE_COMPUTATION_TYPE == INTERPOLATION_WITH_SLOPE_CALCULATION )
    /* with interpolation and slope calculation */
    u16 idx  = alpha >> ( 16 - MATH_SINE_LUT_LENGTH_BITS );
    s16 sinL = math_sine_value_LUT[ idx ];
    s16 sinH = math_sine_value_LUT[ ( idx + 1 ) & MATH_SINE_LUT_IDX_MSK ];
    s16 m    = ( sinH - sinL ) >> ( 16 - MATH_SINE_LUT_LENGTH_BITS - 8 );

    math_mulS16_unsafe( alpha & ( 0xFFFF >> MATH_SINE_LUT_LENGTH_BITS ), m );
    math_mul_shiftright_result( 8 );
    loc_return_value = math_mul_get_result( False ) + sinL;

  #else
  #error "ERROR! Missing type for computation of sine/cosine!";
  #endif

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SINE_CALC_DURATION )
    dbg_pin3_clear();
  #endif

  return loc_return_value;
}

/*! @brief compute arcsine(x)
 *
 * @details
 * compute acrsine(x) from a combination of LUT and interpolation<br>
 * NOTE: this computation implements only 0 ... 90 degrees of the arcsine function!<br>
 * argument 'sine_val' is valid from 0 to .. S16_MAX<br>
 * the result (i.e. the angle) is 0 ... MATH_ANGLE_MAX / 4<br>
 */
INLINE u16 math_arcsine_unsafe ( const s16 sine_val )
{
  u16 loc_return_value = 0u;

  #if ( ARCSINE_COMPUTATION_TYPE == NO_INTERPOLATION )
    /* without interpolation */
    loc_return_value = (u16) math_arcsine_value_LUT[ (u16) sine_val >> 9 ];

  #elif ( ARCSINE_COMPUTATION_TYPE == INTERPOLATION_WITH_SLOPE_LUT )
    /* with interpolation and slope lookup table */
    u16 idx  = (u16) sine_val >> 9;
    u16 sinL = (u16) math_arcsine_value_LUT[ idx ];

    math_mulS16_unsafe((s16) (u16) ((u16) sine_val & ( 0xFFFFu >> 7 /*todo: check*/ )), math_arcsine_gradient_LUT[ idx ] );
    math_mul_shiftright_result( 9u );
    loc_return_value = (u16) math_mul_get_result( False ) + sinL;

  #elif ( ARCSINE_COMPUTATION_TYPE == INTERPOLATION_WITH_SLOPE_CALCULATION )
  #error not supported!
  #else
  #error "ERROR! Missing type for computation of sine/cosine!";
  #endif

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_SINE_CALC_DURATION )
    dbg_pin3_clear();
  #endif

  return loc_return_value;
}

/*! @brief compute sine(alpha)
 * @details see math_sine(alpha)
 */
INLINE s16 math_cosine_unsafe ( const u16 alpha )
{
  return math_sine_unsafe( alpha + (u16) ( MATH_ANGLE_MAX / 4u ));
}

/*! @brief compute angle between x and y component
 * @details actually a wrapper for arctan(y, x) so that x and y can have arbitrary values
 */
INLINE u16 math_get_angle_unsafe ( s16 y, s16 x )
{
  s16 loc_x = x;
  s16 loc_y = y;

  u16 loc_return_value = 0u;

  if( loc_y > 0 )
  {
    if( loc_x > 0 )
    {
      if( loc_y < loc_x )
      {
        /* y > 0  &  x > 0  &  y < x */
        loc_return_value = arctan_LUT[ math_divS32_unsafe( SHL_S32((s32) loc_y, MATH_ARCTAN_LUT_LENGTH_BITS ), loc_x, False ) ];
      }
      else
      {
        /* y > 0  &  x > 0  &  y >= x */
        loc_return_value = (u16) ( 65535uL / 4uL ) - arctan_LUT[ math_divS32_unsafe( SHL_S32 ((s32) loc_x, MATH_ARCTAN_LUT_LENGTH_BITS ), loc_y, False ) ];
      }
    }
    else
    {
      loc_x = -loc_x;
      if( loc_y < loc_x )
      {
        /* y > 0  &  x <= 0  & y < x */
        loc_return_value = (u16) ( 65535uL / 2uL ) - arctan_LUT[ math_divS32_unsafe( SHL_S32((s32) loc_y, MATH_ARCTAN_LUT_LENGTH_BITS ), loc_x, False ) ];
      }
      else
      {
        /* y > 0  &  x <= 0  & y >= x */
        loc_return_value = (u16) ( 65535uL / 4uL ) + arctan_LUT[ math_divS32_unsafe( SHL_S32 ((s32) loc_x, MATH_ARCTAN_LUT_LENGTH_BITS ), loc_y, False ) ];
      }
    }
  }
  else
  {
    loc_y = -loc_y;
    if( loc_x > 0 )
    {
      if( loc_y < loc_x )
      {
        /* y <= 0  &  x > 0  & y < x */
        loc_return_value = (u16) ( 65535uL ) - arctan_LUT[ math_divS32_unsafe( SHL_S32((s32) loc_y, MATH_ARCTAN_LUT_LENGTH_BITS ), loc_x, False ) ];
      }
      else
      {
        /* y <= 0  &  x > 0  & y >= x */
        loc_return_value = (u16) (( 65535uL * 3uL ) / 4uL ) + arctan_LUT[ math_divS32_unsafe( SHL_S32 ((s32) loc_x, MATH_ARCTAN_LUT_LENGTH_BITS ), loc_y, False ) ];
      }
    }
    else
    {
      loc_x = -loc_x;

      if( loc_y < loc_x )
      {
        /* y <= 0  &  x <= 0  &  y < x */
        loc_return_value = (u16) ( 65535uL / 2uL ) + arctan_LUT[ math_divS32_unsafe( SHL_S32((s32) loc_y, MATH_ARCTAN_LUT_LENGTH_BITS ), loc_x, False ) ];
      }
      else
      {
        if( loc_y == 0 )
        {
          /* y == 0  &  x <= 0  &  y >= x */
          loc_return_value = (u16) (( 65535uL * 3uL ) / 4uL ) - arctan_LUT[ MATH_ARCTAN_LUT_LENGTH - 1u ];
        }
        else
        {
          /* y < 0  &  x <= 0  &  y >= x */
          loc_return_value = (u16) (( 65535uL * 3uL ) / 4uL ) - arctan_LUT[ math_divS32_unsafe( SHL_S32 ((s32) loc_x, MATH_ARCTAN_LUT_LENGTH_BITS ), loc_y, False ) ];
        }
      }
    }
  }

  return loc_return_value;
}

/*! @brief wrapper for MUL module of the CPU: U16 * S32; without reading back the result */
INLINE void math_mulU16_S32_unsafe ( u16 m1, s32 m2 )
{
  WRITE_REG_U16( &H430_MUL_MPY, m1 );
  WRITE_REG_U16( &H430_MUL_OP2, HIWORD((u32) m2 ));
  u16 resLow = READ_REG_U16( &H430_MUL_RESLO );

  WRITE_REG_U16( &H430_MUL_OP2, LOWORD((u32) m2 ));
  WRITE_REG_U16( &H430_MUL_RESHI, READ_REG_U16( &H430_MUL_RESHI ) + resLow );
}

/*! @brief wrapper for MUL module of the CPU: S16 * S16; without reading back the result */
INLINE void math_mulS16_unsafe ( s16 m1, s16 m2 )
{
  WRITE_REG_U16( &H430_MUL_MPYS, (u16) ( m1 ));
  WRITE_REG_U16( &H430_MUL_OP2S, (u16) ( m2 ));
}

/*! @brief wrapper for MUL module of the CPU: - (S16 * S16); without reading back the result */
INLINE void math_nmulS16_unsafe ( s16 m1, s16 m2 )
{
  WRITE_REG_U16( &H430_MUL_MPYS, (u16) ( m1 ));
  WRITE_REG_U16( &H430_MUL_OP2SN, (u16) ( m2 ));
}

/*! @brief wrapper for MUL module of the CPU: ACCU + (S16 * S16); without reading back the result */
INLINE void math_mulS16_acc_unsafe ( s16 m1, s16 m2 )
{
  WRITE_REG_U16( &H430_MUL_MACS, (u16) ( m1 ));
  WRITE_REG_U16( &H430_MUL_OP2S, (u16) ( m2 ));
}

/*! @brief wrapper for MUL module of the CPU: ACCU - (S16 * S16); without reading back the result */
INLINE void math_nmulS16_acc_unsafe ( s16 m1, s16 m2 )
{
  WRITE_REG_U16( &H430_MUL_MACS, (u16) ( m1 ));
  WRITE_REG_U16( &H430_MUL_OP2SN, (u16) ( m2 ));
}

/*! @brief wrapper for MUL module of the CPU: U16 * U16; without reading back the result */
INLINE void math_mulU16_unsafe ( u16 m1, u16 m2 )
{
  WRITE_REG_U16( &H430_MUL_MPY, m1 );
  WRITE_REG_U16( &H430_MUL_OP2, m2 );
}

/*! @brief wrapper for MUL module of the CPU: ACCU + (U16 * U16); without reading back the result */
INLINE void math_mulU16_acc_unsafe ( u16 m1, u16 m2 )
{
  WRITE_REG_U16( &H430_MUL_MAC, m1 );
  WRITE_REG_U16( &H430_MUL_OP2, m2 );
}

/*! @brief wrapper for MUL module of the CPU: ACCU >> cnt */
INLINE void math_mul_shiftright_result ( u16 cnt )
{
  WRITE_REG_U16( &H430_MUL_SHIFT_RIGHT, ( cnt ) - 1u );
}

/*! @brief wrapper for MUL module of the CPU: ACCU << cnt */
INLINE void math_mul_shiftleft_result ( u16 cnt )
{
  WRITE_REG_U16( &H430_MUL_SHIFT_LEFT, ( cnt ) - 1u );
}

/*! @brief wrapper for MUL module of the CPU: read back the result */
INLINE s32 math_mul_get_result ( BOOL return32Bit )
{
  s32 loc_return_value = 0L;

  loc_return_value = ( return32Bit ) ? ( READ_REG_S32( &H430_MUL_RESLO )) : ((s32) READ_REG_S16( &H430_MUL_RESLO ));

  return loc_return_value;
}

/*! @brief wrapper for DIV module of the CPU: U32 / U16; including reading back the result
 * @details this function blocks program execution for as long as the DIV module takes for the operation (12 clocks)
 */
INLINE u32 math_divU32_unsafe ( u32 dividend, u16 divisor, BOOL return32Bit )
{
  WRITE_REG_U32( &DIVIDER_OP1LO, dividend );
  WRITE_REG_U16( &DIVIDER_OP2, divisor );

  return math_unblockedDivReadResult_unsafe( return32Bit );
}

/*! @brief wrapper for DIV module of the CPU: U32 / U16; without reading back the result */
INLINE void math_unblockedDivU32_unsafe ( u32 dividend, u16 divisor )
{
  WRITE_REG_U32( &DIVIDER_OP1LO, dividend );
  WRITE_REG_U16( &DIVIDER_OP2, divisor );
}

/*! @brief wrapper for DIV module of the CPU: S32 / S16; including reading back the result
 * @details this function blocks program execution for as long as the DIV module takes for the operation (12 clocks)
 */
INLINE s32 math_divS32_unsafe ( s32 dividend, s16 divisor, BOOL return32Bit )
{
  WRITE_REG_S32( &DIVIDER_OP1LO, dividend );
  WRITE_REG_U16( &DIVIDER_OP2S, (u16) divisor );

  return (s32) math_unblockedDivReadResult_unsafe( return32Bit );
}

/*! @brief wrapper for DIV module of the CPU: S32 / S16; without reading back the result */
INLINE void math_unblockedDivS32_unsafe ( s32 dividend, s16 divisor )
{
  WRITE_REG_S32( &DIVIDER_OP1LO, dividend );
  WRITE_REG_U16( &DIVIDER_OP2S, (u16) divisor );
}

/*! @brief wrapper for DIV module of the CPU: read back the result */
INLINE u32 math_unblockedDivReadResult_unsafe ( BOOL return32Bit )
{
  u32 loc_return_value = 0uL;

  loc_return_value = ( return32Bit ) ? ( READ_REG_U32( &DIVIDER_RESULTLO )) : ((u32) READ_REG_U16( &DIVIDER_RESULTLO ));

  return loc_return_value;
}

/*! @brief wrapper for DIV module of the CPU: read back the remainder of the DIV operation */
INLINE u16 math_unblockedDivReadRemainder_unsafe ( void )
{
  return READ_REG_U16( &DIVIDER_REMAINDER );
}

INLINE u16 math_max_U16( u16 val_1, u16 val_2 )
{
  return ( val_1 > val_2 ) ? val_1 : val_2;
}

INLINE u16 math_min_U16( u16 val_1, u16 val_2 )
{
  return ( val_1 < val_2 ) ? val_1 : val_2;
}

INLINE s16 math_max_S16( s16 val_1, s16 val_2 )
{
  return ( val_1 > val_2 ) ? val_1 : val_2;
}

INLINE s16 math_min_S16( s16 val_1, s16 val_2 )
{
  return ( val_1 < val_2 ) ? val_1 : val_2;
}

INLINE u32 math_max_U32( u32 val_1, u32 val_2 )
{
  return ( val_1 > val_2 ) ? val_1 : val_2;
}

INLINE u32 math_min_U32( u32 val_1, u32 val_2 )
{
  return ( val_1 < val_2 ) ? val_1 : val_2;
}

INLINE s32 math_max_S32( s32 val_1, s32 val_2 )
{
  return ( val_1 > val_2 ) ? val_1 : val_2;
}

INLINE s32 math_min_S32( s32 val_1, s32 val_2 )
{
  return ( val_1 < val_2 ) ? val_1 : val_2;
}

/* actually a macro - DO NOT USE with variables, only with constant values! */
INLINE u16 MATH_DEGREE_TO_MATH_ANGLE( dbl angle )
{
  return (u16) (dbl) ( angle * MATH_DEGREE_TO_MATH_ANGLE_COEF );
}

/* }@
 */
