include $(GHSP_MAKE_DIR)/pre_module.mak
####################################################
# BEGIN: User configurable portion of the makefile #
####################################################

# Module only supports the production build
ifeq ($(_build_),1t50)
# Module only supports the IAR RH850 Toolchain
#ifeq ($($(_build_)_TOOLCHAIN),IAR-RH850)

$(_build_):$(_module_name_)

# Create the binary file name
_binary_name_ := $(_build_)
_binary_file_ := $($(_build_)_BINARY_DIRECTORY)/$(_binary_name_).d43
_hex_file_    := $($(_build_)_BINARY_DIRECTORY)/$(_binary_name_).hex

$(_module_target_):auto_version mod_elmos_motor_tune
      
.PHONY: $(_module_target_)
.PHONY: $(_module_name_)
$(_module_name_):$(_module_target_)
$(_module_target_):BUILD_NAME:=$(_build_)
$(_module_target_):$(_hex_file_) $(_binary_file_)
$(_hex_file_) $(_binary_file_):$($(_build_)_IAR_PROJECT_FILE)
	@$(ECHO) "Building IAR Workbench Project $($(BUILD_NAME)_IAR_PROJECT_FILE)"
	@IarBuild $($(BUILD_NAME)_IAR_PROJECT_FILE) -build $(BUILD_NAME) -log all; true

####################################################
# END: user configurable section of the makefile   #
####################################################

#include $(GHSP_MAKE_DIR)/post_module.mak
ALL_$(_build_)_MODULES += $(_module_name_)

#endif #ifeq ($($(_build_)_TOOLCHAIN),IAR-RH850)
endif #ifeq ($(_build_),1t50)