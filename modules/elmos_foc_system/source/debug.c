/**********************************************************************/
/*! @file debug.c
 * @brief no text
 *
 * @details
 * no text
 *
 * module prefix: dbg_
 *
 * @author       JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <debug.h>

#if ( DEBUG == 1 )

 #if ( DBG_HALT_ON_IC_TYPE_MISMATCH != 0 )
    static uint16_t DBG_FLASH_INFO_IC_VERSION_CONVERTER( const uint16_t val )
    {
      return ((((uint16_t) (( val ) / 1u ) % 10u ) << 0 )
              + (((uint16_t) (( val ) / 10u ) % 10u ) << 4 )
              + (((uint16_t) (( val ) / 100u ) % 10u ) << 8 )
              + (((uint16_t) (( val ) / 1000u ) % 10u ) << 12 ));
    }

/*****************************************************************************/
/* info page stuff                                                           */
/*****************************************************************************/
    NO_INLINE static uint16_t dbg_flash_read_info_word( uint16_t addr )
    {
      static volatile uint16_t data;
      typedef uint16_t * (* fn_ptr)( void );

      static volatile uint16_t code[] = {
        0x1204u,                                                        /* PUSH R4 */
        0x40b2u,                                                        /* MOV  #0xA540, &0x0604   ; switch to IB read mode */
        0xa502u,
        FLASH_CTRL_MODE_ADDR,
        0x4034u,                                                        /* MOV  #addr, R4 */
        0x0000u,                                                        /* addr                    ; code + 0x0A */
        0x44a2u,                                                        /* MOV  @R4, &data */
        0x0000u,                                                        /* data                    ; code + 0x0E */
        0x40b2u,                                                        /* MOV  #0xA501, &0x0604   ; switch to MB read */
        0xa501u,
        FLASH_CTRL_MODE_ADDR,
        0x4134u,                                                        /* POP  R4 */
        0x4130u,                                                        /* RET                     ; return to C program */
        0x4303u,                                                        /* NOP                     ; prevent uninitialized RAM */
        0x4303u                                                         /* NOP                     ; prevent uninitialized RAM */
      };

      code[ 0x0A / 2 ] = (uint16_t) addr;
      code[ 0x0E / 2 ] = U16PTR_TO_U16 ( &data );

      /* call code in RAM */
      __istate_t int_state = __get_interrupt_state();
      __disable_interrupt();
      _Pragma( "diag_suppress = Pm138" )                                /* justification: executing code from SRAM */
        ((fn_ptr) code ) ();                                            /* PRQA S 0307 */ /* justification: executing code from SRAM */
      _Pragma( "diag_default = Pm138" )
      __set_interrupt_state( int_state );

      return data;
    }

 #endif
#endif

/*****************************************************************************/

NO_INLINE void dbg_init( void )
{
  #if ( DEBUG == 1 )

    #if ( DBG_HALT_ON_IC_TYPE_MISMATCH != 0 )

      volatile uint16_t loc_ic_rom_mask_rev = dbg_flash_read_info_word( FLASH_INFO_PAGE_ROM_MASK_REV_ADDR );

      while( loc_ic_rom_mask_rev != FLASH_INFO_PAGE_ROM_MASK_REV )
      {
        __no_operation();
      }

      volatile uint16_t loc_ic_major_id = dbg_flash_read_info_word( FLASH_INFO_PAGE_IC_MAJOR_ID_ADDR );

      while( loc_ic_major_id != (uint16_t) DBG_FLASH_INFO_IC_VERSION_CONVERTER((uint16_t) IC_MAJOR_ID ))
      {
        __no_operation();
      }

      volatile uint16_t loc_ic_minor_id = dbg_flash_read_info_word( FLASH_INFO_PAGE_IC_MINOR_ID_ADDR );

      while( loc_ic_minor_id != (uint16_t) DBG_FLASH_INFO_IC_VERSION_CONVERTER((uint16_t) IC_MINOR_ID ))
      {
        __no_operation();
      }

    #endif

    #if ( DBG_EN_PIN3 == 1 )
      DBG_PIN3_MUX    = 0u;                                             /* GPIO (datasheet 03sp0418.03 - 5.2.5.7) */
      GPIO_C_DATA_OE |= DBG_PIN3;
      GPIO_C_CLEAR( DBG_PIN3 );
    #endif

  #endif
}

/* */
NO_INLINE void dbg_main ( void )
{
  #if ( DEBUG == 1 )

    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_DEBUG_MAIN_DURATION )
      dbg_pin3_set();
    #endif

    #if ( DBG_PIN3_FUNCTION == DBG_SHOW_DEBUG_MAIN_DURATION )
      dbg_pin3_clear();
    #endif

  #endif
}

/*****************************************************************************/

/* */
INLINE void dbg_pin3_pulse ( void )
{
  #if (( DEBUG == 1 ) && ( DBG_EN_PIN3 == 1 ))
    GPIO_C_SET( DBG_PIN3 );
    GPIO_C_CLEAR( DBG_PIN3 );
  #endif
}

/* */
INLINE void dbg_pin3_set ( void )
{
  #if (( DEBUG == 1 ) && ( DBG_EN_PIN3 == 1 ))
    GPIO_C_SET( DBG_PIN3 );
  #endif
}

/* */
INLINE void dbg_pin3_clear ( void )
{
  #if (( DEBUG == 1 ) && ( DBG_EN_PIN3 == 1 ))
    GPIO_C_CLEAR( DBG_PIN3 );
  #endif
}

/* */
INLINE void dbg_pin3_code ( uint8_t code ) /* PRQA S 3206 */ /* justification: parameter is only available when DBG_EN_PIN3 is set */
{
  #if (( DEBUG == 1 ) && ( DBG_EN_PIN3 == 1 ))
    uint8_t shiftVal = code;
    uint8_t cntr     = 8u;

    while( cntr != 0u )
    {
      __disable_interrupt();
      dbg_pin3_set();

      if(( shiftVal & 128u ) != 0u )
      {
        dbg_pin3_set();
        dbg_pin3_set();
      }

      dbg_pin3_clear();
      __enable_interrupt();

      cntr--;
      shiftVal <<= 1;
    }
  #endif
}

/*****************************************************************************/
