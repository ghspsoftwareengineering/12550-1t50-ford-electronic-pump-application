// PRQA S 292 ++
/***************************************************************************//**
 * @file      Lin_MISRA.h
 *
 * @creator		sbai
 * @created		29.06.2016
 *
 * @brief  		Project dependent MISRA rule exceptions
 *
 * $Id: Lin_MISRA.h 1130 2016-07-15 13:10:50Z sbai $
 *
 * $Revision: 1130 $
 *
 * @misra{ M3CM Dir-1.1. - PRQA Msg 105\,828\,930\, 1053 and 1055,
 * The IAR C/C++ Compiler for MSP430 Version 5.40.2 (5.40.2.50380) is compatible to ISO:C99.,
 * A non-C99-compatible compiler will not be able to compile the code.,
 * None}
 * _____
 *
 * @misra{M3CM Dir-1.1. - PRQA Msg 292,
 * The characters '$'\, '@' and "`" are often used by version control tools like SVN or CVS\,
 * code documentation tools like doxygen and many other tools. It does not affect the code.,
 * Wrong displayed letters in comments.,
 * None}
 * _____
 *
 * @misra{M3CM Dir-2.2. - PRQA Msg 2981,
 * It is a a good programming style to initialize variables at the declaration\,
 * even though they are not modified before been used. This prevents the usage
 * of uninitialized variables\, which can be dangerous.,
 * Unnecessary code.,
 * None}
 * _____
 *
 * @misra{M3CM Dir-11.5. - PRQA Msg 316 and 317,
 * One of the main concept of the Elmos LIN Driver is to let the user reserve the
 * memory for it and pass it as a pointer to every function as the first argument.
 * This way it is assured\, that there are no conflicts between the memory management
 * of the bootloader and user application\, because both are using the same driver.
 * As tradeoff the LIN Driver casts this memory to it's internal data structure.
 * Elmos took care of the correct data alignment\, so the reserved memory can used
 * like beeing returned by function malloc\, calloc and realloc.,
 * Alignment conflicts if it has not been taken care off.,
 * The reserved memory is tested against NULL and correct alignment.}
 * _____
 *
 * @misra{M3CM Dir-10.5. - PRQA Msg 4342,
 * The Elmos LIN Driver only uses enums with unsigned values.,
 * Unrecognized type cast when using enums in calculations.,
 * Set up a project rule to only use unsigned enums values.}
 * _____
 *
 * @misra{M3CM Dir-1.2. and 6.1 - PRQA Msg 635,
 * The IAR C/C++ Compiler for MSP430 Version 5.40.2 (5.40.2.50380) supports bit-fields
 * with other types then int. This language extension is not portable but supported
 * by most of the modern c-compilers.,
 * Unportable extension\, if the compiler does not support this language extension.,
 * None, the compiler won't compile the code if this language extension is not supported.}
 * _____
 *
 * @misra{M3CM Dir-19.2. - PRQA Msg 750 and 759,
 * There are useful cases\, where the usage of a union is helpful and efficient. Like
 * access to an register on bit level and as uintX_t.,
 * Can be cause problems, if the developer does not know how to use it correctly.,
 * None.}
 * _____
 *
 * @misra{M3CM Rule-15.4. - PRQA Msg 771,
 * The usage of more then one break statement can be very efficient\, while
 * the code stays readable. It should not be prohibited.,
 * Can make the code unstructured.,
 * It is a good programming directive to make sure the code is structured properly\,
 * regardless of whether one or more break statement is used.}
 * _____
 *
 * @misra{M3CM Rule-8.10. - PRQA Msg 3243,
 * It is a good practice to declare a function inline in a c-file and extern in a
 * header file to give the compiler the possibility to inline function outside of the
 * containing module. On this way a compiler like the IAR compiler, which supports
 * "multi-file compilation"\, is able to inline generic functions\, i.e. like memory
 * write/copy/etc functions\, to other modules.,
 * The compiler will at least warn you about an unexpected behavior or just
 * don't inline the function.,
 * None.}
 * _____
 *
 * @misra{M3CM Rule-13.3. - PRQA Msg 3440 and 3441,
 * It is common practice to use the increment and decrement operator.,
 * None.,
 * A software developer should know how to properly use this operands.}
 * _____
 *
 * @misra{M3CM Dir-4.9. - PRQA Msg 3453,
 * Often it is more efficient to use macros for atomic operations.,
 * No type-checking.,
 * None.}
 *
 ******************************************************************************/
// PRQA S 292 --

#ifndef LIN_MISRA_H_
#define LIN_MISRA_H_

// PRQA S 0105 ++
// PRQA S 0828 ++
// PRQA S 0930 ++
// PRQA S 1053 ++
// PRQA S 1055 ++

// PRQA S 0292 ++

// PRQA S 2981 ++

// PRQA S 0316 ++
// PRQA S 0317 ++

// PRQA S 4342 ++

// PRQA S 0635 ++

// PRQA S 0750 ++
// PRQA S 0759 ++

// PRQA S 0771 ++

// PRQA S 3243 ++

// PRQA S 3440 ++
// PRQA S 3441 ++

// PRQA S 3453 ++

#endif /* LIN_MISRA_H_ */




