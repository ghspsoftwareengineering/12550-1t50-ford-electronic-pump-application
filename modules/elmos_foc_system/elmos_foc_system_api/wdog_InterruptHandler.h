/***************************************************************************//**
 * @file			wdog_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef WDOG_INTERRUPTHANDLER_H_          
#define WDOG_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * WDOG IRQ vector numbers
 ******************************************************************************/
typedef enum {
  wdog_IRQ_EVT_WINDOW             = 0u,
  
  wdog_INTERRUPT_VECTOR_CNT       = 1u  /**< Number of available interrupt vectors */
} wdog_eInterruptVectorNum_t;


/***************************************************************************//**
 * Pointer to WDOG context data
 ******************************************************************************/
typedef void * wdog_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*wdog_InterruptCallback_t) (wdog_eInterruptVectorNum_t irqsrc, wdog_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * WDOG environment data
 ******************************************************************************/
typedef struct wdog_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module */
    wdog_InterruptCallback_t InterrupVectorTable[wdog_INTERRUPT_VECTOR_CNT];
    
    /** WDOG module context data */
    wdog_pInterruptContextData_t ContextData;
    
} wdog_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to WDOG environment data
 ******************************************************************************/
typedef wdog_sInterruptEnvironmentData_t * wdog_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to WDOG module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with wdog_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The WDOG IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void wdog_InterruptEnable(wdog_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to WDOG module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The WDOG IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void wdog_InterruptDisable(wdog_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to WDOG module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void wdog_InterruptRegisterCallback(wdog_eInterruptVectorNum_t irqvecnum, wdog_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to WDOG module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void wdog_InterruptDeregisterCallback(wdog_eInterruptVectorNum_t irqvecnum);

/***************************************************************************//**
 * Handles the WDOG related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void wdog_InterruptHandler(void);

/***************************************************************************//**
 * @brief Initialize WDOG module
 *
 * @param environmentdata  Pointer to Environment data for WDOG module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and WDOG (wdog_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       WDOG module is configured for use.
 *
 * @detaildesc
 * Initializes the WDOG software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as WDOGs or not.
 *
 ******************************************************************************/
void wdog_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, wdog_pInterruptEnvironmentData_t environmentdata, wdog_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

