/**
 * @ingroup MC
 *
 * @{
 */

/**********************************************************************/
/*! @file rotor_pos_detect.c
 * @brief estimate the (electrical) rotor position via test pulse
 *
 * @details
 * todo
 *
 * module prefix: pos_
 *
 * @author       JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <rotor_pos_detect.h>

#include <defines.h>
#include <adc.h>
#include <adc_lists.h>
#include <pwm_ctrl.h>
#include <motor_ctrl.h>
#include <math_utils.h>
#include <uart.h>
#include <debug.h>

static pos_data_t pos_data;                                                                                                                             /* PRQA S 3218 */ /* justification: when USR_EN_INITIAL_POSITION_DETECTION == 1, this variable is accessed all through this compilation unit... */

void pos_generate_struct_signature ( void )
{
  pos_data.struct_signature = U16PTR_TO_U16( &pos_data.struct_signature );
}

#if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )

/*! @brief get-function for state of the state machine */
  pos_detect_state_t pos_get_current_state ( void )
  {
    return pos_data.state;
  }

/*! @brief clear process values */
  static void pos_reset_data( void )
  {
    pos_data.pulse_length_div_period = 0;
    pos_data.pulse_length_mod_period = 0;

    pos_data.meas_time_div_period = 0;
    pos_data.meas_time_mod_period = 0;

    pos_data.pwm_period_cnt = 0;

    pos_data.adc_sample_extension_restore             = (s16) SARADC_CTRL_CFG_SAR_TIMING_bit.sampling_extension;
    SARADC_CTRL_CFG_SAR_TIMING_bit.sampling_extension = (u16) POS_DETECT_ADC_SAMPLING_EXTENSION_VALUE;
  }

/*! @brief compute the needed parameters from the ones set by the user */
  static void pos_calc_params( void )
  {
    math_unblockedDivU32_unsafe((u32) mc_get_data()->pos_detect_data.meas_time, (u16) PWM_PERIOD_DURATION_TICKS );
    pos_data.meas_time_div_period = (s16) math_unblockedDivReadResult_unsafe( False );
    pos_data.meas_time_mod_period = (s16) math_unblockedDivReadRemainder_unsafe();

    math_unblockedDivU32_unsafe((u32) mc_get_data()->pos_detect_data.test_pulse_length, (u16) PWM_PERIOD_DURATION_TICKS );
    pos_data.pulse_length_div_period = (s16) math_unblockedDivReadResult_unsafe( False );
    pos_data.pulse_length_mod_period = (s16) math_unblockedDivReadRemainder_unsafe();

    if((s16) mc_get_data()->pos_detect_data.freerun_duration > 0 )
    {
      pos_data.freerun_duration = mc_get_data()->pos_detect_data.freerun_duration + pos_data.meas_time_div_period;
    }
    else
    {
      pos_data.freerun_duration = 0;
    }
  }

/*! @brief initialize module
 *
 * @details
 */
  void pos_init( void )
  {
    (void) memset((void *) &pos_data, 0, sizeof( pos_data ));                                                                                           /* PRQA S 0314 */ /* justification: come on, it's memset ... */

    pos_data.rev_pol_fet_charge_duration = (s16) POS_REV_POLARITY_FET_CHARGE_DURATION_INIT;

    pos_data.state = POS_DETECT_STATE_INVALID;
  }

/*! @brief the actual calculation of the rotor position from the ADC values
 *
 * @details
 * When the test pulses have been applied, this function is called to calulate the<br>
 * rotor angle from adc_data.pos_detect_xx[3], whereas xx depicts the vector and the<br>
 * index of the array indicates the ordinary number of the measurement on that vector (0..2)<br>
 * <br>
 * NOTE: called on interrupt level!
 */
  static void pos_calc_rotor_position( void )
  {
    s16 loc_average_uv = (s16) (u16) (( adc_get_data()->pos_detect_uv[ 0 ] + adc_get_data()->pos_detect_vu[ 0 ] ) >> 1 );
    s16 loc_average_uw = (s16) (u16) (( adc_get_data()->pos_detect_uw[ 0 ] + adc_get_data()->pos_detect_wu[ 0 ] ) >> 1 );
    s16 loc_average_vw = (s16) (u16) (( adc_get_data()->pos_detect_vw[ 0 ] + adc_get_data()->pos_detect_wv[ 0 ] ) >> 1 );

    s16 loc_rel_ind_u = ((s16) adc_get_data()->pos_detect_uv[ 0 ] - loc_average_uv ) + ( mc_get_data()->pos_detect_data.offset_uv );                    /* UV - average_uv -> L_u */
    s16 loc_rel_ind_w = ((s16) adc_get_data()->pos_detect_wu[ 0 ] - loc_average_uw ) + ( mc_get_data()->pos_detect_data.offset_uw );                    /* WU - average_uw -> L_w */
    s16 loc_rel_ind_v = ((s16) adc_get_data()->pos_detect_vw[ 0 ] - loc_average_vw ) + ( mc_get_data()->pos_detect_data.offset_vw );                    /* VW - average_vw -> L_v */

    /* Clarke transform -> carthesian system */
    /* x_alpha := x_u - ((x_b + x_c) / 2) */
    s16 loc_rel_ind_alpha = loc_rel_ind_u - SHR_S16( loc_rel_ind_v + loc_rel_ind_w, 1u );

    /* x_beta := (x_b - x_c) * sqrt(3) / 2) */
    math_mulS16_unsafe((s16) ( loc_rel_ind_v - loc_rel_ind_w ), (s16) ( SQRT3 * 256.0 ));
    math_mul_shiftright_result( 9u );
    s16 loc_rel_ind_beta = (s16) math_mul_get_result( False );

    u16 loc_rotor_angle = math_get_angle_unsafe( loc_rel_ind_alpha, loc_rel_ind_beta ) - (u16) ( MATH_ANGLE_MAX / 2u );
    loc_rotor_angle >>= 1;

    /* now decide the absolute angle from the polarity of the rotor's magnetic field -> can be deduced from the gradients of high-Z phases */
    s16 loc_grad_vu = (s16) adc_get_data()->pos_detect_vu[ 2 ] - (s16) adc_get_data()->pos_detect_vu[ 0 ];
    s16 loc_grad_wu = (s16) adc_get_data()->pos_detect_wu[ 2 ] - (s16) adc_get_data()->pos_detect_wu[ 0 ];
    s16 loc_grad_wv = (s16) adc_get_data()->pos_detect_wv[ 2 ] - (s16) adc_get_data()->pos_detect_wv[ 0 ];

    /* 0 ... 60 degrees electrical */
    /* explanation: from 0..60 degrees grad_wu is smaller than both, grad_vu and grad_wv -> we compare grad_vu against the average of the
     * two other gradients so as to be less susceptible to measurement errors */
    if( loc_rotor_angle <= (u16) ( MATH_ANGLE_MAX / 6u ))
    {
      if( SHL_S16( loc_grad_wu, 1u ) > ( loc_grad_vu + loc_grad_wv ))
      {
        loc_rotor_angle += (u16) ( MATH_ANGLE_MAX / 2u );
      }
    }
    /* 60 ... 120 degrees electrical */
    else if( loc_rotor_angle <= (u16) ( MATH_ANGLE_MAX / ( 6u / 2u )))
    {
      if( SHL_S16( loc_grad_wv, 1u ) < ( loc_grad_wu + loc_grad_vu ))
      {
        loc_rotor_angle += (u16) ( MATH_ANGLE_MAX / 2u );
      }
    }
    /* 120 ... 180 degrees electrical */
    else /*"if( loc_rotor_angle <= (u16) ( MATH_ANGLE_MAX / ( 6u / 3u )))"*/
    {
      if( SHL_S16( loc_grad_vu, 1u ) > ( loc_grad_wu + loc_grad_wv ))
      {
        loc_rotor_angle += (u16) ( MATH_ANGLE_MAX / 2u );
      }
    }

    /* PRQA S 0311 ++ */ /* justification: measurement results need to be put into structure mc_data.pos_detect_data */
    _Pragma( "diag_suppress = Pm142" )                                                                                                                  /* allow cast from const* to *  ->  disable MISRA C 2004 rule 11.5 */

      ((mc_data_t *) mc_get_data())->pos_detect_data.induct_rel_u = loc_rel_ind_u;
    ((mc_data_t *) mc_get_data())->pos_detect_data.induct_rel_v   = loc_rel_ind_v;
    ((mc_data_t *) mc_get_data())->pos_detect_data.induct_rel_w   = loc_rel_ind_w;

    ((mc_data_t *) mc_get_data())->pos_detect_data.grad_vu = loc_grad_vu;
    ((mc_data_t *) mc_get_data())->pos_detect_data.grad_wu = loc_grad_wu;
    ((mc_data_t *) mc_get_data())->pos_detect_data.grad_wv = loc_grad_wv;

    if( FORWARD == mc_get_direction())
    {
      ((mc_data_t *) mc_get_data())->pos_detect_data.rotor_angle_meas = loc_rotor_angle;
    }
    else
    {
      ((mc_data_t *) mc_get_data())->pos_detect_data.rotor_angle_meas = loc_rotor_angle + (u16) ( MATH_ANGLE_MAX / 2u );
    }

    _Pragma( "diag_default = Pm142" )
    /* PRQA S 0311 -- */ /* justification: measurement results need to be put into structure mc_data.pos_detect_data */
  }

/*! @brief configure the PWM output for the following period
 *
 * @details
 * The position detection module maintains the 20kHz timing like the FOC itself. This function<br>
 * is called from this module's state machine (pos_detect_state_machine_exec()) in order to set outputs and<br>
 * compare values for all three motor phases for the following PWM period.<br>
 * NOTE: called on interrupt level!<br>
 * ALSO NOTE: this module sets a new ADC list!!! (adc_lists_pos_detect)<br>
 */
  NO_INLINE static void pos_configure_next_pwm_period( const pos_pwm_period_type_t period_type, const u8 vec_no, const pulse_polarity_t pulse_polarity, const u16 cmp_val )
  {
    switch( period_type )
    {
      case ( POS_PWM_ALL_PHASES_HIGH_Z ):
      {
        pwm_set_outputs_reload( False, False, False );

        adc_lists_set_pos_detect_channel( ADC_PHASE_W, 0xFFu, (u16) pos_data.meas_time_mod_period );
        break;
      }

      case ( POS_PWM_ALL_LS_ON ):
      {
        pwm_set_c0_reload_values((u16) PWM_PERIOD_DURATION_TICKS, (u16) PWM_PERIOD_DURATION_TICKS, (u16) PWM_PERIOD_DURATION_TICKS );                   /* rising edges */
        pwm_set_c1_reload_values( 0u, 0u, 0u );                                                                                                         /* falling edges */
        pwm_set_outputs_reload( True, True, True );

        adc_lists_set_pos_detect_channel( ADC_PHASE_W, 0xFFu, (u16) pos_data.meas_time_mod_period );
        break;
      }

      case ( POS_PWM_ALL_HS_ON ):
      {
        pwm_set_c0_reload_values( 0u, 0u, 0u );                                                                                                         /* rising edges */
        pwm_set_c1_reload_values((u16) PWM_PERIOD_DURATION_TICKS, (u16) PWM_PERIOD_DURATION_TICKS, (u16) PWM_PERIOD_DURATION_TICKS );                   /* falling edges */
        pwm_set_outputs_reload( True, True, True );

        adc_lists_set_pos_detect_channel( ADC_PHASE_W, 0xFFu, (u16) pos_data.meas_time_mod_period );
        break;
      }

      case ( POS_PWM_ALL_PHASES_ON_W_COMPARE_VALUE ):
      {
        pwm_set_c0_reload_values( 0u, 0u, 0u );                                                                                                         /* rising edges */
        pwm_set_c1_reload_values( cmp_val, cmp_val, cmp_val );                                                                                          /* falling edges */
        pwm_set_outputs_reload( True, True, True );

        adc_lists_set_pos_detect_channel( ADC_PHASE_W, 0xFFu, (u16) pos_data.meas_time_mod_period );
        break;
      }

      case ( POS_PWM_PULSE_UV ):
      {
        pwm_set_c0_reload_values( 0u, 0u, 0u );                                                                                                         /* rising edges */
        ( POS_PULSE_POL_POSITIVE == pulse_polarity ) ? ( pwm_set_c1_reload_values( cmp_val, 0u, 0u )) : ( pwm_set_c1_reload_values( 0u, cmp_val, 0u )); /* falling edges */
        pwm_set_outputs_reload( True, True, False );

        adc_lists_set_pos_detect_channel( ADC_PHASE_W, vec_no, (u16) pos_data.meas_time_mod_period );
        break;
      }

      case ( POS_PWM_PULSE_UW ):
      {
        pwm_set_c0_reload_values( 0u, 0u, 0u );                                                                                                         /* rising edges */
        ( POS_PULSE_POL_POSITIVE == pulse_polarity ) ? ( pwm_set_c1_reload_values( cmp_val, 0u, 0u )) : ( pwm_set_c1_reload_values( 0u, 0u, cmp_val )); /* falling edges */
        pwm_set_outputs_reload( True, False, True );

        adc_lists_set_pos_detect_channel( ADC_PHASE_V, vec_no, (u16) pos_data.meas_time_mod_period );
        break;
      }

      case ( POS_PWM_PULSE_VW ):
      {
        pwm_set_c0_reload_values( 0u, 0u, 0u );                                                                                                         /* rising edges */
        ( POS_PULSE_POL_POSITIVE == pulse_polarity ) ? ( pwm_set_c1_reload_values( 0u, cmp_val, 0u )) : ( pwm_set_c1_reload_values( 0u, 0u, cmp_val )); /* falling edges */
        pwm_set_outputs_reload( False, True, True );

        adc_lists_set_pos_detect_channel( ADC_PHASE_U, vec_no, (u16) pos_data.meas_time_mod_period );
        break;
      }

      default:
      {
        pwm_set_outputs_reload( False, False, False );

        adc_lists_set_pos_detect_channel( ADC_PHASE_W, 0xFFu, (u16) pos_data.meas_time_mod_period );
        break;
      }
    }

    /* set new adc_list */
    adc_measurement_list_set_and_skip( adc_lists_get_pos_detect_list());
  }

/*! @brief state machine for generation of the test pulses and calculation of the rotor position
 *
 * @details
 * todo <br>
 * NOTE: called on interrupt level!
 */
  INLINE BOOL pos_detect_state_machine_exec( const BOOL exec_pos_detect )
  {
    pos_detect_state_t loc_last_state = pos_get_current_state();

    /* in case of a return value of False, one must set the new ADC list before returning from this function! usually done via pos_configure_next_pwm_period() */
    BOOL loc_return_value = True;

    #if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
      if( mc_get_data()->pos_detect_data.meas_permanently )
      {
        uart_set_data_logger_trigger();
      }
    #endif

    switch( pos_data.state )
    {
      case ( POS_DETECT_STATE_DISABLED ):
      {
        if( exec_pos_detect )
        {
          pos_reset_data();
          pos_calc_params();
          adc_lists_set_pos_detect_meas_dly( mc_get_data()->pos_detect_data.meas_delay );

          pos_data.state = POS_DETECT_STATE_CHARGE_BOOTSTRAPS;

          adc_measurement_list_set_and_skip( adc_lists_get_pos_detect_list());
          loc_return_value = False;
        }
        else
        {
          loc_return_value = True;
        }

        break;
      }

      case ( POS_DETECT_STATE_CHARGE_BOOTSTRAPS ):
      {
        if( mc_get_data()->pos_detect_data.run_calibration )
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_HIGH_Z, 0xFFu, POS_PULSE_POL_POSITIVE, 0u );
          pos_data.state = POS_DETECT_STATE_PULSE_UV;
        }
        else
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_LS_ON, 0xFFu, POS_PULSE_POL_POSITIVE, 0u );
          pos_data.state = POS_DETECT_STATE_CHARGE_REV_POL_FET;
        }

        loc_return_value = False;
        break;
      }

      case ( POS_DETECT_STATE_CHARGE_REV_POL_FET ):
      {
        if( pos_data.pwm_period_cnt < pos_data.rev_pol_fet_charge_duration )
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_ON_W_COMPARE_VALUE, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) ((dbl) PWM_PERIOD_DURATION_TICKS * 0.95 ));
        }
        else
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_HIGH_Z, 0xFFu, POS_PULSE_POL_POSITIVE, 0u );
          pos_data.state = POS_DETECT_STATE_PULSE_UV;
        }

        loc_return_value = False;
        break;
      }

      case ( POS_DETECT_STATE_PULSE_UV ):
      {
        if( pos_data.pwm_period_cnt < pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_UV, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) PWM_PERIOD_DURATION_TICKS );
        }
        else if( pos_data.pwm_period_cnt == pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_UV, 0x1u, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );
          if( 0 == pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_VU; }
        }
        else
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_HIGH_Z, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );
          if( pos_data.pwm_period_cnt >= pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_VU; }
        }

        loc_return_value = False;
        break;
      }

      case ( POS_DETECT_STATE_PULSE_VU ):
      {
        if( pos_data.pwm_period_cnt < pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_UV, 0xFFu, POS_PULSE_POL_NEGATIVE, (u16) PWM_PERIOD_DURATION_TICKS );
        }
        else if( pos_data.pwm_period_cnt == pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_UV, 0x2u, POS_PULSE_POL_NEGATIVE, (u16) pos_data.pulse_length_mod_period );
          if( 0 == pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_UW; }
        }
        else
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_HIGH_Z, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );
          if( pos_data.pwm_period_cnt == pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_UW; }
        }

        loc_return_value = False;
        break;
      }

      case ( POS_DETECT_STATE_PULSE_UW ):
      {
        if( pos_data.pwm_period_cnt < pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_UW, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) PWM_PERIOD_DURATION_TICKS );
        }
        else if( pos_data.pwm_period_cnt == pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_UW, 0x3u, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );
          if( 0 == pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_WU; }
        }
        else
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_HIGH_Z, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );
          if( pos_data.pwm_period_cnt >= pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_WU; }
        }

        loc_return_value = False;
        break;
      }

      case ( POS_DETECT_STATE_PULSE_WU ):
      {
        if( pos_data.pwm_period_cnt < pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_UW, 0xFFu, POS_PULSE_POL_NEGATIVE, (u16) PWM_PERIOD_DURATION_TICKS );
        }
        else if( pos_data.pwm_period_cnt == pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_UW, 0x4u, POS_PULSE_POL_NEGATIVE, (u16) pos_data.pulse_length_mod_period );
          if( 0 == pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_VW; }
        }
        else
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_HIGH_Z, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );
          if( pos_data.pwm_period_cnt >= pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_VW; }
        }

        loc_return_value = False;
        break;
      }

      case ( POS_DETECT_STATE_PULSE_VW ):
      {
        if( pos_data.pwm_period_cnt < pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_VW, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) PWM_PERIOD_DURATION_TICKS );
        }
        else if( pos_data.pwm_period_cnt == pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_VW, 0x5u, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );
          if( 0 == pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_WV; }
        }
        else
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_HIGH_Z, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );
          if( pos_data.pwm_period_cnt >= pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_PULSE_WV; }
        }

        loc_return_value = False;
        break;
      }

      case ( POS_DETECT_STATE_PULSE_WV ):
      {
        if( pos_data.pwm_period_cnt < pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_VW, 0xFFu, POS_PULSE_POL_NEGATIVE, (u16) PWM_PERIOD_DURATION_TICKS );
        }
        else if( pos_data.pwm_period_cnt == pos_data.meas_time_div_period )
        {
          pos_configure_next_pwm_period( POS_PWM_PULSE_VW, 0x6u, POS_PULSE_POL_NEGATIVE, (u16) pos_data.pulse_length_mod_period );
          if( 0 == pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_CALC; }
        }
        else
        {
          pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_HIGH_Z, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );
          if( pos_data.pwm_period_cnt >= pos_data.freerun_duration ){pos_data.state = POS_DETECT_STATE_CALC; }
        }

        loc_return_value = False;
        break;
      }

      case ( POS_DETECT_STATE_CALC ):
      {
        pos_configure_next_pwm_period( POS_PWM_ALL_PHASES_HIGH_Z, 0xFFu, POS_PULSE_POL_POSITIVE, (u16) pos_data.pulse_length_mod_period );

        pos_calc_rotor_position();
        pos_data.state = POS_DETECT_STATE_FINISHED;

        SARADC_CTRL_CFG_SAR_TIMING_bit.sampling_extension = (u16) pos_data.adc_sample_extension_restore;

        loc_return_value = False;
        break;
      }

      case ( POS_DETECT_STATE_FINISHED ):
      {
        if( !exec_pos_detect )
        {
          pos_data.state = POS_DETECT_STATE_DISABLED;
        }

        loc_return_value = True;
        break;
      }

      default:
      {
        pos_data.state = POS_DETECT_STATE_DISABLED;

        loc_return_value = True;
        break;
      }
    }

    if( loc_last_state == pos_data.state )
    {
      pos_data.pwm_period_cnt++;
    }
    else
    {
      pos_data.pwm_period_cnt = 0;
    }

    return loc_return_value;
  }

#endif  /* _#if ( USR_EN_INITIAL_POSITION_DETECTION == 1 ) */

/* }@
 */
