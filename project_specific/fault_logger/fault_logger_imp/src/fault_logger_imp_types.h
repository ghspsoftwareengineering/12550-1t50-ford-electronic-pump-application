#ifndef FAULT_LOGGER_IMP_TYPES_H
#define FAULT_LOGGER_IMP_TYPES_H

/**
 *  @file fault_logger_imp_types.h
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_logger_imp 1T50 Fault Logger Implementation
 *  @{
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
#pragma diag_suppress=Pm093
typedef struct
{
    char_t   Serial_Number[16];
    uint16_t Max_Lifetime_Voltage;
    int16_t  Max_Lifetime_CPU_Temperature;
    uint16_t Power_Cycles_Since_Stall;
    uint16_t Time_Since_Stall;
    uint32_t Run_Time;
    uint16_t Lifetime_Fault_Count;         /**< Total number of faults over the lifetime of the part */
    uint8_t  Fault_Index;                  /**< Current index in the Fault Incident Buffer - Bit 0 is the Index in the Block, Bit 3-1 is the Block ID */
} flt_log_General_Info_T;

typedef struct
{
    struct
    {
        FLT_Record_T Fault_Occurrence_Data[7];
        uint8_t Pad[3];
    } Block_Data;
    bool_t Dirty;
    uint8_t const NVM_Block_ID;
}flt_log_Occurence_Block_T;

typedef struct
{
    uint8_t Block_ID;
    uint8_t Index;
} flt_log_Data_Map_T;

typedef struct
{
    uint16_t Incident_Number;
    uint8_t  Fault_ID;
    uint8_t  PWM_Duty_Cycle;
    int16_t  CPU_Temperature;
    uint16_t Supply_Voltage;
    int16_t  Supply_Current;
    uint16_t Commanded_RPM;
    uint16_t IRQSTAT;
    uint8_t  Reserved[1];
} flt_Environmental_Record_T;

typedef struct
{
    flt_Environmental_Record_T Fault_Env_Record[2];
    uint8_t Pad;
    uint8_t const NVM_Block_ID;
} flt_Environmental_Log_Block_T;

/** @} doxygen end group */
#endif /* FAULT_LOGGER_IMP_TYPES_H */
