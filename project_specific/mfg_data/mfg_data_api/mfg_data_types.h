#ifndef MFG_DATA_TYPES_H
#define MFG_DATA_TYPES_H

/**
 *  @file mfg_data_types.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup mfg_data_api mfg_data Interface Documentation
 *  @{
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "mfg_data_defines.h"
/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

/** @} doxygen end group */
#endif /* MFG_DATA_TYPES_H */
