/**
 *  @file test_SYSIG_Get_Voltage.c
 *
 */
#include "unity.h"
#include "global.h"
#include "system_signals.h"
#include "uds.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_adc_lists.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"

/* STATIC's */
STATIC bool_t sysig_Use_CPU_Temp_Override;
STATIC int16_t sysig_Override_CPU_Temp;

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that SYSIG_Diag_Get_CPU_Temp() will serialize the currently reported
 * CPU temperature and return UDS_RESP_POSITIVE.
 *
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Diag_Get_CPU_Temp_Serializes_reported_CPU_Temp(void)
{
    UDS_Response_Code_T result = UDS_RESP_GENERAL_REJECT;
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    adc_get_user_data_ExpectAndReturn(0, 2158); /* User data 0 is the CPU temperature, 100 degrees C */
    SDB_Serialize_I16_ExpectAndReturn(&Test_SDB, 100, SDB_EOK);

    /* Call function under test */
    result = SYSIG_Diag_Get_CPU_Temp(&Test_SDB);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
}

/**
 * @test Verifies that SYSIG_Diag_Get_CPU_Temp() will serialize the override
 * CPU temperature when there is an override enabled and return UDS_RESP_POSITIVE.
 *
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Diag_Get_CPU_Temp_Serializes_override_CPU_Temp(void)
{
    UDS_Response_Code_T result = UDS_RESP_GENERAL_REJECT;
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    sysig_Use_CPU_Temp_Override = true;
    sysig_Override_CPU_Temp = -40;

    /* Setup expected call chain */
    /* Will not read the ADC for CPU temperature when the override is active */
    SDB_Serialize_I16_ExpectAndReturn(&Test_SDB, -40, SDB_EOK);

    /* Call function under test */
    result = SYSIG_Diag_Get_CPU_Temp(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
}

/**
 * @test Verifies that the function SYSIG_Diag_Override_CPU_Temp() will enable
 * the override and set the override temperature to the value supplied
 * in the Serial Data Buffer
 *
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Diag_Override_CPU_Temp_Sets_and_Enables_Override(void)
{
    UDS_Response_Code_T result = UDS_RESP_GENERAL_REJECT;
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    sysig_Override_CPU_Temp = 0xFFFF;
    sysig_Use_CPU_Temp_Override = false;

    /* Setup expected call chain */
    SDB_Deserialize_I16_ExpectAndReturn(&Test_SDB, -40);

    /* Call function under test */
    result = SYSIG_Diag_Override_CPU_Temp(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
    TEST_ASSERT_EQUAL_INT16(-40, sysig_Override_CPU_Temp);
    TEST_ASSERT_TRUE(sysig_Use_CPU_Temp_Override);
}

/**
 * @test Verifies that the function SYSIG_Diag_Override_CPU_Temp() will disable
 * the override when the serialized value is INT16_MIN.
 *
 */
/* Reqs: SWREQ_1930, SWREQ_1931 */
void test_SYSIG_Diag_Override_CPU_Temp_Clears_and_Disables_Override(void)
{
    UDS_Response_Code_T result = UDS_RESP_GENERAL_REJECT;
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    sysig_Override_CPU_Temp = 0;
    sysig_Use_CPU_Temp_Override = true;

    /* Setup expected call chain */
    SDB_Deserialize_I16_ExpectAndReturn(&Test_SDB, INT16_MIN);

    /* Call function under test */
    result = SYSIG_Diag_Override_CPU_Temp(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
    TEST_ASSERT_FALSE(sysig_Use_CPU_Temp_Override);
}
