/***************************************************************************//**
 * @file		LinSNPD_Interface.h
 *
 * @creator		rpy
 * @created		9.9.2015
 * @sdfv                TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINSNPD_INTERFACE_H_
#define LINSNPD_INTERFACE_H_

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "Lin_Basictypes.h"
#include "LinSNPD_Types.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINSNPD_INTERFACE_MODULE_API_VERSION          0x0102u /**< TODO: Add description **/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * Interface callback function types
 ******************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinSNPDIf_InitializationIfFun_t) (LinSNPDIf_pGenericEnvData_t    genericSNPDEnvData, LinSNPDIf_EnvDataSze_t SNPDEnvDataSze,
                                                       LinSNPDIf_pGenericImpCfgData_t genericSNPDImpCfgData);

/***************************************************************************//**
 * @brief Typedef of BUS layer 'Get Sub Interface' interface function.
 *
 * @param genericBusEnvData
 * @param interfaceId
 * @param ifThisPtr
 *
 * @return
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinSNPDIf_GetSubInterfaceIfFun_t) (LinSNPDIf_pGenericEnvData_t genericBusEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinSNPDIf_StartMeasurementIfFun_t)  ( LinSNPDIf_pGenericEnvData_t genericSNPDEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
typedef void (*LinSNPDIf_EndMeasurementIfFun_t) ( LinSNPDIf_pGenericEnvData_t genericSNPDEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinSNPDIf_NextMeasurementIfFun_t)  (LinSNPDIf_pGenericEnvData_t genericSNPDEnvData, Lin_Bool_t addressed);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
typedef LinSNPDIf_eState_t (*LinSNPDIf_GetStateIfFun_t)  (LinSNPDIf_pGenericEnvData_t   genericLinSNPDIfEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
typedef LinSNPDIf_MeasCount_t (*LinSNPDIf_GetMeasurementCountIfFun_t)  (LinSNPDIf_pGenericEnvData_t   genericSNPDEnvData);

/***************************************************************************//**
 * LIN driver interface function pointer
 ******************************************************************************/
struct LinSNPDIf_sInterfaceFunctions
{
  Lin_Version_t                        InterfaceVersion;

  LinSNPDIf_InitializationIfFun_t      Initialization;
  LinSNPDIf_GetSubInterfaceIfFun_t     GetSubInterface;
  LinSNPDIf_StartMeasurementIfFun_t    StartMeasurement;
  LinSNPDIf_EndMeasurementIfFun_t      EndMeasurement;
  LinSNPDIf_NextMeasurementIfFun_t     NextMeasurement;
  LinSNPDIf_GetStateIfFun_t            GetState;
  LinSNPDIf_GetMeasurementCountIfFun_t GetMeasurementCount;
};

/***************************************************************************//**
 * LIN PID lookup configuration parameter
 ******************************************************************************/
struct LinSNPDIf_sThis                        /**< TODO: Add description */
{
  LinSNPDIf_cpInterfaceFunctions_t IfFunsTbl; /**< TODO: Add description */
  LinSNPDIf_pGenericEnvData_t      EnvData;   /**< TODO: Add description */
};

struct LinSNPDIf_sInitParam                                /**< TODO: Add description */
{
  LinSNPDIf_cpInterfaceFunctions_t IfFunsTbl;              /**< TODO: Add description */
  LinSNPDIf_pGenericEnvData_t      EnvData;                /**< TODO: Add description */
  LinSNPDIf_EnvDataSze_t           EnvDataLen;             /**< TODO: Add description */
  LinSNPDIf_pGenericImpCfgData_t   ImpCfgData;             /**< TODO: Add description */
};

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* LINSNPD_INTERFACE_H_ */
