/**
 * @ingroup HW
 *
 * @{
 */

#ifndef DIAG_PWM_IN_H
#define DIAG_PWM_IN_H

#include <defines.h>

/* -------------------
   global declarations
   ------------------- */

#if ( USE_PWM_DIAG_INPUT == 1 )
  void diagi_init( const u16 prescaler );
  void diagi_enable( const BOOL enable );
  BOOL diagi_new_meas_available( void );

  u16 diagi_get_dutycycle( void );
  u16 diagi_get_period( void );
#endif

#endif /*_#ifndef DIAG_PWM_IN_H */

/* }@
 */
