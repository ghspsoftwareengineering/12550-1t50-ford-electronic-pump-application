/**************************************************************************** 
//                                                                           
// LIN Description file   		                                      
//                                                                         
// Version: 4 						              
//                                                                            
// Last Modified: Apr 12, 2017	    	      
//									                   
//                                                                             
// Filename:  Powertrain_2020_BIB_AGM_TAOP_LIN2.1_ver4.ldf 	      
//                                                                          
//  Usage:   Provides battery isolation block (BIB) support plus                  
//		 2 AGS and transmission aux oil pump. 			      
//	 
//  Change history:
//    4-12-2017:  add configurable frame field to TAOP node
//			attributes block by request of supplier
//	4-6-2017:  updates to protocol/language fields, add
//		   product_id line to TAOP node attribute block,
//      7-22-2016:  Changed one BIB signal name middle field
//                  to align with naming convention, and put
//		     back original byte size of bib frame, and
//		     put delta times of that frame back to 20.
//	7-22-2016:  Add second AGS back in, program has either
//		     no AGS or two AGS, and BIB and TAOP.
//		     Convert from draft to ver1.
//      7-21-2016:  Add TAOP signals/frames, remove placeholder AGM2
//	7-1-2016:  Updated tx signal to BIB from 1 to 2 bit
//		   Added epump content
//
//	3-18-2016:  Created draft1 file.
//	There will be another xEV device added in future. 
//                                   
//                                                                            
*****************************************************************************/ 



LIN_description_file;
LIN_protocol_version = "J2602_1_1.0";
LIN_language_version = "J2602_3_1.0";
LIN_speed =    10.417 kbps;

Nodes {
	Master : PCM,    5.000 ms,    1.500 ms;
	Slaves: BIB, AGM1, AGM2, TAOP;
}

Signals {

/* Status byte signals to start each slave device frame per LIN2.1 */
/* Satus byte signal names xxxxLINStatus, with following description: */

        /* SAE J2602 Status - Bits 7-5
        logical_value, 0, "No Fault Detected";
        logical_value, 1, "Reset";
        logical_value, 2, "Reserved";
        logical_value, 3, "Reserved";
        logical_value, 4, "Data Error";
        logical_value, 5, "Checksum Error";
        logical_value, 6, "Byte Field Framing Error";
        logical_value, 7, "ID Parity Error";
        
        APinfo4 - Bit 4
        logical_value, 0, "configured";
        logical_value, 1, "Not configured";
        
        Slave_Outputs_Error - Bits 3-2
        logical_value, 0, "No Fault Detected";
        logical_value, 1, "Output disabled environmental";
        logical_value, 2, "Output disabled Short";
        logical_value, 3, "ECU Fault outputs may be disabled";
        
        Slave_Inputs_Error - Bit 1
        logical_value, 0, "No input fault";
        logical_value, 1, "input fault";
        
        ApInfo0 - Bit 0
        logical_value, 0, "reserved";
        logical_value, 1, "reserved";
        */

        AGM1CtlFrm_Status: 8, 0, AGM1, PCM;
        AGM1PN1Frm_Status: 8, 0, AGM1, PCM;
        AGM1PN2Frm_Status: 8, 0, AGM1, PCM;
        AGM1PN3Frm_Status: 8, 0, AGM1, PCM;
        AGM1PN4Frm_Status: 8, 0, AGM1, PCM;
        AGM2CtlFrm_Status: 8, 0, AGM2, PCM;
        AGM2PN1Frm_Status: 8, 0, AGM2, PCM;
        AGM2PN2Frm_Status: 8, 0, AGM2, PCM;
        AGM2PN3Frm_Status: 8, 0, AGM2, PCM;
        AGM2PN4Frm_Status: 8, 0, AGM2, PCM;
        BIBCtlFrm_Status:  8, 0, BIB,  PCM;
        TAOPCtlFrm_Status: 8, 0, TAOP, PCM;


	AGM1PartNoWERSSegm1_1: 16, 0, AGM1, PCM;
	AGM1PartNoWERSSegm1_2: 16, 0, AGM1, PCM;
	AGM1PartNoWERSSegm1_3: 16, 0, AGM1, PCM;
	AGM1PartNoWERSSegm1_4: 16, 0, AGM1, PCM;
	AGM1PartNoWERSSegm2_1: 16, 0, AGM1, PCM;
	AGM1PartNoWERSSegm2_2: 16, 0, AGM1, PCM;
	AGM1PartNoWERSSegm2_3: 16, 0, AGM1, PCM;
	AGM1PartNoWERSSegm2_4: 16, 0, AGM1, PCM;
	AGM2PartNoWERSSegm1_1: 16, 0, AGM2, PCM;
	AGM2PartNoWERSSegm1_2: 16, 0, AGM2, PCM;
	AGM2PartNoWERSSegm1_3: 16, 0, AGM2, PCM;
	AGM2PartNoWERSSegm1_4: 16, 0, AGM2, PCM;
	AGM2PartNoWERSSegm2_1: 16, 0, AGM2, PCM;
	AGM2PartNoWERSSegm2_2: 16, 0, AGM2, PCM;
	AGM2PartNoWERSSegm2_3: 16, 0, AGM2, PCM;
	AGM2PartNoWERSSegm2_4: 16, 0, AGM2, PCM;
        Agm1PosHldCurr_D_Actl: 2, 0, AGM1, PCM;
        Agm1PosHldCurr_D_Rq: 2, 0, PCM, AGM1;
        Agm2PosHldCurr_D_Actl: 2, 0, AGM2, PCM;
        Agm2PosHldCurr_D_Rq: 2, 0, PCM, AGM2;
        AgsTqBoost_B_Actl: 1, 0, AGM1, PCM;
        AgsTqBoost_B_Actl2: 1, 0, AGM2, PCM;
        AgsTqBoost_B_Rq: 1, 0, PCM, AGM1;
        AgsTqBoost_B_Rq2: 1, 0, PCM, AGM2;
        BDSFalt_B_Stat: 1, 0, BIB, PCM;
        BDS_Te_Actl: 8, 0, BIB, PCM;
        BDSState_B_Actl: 1, 0, BIB, PCM;
        BDSState_D_Rq:  2, 0, BIB, PCM;
        BDSVoltDifftl_U_Actl: 9, 0, BIB, PCM;
        BDSCurrent_I_Actl: 12, 0, BIB, PCM;
        BDSState_D_Cmd: 2, 0, PCM, BIB;
        TrnOilPump_I_Meas: 8, 0, TAOP, PCM;
        TrnOilPump_U_Meas: 8, 0, TAOP, PCM;
        TrnOilPumpBlk_B_Falt: 1, 0, TAOP, PCM;
        TrnOilPumpI_D_Falt: 2, 0, TAOP, PCM;
        TrnOilPumpMde_D_Rq: 2, 0, PCM, TAOP;
        TrnOilPumpMde_D_Stat: 2, 0, TAOP, PCM;
        TrnOilPumpPrtct_B_Falt: 1, 0, TAOP, PCM;
        TrnOilPumpTe_D_Falt: 2, 0, TAOP, PCM;
        TrnOilPumpU_D_Falt: 2, 0, TAOP, PCM;
        TrnOilPumpV_D_Falt: 2, 0, TAOP, PCM;
        TrnOilPumpV_D_Stat: 2, 0, TAOP, PCM;
        TrnOilPumpV_Pc_Actl: 8, 0, TAOP, PCM;
        TrnOilPumpV_Pc_Rq: 8, 0, PCM, TAOP;
        TrnSumpOil_Te_Actl: 8, 0, PCM, TAOP;
	VaneActFalt_B_Actl: 1, 0, AGM1, PCM;
	VaneBlck_B_Actl: 1, 0, AGM1, PCM;
	VaneCalActv_B_Actl: 1, 0, AGM1, PCM;
	VaneCalIndic_B_Actl: 1, 0, AGM1, PCM;
	VaneCal_B_Enbl: 1, 0, PCM, AGM1;
	VaneElFalt_B_Actl: 1, 0, AGM1, PCM;
	VanePos_Rt_ActlRaw: 8, 0, AGM1, PCM;
	VanePos_Rt_Req: 8, 0, PCM, AGM1;
	VaneSnsFalt_B_Actl: 1, 0, AGM1, PCM;
	VaneTeOvr_B_Actl: 1, 0, AGM1, PCM;
	VaneUUnd_B_Actl: 1, 0, AGM1, PCM;
	VaneActFalt_B_Actl2: 1, 0, AGM2, PCM;
	VaneBlck_B_Actl2: 1, 0, AGM2, PCM;
	VaneCalActv_B_Actl2: 1, 0, AGM2, PCM;
	VaneCalIndic_B_Actl2: 1, 0, AGM2, PCM;
	VaneCal_B_Enbl2: 1, 0, PCM, AGM2;
	VaneElFalt_B_Actl2: 1, 0, AGM2, PCM;
	VanePos_Rt_ActlRaw2: 8, 0, AGM2, PCM;
	VanePos_Rt_Req2: 8, 0, PCM, AGM2;
	VaneSnsFalt_B_Actl2: 1, 0, AGM2, PCM;
	VaneTeOvr_B_Actl2: 1, 0, AGM2, PCM;
	VaneUUnd_B_Actl2: 1, 0, AGM2, PCM;
}


Frames {
        ECM_BIB_DEMAND : 04, PCM, 1     {
                BDSState_D_Cmd, 0;               
        }
        BIB_ECM_STATUS : 05, BIB, 6     {
                BIBCtlFrm_Status, 0;
                BDS_Te_Actl, 8;
                BDSVoltDifftl_U_Actl, 16;
                BDSFalt_B_Stat, 25; 
                BDSState_D_Rq, 26;
                BDSCurrent_I_Actl, 28;      
                BDSState_B_Actl, 40;
        }
        ECM_TAOP_DEMAND : 08, PCM, 3    {
                TrnOilPumpV_Pc_Rq, 0;
                TrnSumpOil_Te_Actl, 8;
                TrnOilPumpMde_D_Rq, 16;
        }
        TAOP_ECM_STATUS : 09, TAOP, 6  {
                TAOPCtlFrm_Status, 0;
                TrnOilPumpV_Pc_Actl, 8;
                TrnOilPump_U_Meas, 16;
                TrnOilPump_I_Meas, 24;
                TrnOilPumpV_D_Stat, 32;
                TrnOilPumpMde_D_Stat, 34;
                TrnOilPumpTe_D_Falt, 36;
                TrnOilPumpI_D_Falt, 38;
                TrnOilPumpU_D_Falt, 40;
                TrnOilPumpV_D_Falt, 42;
                TrnOilPumpBlk_B_Falt, 44;
                TrnOilPumpPrtct_B_Falt, 45;
        }
       	ECM_AGM1_DEMAND : 24, PCM, 4	{
		VanePos_Rt_Req, 0;
		VaneCal_B_Enbl, 8;
                AgsTqBoost_B_Rq, 9;
                Agm1PosHldCurr_D_Rq, 10;
	}
 	AGM1_ECM_STATUS : 25, AGM1, 4	{
                AGM1CtlFrm_Status, 0;
		VanePos_Rt_ActlRaw, 8;
		VaneActFalt_B_Actl, 16;
		VaneSnsFalt_B_Actl, 17;
		VaneCalActv_B_Actl, 18;
		VaneCalIndic_B_Actl, 19;
		VaneUUnd_B_Actl, 20;
		VaneBlck_B_Actl, 21;
		VaneElFalt_B_Actl, 22;
		VaneTeOvr_B_Actl, 23;
                AgsTqBoost_B_Actl, 24;
                Agm1PosHldCurr_D_Actl, 25;
	}
	AGM1_ECM_WERS1 : 26, AGM1, 5	{ 
                AGM1PN1Frm_Status, 0;
		AGM1PartNoWERSSegm1_1, 8;
		AGM1PartNoWERSSegm1_2, 24;
        }
        AGM1_ECM_WERS2 : 27, AGM1, 5    {
                AGM1PN2Frm_Status, 0;
		AGM1PartNoWERSSegm1_3, 8;
		AGM1PartNoWERSSegm1_4, 24;
	}
	AGM1_ECM_WERS3 : 28, AGM1, 5	{
                AGM1PN3Frm_Status, 0;
		AGM1PartNoWERSSegm2_1, 8;
		AGM1PartNoWERSSegm2_2, 24;
        }
        AGM1_ECM_WERS4 : 29, AGM1, 5    {
                AGM1PN4Frm_Status, 0;
		AGM1PartNoWERSSegm2_3, 8;
		AGM1PartNoWERSSegm2_4, 24;
	}
 	ECM_AGM2_DEMAND : 32, PCM, 4	{
		VanePos_Rt_Req2, 0;
		VaneCal_B_Enbl2, 8;
                AgsTqBoost_B_Rq2, 9;
                Agm2PosHldCurr_D_Rq, 10;
	}
 	AGM2_ECM_STATUS : 33, AGM2, 4	{
                AGM2CtlFrm_Status, 0;
		VanePos_Rt_ActlRaw2, 8;
		VaneActFalt_B_Actl2, 16;
		VaneSnsFalt_B_Actl2, 17;
		VaneCalActv_B_Actl2, 18;
		VaneCalIndic_B_Actl2, 19;
		VaneUUnd_B_Actl2, 20;
		VaneBlck_B_Actl2, 21;
		VaneElFalt_B_Actl2, 22;
		VaneTeOvr_B_Actl2, 23;
                AgsTqBoost_B_Actl2, 24;
                Agm2PosHldCurr_D_Actl, 25;
	}
	AGM2_ECM_WERS1 : 34, AGM2, 5	{
                AGM2PN1Frm_Status, 0;
		AGM2PartNoWERSSegm1_1, 8;
		AGM2PartNoWERSSegm1_2, 24;
        }
        AGM2_ECM_WERS2 : 35, AGM2, 5    {
                AGM2PN2Frm_Status, 0;
		AGM2PartNoWERSSegm1_3, 8;
		AGM2PartNoWERSSegm1_4, 24;
	}
	AGM2_ECM_WERS3 : 36, AGM2, 5	{
                AGM2PN3Frm_Status, 0;
		AGM2PartNoWERSSegm2_1, 8;
		AGM2PartNoWERSSegm2_2, 24;
        }
        AGM2_ECM_WERS4 : 37, AGM2, 5    {
                AGM2PN4Frm_Status, 0;  
		AGM2PartNoWERSSegm2_3, 8;
		AGM2PartNoWERSSegm2_4, 24;
	  }
 }

Node_attributes {
/*
// The response_error attribute is not utilized at this time.
// Note that signals "xxxCtlFrm_Status"  
// are used as placeholders 
// and may produce a warning during a consistency check.
// Attribute added per recommendation from Core NetCom, 
// NetCom has suggested defining status signal as 8-bit.
*/

        AGM1{
                   LIN_protocol = "J2602_1_1.0" ;
                   configured_NAD = 0x66 ;
                   response_error = AGM1CtlFrm_Status;
        }

        AGM2{
                   LIN_protocol = "J2602_1_1.0" ;
                   configured_NAD = 0x68 ;
                   response_error = AGM2CtlFrm_Status;
        }
        BIB{
                   LIN_protocol = "J2602_1_1.0" ;
                   configured_NAD = 0x61 ;
                   response_error = BIBCtlFrm_Status;
        }

        TAOP{
                   LIN_protocol = "J2602_1_1.0" ;
                   configured_NAD = 0x62 ;
                   product_id = 0x0000, 0x0000, 0x0000 ;
                   response_error = TAOPCtlFrm_Status;
                   configurable_frames {
                         ECM_TAOP_DEMAND = 0x8 ;
                         TAOP_ECM_STATUS = 0x9 ;
                   }
        }
}


/*
//  NAD 0x65 reserved for possible, future second ACM.
*/

Schedule_tables {

	BIB_TAOP_TABLE {
                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
                ECM_TAOP_DEMAND  delay  15.000 ms;
                TAOP_ECM_STATUS  delay  20.000 ms;
 	}
	BIB_TAOP_2AGM_TABLE {
                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
		ECM_AGM1_DEMAND	delay   15.000 ms;
		AGM1_ECM_STATUS	delay   15.000 ms;
		ECM_AGM2_DEMAND	delay   15.000 ms;
		AGM2_ECM_STATUS	delay   15.000 ms;
                ECM_TAOP_DEMAND  delay  15.000 ms;
                TAOP_ECM_STATUS  delay  20.000 ms;
 	}
 	BIB_TAOP_2AGM_PARTNUM_TABLE {
                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
		ECM_AGM1_DEMAND	delay   15.000 ms;
		AGM1_ECM_STATUS	delay   15.000 ms;
  		ECM_AGM2_DEMAND	delay   15.000 ms;
		AGM2_ECM_STATUS	delay   15.000 ms;
                ECM_TAOP_DEMAND delay   15.000 ms;
                TAOP_ECM_STATUS delay   20.000 ms;
		AGM1_ECM_WERS1	delay   15.000 ms;

                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
		ECM_AGM1_DEMAND	delay   15.000 ms;
		AGM1_ECM_STATUS	delay   15.000 ms;
 		ECM_AGM2_DEMAND	delay   15.000 ms;
		AGM2_ECM_STATUS	delay   15.000 ms;
                ECM_TAOP_DEMAND delay   15.000 ms;
                TAOP_ECM_STATUS delay   20.000 ms;
		AGM2_ECM_WERS1	delay   15.000 ms;

                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
 		ECM_AGM1_DEMAND	delay   15.000 ms;
		AGM1_ECM_STATUS	delay   15.000 ms;
 		ECM_AGM2_DEMAND	delay   15.000 ms;
		AGM2_ECM_STATUS	delay   15.000 ms;
                ECM_TAOP_DEMAND delay   15.000 ms;
                TAOP_ECM_STATUS delay   20.000 ms;
                AGM1_ECM_WERS2  delay   15.000 ms;

                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
 		ECM_AGM1_DEMAND	delay   15.000 ms;
		AGM1_ECM_STATUS	delay   15.000 ms;
		ECM_AGM2_DEMAND	delay   15.000 ms;
		AGM2_ECM_STATUS	delay   15.000 ms;
                ECM_TAOP_DEMAND delay   15.000 ms;
                TAOP_ECM_STATUS delay   20.000 ms;
                AGM2_ECM_WERS2  delay   15.000 ms;

                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
		ECM_AGM1_DEMAND	delay   15.000 ms;
		AGM1_ECM_STATUS	delay   15.000 ms;
		ECM_AGM2_DEMAND	delay   15.000 ms;
		AGM2_ECM_STATUS	delay   15.000 ms;
                ECM_TAOP_DEMAND delay   15.000 ms;
                TAOP_ECM_STATUS delay   20.000 ms;
		AGM1_ECM_WERS3	delay   15.000 ms;

                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
		ECM_AGM1_DEMAND	delay   15.000 ms;
		AGM1_ECM_STATUS	delay   15.000 ms;
		ECM_AGM2_DEMAND	delay   15.000 ms;
		AGM2_ECM_STATUS	delay   15.000 ms;
                ECM_TAOP_DEMAND delay   15.000 ms;
                TAOP_ECM_STATUS delay   20.000 ms;
		AGM2_ECM_WERS3	delay   15.000 ms;

                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
 		ECM_AGM1_DEMAND	delay   15.000 ms;
		AGM1_ECM_STATUS	delay   15.000 ms;
		ECM_AGM2_DEMAND	delay   15.000 ms;
		AGM2_ECM_STATUS	delay   15.000 ms;
                ECM_TAOP_DEMAND delay   15.000 ms;
                TAOP_ECM_STATUS delay   20.000 ms;
                AGM1_ECM_WERS4  delay   15.000 ms;

                ECM_BIB_DEMAND  delay   15.000 ms;
                BIB_ECM_STATUS  delay   20.000 ms;
 		ECM_AGM1_DEMAND	delay   15.000 ms;
		AGM1_ECM_STATUS	delay   15.000 ms;
		ECM_AGM2_DEMAND	delay   15.000 ms;
		AGM2_ECM_STATUS	delay   15.000 ms;
                ECM_TAOP_DEMAND delay   15.000 ms;
                TAOP_ECM_STATUS delay   20.000 ms;
                AGM2_ECM_WERS4  delay   15.000 ms;
  	} 
}

Signal_encoding_types {
         Active_InactiveCoding {
                logical_value, 0, "Inactive";
		logical_value, 1, "Active";
	}
        AgmPosHldCurrCoding { 
                logical_value, 0, "Inactive";
                logical_value, 1, "Current level for slow vehicle speed range"; 
                logical_value, 2, "Current level for medium vehicle speed range";
                logical_value, 3, "Current level for fast vehicle speed range";
        }
        BDSTemperatureCoding {
                physical_value, 0, 165, 1, -40, "C";
                logical_value, 255, "Fault";
        }
        BDSOpenClosedStateCoding {
                logical_value, 0, "Open";
                logical_value, 1, "Closed";
        }
        BDSRequestCoding {
                logical_value, 0, "Null";
                logical_value, 1, "Open";
                logical_value, 2, "Closed";
        }
        BDSCommandCoding {
                logical_value, 0, "Open";
                logical_value, 1, "Closed";
        }
        BDSVoltageDifferentialCoding {
                physical_value, 0, 10, 0.025, -5, "Volt";
                logical_value, 511, "Fault";
        }
        BDSCurrentCoding {
                physical_value, 0, 400, 0.1, -200, "Amp";
                logical_value, 4095, "Fault";
        }
	BooleanCoding {
		logical_value, 0, "FALSE";
		logical_value, 1, "TRUE";
	}
 	FaultDetCoding {
		logical_value, 0, "No fault";
		logical_value, 1, "Fault detected";
	}
         LINStatus {
		physical_value, 0, 255, 1, 0, "LIN Status";
	}
        TAOPModeCoding { 
                logical_value, 0, "Idle Mode";
                logical_value, 1, "Run Mode"; 
                logical_value, 2, "Undefined state";
                logical_value, 3, "Undefined state";
        }
        StatusTAOPModeCoding { 
                logical_value, 0, "No request";
                logical_value, 1, "Request honored"; 
                logical_value, 2, "Request denied";
                logical_value, 3, "Request out of range";
        }
        TAOPCurrentCoding {
                physical_value, 0, 255, 0.2, 0, "Amp";
        }
        TAOPCurrFltCoding { 
                logical_value, 0, "No fault";
                logical_value, 1, "Over current fault"; 
                logical_value, 2, "Under current fault";
                logical_value, 3, "Signal not available";
        }
        TAOPSpeedFltCoding { 
                logical_value, 0, "No fault";
                logical_value, 1, "Over speed fault"; 
                logical_value, 2, "Under speed fault";
                logical_value, 3, "Signal not available";
        }
        TAOPTempFltCoding { 
                logical_value, 0, "No fault";
                logical_value, 1, "Over temperature fault"; 
                logical_value, 2, "Under temperature fault";
                logical_value, 3, "Signal not available";
        }
        TAOPVoltFltCoding { 
                logical_value, 0, "No fault";
                logical_value, 1, "Over voltage fault"; 
                logical_value, 2, "Under voltage fault";
                logical_value, 3, "Signal not available";
        }
        TAOPVoltageCoding {
                physical_value, 0, 255, 0.1, 0, "Volt";
        }        
        TranSumpOilTempCoding {
                physical_value, 0, 254, 1, -50, "C";
                logical_value, 255, "Fault";        
        }                
        TqBoostRqCoding {
                logical_value, 0, "No request";
                logical_value, 1, "Tq boost request";
        }
	VaneCtrlReqCoding {
		physical_value, 0, 255, 0.392157, 0, "%";
	}
	VanePosCoding {
		physical_value, 0, 255, 0.392157, 0, "%";
	}
}

Signal_representation {
         Active_InactiveCoding:
                  AgsTqBoost_B_Actl, AgsTqBoost_B_Actl2;
        AgmPosHldCurrCoding: Agm1PosHldCurr_D_Actl, Agm2PosHldCurr_D_Actl,
                             Agm1PosHldCurr_D_Rq, Agm2PosHldCurr_D_Rq;
        BDSTemperatureCoding: BDS_Te_Actl;
        BDSOpenClosedStateCoding: BDSState_B_Actl;
        BDSRequestCoding: BDSState_D_Rq;
        BDSCommandCoding: BDSState_D_Cmd;
        BDSVoltageDifferentialCoding: BDSVoltDifftl_U_Actl;
        BDSCurrentCoding: BDSCurrent_I_Actl;
	BooleanCoding: TrnOilPumpBlk_B_Falt, TrnOilPumpPrtct_B_Falt, 
                VaneActFalt_B_Actl, VaneBlck_B_Actl, VaneCalActv_B_Actl,
        		VaneCalIndic_B_Actl, VaneCal_B_Enbl, VaneElFalt_B_Actl,
		VaneSnsFalt_B_Actl, VaneTeOvr_B_Actl, VaneUUnd_B_Actl,
                            VaneActFalt_B_Actl2, VaneBlck_B_Actl2, VaneCalActv_B_Actl2,
		VaneCalIndic_B_Actl2, VaneCal_B_Enbl2, VaneElFalt_B_Actl2,
		VaneSnsFalt_B_Actl2, VaneTeOvr_B_Actl2, VaneUUnd_B_Actl2;
        FaultDetCoding: BDSFalt_B_Stat;
        LINStatus: BIBCtlFrm_Status, AGM1CtlFrm_Status, AGM2CtlFrm_Status, TAOPCtlFrm_Status, 
		   AGM1PN1Frm_Status, AGM2PN1Frm_Status,
		   AGM1PN2Frm_Status, AGM2PN2Frm_Status,
		   AGM1PN3Frm_Status, AGM2PN3Frm_Status,
		   AGM1PN4Frm_Status, AGM2PN4Frm_Status;
        StatusTAOPModeCoding: TrnOilPumpV_D_Stat;
        TAOPCurrentCoding: TrnOilPump_I_Meas; 
        TAOPCurrFltCoding: TrnOilPumpI_D_Falt;
        TAOPModeCoding: TrnOilPumpMde_D_Rq, TrnOilPumpMde_D_Stat;
        TAOPSpeedFltCoding: TrnOilPumpV_D_Falt; 	
        TAOPTempFltCoding: TrnOilPumpTe_D_Falt;
        TAOPVoltageCoding: TrnOilPump_U_Meas;
        TAOPVoltFltCoding: TrnOilPumpU_D_Falt;
        TqBoostRqCoding: AgsTqBoost_B_Rq, AgsTqBoost_B_Rq2;
        TranSumpOilTempCoding: TrnSumpOil_Te_Actl;
	VaneCtrlReqCoding: VanePos_Rt_Req,VanePos_Rt_Req2, TrnOilPumpV_Pc_Rq, TrnOilPumpV_Pc_Actl;
	VanePosCoding: VanePos_Rt_ActlRaw,VanePos_Rt_ActlRaw2;
}
