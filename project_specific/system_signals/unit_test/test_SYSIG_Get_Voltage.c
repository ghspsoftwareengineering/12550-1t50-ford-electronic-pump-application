/**
 *  @file test_SYSIG_Get_Voltage.c
 *
 */
#include "unity.h"
#include "global.h"
#include "system_signals.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_adc_lists.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 0 when the supply voltage
 * is reported as 0.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_0_V_when_supply_voltage_0(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(0);

    /* Call function under test */
    result = SYSIG_Get_Voltage();
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(0, result);
}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 70 (7.0 volts in 10ths of a volt)
 * when the supply voltage is reported as 955.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_7_V_when_supply_voltage_955(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(955);

    /* Call function under test */
    result = SYSIG_Get_Voltage();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(70, result);
}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 75 (7.5 volts in 10ths of a volt)
 * when the supply voltage is reported as 1023.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_7_5_V_when_supply_voltage_1023(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(1023);

    /* Call function under test */
    result = SYSIG_Get_Voltage();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(75, result);
}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 80 (8.0 volts in 10ths of a volt)
 * when the supply voltage is reported as 1092.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_8_0_V_when_supply_voltage_1092(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(1092);

    /* Call function under test */
    result = SYSIG_Get_Voltage();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(80, result);
}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 133 (13.3 volts in 10ths of a volt)
 * when the supply voltage is reported as 1815.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_13_3_V_when_supply_voltage_1815(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(1815);

    /* Call function under test */
    result = SYSIG_Get_Voltage();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(133, result);
}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 160 (16.0 volts in 10ths of a volt)
 * when the supply voltage is reported as 2184.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_16_0_V_when_supply_voltage_2184(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(2184);

    /* Call function under test */
    result = SYSIG_Get_Voltage();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(160, result);
}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 165 (16.5 volts in 10ths of a volt)
 * when the supply voltage is reported as 2252.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_16_5_V_when_supply_voltage_2252(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(2252);

    /* Call function under test */
    result = SYSIG_Get_Voltage();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(165, result);
}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 180 (18.0 volts in 10ths of a volt)
 * when the supply voltage is reported as 2457.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_18_0_V_when_supply_voltage_2457(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(2457);

    /* Call function under test */
    result = SYSIG_Get_Voltage();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(180, result);
}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 185 (18.5 volts in 10ths of a volt)
 * when the supply voltage is reported as 2525.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_18_5_V_when_supply_voltage_2525(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(2525);

    /* Call function under test */
    result = SYSIG_Get_Voltage();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(185, result);
}

/**
 * @test Verifies that SYSIG_Get_Voltage() will return 255 (25.5 volts in 10ths of a volt)
 * when the supply voltage is reported as 3480.
 */
/* Reqs: SWREQ_1936, SWREQ_1940, SWREQ_2019, SWREQ_2022, SWREQ_2025, SWREQ_2028 */
void test_SYSIG_Get_Voltage_returns_25_5_V_when_supply_voltage_3480(void)
{
    uint16_t result = UINT16_MAX;

    /* Ensure known test state */

    /* Setup expected call chain */
    adc_get_supply_voltage_ExpectAndReturn(3480);

    /* Call function under test */
    result = SYSIG_Get_Voltage();

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT16(255, result);
}
