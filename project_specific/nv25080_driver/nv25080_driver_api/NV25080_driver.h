/**
 * @defgroup HW Hardware driver
 * @ingroup HW
 *
 * @{
 */

#ifndef DRIVER_NV25080
#define DRIVER_NV25080

#include <defines.h>
#include "global.h"


/* instruction set */
#define NV25080_WREN     (0x06u)        /* Enable Write Operations */
#define NV25080_WRDI     (0x04u)        /* Disable_Write_Operations */
#define NV25080_RDSR     (0x05u)        /* Read Status Register */
#define NV25080_WRSR     (0x01u)        /* Write Status Register */
#define NV25080_READ     (0x03u)        /* Read Data from Memory */
#define NV25080_WRITE    (0x02u)        /* Write Data to Memory */


/* BEMF Phase U on PB7 */
#define NV25080_BEMF_IO_INPUT()       IOMUX_CTRL_PB_IO_SEL_bit.sel_7 = 0u;    /* to select GPIO input (IO7) */
#define NV25080_PB7_BEMF_OE_DISABLE()       (GPIO_B_DATA_OE &= (~0x80u))

/* Status Register */
#define  NV25080_NOT_RDY    (0x01u)
#define  NV25080_WEL        (0x02u)
#define  NV25080_BP0        (0x04u)
#define  NV25080_BP1        (0x08u)
#define  NV25080_LIP        (0x10u)
#define  NV25080_IPL        (0x40u)
#define  NV25080_WPEN       (0x80u)



/* --------------------------------
   bit positions of some registers
   -------------------------------- */

/* -------------------
   global declarations
   ------------------- */
/* initialize SPI_1 interface for use with NV25080 eeprom */
void NV25080_Init( void );


/**
 * Enable or disable writing. True to enable WEN, false to disable (clears WEN.)
 * Executes a WREN (when passing true) or WRDI (when passing false).
 *
 * @return - None
 */
void NV25080_Enable_Write( BOOL enable );
/**
 * Reads one byte from NV25080, from the address that is passed
 * 
 *
 * @return - one byte from address requested, unsigned 8 bit
 */
uint8_t NV25080_Read_Byte(uint16_t address);

/**
 * Reads one byte , the contents of the status register of the NV25080. 
 * 
 *
 * @return - one byte contents  of status register of NV25080.
 */
uint8_t NV25080_Read_Status_Reg(void);

/**
 * Writes one byte to the NV25080
 *
 * @return - Boolean, true when complete
 */
bool_t NV25080_Write_Byte(uint16_t address, uint8_t data);
/**
 * Reads multiple bytes from the NV25080
 * Executes a read starting with address, and continuing for as many bytes as requested.
 * If write is not enabled, this function with enable WEN bit in the config register
 * It will leave write enabled, application code must disable it using NV25080_enable_write(false) if desired
 *
 * @return - none.
 */

void NV25080_Read_Multiple_Bytes(uint16_t address, uint8_t * const data, uint8_t numbytes);
/**
 * Writes multiple bytes to the NV25080 from a data structure who's address is an argument
 * Executes a Page write per the data sheet is a maximum of 32 bytes (0x20)
 * Note!!!  When a write exceeds the page length (0x20), the write "wraps around"
 * For example writing 16 bytes to address 30, will write address 30 and 31
 * then continues writing starting with address 0 until all 16 bytes are written. 
 * If write is not enabled, this function with enable WEN bit in the config register
 * It will leave write enabled, application code must disable it using NV25080_enable_write(false) if desired
 *
 * @return - Boolean, true when complete
 */
void NV25080_Write_Multiple_Bytes(uint16_t address, uint8_t const *data, uint8_t numbytes);

/**
 * Erases multiple bytes of the NV25080, starting at the address passed parameter. 
 * An erased byte == 0xff.
 */

void NV25080_Erase(uint16_t address,  uint8_t numbytes);
/**
* Things causing an eeprom is busy return (true) :
* EEPROM status regiser NV25080_NOT_RDY (Bit 0) is 1 indicating a write cycle in progress 
* OR tx_fifo_level > 0 indicating SPI is sending queued bytes to the eeprom 
* OR tx_fifo_empty == 0 which indicates the transmit fifo is not empty 
* @return - Boolean, true if busy
*/
bool_t NV25080_Is_Busy(void);


#endif /* "#ifndef DRIVER_NV25080" */

/* }@
 */
