/**
 * @defgroup MC Motor control state machine
 * @ingroup MC
 *
 * @{
 */

#ifndef MOTOR_CTRL_H
#define MOTOR_CTRL_H

#include "debug.h"
#include "global.h"
#include "pid_controller.h"
#include "usr_cfg.h"

#define MC_STATE_HISTORY_LENGTH ( 8 )

typedef enum
{
  FORWARD  = 0,
  BACKWARD = 1
} motor_direction_t;

typedef enum
{
  MC_OPEN_LOOP,
  MC_CLOSED_LOOP
} mc_motor_comm_mode_t;

typedef enum
{
  MC_IDLE                  = 1u,
  MC_START_REQUEST         = 2u,
  MC_CHARGE_BOOTSTRAP      = 3u,
  MC_START_COMMUTATION     = 4u,
  MC_RAMP_CURRENT          = 5u,
  MC_ALIGN_ROTOR           = 6u,
  MC_RAMP_SPEED            = 7u,
  MC_RUNNING               = 8u,
  MC_STALLED               = 9u,
  MC_DRIVER_ERROR          = 10u,
  MC_CLEAR_DRIVER_ERROR    = 11u,
  MC_SYNC_ROTOR_DELAY      = 12u,
  MC_SYNC_TO_TURNING_ROTOR = 13u,
  MC_FAST_STARTUP          = 14u,
  MC_APPLY_MOTOR_PHASE_SC  = 15u,
  MC_DETECT_ROTOR_POSITION = 16u,

  /* application-specific states */
  MC_AUDI_ACOUSTICS = 200u
} mc_motor_state_t;

typedef enum
{
  MC_SYNC_STATE_INVALID                 = 0u,
  MC_SYNC_STATE_DISABLED                = 1u,
  MC_SYNC_STATE_MEASUREMENT_BEMF_ACTIVE = 2u,
  MC_SYNC_STATE_WAIT_FOR_HALL_SIGNAL    = 3u,
  MC_SYNC_STATE_COMMUTATION_ACTIVE      = 4u,
  MC_SYNC_STATE_FINISHED                = 5u,
  MC_SYNC_STATE_FAILED                  = 6u
} mc_sync_state_t;

typedef enum
{
  MC_SYNC_NO_FAIL               = 0u,
  MC_SYNC_FAIL_EXCESSIVE_BEMF   = 1u,
  MC_SYNC_FAIL_REVERSE_ROTATION = 2u,
  MC_SYNC_FAIL_TIMEOUT          = 3u
} mc_sync_fail_reason_t;

typedef enum
{
  MC_PHASE_U = 1u,
  MC_PHASE_V = 2u,
  MC_PHASE_W = 3u
} mc_motor_phase_t;

typedef struct
{
  uint16_t test_pulse_length;
  uint16_t meas_time;
  uint16_t meas_delay;

  int16_t freerun_duration;

  int16_t offset_uv;
  int16_t offset_uw;
  int16_t offset_vw;

  int16_t induct_rel_u;
  int16_t induct_rel_v;
  int16_t induct_rel_w;

  int16_t grad_vu;
  int16_t grad_wu;
  int16_t grad_wv;

  uint16_t rotor_angle_meas;

  bool_t meas_permanently;

  bool_t run_calibration;
  uint16_t calibration_forced_angle;

} mc_pos_detect_t;

typedef struct
{
  int16_t i_q_ref;
  uint32_t timeout;

} mc_fast_startup_t;

typedef struct
{
  mc_sync_state_t sync_state;                                /**< state of the sync state machine */

  motor_direction_t rotor_direction;                         /**< direction, in which the rotor is turning, as monitored from the BEMFs */

  uint16_t crossing_evt_cntr;                                     /**< counter for the amount of BEMF crossings that have occurred during sync process (only after a certain amount of valid BEMF crossings, the system attempts tosync to the rotor */

  int16_t bemf_ampl;                                             /**< amplitude of the measured BEMF [ADC LSB] */
  int16_t bemf_ampl_min;                                         /**< minimum value for 'bemf_ampl', below which the system does not try to sync to the rotor [ADC LSB] */

  uint16_t sync_duration;                                         /**< after having sync'ed to a turning rotor, the system keeps i_d and i_q at 0 for this time (to let the FOC "tune in") [systime ticks] */
  uint32_t sync_timeout;                                          /**< if after this time no valid BEMF has been detected, the sync attempt is aborted with fail_reason == MC_SYNC_FAIL_TIMEOUT [systime ticks] */

  int16_t bemf_ampl_gain;                                        /**< deprecated: one can change the amplitude of the synchronization to a bigger or smaller value than the one that was measured, a value of 256 equals K = 1, 512 equals K = 2, ... */
  int16_t bemf_starpoint;                                        /**< voltage of the starpoint (average of all 3 measured BEMF voltages [ADC LSB] */
  int16_t bemf_u;                                                /**< phase voltage of U during synchronization process (i.e. BEMF U) [ADC LSB] */
  int16_t bemf_v;                                                /**< phase voltage of V during synchronization process(i.e. BEMF V) [ADC LSB] */
  int16_t bemf_w;                                                /**< phase voltage of W during synchronization process(i.e. BEMF W) [ADC LSB] */

  uint16_t vsup_nominal;                                          /**< only needed for the 523.05 since it can not measure the supply voltage when the gate drivers are switched off -> MC_52305_VBAT_NOMINAL Volts is assumed instead [ADC LSB] */

  bool_t sync_permission;                                      /**< if set to "False", the control can not sync to the turning motor but instead monitors the BEMF voltages indefinitely (rather kinda debugging stuff) */

  mc_sync_fail_reason_t sync_fail_reason;

} mc_sync_data_t;

/**! @typedef mc_startup_t
 * @brief
 * structure contains all parameters for the startup phase
 */
typedef struct
{
  uint16_t speed_ramp_start;                                      /**< starting speed of the speed ramp (in angle increments per PWM period) */
  int16_t speed_slope;                                           /**< slope of the speed ramp: the higher the value, the steeper the speed ramp; negative values are possible (considered as prescaler) */
  uint16_t speed_ramp_end;                                        /**< end speed of the speed ramp (in angle increments per PWM period); when this speed has been reached, the system changes into closed loop operation */
  uint16_t angle_incr;                                            /**< momentary speed during the speed ramp (in angle increments per PWM period) */

  int16_t i_q_ref;                                               /**< target value of i_q during the startup phase [ADC LSB] */

  int16_t current_slope;                                         /**< slope of the current ramp: the higher the value, the steeper the current ramp; negative values are possible (considered as prescaler) */
  int16_t i_q_ref_start_ramp;                                    /**< momentary value of target value of i_q during startup phase [ADC LSB] */

  uint16_t align_rotor_duration;                                  /**< duration between current ramp and speed ramp where the current vector is held static [systime ticks] */

} mc_startup_t;

/**! @typedef mc_motor_status_t
 * @brief
 * structure contains all parameters and process values of the motor control
 */
typedef struct                                               /* PRQA S 3630 */ /* justification: typedef necessary for exporting function mc_get_data() */
{
  mc_motor_state_t state;                                    /**< state of the motor control state machine (refer to the defining enumerator) */

  motor_direction_t direction;                               /**< rotational direction of the rotor */
  mc_motor_comm_mode_t commutation_mode;                     /**< indicating whether control operates in open-loop or closed-loop mode */

  uint16_t target_rotor_speed;                                    /**< target speed that is fed to the speed controller, if enabled [eRPM] */

  pi_controller_t controller_rotor_speed;                    /**< PI controller of the rotational speed (sets i_q) */
  int16_t speed_ctrl_output_max;                                 /**< saturation of the speed controller (maximum output value for i_q) */
  int16_t speed_ctrl_output_min;                                 /**< saturation of the speed controller (minimum output value for i_q) */
  uint16_t rotor_speed_ctrl_time_base;                            /**< delay between two iterations of the speed controller [ms] */
  uint16_t rotor_speed_ctrl_timestamp;                            /**< timestamp for generating ->rotor_speed_ctrl_timebase */
  int32_t speed_ctrl_err_sum_preload;                            /**< see function mc_calc_speed_ctrl_preload_value() */

  int16_t supply_current;                                        /**< the current drawn from the supply (DC link current), computed from FOC data [A/256] */

  mc_startup_t startup;                                      /**< all parameters/process variables that are related to the (open loop) startup procedure */

  bool_t stall_detected;                                       /**< flag: stalled rotor has been detected */
  bool_t wrong_direction;                                      /**< flag: rotor was turning into the False direction */

  bool_t remain_in_open_loop;                                  /**< if set to 1 by the user, the motor control does not change into closed loop mode after having reached -> speed_ramp_end */

  bool_t enable_active_breaking_when_idle;                     /**< if (and for as long as) this flag is True, the motor phases are short-circuited with a 50% duty cycle when target_speed == 0 */

  uint8_t irqstat1;                                               /**< content of register IRQSTAT1 of the gate driver die */
  uint8_t irqstat2;                                               /**< content of register IRQSTAT2 of the gate driver die */

  /* ------------------ */

  bool_t foc_enable;                                           /**< internal flag which decides whether or not the FOC algorithm is computed */
  bool_t outputs_enable;                                       /**< internal flag that enabled/disables the gate driver output */
  bool_t meas_bemf;                                            /**< internal flag for measuring the BEMF with gate driver being switched off (attempting to sync to turining rotor) */
  bool_t exec_speed_ctrl;                                      /**< internal flag which decides whether or not the PI controller for the rotor speed is executed or not */
  bool_t exec_pos_detect;                                      /**< internal flag for measuring the rotor position (i.e. electrical angle) when the motor stands still */

  /* ------------------ */

  int16_t pwm0_cmp_val;                                          /**< actual compare value of phase U */
  int16_t pwm1_cmp_val;                                          /**< actual compare value of phase V */
  int16_t pwm2_cmp_val;                                          /**< actual compare value of phase W */

  int16_t min_cmp_val;                                           /**< mininmum of pwm0_cmp_val, pwm1_cmp_val, pwm2_cmp_val */
  int16_t max_cmp_val;                                           /**< maxinmum of pwm0_cmp_val, pwm1_cmp_val, pwm2_cmp_val */

  bool_t pwm_cmp_val_overflow_flag;                            /**< flag: compare values can not be set, abs(max_cmp_val - min_cmp_val) is too big; overflow */

  int16_t curr1_offset_val;                                      /**< ADC value of the offset of either the sum current OPA or the first phase current OPA [ADC LSB] */
  int16_t curr2_offset_val;                                      /**< ADC value of the offset of the second phase current OPA [ADC LSB] */

  #if ( USR_EN_SINGLE_SHUNT_SILENT_MODE != 1 )
    mc_motor_phase_t highest_phase;                          /**< holds the motor phase for which the FOC computed the highest value when USR_EN_SINGLE_SHUNT_SILENT_MODE != 1 [enum] */
    bool_t force_fixed_meas_window;                            /**< if set, the measurement window is fixed even though USR_EN_SINGLE_SHUNT_SILENT_MODE != 1; thereby you can switch between the two operation modes during runtime */
  #endif

  /* ------------------ */

  bool_t enable_sync_to_rotor;                                 /**< if set by the user, an attempt to synchronize to a turning rotor is made before starting the motor normally */
  mc_sync_data_t sync_data;                                  /**< all parameters/process variables that are related to the synchronization to a turning rotor */

  bool_t enable_fast_startup;
  mc_fast_startup_t fast_startup_data;

  #if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
    bool_t enable_initial_pos_detect;
    mc_pos_detect_t pos_detect_data;
  #endif

  /* ------------------ */

  uint16_t state_history_idx;
  mc_motor_state_t state_history[ MC_STATE_HISTORY_LENGTH ]; /**< debug variable: the last XX values of ->mc_motor_state_t state */

  uint16_t struct_signature;

} mc_data_t;

/* --------------------
   global declarations
   -------------------- */
void mc_init( void );
void mc_generate_struct_signature ( void );
void mc_main( void );
void mc_isr_handler( void );

/* getter/setter for motor values/parameters */

motor_direction_t mc_get_direction( void );
void mc_set_direction ( motor_direction_t direction );
void mc_set_active_breaking_when_idle( const bool_t enable );

const mc_data_t * mc_get_data( void );
mc_motor_state_t mc_get_motor_state( void );
mc_motor_comm_mode_t mc_get_comm_mode( void );
uint16_t mc_get_startup_angle_incr( void );

void mc_set_sync_permission( bool_t sync_permission );

void mc_set_target_rotor_speed( uint16_t target_speed );
void mc_set_speed_ctrl_kp(int16_t speed_control_kp);
void mc_set_speed_ctrl_ki(uint16_t speed_control_ki);
void mc_set_speed_ctrl_output_min( int16_t output_min );
void mc_set_speed_ctrl_output_max( int16_t output_max );

void mc_output_voltage_overflow_handler( void );
void mc_stalled_rotor_condition_handler( void );

mc_motor_state_t mc_sync_to_turning_rotor_failed_handler ( const mc_sync_fail_reason_t fail_reason );

void mc_set_startup_i_q_ref(int16_t startup_i_q_ref);
void mc_set_startup_current_slope(int16_t startup_current_slope);
void mc_set_startup_speed_slope(int16_t startup_speed_slope);
void mc_set_startup_speed_ramp_start(uint16_t speed_ramp_start);
void mc_set_startup_speed_ramp_end(uint16_t speed_ramp_end);
void mc_set_startup_align_rotor_duration(uint16_t align_rotor_duration);
void mc_set_remain_in_open_loop(bool_t enable);

void mc_force_fixed_meas_window ( bool_t enable );

int16_t mc_get_supply_current ( void );
uint16_t mc_get_irqstat( void );

#endif /* _#ifndef MOTOR_CTRL_H_ */

/* }@
 */
