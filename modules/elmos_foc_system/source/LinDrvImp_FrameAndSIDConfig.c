/***************************************************************************//**
 * @file			LinDrvImp_FrameConfig.c
 *
 * @creator		sbai
 * @created		15.06.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinDrvImp_FrameAndSIDConfig.h"
#include "LinDrvImp_Config.h"

/* ****************************************************************************/
/* ****************************** USER INCLUDES *******************************/
/* ****************************************************************************/
#include "lin_vehicle_comm.h"

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************* SIGNAL CONFIGRUATIONS ************************** */
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ FRAME DESCRIPTION LIST ************************** */
/* ****************************************************************************/

const LinProtoIf_sFrameDescription_t LinDrvImp_FrameDescriptionList[LINPROTOIMP_FRAMEDESCLST_SZE] =
{
  /*                                      | FRAME ID |         CRC TYPE         |                   FRAME KIND                  | LEN | POINTER TO SIGNAL LIST                  | PROCESSING                                      |            FRAME ID PROCESSED CALLBACK          |            CALLBACK CTX DATA TYPE             |           CTX DATA POINTER          | */
  LINPROTO_INTERFACE_ADD_FRAME_DESCRIPTION( 8,         LinProtoIf_CRC_Extended,   LinProtoIf_FrameKind_SUBSCRIBE_WITH_FUN,        3,    app_ECM_TAOP_DEMAND,                      LinProtoIf_TxRxCbProc_SCHEDULED,                  &LinDrvImp_ProtoFrameIdProcCbFun,                 LinProtoIf_FrmIdPrcCbCtxDataType_PER_CALLBACK,   LIN_NULL),
  LINPROTO_INTERFACE_ADD_FRAME_DESCRIPTION( 9,         LinProtoIf_CRC_Extended,   LinProtoIf_FrameKind_PUBLISH_WITH_FUN,          6,    app_TAOP_ECM_STATUS,                      LinProtoIf_TxRxCbProc_SCHEDULED,                  &LinDrvImp_ProtoFrameIdProcCbFun,                 LinProtoIf_FrmIdPrcCbCtxDataType_PER_CALLBACK,   LIN_NULL),
  LINPROTO_INTERFACE_ADD_FRAME_DESCRIPTION( 0x3C,      LinProtoIf_CRC_Classic,    LinProtoIf_FrameKind_SUBSCRIBE_WITH_FUN,        8,    app_Master_Diag_Request,                  LinProtoIf_TxRxCbProc_SCHEDULED,                  &LinDrvImp_ProtoFrameIdProcCbFun,                 LinProtoIf_FrmIdPrcCbCtxDataType_PER_CALLBACK,   LIN_NULL),
  LINPROTO_INTERFACE_ADD_FRAME_DESCRIPTION( 0x3D,      LinProtoIf_CRC_Classic,    LinProtoIf_FrameKind_PUBLISH_WITH_FUN,          8,    app_Slave_Diag_Response,                  LinProtoIf_TxRxCbProc_SCHEDULED,                  &LinDrvImp_ProtoFrameIdProcCbFun,                 LinProtoIf_FrmIdPrcCbCtxDataType_PER_CALLBACK,   LIN_NULL),

  LINPROTO_INTERFACE_FRAME_DESCRIPTION_END          
};
