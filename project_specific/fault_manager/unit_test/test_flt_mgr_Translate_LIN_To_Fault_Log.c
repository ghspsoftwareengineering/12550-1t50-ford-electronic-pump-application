/**
 *  @file test_flt_mgr_Translate_LIN_To_Fault_Log.c
 *
 */
#include "unity.h"
#include "fault_logger.h"
#include "LinBus_Types.h"

/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"

/* STATIC's */
FLT_LOG_Faults_T flt_mgr_Translate_LIN_To_Fault_Log(FLT_MGR_LIN_J2602_Err_T LIN_Fault);

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * Verifies that the function flt_mgr_Translate_LIN_To_Fault_Log will return
 * FLT_LOG_LIN_ID_PARITY_ERROR when provided LinBusIf_ERR_SYNC_FAIL.
 */
void test_flt_mgr_Translate_LIN_To_Fault_Log_returns_FLT_LOG_LIN_ID_PARITY_ERROR_for_LinBusIf_ERR_SYNC_FAIL(void)
{
    FLT_LOG_Faults_T result = FLT_LOG_NO_ERROR;

    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    result = flt_mgr_Translate_LIN_To_Fault_Log(FLT_MGR_J2602_ID_PARITY_ERROR);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL(FLT_LOG_LIN_ID_PARITY_ERROR, result);
}

