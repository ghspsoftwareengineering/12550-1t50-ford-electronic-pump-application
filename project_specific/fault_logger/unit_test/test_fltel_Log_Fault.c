/**
 *  @file test_fltel_Log_Fault.c
 *
 */
#include "unity.h"
#include "52307_test_regs.h"
#include "fault_environmental_logger.h"
#include "fault_logger_imp_defines.h"
#include "fault_logger_imp_types.h"
#include <string.h>

/* MOCKS */
#include "mock_lin_vehicle_comm.h"
#include "mock_motor_ctrl.h"
#include "mock_nvm_mgr.h"
#include "mock_serial_data_buffer.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"

/* STATIC's */
flt_log_General_Info_T flt_log_General_Info;
flt_Environmental_Log_Block_T fltel_Blocks[FLT_MAX_INCIDENTS_TO_LOG / 2];

uint16_t Test_Regs[TEST_REG_END] = {0};

void setUp(void)
{
    memset(&Test_Regs, 0, sizeof(Test_Regs));
}

void tearDown(void)
{

}

/**
 * @test Verifies that fltel_Log_Fault() will log the correct environmental data
 * to the Fault Incident Buffer [0] in NVM Block 3 when the Fault Index is 0.
 */
void test_fltel_Log_Fault_Writes_to_Block_3_when_Fault_Index_0(void)
{
    /* Ensure known test state */
    flt_log_General_Info.Fault_Index = 0;
    flt_log_General_Info.Lifetime_Fault_Count = 542;
    PWMN_CNT = 1200;
    PWMN_CNT_MAX = 2400;

    /* Setup expected call chain */
    SYSIG_Get_CPU_Temp_ExpectAndReturn(125); /* 125 degrees C */
    SYSIG_Get_Voltage_ExpectAndReturn(191); /* 19.1 Volts */
    SYSIG_Get_Current_ExpectAndReturn(103); /* 10.3 Amps */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1500); /* 1500 RPM */
    mc_get_irqstat_ExpectAndReturn(0x1234); /* Arbitrary IRQ STAT flags */
    NVMMGR_Write_NVM_Block_ExpectAndReturn(15u, NVM_STATUS_SUCCESS); /* Update to the lifetime data record */
    NVMMGR_Write_NVM_Block_ExpectAndReturn(3u, NVM_STATUS_SUCCESS);  /* Update to block 3 that contains this buffer element 0 */

    /* Call function under test */
    fltel_Log_Fault(FLT_LOG_OVER_VOLTAGE);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL(3, fltel_Blocks[0].NVM_Block_ID); /* NVM Block ID must be 3 according to spec */
    TEST_ASSERT_EQUAL(FLT_LOG_OVER_VOLTAGE, fltel_Blocks[0].Fault_Env_Record[0].Fault_ID);
    TEST_ASSERT_EQUAL_INT16(125, fltel_Blocks[0].Fault_Env_Record[0].CPU_Temperature);
    TEST_ASSERT_EQUAL_UINT16(191, fltel_Blocks[0].Fault_Env_Record[0].Supply_Voltage);
    TEST_ASSERT_EQUAL_INT16(103, fltel_Blocks[0].Fault_Env_Record[0].Supply_Current);
    TEST_ASSERT_EQUAL_INT16(1500, fltel_Blocks[0].Fault_Env_Record[0].Commanded_RPM);
    TEST_ASSERT_EQUAL_INT16(0x1234, fltel_Blocks[0].Fault_Env_Record[0].IRQSTAT);
    TEST_ASSERT_EQUAL_INT16(50, fltel_Blocks[0].Fault_Env_Record[0].PWM_Duty_Cycle);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_General_Info.Fault_Index);
    TEST_ASSERT_EQUAL_UINT16(543, flt_log_General_Info.Lifetime_Fault_Count);
}

/**
 * @test Verifies that fltel_Log_Fault() will log the correct environmental data
 * to the Fault Incident Buffer [0] in NVM Block 7 when the Fault Index is 9.
 */
void test_fltel_Log_Fault_Writes_to_Block_7_when_Fault_Index_9(void)
{
    /* Ensure known test state */
    flt_log_General_Info.Fault_Index = 9;
    flt_log_General_Info.Lifetime_Fault_Count = 542;
    PWMN_CNT = 1200;
    PWMN_CNT_MAX = 2400;

    /* Setup expected call chain */
    SYSIG_Get_CPU_Temp_ExpectAndReturn(125); /* 125 degrees C */
    SYSIG_Get_Voltage_ExpectAndReturn(191); /* 19.1 Volts */
    SYSIG_Get_Current_ExpectAndReturn(103); /* 10.3 Amps */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1500); /* 1500 RPM */
    mc_get_irqstat_ExpectAndReturn(0x1234); /* Arbitrary IRQ STAT flags */
    NVMMGR_Write_NVM_Block_ExpectAndReturn(15u, NVM_STATUS_SUCCESS); /* Update to the lifetime data record */
    NVMMGR_Write_NVM_Block_ExpectAndReturn(7u, NVM_STATUS_SUCCESS);  /* Update to block 7 that contains this buffer element 9 */

    /* Call function under test */
    fltel_Log_Fault(FLT_LOG_LIN_ID_PARITY_ERROR);

    /* Verify test results */
    TEST_ASSERT_EQUAL(7, fltel_Blocks[4].NVM_Block_ID); /* NVM Block ID must be 3 according to spec */
    TEST_ASSERT_EQUAL(FLT_LOG_LIN_ID_PARITY_ERROR, fltel_Blocks[4].Fault_Env_Record[1].Fault_ID);
    TEST_ASSERT_EQUAL_INT16(125, fltel_Blocks[4].Fault_Env_Record[1].CPU_Temperature);
    TEST_ASSERT_EQUAL_UINT16(191, fltel_Blocks[4].Fault_Env_Record[1].Supply_Voltage);
    TEST_ASSERT_EQUAL_INT16(103, fltel_Blocks[4].Fault_Env_Record[1].Supply_Current);
    TEST_ASSERT_EQUAL_INT16(1500, fltel_Blocks[4].Fault_Env_Record[1].Commanded_RPM);
    TEST_ASSERT_EQUAL_INT16(0x1234, fltel_Blocks[4].Fault_Env_Record[1].IRQSTAT);
    TEST_ASSERT_EQUAL_INT16(50, fltel_Blocks[4].Fault_Env_Record[1].PWM_Duty_Cycle);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_General_Info.Fault_Index);
    TEST_ASSERT_EQUAL_UINT16(543, flt_log_General_Info.Lifetime_Fault_Count);
}

/**
 * @test Verifies that fltel_Log_Fault() will log the correct environmental data
 * to the Fault Incident Buffer [15] in NVM Block 10 when the Fault Index is 15.
 */
void test_fltel_Log_Fault_Writes_to_Block_10_when_Fault_Index_15(void)
{
    /* Ensure known test state */
    flt_log_General_Info.Fault_Index = 15;
    flt_log_General_Info.Lifetime_Fault_Count = 542;
    PWMN_CNT = 1200;
    PWMN_CNT_MAX = 2400;

    /* Setup expected call chain */
    SYSIG_Get_CPU_Temp_ExpectAndReturn(125); /* 125 degrees C */
    SYSIG_Get_Voltage_ExpectAndReturn(131); /* 13.1 Volts */
    SYSIG_Get_Current_ExpectAndReturn(103); /* 10.3 Amps */
    VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq_ExpectAndReturn(1500); /* 1500 RPM */
    mc_get_irqstat_ExpectAndReturn(0x1234); /* Arbitrary IRQ STAT flags */
    NVMMGR_Write_NVM_Block_ExpectAndReturn(15u, NVM_STATUS_SUCCESS); /* Update to the lifetime data record */
    NVMMGR_Write_NVM_Block_ExpectAndReturn(10u, NVM_STATUS_SUCCESS);  /* Update to block 10 that contains this buffer element 15 */

    /* Call function under test */
    fltel_Log_Fault(FLT_LOG_BLOCKED_GEROTOR);

    /* Verify test results */
    TEST_ASSERT_EQUAL(FLT_LOG_BLOCKED_GEROTOR, fltel_Blocks[7].Fault_Env_Record[1].Fault_ID);
    TEST_ASSERT_EQUAL_INT16(125, fltel_Blocks[7].Fault_Env_Record[1].CPU_Temperature);
    TEST_ASSERT_EQUAL_UINT16(131, fltel_Blocks[7].Fault_Env_Record[1].Supply_Voltage);
    TEST_ASSERT_EQUAL_INT16(103, fltel_Blocks[7].Fault_Env_Record[1].Supply_Current);
    TEST_ASSERT_EQUAL_INT16(1500, fltel_Blocks[7].Fault_Env_Record[1].Commanded_RPM);
    TEST_ASSERT_EQUAL_INT16(0x1234, fltel_Blocks[7].Fault_Env_Record[1].IRQSTAT);
    TEST_ASSERT_EQUAL_INT16(50, fltel_Blocks[7].Fault_Env_Record[1].PWM_Duty_Cycle);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_General_Info.Fault_Index);
    TEST_ASSERT_EQUAL_UINT16(543, flt_log_General_Info.Lifetime_Fault_Count);
}
