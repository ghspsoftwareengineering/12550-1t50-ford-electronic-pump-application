#ifndef __EL_REGS_M11720A_H__
#define __EL_REGS_M11720A_H__

#include "io_m11720a_IAR.h"

// Register definition mapping from CMSIS style to ELMOS style

// module WDOG instance WDOG

#define WDOG_CONTROL                              (WDOG->CONTROL.reg)
#define WDOG_CONTROL_bit                          (WDOG->CONTROL.bit)

#define E_WDOG_CONTROL_RUN_ENABLE                 (0x0001u)
#define E_WDOG_CONTROL_RESTART                    (0x0002u)

#define WDOG_WINDOW                               (WDOG->WINDOW.reg)
#define WDOG_WINDOW_bit                           (WDOG->WINDOW.bit)

#define E_WDOG_WINDOW_ENABLE                      (0x0010u)

#define WDOG_PRESCALER                            (WDOG->PRESCALER.reg)
#define WDOG_PRESCALER_bit                        (WDOG->PRESCALER.bit)

#define WDOG_RELOAD                               (WDOG->RELOAD.reg)
#define WDOG_RELOAD_bit                           (WDOG->RELOAD.bit)

#define WDOG_COUNTER                              (WDOG->COUNTER.reg)
#define WDOG_COUNTER_bit                          (WDOG->COUNTER.bit)

// module SYS_STATE instance SYS_STATE

#define SYS_STATE_CONTROL                         (SYS_STATE->CONTROL.reg)
#define SYS_STATE_CONTROL_bit                     (SYS_STATE->CONTROL.bit)

#define SYS_STATE_RESET_STATUS                    (SYS_STATE->RESET_STATUS.reg)
#define SYS_STATE_RESET_STATUS_bit                (SYS_STATE->RESET_STATUS.bit)

#define E_SYS_STATE_RESET_STATUS_VCORE_OK         (0x0001u)
#define E_SYS_STATE_RESET_STATUS_SYS_CLK_FAIL     (0x0002u)
#define E_SYS_STATE_RESET_STATUS_CPU_LOCKUP       (0x0004u)
#define E_SYS_STATE_RESET_STATUS_SOFTWARE         (0x0008u)
#define E_SYS_STATE_RESET_STATUS_SRAM_PARITY      (0x0010u)
#define E_SYS_STATE_RESET_STATUS_OTP_ECC_FAIL     (0x0020u)
#define E_SYS_STATE_RESET_STATUS_WATCHDOG_WINDOW  (0x0040u)
#define E_SYS_STATE_RESET_STATUS_WATCHDOG         (0x0080u)
#define E_SYS_STATE_RESET_STATUS_WATCHDOG_ZERO    (0x0100u)
#define E_SYS_STATE_RESET_STATUS_CRC16_MISMATCH   (0x0200u)
#define E_SYS_STATE_RESET_STATUS_TRIM_PARITY      (0x0400u)

#define SYS_STATE_RESET_STATUS_CLEAR              (SYS_STATE->RESET_STATUS_CLEAR.reg)
#define SYS_STATE_RESET_STATUS_CLEAR_bit          (SYS_STATE->RESET_STATUS_CLEAR.bit)

#define E_SYS_STATE_RESET_STATUS_CLEAR_CLEAR      (0x0001u)

#define SYS_STATE_RESET_ENABLE                    (SYS_STATE->RESET_ENABLE.reg)
#define SYS_STATE_RESET_ENABLE_bit                (SYS_STATE->RESET_ENABLE.bit)

#define E_SYS_STATE_RESET_ENABLE_SOFTWARE         (0x0001u)
#define E_SYS_STATE_RESET_ENABLE_SRAM_PARITY      (0x0002u)
#define E_SYS_STATE_RESET_ENABLE_OTP_ECC_FAIL     (0x0004u)
#define E_SYS_STATE_RESET_ENABLE_WATCHDOG_WINDOW  (0x0008u)
#define E_SYS_STATE_RESET_ENABLE_WATCHDOG         (0x0010u)
#define E_SYS_STATE_RESET_ENABLE_WATCHDOG_ZERO    (0x0020u)
#define E_SYS_STATE_RESET_ENABLE_CRC16_MISMATCH   (0x0040u)
#define E_SYS_STATE_RESET_ENABLE_TRIM_PARITY      (0x0080u)

#define SYS_STATE_SW_RESET                        (SYS_STATE->SW_RESET.reg)
#define SYS_STATE_SW_RESET_bit                    (SYS_STATE->SW_RESET.bit)

#define E_SYS_STATE_SW_RESET_POR_FLAG             (0x0001u)
#define E_SYS_STATE_SW_RESET_SW_RESET             (0x0002u)

#define SYS_STATE_ENABLE_DEBUG                    (SYS_STATE->ENABLE_DEBUG.reg)
#define SYS_STATE_ENABLE_DEBUG_bit                (SYS_STATE->ENABLE_DEBUG.bit)

#define E_SYS_STATE_ENABLE_DEBUG_ENABLE           (0x0001u)
#define E_SYS_STATE_ENABLE_DEBUG_EXIT_BOOT_LOADER (0x0002u)

#define SYS_STATE_WDOG_CLK_FREQ                   (SYS_STATE->WDOG_CLK_FREQ.reg)
#define SYS_STATE_WDOG_CLK_FREQ_bit               (SYS_STATE->WDOG_CLK_FREQ.bit)

#define SYS_STATE_SYS_CLK_MEAS                    (SYS_STATE->SYS_CLK_MEAS.reg)
#define SYS_STATE_SYS_CLK_MEAS_bit                (SYS_STATE->SYS_CLK_MEAS.bit)

#define SYS_STATE_SYS_OSC_TRIM                    (SYS_STATE->SYS_OSC_TRIM.reg)
#define SYS_STATE_SYS_OSC_TRIM_bit                (SYS_STATE->SYS_OSC_TRIM.bit)

#define E_SYS_STATE_SYS_OSC_TRIM_LOCK             (0x0100u)

#define SYS_STATE_VECTOR_TABLE_REMAP              (SYS_STATE->VECTOR_TABLE_REMAP.reg)
#define SYS_STATE_VECTOR_TABLE_REMAP_bit          (SYS_STATE->VECTOR_TABLE_REMAP.bit)

#define E_SYS_STATE_VECTOR_TABLE_REMAP_SEL        (0x0004u)
#define E_SYS_STATE_VECTOR_TABLE_REMAP_LOCK       (0x0008u)
#define E_SYS_STATE_VECTOR_TABLE_REMAP_LOCK_TABLE_BASE (0x0040u)

#define SYS_STATE_VECTOR_TABLE_BASE0              (SYS_STATE->VECTOR_TABLE_BASE0.reg)
#define SYS_STATE_VECTOR_TABLE_BASE0_bit          (SYS_STATE->VECTOR_TABLE_BASE0.bit)

#define SYS_STATE_VECTOR_TABLE_BASE1              (SYS_STATE->VECTOR_TABLE_BASE1.reg)
#define SYS_STATE_VECTOR_TABLE_BASE1_bit          (SYS_STATE->VECTOR_TABLE_BASE1.bit)

#define SYS_STATE_TEST_MASTER_DATA_L              (SYS_STATE->TEST_MASTER_DATA_L.reg)
#define SYS_STATE_TEST_MASTER_DATA_L_bit          (SYS_STATE->TEST_MASTER_DATA_L.bit)

#define SYS_STATE_TEST_MASTER_DATA_H              (SYS_STATE->TEST_MASTER_DATA_H.reg)
#define SYS_STATE_TEST_MASTER_DATA_H_bit          (SYS_STATE->TEST_MASTER_DATA_H.bit)

#define SYS_STATE_TEST_MASTER_ADDR                (SYS_STATE->TEST_MASTER_ADDR.reg)
#define SYS_STATE_TEST_MASTER_ADDR_bit            (SYS_STATE->TEST_MASTER_ADDR.bit)

#define E_SYS_STATE_TEST_MASTER_ADDR_WRITE        (0x1000u)

#define SYS_STATE_IRQ_STATUS                      (SYS_STATE->IRQ_STATUS.reg)
#define SYS_STATE_IRQ_STATUS_bit                  (SYS_STATE->IRQ_STATUS.bit)

#define E_SYS_STATE_IRQ_STATUS_SOFTWARE           (0x0001u)
#define E_SYS_STATE_IRQ_STATUS_SRAM_PARITY        (0x0002u)
#define E_SYS_STATE_IRQ_STATUS_OTP_ECC_FAIL       (0x0004u)
#define E_SYS_STATE_IRQ_STATUS_WATCHDOG_WINDOW    (0x0008u)
#define E_SYS_STATE_IRQ_STATUS_WATCHDOG           (0x0010u)
#define E_SYS_STATE_IRQ_STATUS_WATCHDOG_ZERO      (0x0020u)
#define E_SYS_STATE_IRQ_STATUS_CRC16_MISMATCH     (0x0040u)
#define E_SYS_STATE_IRQ_STATUS_TRIM_PARITY        (0x0080u)

#define SYS_STATE_IRQ_MASK                        (SYS_STATE->IRQ_MASK.reg)
#define SYS_STATE_IRQ_MASK_bit                    (SYS_STATE->IRQ_MASK.bit)

#define SYS_STATE_IRQ_VENABLE                     (SYS_STATE->IRQ_VENABLE.reg)
#define SYS_STATE_IRQ_VENABLE_bit                 (SYS_STATE->IRQ_VENABLE.bit)

#define SYS_STATE_IRQ_VDISABLE                    (SYS_STATE->IRQ_VDISABLE.reg)
#define SYS_STATE_IRQ_VDISABLE_bit                (SYS_STATE->IRQ_VDISABLE.bit)

#define SYS_STATE_IRQ_VMAX                        (SYS_STATE->IRQ_VMAX.reg)
#define SYS_STATE_IRQ_VMAX_bit                    (SYS_STATE->IRQ_VMAX.bit)

#define SYS_STATE_IRQ_VNO                         (SYS_STATE->IRQ_VNO.reg)
#define SYS_STATE_IRQ_VNO_bit                     (SYS_STATE->IRQ_VNO.bit)

// module CRC16 instance CRC16

#define CRC16_START0                              (CRC16->START0.reg)
#define CRC16_START0_bit                          (CRC16->START0.bit)

#define CRC16_START1                              (CRC16->START1.reg)
#define CRC16_START1_bit                          (CRC16->START1.bit)

#define CRC16_LENGTH0                             (CRC16->LENGTH0.reg)
#define CRC16_LENGTH0_bit                         (CRC16->LENGTH0.bit)

#define CRC16_LENGTH1                             (CRC16->LENGTH1.reg)
#define CRC16_LENGTH1_bit                         (CRC16->LENGTH1.bit)

#define CRC16_EXPECTED_CRC                        (CRC16->EXPECTED_CRC.reg)
#define CRC16_EXPECTED_CRC_bit                    (CRC16->EXPECTED_CRC.bit)

#define CRC16_CONFIG                              (CRC16->CONFIG.reg)
#define CRC16_CONFIG_bit                          (CRC16->CONFIG.bit)

#define E_CRC16_CONFIG_KEEP_SUM                   (0x0008u)

#define CRC16_STATUS                              (CRC16->STATUS.reg)
#define CRC16_STATUS_bit                          (CRC16->STATUS.bit)

#define CRC16_CRC_SUM                             (CRC16->CRC_SUM.reg)
#define CRC16_CRC_SUM_bit                         (CRC16->CRC_SUM.bit)

// module OTP_CTRL instance OTP0_CTRL

#define OTP0_CTRL_READ_CONFIG                     (OTP0_CTRL->READ_CONFIG.reg)
#define OTP0_CTRL_READ_CONFIG_bit                 (OTP0_CTRL->READ_CONFIG.bit)

#define E_OTP_CTRL_READ_CONFIG_DIFFERENTIAL       (0x0001u)
#define E_OTP_CTRL_READ_CONFIG_REDUNDANT          (0x0002u)

#define OTP0_CTRL_PROG_CONFIG                     (OTP0_CTRL->PROG_CONFIG.reg)
#define OTP0_CTRL_PROG_CONFIG_bit                 (OTP0_CTRL->PROG_CONFIG.bit)

#define OTP0_CTRL_PROG_CONTROL                    (OTP0_CTRL->PROG_CONTROL.reg)
#define OTP0_CTRL_PROG_CONTROL_bit                (OTP0_CTRL->PROG_CONTROL.bit)

#define E_OTP_CTRL_PROG_CONTROL_OTP_PROG          (0x0001u)
#define E_OTP_CTRL_PROG_CONTROL_DIFFERENTIAL      (0x0002u)

#define OTP0_CTRL_WDATA0                          (OTP0_CTRL->WDATA0.reg)
#define OTP0_CTRL_WDATA0_bit                      (OTP0_CTRL->WDATA0.bit)

#define OTP0_CTRL_WDATA1                          (OTP0_CTRL->WDATA1.reg)
#define OTP0_CTRL_WDATA1_bit                      (OTP0_CTRL->WDATA1.bit)

#define OTP0_CTRL_WADDR                           (OTP0_CTRL->WADDR.reg)
#define OTP0_CTRL_WADDR_bit                       (OTP0_CTRL->WADDR.bit)

#define OTP0_CTRL_PROG_STATUS                     (OTP0_CTRL->PROG_STATUS.reg)
#define OTP0_CTRL_PROG_STATUS_bit                 (OTP0_CTRL->PROG_STATUS.bit)

#define E_OTP_CTRL_PROG_STATUS_BUSY               (0x0010u)
#define E_OTP_CTRL_PROG_STATUS_FAIL0              (0x0020u)
#define E_OTP_CTRL_PROG_STATUS_FAIL1              (0x0040u)

#define OTP0_CTRL_BIT_SOAK_STATUS                 (OTP0_CTRL->BIT_SOAK_STATUS.reg)
#define OTP0_CTRL_BIT_SOAK_STATUS_bit             (OTP0_CTRL->BIT_SOAK_STATUS.bit)

#define E_OTP_CTRL_BIT_SOAK_STATUS_TAKE           (0x0001u)

#define OTP0_CTRL_PROTECT                         (OTP0_CTRL->PROTECT.reg)
#define OTP0_CTRL_PROTECT_bit                     (OTP0_CTRL->PROTECT.bit)

#define OTP0_CTRL_BOOT_PROTECT                    (OTP0_CTRL->BOOT_PROTECT.reg)
#define OTP0_CTRL_BOOT_PROTECT_bit                (OTP0_CTRL->BOOT_PROTECT.bit)

#define OTP0_CTRL_IO_FLAG                         (OTP0_CTRL->IO_FLAG.reg)
#define OTP0_CTRL_IO_FLAG_bit                     (OTP0_CTRL->IO_FLAG.bit)

#define E_OTP_CTRL_IO_FLAG_FLAG                   (0x0001u)

#define OTP0_CTRL_TEST_LOCK                       (OTP0_CTRL->TEST_LOCK.reg)
#define OTP0_CTRL_TEST_LOCK_bit                   (OTP0_CTRL->TEST_LOCK.bit)

#define E_OTP_CTRL_TEST_LOCK_LOCK                 (0x0001u)

#define OTP0_CTRL_TEST_TRP_CONFIG                 (OTP0_CTRL->TEST_TRP_CONFIG.reg)
#define OTP0_CTRL_TEST_TRP_CONFIG_bit             (OTP0_CTRL->TEST_TRP_CONFIG.bit)

#define E_OTP_CTRL_TEST_TRP_CONFIG_SEL            (0x0100u)

#define OTP0_CTRL_TEST_MR                         (OTP0_CTRL->TEST_MR.reg)
#define OTP0_CTRL_TEST_MR_bit                     (OTP0_CTRL->TEST_MR.bit)

#define E_OTP_CTRL_TEST_MR_READ_SEL               (0x0100u)
#define E_OTP_CTRL_TEST_MR_PROG_SEL               (0x0200u)

#define OTP0_CTRL_TEST_FORCE                      (OTP0_CTRL->TEST_FORCE.reg)
#define OTP0_CTRL_TEST_FORCE_bit                  (OTP0_CTRL->TEST_FORCE.bit)

#define E_OTP_CTRL_TEST_FORCE_FORCE               (0x0001u)
#define E_OTP_CTRL_TEST_FORCE_CK                  (0x0002u)
#define E_OTP_CTRL_TEST_FORCE_SEL                 (0x0004u)
#define E_OTP_CTRL_TEST_FORCE_WE                  (0x0008u)
#define E_OTP_CTRL_TEST_FORCE_OE                  (0x0010u)

#define OTP0_CTRL_MPP                             (OTP0_CTRL->MPP.reg)
#define OTP0_CTRL_MPP_bit                         (OTP0_CTRL->MPP.bit)

#define OTP0_CTRL_MRR                             (OTP0_CTRL->MRR.reg)
#define OTP0_CTRL_MRR_bit                         (OTP0_CTRL->MRR.bit)

#define OTP0_CTRL_VRR                             (OTP0_CTRL->VRR.reg)
#define OTP0_CTRL_VRR_bit                         (OTP0_CTRL->VRR.bit)

#define OTP0_CTRL_ERASE                           (OTP0_CTRL->ERASE.reg)
#define OTP0_CTRL_ERASE_bit                       (OTP0_CTRL->ERASE.bit)

#define E_OTP_CTRL_ERASE_START                    (0x0001u)
#define E_OTP_CTRL_ERASE_READY                    (0x0002u)

#define OTP0_CTRL_TEST_CELL_STRESS                (OTP0_CTRL->TEST_CELL_STRESS.reg)
#define OTP0_CTRL_TEST_CELL_STRESS_bit            (OTP0_CTRL->TEST_CELL_STRESS.bit)

#define E_OTP_CTRL_TEST_CELL_STRESS_STRESS        (0x0001u)

#define OTP0_CTRL_TEST_SEL_VAL                    (OTP0_CTRL->TEST_SEL_VAL.reg)
#define OTP0_CTRL_TEST_SEL_VAL_bit                (OTP0_CTRL->TEST_SEL_VAL.bit)

#define E_OTP_CTRL_TEST_SEL_VAL_VPP_E_SEL         (0x0001u)
#define E_OTP_CTRL_TEST_SEL_VAL_VPP_E_VAL         (0x0002u)
#define E_OTP_CTRL_TEST_SEL_VAL_VRR_E_SEL         (0x0004u)
#define E_OTP_CTRL_TEST_SEL_VAL_VRR_E_VAL         (0x0008u)
#define E_OTP_CTRL_TEST_SEL_VAL_EHV_E_SEL         (0x0010u)
#define E_OTP_CTRL_TEST_SEL_VAL_EHV_E_VAL         (0x0020u)
#define E_OTP_CTRL_TEST_SEL_VAL_DBEN_SEL          (0x0040u)
#define E_OTP_CTRL_TEST_SEL_VAL_DBEN_VAL          (0x0080u)
#define E_OTP_CTRL_TEST_SEL_VAL_MASK_IPS_CLKS     (0x0100u)

#define OTP0_CTRL_TEST_ECC                        (OTP0_CTRL->TEST_ECC.reg)
#define OTP0_CTRL_TEST_ECC_bit                    (OTP0_CTRL->TEST_ECC.bit)

#define E_OTP_CTRL_TEST_ECC_ECC_1BIT              (0x1000u)
#define E_OTP_CTRL_TEST_ECC_ECC_2BIT              (0x2000u)
#define E_OTP_CTRL_TEST_ECC_DISABLE_READ_ECC      (0x4000u)
#define E_OTP_CTRL_TEST_ECC_FORCE_ECC_WRITE_1     (0x8000u)

#define OTP0_CTRL_MDB                             (OTP0_CTRL->MDB.reg)
#define OTP0_CTRL_MDB_bit                         (OTP0_CTRL->MDB.bit)

// module OTP_CTRL instance OTP1_CTRL

#define OTP1_CTRL_READ_CONFIG                     (OTP1_CTRL->READ_CONFIG.reg)
#define OTP1_CTRL_READ_CONFIG_bit                 (OTP1_CTRL->READ_CONFIG.bit)

#define E_OTP_CTRL_READ_CONFIG_DIFFERENTIAL       (0x0001u)
#define E_OTP_CTRL_READ_CONFIG_REDUNDANT          (0x0002u)

#define OTP1_CTRL_PROG_CONFIG                     (OTP1_CTRL->PROG_CONFIG.reg)
#define OTP1_CTRL_PROG_CONFIG_bit                 (OTP1_CTRL->PROG_CONFIG.bit)

#define OTP1_CTRL_PROG_CONTROL                    (OTP1_CTRL->PROG_CONTROL.reg)
#define OTP1_CTRL_PROG_CONTROL_bit                (OTP1_CTRL->PROG_CONTROL.bit)

#define E_OTP_CTRL_PROG_CONTROL_OTP_PROG          (0x0001u)
#define E_OTP_CTRL_PROG_CONTROL_DIFFERENTIAL      (0x0002u)

#define OTP1_CTRL_WDATA0                          (OTP1_CTRL->WDATA0.reg)
#define OTP1_CTRL_WDATA0_bit                      (OTP1_CTRL->WDATA0.bit)

#define OTP1_CTRL_WDATA1                          (OTP1_CTRL->WDATA1.reg)
#define OTP1_CTRL_WDATA1_bit                      (OTP1_CTRL->WDATA1.bit)

#define OTP1_CTRL_WADDR                           (OTP1_CTRL->WADDR.reg)
#define OTP1_CTRL_WADDR_bit                       (OTP1_CTRL->WADDR.bit)

#define OTP1_CTRL_PROG_STATUS                     (OTP1_CTRL->PROG_STATUS.reg)
#define OTP1_CTRL_PROG_STATUS_bit                 (OTP1_CTRL->PROG_STATUS.bit)

#define E_OTP_CTRL_PROG_STATUS_BUSY               (0x0010u)
#define E_OTP_CTRL_PROG_STATUS_FAIL0              (0x0020u)
#define E_OTP_CTRL_PROG_STATUS_FAIL1              (0x0040u)

#define OTP1_CTRL_BIT_SOAK_STATUS                 (OTP1_CTRL->BIT_SOAK_STATUS.reg)
#define OTP1_CTRL_BIT_SOAK_STATUS_bit             (OTP1_CTRL->BIT_SOAK_STATUS.bit)

#define E_OTP_CTRL_BIT_SOAK_STATUS_TAKE           (0x0001u)

#define OTP1_CTRL_PROTECT                         (OTP1_CTRL->PROTECT.reg)
#define OTP1_CTRL_PROTECT_bit                     (OTP1_CTRL->PROTECT.bit)

#define OTP1_CTRL_BOOT_PROTECT                    (OTP1_CTRL->BOOT_PROTECT.reg)
#define OTP1_CTRL_BOOT_PROTECT_bit                (OTP1_CTRL->BOOT_PROTECT.bit)

#define OTP1_CTRL_IO_FLAG                         (OTP1_CTRL->IO_FLAG.reg)
#define OTP1_CTRL_IO_FLAG_bit                     (OTP1_CTRL->IO_FLAG.bit)

#define E_OTP_CTRL_IO_FLAG_FLAG                   (0x0001u)

#define OTP1_CTRL_TEST_LOCK                       (OTP1_CTRL->TEST_LOCK.reg)
#define OTP1_CTRL_TEST_LOCK_bit                   (OTP1_CTRL->TEST_LOCK.bit)

#define E_OTP_CTRL_TEST_LOCK_LOCK                 (0x0001u)

#define OTP1_CTRL_TEST_TRP_CONFIG                 (OTP1_CTRL->TEST_TRP_CONFIG.reg)
#define OTP1_CTRL_TEST_TRP_CONFIG_bit             (OTP1_CTRL->TEST_TRP_CONFIG.bit)

#define E_OTP_CTRL_TEST_TRP_CONFIG_SEL            (0x0100u)

#define OTP1_CTRL_TEST_MR                         (OTP1_CTRL->TEST_MR.reg)
#define OTP1_CTRL_TEST_MR_bit                     (OTP1_CTRL->TEST_MR.bit)

#define E_OTP_CTRL_TEST_MR_READ_SEL               (0x0100u)
#define E_OTP_CTRL_TEST_MR_PROG_SEL               (0x0200u)

#define OTP1_CTRL_TEST_FORCE                      (OTP1_CTRL->TEST_FORCE.reg)
#define OTP1_CTRL_TEST_FORCE_bit                  (OTP1_CTRL->TEST_FORCE.bit)

#define E_OTP_CTRL_TEST_FORCE_FORCE               (0x0001u)
#define E_OTP_CTRL_TEST_FORCE_CK                  (0x0002u)
#define E_OTP_CTRL_TEST_FORCE_SEL                 (0x0004u)
#define E_OTP_CTRL_TEST_FORCE_WE                  (0x0008u)
#define E_OTP_CTRL_TEST_FORCE_OE                  (0x0010u)

#define OTP1_CTRL_MPP                             (OTP1_CTRL->MPP.reg)
#define OTP1_CTRL_MPP_bit                         (OTP1_CTRL->MPP.bit)

#define OTP1_CTRL_MRR                             (OTP1_CTRL->MRR.reg)
#define OTP1_CTRL_MRR_bit                         (OTP1_CTRL->MRR.bit)

#define OTP1_CTRL_VRR                             (OTP1_CTRL->VRR.reg)
#define OTP1_CTRL_VRR_bit                         (OTP1_CTRL->VRR.bit)

#define OTP1_CTRL_ERASE                           (OTP1_CTRL->ERASE.reg)
#define OTP1_CTRL_ERASE_bit                       (OTP1_CTRL->ERASE.bit)

#define E_OTP_CTRL_ERASE_START                    (0x0001u)
#define E_OTP_CTRL_ERASE_READY                    (0x0002u)

#define OTP1_CTRL_TEST_CELL_STRESS                (OTP1_CTRL->TEST_CELL_STRESS.reg)
#define OTP1_CTRL_TEST_CELL_STRESS_bit            (OTP1_CTRL->TEST_CELL_STRESS.bit)

#define E_OTP_CTRL_TEST_CELL_STRESS_STRESS        (0x0001u)

#define OTP1_CTRL_TEST_SEL_VAL                    (OTP1_CTRL->TEST_SEL_VAL.reg)
#define OTP1_CTRL_TEST_SEL_VAL_bit                (OTP1_CTRL->TEST_SEL_VAL.bit)

#define E_OTP_CTRL_TEST_SEL_VAL_VPP_E_SEL         (0x0001u)
#define E_OTP_CTRL_TEST_SEL_VAL_VPP_E_VAL         (0x0002u)
#define E_OTP_CTRL_TEST_SEL_VAL_VRR_E_SEL         (0x0004u)
#define E_OTP_CTRL_TEST_SEL_VAL_VRR_E_VAL         (0x0008u)
#define E_OTP_CTRL_TEST_SEL_VAL_EHV_E_SEL         (0x0010u)
#define E_OTP_CTRL_TEST_SEL_VAL_EHV_E_VAL         (0x0020u)
#define E_OTP_CTRL_TEST_SEL_VAL_DBEN_SEL          (0x0040u)
#define E_OTP_CTRL_TEST_SEL_VAL_DBEN_VAL          (0x0080u)
#define E_OTP_CTRL_TEST_SEL_VAL_MASK_IPS_CLKS     (0x0100u)

#define OTP1_CTRL_TEST_ECC                        (OTP1_CTRL->TEST_ECC.reg)
#define OTP1_CTRL_TEST_ECC_bit                    (OTP1_CTRL->TEST_ECC.bit)

#define E_OTP_CTRL_TEST_ECC_ECC_1BIT              (0x1000u)
#define E_OTP_CTRL_TEST_ECC_ECC_2BIT              (0x2000u)
#define E_OTP_CTRL_TEST_ECC_DISABLE_READ_ECC      (0x4000u)
#define E_OTP_CTRL_TEST_ECC_FORCE_ECC_WRITE_1     (0x8000u)

#define OTP1_CTRL_MDB                             (OTP1_CTRL->MDB.reg)
#define OTP1_CTRL_MDB_bit                         (OTP1_CTRL->MDB.bit)

// module EEPROM_CTRL instance EEPROM_CTRL

#define EEPROM_CTRL_MODE                          (EEPROM_CTRL->MODE.reg)
#define EEPROM_CTRL_MODE_bit                      (EEPROM_CTRL->MODE.bit)

#define E_EEPROM_CTRL_MODE_ERASE                  (0x0001u)
#define E_EEPROM_CTRL_MODE_PROGRAM                (0x0002u)

#define EEPROM_CTRL_STATUS                        (EEPROM_CTRL->STATUS.reg)
#define EEPROM_CTRL_STATUS_bit                    (EEPROM_CTRL->STATUS.bit)

#define E_EEPROM_CTRL_STATUS_BUSY                 (0x0001u)

#define EEPROM_CTRL_LOCK_L                        (EEPROM_CTRL->LOCK_L.reg)
#define EEPROM_CTRL_LOCK_L_bit                    (EEPROM_CTRL->LOCK_L.bit)

#define EEPROM_CTRL_LOCK_U                        (EEPROM_CTRL->LOCK_U.reg)
#define EEPROM_CTRL_LOCK_U_bit                    (EEPROM_CTRL->LOCK_U.bit)

#define EEPROM_CTRL_LOCK_CNT                      (EEPROM_CTRL->LOCK_CNT.reg)
#define EEPROM_CTRL_LOCK_CNT_bit                  (EEPROM_CTRL->LOCK_CNT.bit)

#define EEPROM_CTRL_LOCK_L_FREEZE                 (EEPROM_CTRL->LOCK_L_FREEZE.reg)
#define EEPROM_CTRL_LOCK_L_FREEZE_bit             (EEPROM_CTRL->LOCK_L_FREEZE.bit)

#define EEPROM_CTRL_LOCK_U_FREEZE                 (EEPROM_CTRL->LOCK_U_FREEZE.reg)
#define EEPROM_CTRL_LOCK_U_FREEZE_bit             (EEPROM_CTRL->LOCK_U_FREEZE.bit)

#define EEPROM_CTRL_IP_ENABLE                     (EEPROM_CTRL->IP_ENABLE.reg)
#define EEPROM_CTRL_IP_ENABLE_bit                 (EEPROM_CTRL->IP_ENABLE.bit)

#define E_EEPROM_CTRL_IP_ENABLE_ENABLE            (0x0001u)

#define EEPROM_CTRL_IRQ_STATUS                    (EEPROM_CTRL->IRQ_STATUS.reg)
#define EEPROM_CTRL_IRQ_STATUS_bit                (EEPROM_CTRL->IRQ_STATUS.bit)

#define E_EEPROM_CTRL_IRQ_STATUS_TIMEOUT          (0x0001u)
#define E_EEPROM_CTRL_IRQ_STATUS_INVALID_ADDR     (0x0002u)
#define E_EEPROM_CTRL_IRQ_STATUS_IP_OFF_ACCESS    (0x0004u)

#define EEPROM_CTRL_IRQ_MASK                      (EEPROM_CTRL->IRQ_MASK.reg)
#define EEPROM_CTRL_IRQ_MASK_bit                  (EEPROM_CTRL->IRQ_MASK.bit)

#define EEPROM_CTRL_IRQ_VENABLE                   (EEPROM_CTRL->IRQ_VENABLE.reg)
#define EEPROM_CTRL_IRQ_VENABLE_bit               (EEPROM_CTRL->IRQ_VENABLE.bit)

#define EEPROM_CTRL_IRQ_VDISABLE                  (EEPROM_CTRL->IRQ_VDISABLE.reg)
#define EEPROM_CTRL_IRQ_VDISABLE_bit              (EEPROM_CTRL->IRQ_VDISABLE.bit)

#define EEPROM_CTRL_IRQ_VMAX                      (EEPROM_CTRL->IRQ_VMAX.reg)
#define EEPROM_CTRL_IRQ_VMAX_bit                  (EEPROM_CTRL->IRQ_VMAX.bit)

#define EEPROM_CTRL_IRQ_VNO                       (EEPROM_CTRL->IRQ_VNO.reg)
#define EEPROM_CTRL_IRQ_VNO_bit                   (EEPROM_CTRL->IRQ_VNO.bit)

// module GPIO instance GPIO

#define GPIO_DATA_OUT                             (GPIO->DATA_OUT.reg)
#define GPIO_DATA_OUT_bit                         (GPIO->DATA_OUT.bit)

#define GPIO_DATA_OE                              (GPIO->DATA_OE.reg)
#define GPIO_DATA_OE_bit                          (GPIO->DATA_OE.bit)

#define GPIO_DATA_IN                              (GPIO->DATA_IN.reg)
#define GPIO_DATA_IN_bit                          (GPIO->DATA_IN.bit)

#define GPIO_DATA_IE                              (GPIO->DATA_IE.reg)
#define GPIO_DATA_IE_bit                          (GPIO->DATA_IE.bit)

#define GPIO_PULL_UP                              (GPIO->PULL_UP.reg)
#define GPIO_PULL_UP_bit                          (GPIO->PULL_UP.bit)

#define GPIO_PULL_DOWN                            (GPIO->PULL_DOWN.reg)
#define GPIO_PULL_DOWN_bit                        (GPIO->PULL_DOWN.bit)

#define GPIO_TRIG_EDGE                            (GPIO->TRIG_EDGE.reg)
#define GPIO_TRIG_EDGE_bit                        (GPIO->TRIG_EDGE.bit)

#define GPIO_IRQ_STATUS                           (GPIO->IRQ_STATUS.reg)
#define GPIO_IRQ_STATUS_bit                       (GPIO->IRQ_STATUS.bit)

#define E_GPIO_IRQ_STATUS_EVT_POS_0               (0x0001u)
#define E_GPIO_IRQ_STATUS_EVT_NEG_0               (0x0002u)
#define E_GPIO_IRQ_STATUS_EVT_POS_1               (0x0004u)
#define E_GPIO_IRQ_STATUS_EVT_NEG_1               (0x0008u)
#define E_GPIO_IRQ_STATUS_EVT_POS_2               (0x0010u)
#define E_GPIO_IRQ_STATUS_EVT_NEG_2               (0x0020u)
#define E_GPIO_IRQ_STATUS_EVT_POS_3               (0x0040u)
#define E_GPIO_IRQ_STATUS_EVT_NEG_3               (0x0080u)
#define E_GPIO_IRQ_STATUS_EVT_POS_4               (0x0100u)
#define E_GPIO_IRQ_STATUS_EVT_NEG_4               (0x0200u)

#define GPIO_IRQ_MASK                             (GPIO->IRQ_MASK.reg)
#define GPIO_IRQ_MASK_bit                         (GPIO->IRQ_MASK.bit)

#define GPIO_IRQ_VENABLE                          (GPIO->IRQ_VENABLE.reg)
#define GPIO_IRQ_VENABLE_bit                      (GPIO->IRQ_VENABLE.bit)

#define GPIO_IRQ_VDISABLE                         (GPIO->IRQ_VDISABLE.reg)
#define GPIO_IRQ_VDISABLE_bit                     (GPIO->IRQ_VDISABLE.bit)

#define GPIO_IRQ_VMAX                             (GPIO->IRQ_VMAX.reg)
#define GPIO_IRQ_VMAX_bit                         (GPIO->IRQ_VMAX.bit)

#define GPIO_IRQ_VNO                              (GPIO->IRQ_VNO.reg)
#define GPIO_IRQ_VNO_bit                          (GPIO->IRQ_VNO.bit)

// module SWTIMER instance SWTIMER

#define SWTIMER_CONFIG                            (SWTIMER->CONFIG.reg)
#define SWTIMER_CONFIG_bit                        (SWTIMER->CONFIG.bit)

#define SWTIMER_COMMAND                           (SWTIMER->COMMAND.reg)
#define SWTIMER_COMMAND_bit                       (SWTIMER->COMMAND.bit)

#define SWTIMER_CNT0_RELOAD                       (SWTIMER->CNT0_RELOAD.reg)
#define SWTIMER_CNT0_RELOAD_bit                   (SWTIMER->CNT0_RELOAD.bit)

#define SWTIMER_CNT1_RELOAD                       (SWTIMER->CNT1_RELOAD.reg)
#define SWTIMER_CNT1_RELOAD_bit                   (SWTIMER->CNT1_RELOAD.bit)

#define SWTIMER_DIV0_RELOAD                       (SWTIMER->DIV0_RELOAD.reg)
#define SWTIMER_DIV0_RELOAD_bit                   (SWTIMER->DIV0_RELOAD.bit)

#define SWTIMER_DIV1_RELOAD                       (SWTIMER->DIV1_RELOAD.reg)
#define SWTIMER_DIV1_RELOAD_bit                   (SWTIMER->DIV1_RELOAD.bit)

#define SWTIMER_CNT0_VALUE                        (SWTIMER->CNT0_VALUE.reg)
#define SWTIMER_CNT0_VALUE_bit                    (SWTIMER->CNT0_VALUE.bit)

#define SWTIMER_CNT1_VALUE                        (SWTIMER->CNT1_VALUE.reg)
#define SWTIMER_CNT1_VALUE_bit                    (SWTIMER->CNT1_VALUE.bit)

#define SWTIMER_IRQ_STATUS                        (SWTIMER->IRQ_STATUS.reg)
#define SWTIMER_IRQ_STATUS_bit                    (SWTIMER->IRQ_STATUS.bit)

#define SWTIMER_IRQ_MASK                          (SWTIMER->IRQ_MASK.reg)
#define SWTIMER_IRQ_MASK_bit                      (SWTIMER->IRQ_MASK.bit)

#define SWTIMER_IRQ_VENABLE                       (SWTIMER->IRQ_VENABLE.reg)
#define SWTIMER_IRQ_VENABLE_bit                   (SWTIMER->IRQ_VENABLE.bit)

#define SWTIMER_IRQ_VDISABLE                      (SWTIMER->IRQ_VDISABLE.reg)
#define SWTIMER_IRQ_VDISABLE_bit                  (SWTIMER->IRQ_VDISABLE.bit)

#define SWTIMER_IRQ_VMAX                          (SWTIMER->IRQ_VMAX.reg)
#define SWTIMER_IRQ_VMAX_bit                      (SWTIMER->IRQ_VMAX.bit)

#define SWTIMER_IRQ_VNO                           (SWTIMER->IRQ_VNO.reg)
#define SWTIMER_IRQ_VNO_bit                       (SWTIMER->IRQ_VNO.bit)

// module HB instance HB

#define HB_HBCTRL                                 (HB->HBCTRL.reg)
#define HB_HBCTRL_bit                             (HB->HBCTRL.bit)

#define HB_HBMEAS                                 (HB->HBMEAS.reg)
#define HB_HBMEAS_bit                             (HB->HBMEAS.bit)

#define E_HB_HBMEAS_MEAS1A                        (0x0001u)
#define E_HB_HBMEAS_MEAS1B                        (0x0002u)
#define E_HB_HBMEAS_ASELPWM                       (0x0004u)
#define E_HB_HBMEAS_BSELPWM                       (0x0008u)
#define E_HB_HBMEAS_ASELINV                       (0x0010u)
#define E_HB_HBMEAS_BSELINV                       (0x0020u)
#define E_HB_HBMEAS_CURRDIRA                      (0x0040u)
#define E_HB_HBMEAS_CURRDIRB                      (0x0080u)

#define HB_HBDATA                                 (HB->HBDATA.reg)
#define HB_HBDATA_bit                             (HB->HBDATA.bit)

#define E_HB_HBDATA_HB_EN                         (0x2000u)

#define HB_HBSTATUS                               (HB->HBSTATUS.reg)
#define HB_HBSTATUS_bit                           (HB->HBSTATUS.bit)

#define HB_HBSTATUS1                              (HB->HBSTATUS1.reg)
#define HB_HBSTATUS1_bit                          (HB->HBSTATUS1.bit)

#define HB_HBADJA                                 (HB->HBADJA.reg)
#define HB_HBADJA_bit                             (HB->HBADJA.bit)

#define HB_HBADJB                                 (HB->HBADJB.reg)
#define HB_HBADJB_bit                             (HB->HBADJB.bit)

#define HB_HBADJIREF                              (HB->HBADJIREF.reg)
#define HB_HBADJIREF_bit                          (HB->HBADJIREF.bit)

#define HB_HBDELA0                                (HB->HBDELA0.reg)
#define HB_HBDELA0_bit                            (HB->HBDELA0.bit)

#define HB_HBDELA1                                (HB->HBDELA1.reg)
#define HB_HBDELA1_bit                            (HB->HBDELA1.bit)

#define HB_HBDELB0                                (HB->HBDELB0.reg)
#define HB_HBDELB0_bit                            (HB->HBDELB0.bit)

#define HB_HBDELB1                                (HB->HBDELB1.reg)
#define HB_HBDELB1_bit                            (HB->HBDELB1.bit)

#define HB_HBTHRA                                 (HB->HBTHRA.reg)
#define HB_HBTHRA_bit                             (HB->HBTHRA.bit)

#define HB_HBTHRB                                 (HB->HBTHRB.reg)
#define HB_HBTHRB_bit                             (HB->HBTHRB.bit)

#define HB_IRQ_STATUS                             (HB->IRQ_STATUS.reg)
#define HB_IRQ_STATUS_bit                         (HB->IRQ_STATUS.bit)

#define E_HB_IRQ_STATUS_EVT_PWMA                  (0x0010u)
#define E_HB_IRQ_STATUS_EVT_PWMB                  (0x0020u)

#define HB_IRQ_MASK                               (HB->IRQ_MASK.reg)
#define HB_IRQ_MASK_bit                           (HB->IRQ_MASK.bit)

#define HB_IRQ_VENABLE                            (HB->IRQ_VENABLE.reg)
#define HB_IRQ_VENABLE_bit                        (HB->IRQ_VENABLE.bit)

#define HB_IRQ_VDISABLE                           (HB->IRQ_VDISABLE.reg)
#define HB_IRQ_VDISABLE_bit                       (HB->IRQ_VDISABLE.bit)

#define HB_IRQ_VMAX                               (HB->IRQ_VMAX.reg)
#define HB_IRQ_VMAX_bit                           (HB->IRQ_VMAX.bit)

#define HB_IRQ_VNO                                (HB->IRQ_VNO.reg)
#define HB_IRQ_VNO_bit                            (HB->IRQ_VNO.bit)

// module PWM instance PWM

#define PWM_PWMCTRL                               (PWM->PWMCTRL.reg)
#define PWM_PWMCTRL_bit                           (PWM->PWMCTRL.bit)

#define E_PWM_PWMCTRL_FORMAT                      (0x0008u)
#define E_PWM_PWMCTRL_MOD                         (0x0040u)

#define PWM_PWMVALA                               (PWM->PWMVALA.reg)
#define PWM_PWMVALA_bit                           (PWM->PWMVALA.bit)

#define PWM_PWMVALB                               (PWM->PWMVALB.reg)
#define PWM_PWMVALB_bit                           (PWM->PWMVALB.bit)

#define PWM_PWMSHIFT                              (PWM->PWMSHIFT.reg)
#define PWM_PWMSHIFT_bit                          (PWM->PWMSHIFT.bit)

#define PWM_TRGA1                                 (PWM->TRGA1.reg)
#define PWM_TRGA1_bit                             (PWM->TRGA1.bit)

#define PWM_TRGA2                                 (PWM->TRGA2.reg)
#define PWM_TRGA2_bit                             (PWM->TRGA2.bit)

#define PWM_TRGB1                                 (PWM->TRGB1.reg)
#define PWM_TRGB1_bit                             (PWM->TRGB1.bit)

#define PWM_TRGB2                                 (PWM->TRGB2.reg)
#define PWM_TRGB2_bit                             (PWM->TRGB2.bit)

// module HALL_SENS_CTRL instance HALL_SENS_CTRL

#define HALL_SENS_CTRL_HSCTRL                     (HALL_SENS_CTRL->HSCTRL.reg)
#define HALL_SENS_CTRL_HSCTRL_bit                 (HALL_SENS_CTRL->HSCTRL.bit)

#define E_HALL_SENS_CTRL_HSCTRL_ENABLE            (0x0001u)
#define E_HALL_SENS_CTRL_HSCTRL_ZERO              (0x0010u)

#define HALL_SENS_CTRL_HSADJ                      (HALL_SENS_CTRL->HSADJ.reg)
#define HALL_SENS_CTRL_HSADJ_bit                  (HALL_SENS_CTRL->HSADJ.bit)

// module ADC instance ADC

#define ADC_ADCCTRL                               (ADC->ADCCTRL.reg)
#define ADC_ADCCTRL_bit                           (ADC->ADCCTRL.bit)

#define E_ADC_ADCCTRL_AON                         (0x0004u)

#define ADC_MREQ0                                 (ADC->MREQ0.reg)
#define ADC_MREQ0_bit                             (ADC->MREQ0.bit)

#define E_ADC_MREQ0_AUTOTRG                       (0x4000u)

#define ADC_MREQ1                                 (ADC->MREQ1.reg)
#define ADC_MREQ1_bit                             (ADC->MREQ1.bit)

#define E_ADC_MREQ1_AUTOTRG                       (0x4000u)

#define ADC_MREQ2                                 (ADC->MREQ2.reg)
#define ADC_MREQ2_bit                             (ADC->MREQ2.bit)

#define E_ADC_MREQ2_AUTOTRG                       (0x4000u)

#define ADC_MREQ3                                 (ADC->MREQ3.reg)
#define ADC_MREQ3_bit                             (ADC->MREQ3.bit)

#define E_ADC_MREQ3_AUTOTRG                       (0x4000u)

#define ADC_MREQ4                                 (ADC->MREQ4.reg)
#define ADC_MREQ4_bit                             (ADC->MREQ4.bit)

#define E_ADC_MREQ4_AUTOTRG                       (0x4000u)

#define ADC_MREQ5                                 (ADC->MREQ5.reg)
#define ADC_MREQ5_bit                             (ADC->MREQ5.bit)

#define E_ADC_MREQ5_AUTOTRG                       (0x4000u)

#define ADC_MREQ6                                 (ADC->MREQ6.reg)
#define ADC_MREQ6_bit                             (ADC->MREQ6.bit)

#define E_ADC_MREQ6_AUTOTRG                       (0x4000u)

#define ADC_MREQ7                                 (ADC->MREQ7.reg)
#define ADC_MREQ7_bit                             (ADC->MREQ7.bit)

#define E_ADC_MREQ7_AUTOTRG                       (0x4000u)

#define ADC_MREQ8                                 (ADC->MREQ8.reg)
#define ADC_MREQ8_bit                             (ADC->MREQ8.bit)

#define E_ADC_MREQ8_AUTOTRG                       (0x4000u)

#define ADC_MREQ9                                 (ADC->MREQ9.reg)
#define ADC_MREQ9_bit                             (ADC->MREQ9.bit)

#define E_ADC_MREQ9_AUTOTRG                       (0x4000u)

#define ADC_MVAL0                                 (ADC->MVAL0.reg)
#define ADC_MVAL0_bit                             (ADC->MVAL0.bit)

#define ADC_MVAL1                                 (ADC->MVAL1.reg)
#define ADC_MVAL1_bit                             (ADC->MVAL1.bit)

#define ADC_MVAL2                                 (ADC->MVAL2.reg)
#define ADC_MVAL2_bit                             (ADC->MVAL2.bit)

#define ADC_MVAL3                                 (ADC->MVAL3.reg)
#define ADC_MVAL3_bit                             (ADC->MVAL3.bit)

#define ADC_MVAL4                                 (ADC->MVAL4.reg)
#define ADC_MVAL4_bit                             (ADC->MVAL4.bit)

#define ADC_MVAL5                                 (ADC->MVAL5.reg)
#define ADC_MVAL5_bit                             (ADC->MVAL5.bit)

#define ADC_MVAL6                                 (ADC->MVAL6.reg)
#define ADC_MVAL6_bit                             (ADC->MVAL6.bit)

#define ADC_MVAL7                                 (ADC->MVAL7.reg)
#define ADC_MVAL7_bit                             (ADC->MVAL7.bit)

#define ADC_MVAL8                                 (ADC->MVAL8.reg)
#define ADC_MVAL8_bit                             (ADC->MVAL8.bit)

#define ADC_MVAL9                                 (ADC->MVAL9.reg)
#define ADC_MVAL9_bit                             (ADC->MVAL9.bit)

#define ADC_TADC                                  (ADC->TADC.reg)
#define ADC_TADC_bit                              (ADC->TADC.bit)

#define E_ADC_TADC_EOC                            (0x1000u)
#define E_ADC_TADC_SM                             (0x2000u)
#define E_ADC_TADC_COMP                           (0x4000u)

#define ADC_IRQ_STATUS                            (ADC->IRQ_STATUS.reg)
#define ADC_IRQ_STATUS_bit                        (ADC->IRQ_STATUS.bit)

#define ADC_IRQ_MASK                              (ADC->IRQ_MASK.reg)
#define ADC_IRQ_MASK_bit                          (ADC->IRQ_MASK.bit)

#define ADC_IRQ_VENABLE                           (ADC->IRQ_VENABLE.reg)
#define ADC_IRQ_VENABLE_bit                       (ADC->IRQ_VENABLE.bit)

#define ADC_IRQ_VDISABLE                          (ADC->IRQ_VDISABLE.reg)
#define ADC_IRQ_VDISABLE_bit                      (ADC->IRQ_VDISABLE.bit)

#define ADC_IRQ_VMAX                              (ADC->IRQ_VMAX.reg)
#define ADC_IRQ_VMAX_bit                          (ADC->IRQ_VMAX.bit)

#define ADC_IRQ_VNO                               (ADC->IRQ_VNO.reg)
#define ADC_IRQ_VNO_bit                           (ADC->IRQ_VNO.bit)

// module SUP instance SUP

#define SUP_OPCTRL                                (SUP->OPCTRL.reg)
#define SUP_OPCTRL_bit                            (SUP->OPCTRL.bit)

#define E_SUP_OPCTRL_DS                           (0x0001u)
#define E_SUP_OPCTRL_SB                           (0x0002u)
#define E_SUP_OPCTRL_UV                           (0x0004u)
#define E_SUP_OPCTRL_OT                           (0x0008u)

#define SUP_SUPADJ                                (SUP->SUPADJ.reg)
#define SUP_SUPADJ_bit                            (SUP->SUPADJ.bit)

#define SUP_VER                                   (SUP->VER.reg)
#define SUP_VER_bit                               (SUP->VER.bit)

#define SUP_TEST0                                 (SUP->TEST0.reg)
#define SUP_TEST0_bit                             (SUP->TEST0.bit)

#define SUP_TEST1                                 (SUP->TEST1.reg)
#define SUP_TEST1_bit                             (SUP->TEST1.bit)

#define SUP_TEST2                                 (SUP->TEST2.reg)
#define SUP_TEST2_bit                             (SUP->TEST2.bit)

#define SUP_TEST3                                 (SUP->TEST3.reg)
#define SUP_TEST3_bit                             (SUP->TEST3.bit)

#define SUP_TEST4                                 (SUP->TEST4.reg)
#define SUP_TEST4_bit                             (SUP->TEST4.bit)

#define SUP_TEST5                                 (SUP->TEST5.reg)
#define SUP_TEST5_bit                             (SUP->TEST5.bit)

// module LINSCI instance LINSCI

#define LINSCI_BAUD_RATE                          (LINSCI->BAUD_RATE.reg)
#define LINSCI_BAUD_RATE_bit                      (LINSCI->BAUD_RATE.bit)

#define LINSCI_UART_CONFIG                        (LINSCI->UART_CONFIG.reg)
#define LINSCI_UART_CONFIG_bit                    (LINSCI->UART_CONFIG.bit)

#define E_LINSCI_UART_CONFIG_RE                   (0x0001u)
#define E_LINSCI_UART_CONFIG_TE                   (0x0002u)
#define E_LINSCI_UART_CONFIG_STOP                 (0x0010u)
#define E_LINSCI_UART_CONFIG_MASK_BRK_ERR         (0x0020u)
#define E_LINSCI_UART_CONFIG_CLK_SRC              (0x0040u)
#define E_LINSCI_UART_CONFIG_TXD_MASK             (0x0100u)
#define E_LINSCI_UART_CONFIG_TXD_VAL              (0x0200u)
#define E_LINSCI_UART_CONFIG_RXD_MASK             (0x0400u)
#define E_LINSCI_UART_CONFIG_RXD_VAL              (0x0800u)

#define LINSCI_LIN_CONFIG                         (LINSCI->LIN_CONFIG.reg)
#define LINSCI_LIN_CONFIG_bit                     (LINSCI->LIN_CONFIG.bit)

#define E_LINSCI_LIN_CONFIG_AUTOBAUD              (0x0001u)
#define E_LINSCI_LIN_CONFIG_COLLISION             (0x0002u)
#define E_LINSCI_LIN_CONFIG_BREAK_THD             (0x0004u)
#define E_LINSCI_LIN_CONFIG_HEADER_PROCESSING     (0x0008u)
#define E_LINSCI_LIN_CONFIG_FILTER_PID            (0x0010u)
#define E_LINSCI_LIN_CONFIG_CHKSUM_ENABLE         (0x0020u)
#define E_LINSCI_LIN_CONFIG_CHKSUM_TYPE           (0x0040u)
#define E_LINSCI_LIN_CONFIG_CHKSUM_INSERT         (0x0080u)
#define E_LINSCI_LIN_CONFIG_SUPPRESS_TX_FB        (0x0100u)
#define E_LINSCI_LIN_CONFIG_SYNC_VALIDATION       (0x0200u)
#define E_LINSCI_LIN_CONFIG_DMA_RX_SKIP_LAST      (0x0400u)

#define LINSCI_LIN_CONTROL                        (LINSCI->LIN_CONTROL.reg)
#define LINSCI_LIN_CONTROL_bit                    (LINSCI->LIN_CONTROL.bit)

#define E_LINSCI_LIN_CONTROL_ABORT_RX             (0x0001u)
#define E_LINSCI_LIN_CONTROL_ABORT_TX             (0x0002u)
#define E_LINSCI_LIN_CONTROL_TX_CHKSUM            (0x0004u)
#define E_LINSCI_LIN_CONTROL_RX_SLEEP             (0x0008u)

#define LINSCI_STATUS                             (LINSCI->STATUS.reg)
#define LINSCI_STATUS_bit                         (LINSCI->STATUS.bit)

#define E_LINSCI_STATUS_RX_IDLE                   (0x0001u)
#define E_LINSCI_STATUS_TX_IDLE                   (0x0002u)
#define E_LINSCI_STATUS_RX_FIFO_FULL              (0x0004u)
#define E_LINSCI_STATUS_TX_FIFO_FULL              (0x0008u)
#define E_LINSCI_STATUS_RX_CHKSUM_VALID           (0x0040u)
#define E_LINSCI_STATUS_RX_POST_PID_EDGE          (0x0080u)

#define LINSCI_ERROR                              (LINSCI->ERROR.reg)
#define LINSCI_ERROR_bit                          (LINSCI->ERROR.bit)

#define E_LINSCI_ERROR_FRAME_ERR                  (0x0001u)
#define E_LINSCI_ERROR_PARITY_ERR                 (0x0002u)
#define E_LINSCI_ERROR_SYNC_ERR                   (0x0004u)
#define E_LINSCI_ERROR_SYNC_OV                    (0x0008u)
#define E_LINSCI_ERROR_SYNC_INVALID               (0x0010u)
#define E_LINSCI_ERROR_PID_PARITY_ERR             (0x0020u)
#define E_LINSCI_ERROR_BUS_COLLISION              (0x0040u)
#define E_LINSCI_ERROR_CONCURRENT_BRK             (0x0080u)
#define E_LINSCI_ERROR_TXD_TIMEOUT                (0x0100u)
#define E_LINSCI_ERROR_RX_OVERFLOW                (0x0200u)

#define LINSCI_LIN_CONFIGURATION                  (LINSCI->LIN_CONFIGURATION.reg)
#define LINSCI_LIN_CONFIGURATION_bit              (LINSCI->LIN_CONFIGURATION.bit)

#define E_LINSCI_LIN_CONFIGURATION_CBM            (0x0001u)
#define E_LINSCI_LIN_CONFIGURATION_TXD_TIMEOUT_REG (0x0002u)
#define E_LINSCI_LIN_CONFIGURATION_TIMER          (0x0004u)
#define E_LINSCI_LIN_CONFIGURATION_DMA            (0x0008u)

#define LINSCI_DATA                               (LINSCI->DATA.reg)
#define LINSCI_DATA_bit                           (LINSCI->DATA.bit)

#define E_LINSCI_DATA_SEND_BREAK                  (0x0100u)

#define LINSCI_TBIT2_LENGTH                       (LINSCI->TBIT2_LENGTH.reg)
#define LINSCI_TBIT2_LENGTH_bit                   (LINSCI->TBIT2_LENGTH.bit)

#define LINSCI_LIN_PID                            (LINSCI->LIN_PID.reg)
#define LINSCI_LIN_PID_bit                        (LINSCI->LIN_PID.bit)

#define LINSCI_LIN_CHECKSUM                       (LINSCI->LIN_CHECKSUM.reg)
#define LINSCI_LIN_CHECKSUM_bit                   (LINSCI->LIN_CHECKSUM.bit)

#define E_LINSCI_LIN_CHECKSUM_INITIALIZE          (0x0100u)

#define LINSCI_TIMER                              (LINSCI->TIMER.reg)
#define LINSCI_TIMER_bit                          (LINSCI->TIMER.bit)

#define E_LINSCI_TIMER_ENABLE                     (0x0001u)
#define E_LINSCI_TIMER_CLK_SRC                    (0x0002u)
#define E_LINSCI_TIMER_BREAK_RESTART              (0x0010u)
#define E_LINSCI_TIMER_TXD_TIMEOUT_E              (0x0020u)
#define E_LINSCI_TIMER_CAPTURE_E                  (0x0040u)
#define E_LINSCI_TIMER_PID_CMP_VAL_E              (0x0080u)

#define LINSCI_TIMER_COUNTER                      (LINSCI->TIMER_COUNTER.reg)
#define LINSCI_TIMER_COUNTER_bit                  (LINSCI->TIMER_COUNTER.bit)

#define LINSCI_TIMER_COMPARE                      (LINSCI->TIMER_COMPARE.reg)
#define LINSCI_TIMER_COMPARE_bit                  (LINSCI->TIMER_COMPARE.bit)

#define LINSCI_TIMER_CAPTURE                      (LINSCI->TIMER_CAPTURE.reg)
#define LINSCI_TIMER_CAPTURE_bit                  (LINSCI->TIMER_CAPTURE.bit)

#define LINSCI_DMA_TX_ADDRESS                     (LINSCI->DMA_TX_ADDRESS.reg)
#define LINSCI_DMA_TX_ADDRESS_bit                 (LINSCI->DMA_TX_ADDRESS.bit)

#define LINSCI_DMA_TX_ADDRESS_HIGH                (LINSCI->DMA_TX_ADDRESS_HIGH.reg)
#define LINSCI_DMA_TX_ADDRESS_HIGH_bit            (LINSCI->DMA_TX_ADDRESS_HIGH.bit)

#define LINSCI_DMA_TX_LENGTH                      (LINSCI->DMA_TX_LENGTH.reg)
#define LINSCI_DMA_TX_LENGTH_bit                  (LINSCI->DMA_TX_LENGTH.bit)

#define LINSCI_DMA_RX_ADDRESS                     (LINSCI->DMA_RX_ADDRESS.reg)
#define LINSCI_DMA_RX_ADDRESS_bit                 (LINSCI->DMA_RX_ADDRESS.bit)

#define LINSCI_DMA_RX_ADDRESS_HIGH                (LINSCI->DMA_RX_ADDRESS_HIGH.reg)
#define LINSCI_DMA_RX_ADDRESS_HIGH_bit            (LINSCI->DMA_RX_ADDRESS_HIGH.bit)

#define LINSCI_DMA_RX_LENGTH                      (LINSCI->DMA_RX_LENGTH.reg)
#define LINSCI_DMA_RX_LENGTH_bit                  (LINSCI->DMA_RX_LENGTH.bit)

#define LINSCI_IRQ_STATUS                         (LINSCI->IRQ_STATUS.reg)
#define LINSCI_IRQ_STATUS_bit                     (LINSCI->IRQ_STATUS.bit)

#define E_LINSCI_IRQ_STATUS_RXD_FALLING           (0x0001u)
#define E_LINSCI_IRQ_STATUS_RXD_RISING            (0x0002u)
#define E_LINSCI_IRQ_STATUS_SCI_TIMER_CMP         (0x0004u)
#define E_LINSCI_IRQ_STATUS_SCI_TIMER_OV          (0x0008u)
#define E_LINSCI_IRQ_STATUS_BUS_ERR               (0x0010u)
#define E_LINSCI_IRQ_STATUS_RECEIVER_ERR          (0x0020u)
#define E_LINSCI_IRQ_STATUS_HEADER_ERR            (0x0040u)
#define E_LINSCI_IRQ_STATUS_BREAK_EVT             (0x0080u)
#define E_LINSCI_IRQ_STATUS_SYNC_EVT              (0x0100u)
#define E_LINSCI_IRQ_STATUS_PID_EVT               (0x0200u)
#define E_LINSCI_IRQ_STATUS_RX_FIFO_FULL          (0x0400u)
#define E_LINSCI_IRQ_STATUS_RX_DMA_FINISHED       (0x0800u)
#define E_LINSCI_IRQ_STATUS_TX_FIFO_EMPTY         (0x1000u)
#define E_LINSCI_IRQ_STATUS_TX_DMA_FINISHED       (0x2000u)
#define E_LINSCI_IRQ_STATUS_TX_FINISH_EVT         (0x4000u)
#define E_LINSCI_IRQ_STATUS_TICK_1MS              (0x8000u)

#define LINSCI_IRQ_MASK                           (LINSCI->IRQ_MASK.reg)
#define LINSCI_IRQ_MASK_bit                       (LINSCI->IRQ_MASK.bit)

#define LINSCI_IRQ_VENABLE                        (LINSCI->IRQ_VENABLE.reg)
#define LINSCI_IRQ_VENABLE_bit                    (LINSCI->IRQ_VENABLE.bit)

#define LINSCI_IRQ_VDISABLE                       (LINSCI->IRQ_VDISABLE.reg)
#define LINSCI_IRQ_VDISABLE_bit                   (LINSCI->IRQ_VDISABLE.bit)

#define LINSCI_IRQ_VMAX                           (LINSCI->IRQ_VMAX.reg)
#define LINSCI_IRQ_VMAX_bit                       (LINSCI->IRQ_VMAX.bit)

#define LINSCI_IRQ_VNO                            (LINSCI->IRQ_VNO.reg)
#define LINSCI_IRQ_VNO_bit                        (LINSCI->IRQ_VNO.bit)

// module LIN_CTRL instance LIN_CTRL

#define LIN_CTRL_PHY_CONFIG                       (LIN_CTRL->PHY_CONFIG.reg)
#define LIN_CTRL_PHY_CONFIG_bit                   (LIN_CTRL->PHY_CONFIG.bit)

#define E_LIN_CTRL_PHY_CONFIG_LIN_ON              (0x0001u)
#define E_LIN_CTRL_PHY_CONFIG_LIN_HS              (0x0002u)
#define E_LIN_CTRL_PHY_CONFIG_AA_AMP_ON           (0x0004u)
#define E_LIN_CTRL_PHY_CONFIG_AA_AUTOZERO         (0x0008u)
#define E_LIN_CTRL_PHY_CONFIG_AA_FSM_ENABLE       (0x0010u)
#define E_LIN_CTRL_PHY_CONFIG_AA_ADDRESSED        (0x0020u)
#define E_LIN_CTRL_PHY_CONFIG_AA_OVERSAMPLING     (0x0040u)
#define E_LIN_CTRL_PHY_CONFIG_AA_RESTORE_EOB      (0x0080u)
#define E_LIN_CTRL_PHY_CONFIG_AA_ST5_MID_WAIT     (0x0100u)
#define E_LIN_CTRL_PHY_CONFIG_AA_ST4_SW_WAIT      (0x0200u)
#define E_LIN_CTRL_PHY_CONFIG_AA_DISABLE_ADC_TRIG (0x0400u)

#define LIN_CTRL_LIN_AA_STATUS                    (LIN_CTRL->LIN_AA_STATUS.reg)
#define LIN_CTRL_LIN_AA_STATUS_bit                (LIN_CTRL->LIN_AA_STATUS.bit)

#define E_LIN_CTRL_LIN_AA_STATUS_AA_RESULT        (0x0100u)
#define E_LIN_CTRL_LIN_AA_STATUS_AA_ADC_IDLE      (0x0200u)
#define E_LIN_CTRL_LIN_AA_STATUS_AA_ADC_VALID     (0x0400u)

#define LIN_CTRL_LIN_AA_CONFIG_MODES0             (LIN_CTRL->LIN_AA_CONFIG_MODES0.reg)
#define LIN_CTRL_LIN_AA_CONFIG_MODES0_bit         (LIN_CTRL->LIN_AA_CONFIG_MODES0.bit)

#define E_LIN_CTRL_LIN_AA_CONFIG_MODES0_LIN_PU_E_DEF (0x0001u)
#define E_LIN_CTRL_LIN_AA_CONFIG_MODES0_LIN_IPU_E_DEF (0x0002u)
#define E_LIN_CTRL_LIN_AA_CONFIG_MODES0_LIN_PU_E_OFF (0x0100u)
#define E_LIN_CTRL_LIN_AA_CONFIG_MODES0_LIN_IPU_E_OFF (0x0200u)

#define LIN_CTRL_LIN_AA_CONFIG_MODES1             (LIN_CTRL->LIN_AA_CONFIG_MODES1.reg)
#define LIN_CTRL_LIN_AA_CONFIG_MODES1_bit         (LIN_CTRL->LIN_AA_CONFIG_MODES1.bit)

#define E_LIN_CTRL_LIN_AA_CONFIG_MODES1_LIN_PU_E_PRE (0x0001u)
#define E_LIN_CTRL_LIN_AA_CONFIG_MODES1_LIN_IPU_E_PRE (0x0002u)
#define E_LIN_CTRL_LIN_AA_CONFIG_MODES1_LIN_PU_E_SEL (0x0100u)
#define E_LIN_CTRL_LIN_AA_CONFIG_MODES1_LIN_IPU_E_SEL (0x0200u)

#define LIN_CTRL_LIN_AA_ADC_RESULT                (LIN_CTRL->LIN_AA_ADC_RESULT.reg)
#define LIN_CTRL_LIN_AA_ADC_RESULT_bit            (LIN_CTRL->LIN_AA_ADC_RESULT.bit)

#define LIN_CTRL_LIN_AA_I_DIFF_THD_1              (LIN_CTRL->LIN_AA_I_DIFF_THD_1.reg)
#define LIN_CTRL_LIN_AA_I_DIFF_THD_1_bit          (LIN_CTRL->LIN_AA_I_DIFF_THD_1.bit)

#define LIN_CTRL_LIN_AA_I_DIFF_THD_2              (LIN_CTRL->LIN_AA_I_DIFF_THD_2.reg)
#define LIN_CTRL_LIN_AA_I_DIFF_THD_2_bit          (LIN_CTRL->LIN_AA_I_DIFF_THD_2.bit)

#define LIN_CTRL_LIN_AA_I_SHUNT_1                 (LIN_CTRL->LIN_AA_I_SHUNT_1.reg)
#define LIN_CTRL_LIN_AA_I_SHUNT_1_bit             (LIN_CTRL->LIN_AA_I_SHUNT_1.bit)

#define LIN_CTRL_LIN_AA_CONTROL                   (LIN_CTRL->LIN_AA_CONTROL.reg)
#define LIN_CTRL_LIN_AA_CONTROL_bit               (LIN_CTRL->LIN_AA_CONTROL.bit)

#define E_LIN_CTRL_LIN_AA_CONTROL_AA_ST4_SW_PROCEED (0x0001u)

#define LIN_CTRL_IRQ_STATUS                       (LIN_CTRL->IRQ_STATUS.reg)
#define LIN_CTRL_IRQ_STATUS_bit                   (LIN_CTRL->IRQ_STATUS.bit)

#define E_LIN_CTRL_IRQ_STATUS_AA_FINISHED         (0x0001u)
#define E_LIN_CTRL_IRQ_STATUS_AA_AMP_TIMOUT_EVT   (0x0002u)
#define E_LIN_CTRL_IRQ_STATUS_AA_ADC_TIMOUT_EVT   (0x0004u)
#define E_LIN_CTRL_IRQ_STATUS_AA_ADC_VALID_EVT    (0x0008u)
#define E_LIN_CTRL_IRQ_STATUS_AA_T_BIT_EVT        (0x0010u)

#define LIN_CTRL_IRQ_MASK                         (LIN_CTRL->IRQ_MASK.reg)
#define LIN_CTRL_IRQ_MASK_bit                     (LIN_CTRL->IRQ_MASK.bit)

#define LIN_CTRL_IRQ_VENABLE                      (LIN_CTRL->IRQ_VENABLE.reg)
#define LIN_CTRL_IRQ_VENABLE_bit                  (LIN_CTRL->IRQ_VENABLE.bit)

#define LIN_CTRL_IRQ_VDISABLE                     (LIN_CTRL->IRQ_VDISABLE.reg)
#define LIN_CTRL_IRQ_VDISABLE_bit                 (LIN_CTRL->IRQ_VDISABLE.bit)

#define LIN_CTRL_IRQ_VMAX                         (LIN_CTRL->IRQ_VMAX.reg)
#define LIN_CTRL_IRQ_VMAX_bit                     (LIN_CTRL->IRQ_VMAX.bit)

#define LIN_CTRL_IRQ_VNO                          (LIN_CTRL->IRQ_VNO.reg)
#define LIN_CTRL_IRQ_VNO_bit                      (LIN_CTRL->IRQ_VNO.bit)

#endif

