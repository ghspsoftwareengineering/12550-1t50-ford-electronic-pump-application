/***************************************************************************//**
 * @file			bootloader_interface.h
 *
 * @creator		RPY
 * @created		21.04.2015
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef BOOTLOADER_INTERFACE_H_
#define BOOTLOADER_INTERFACE_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "bootloader_UserConfig.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define BOOTLOADER_INTERFACE_VERSION           0x0101


/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

typedef uint16_t     bldIf_Version_t;

typedef enum 
{
  bldIf_RESTART_APPLICATION       = 0,    /**<< Restart device (software reset), and go to application. */
  bldIf_RESTART_BOOTLOADER        = 1,    /**<< Restart device (software reset), and start bootloader as it would occured due to POR. */
  bldIf_RESTART_PERM_BOOTLOADER   = 2     /**<< Restart device (software reset), and go force bootloader operation, stay in BL until idle timeout or until power off. */
    
} bldIf_RestartCode_t;



/* *****************************************************************************
 * Interface callback function types
 ******************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
typedef bool_t (*bldIf_ReadConfig_t) (bldConf_pConfigData_t pConf);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
typedef bool_t (*bldIf_WriteConfig_t) (bldConf_cpConfigData_t cpConf);


/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
typedef bool_t (*bldIf_ReadDeviceSerial_t) (bldConf_pDeviceSerial_t pSerial);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
typedef bool_t (*bldIf_KillApplicationCookie_t) (void);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *              Function will not return.  
 *              If an unsupported restart code is provided, will perform an restart into application 
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
typedef void (*bldIf_RestartDevice_t) (bldIf_RestartCode_t code);

/***************************************************************************//**
 * bootloader application layer interface function pointer
 ******************************************************************************/
struct bldIf_sBootloaderFunctions
{
    bldIf_Version_t                     InterfaceVersion;
 
    bldIf_ReadConfig_t                  ReadConfig;
    bldIf_WriteConfig_t                 WriteConfig;

    bldIf_ReadDeviceSerial_t            ReadDeviceSerial;
      
    bldIf_KillApplicationCookie_t       KillApplicationCookie;
    
    bldIf_RestartDevice_t               RestartDevice;
};
  
typedef struct bldIf_sBootloaderFunctions    bldIf_BootloaderFunctions_t;
typedef        bldIf_BootloaderFunctions_t*  bldIf_pBootloaderFunctions_t;
typedef const  bldIf_BootloaderFunctions_t*  bldIf_cpBootloaderFunctions_t;


/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

#define BLDIF_ENVIRONMENT_DATA_SIZE    0  // currently no env data needed. 

#ifdef BUILD_ROM
extern const bldIf_BootloaderFunctions_t bldIf_InterfaceFunctions;
#endif

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* BOOTLOADER_INTERFACE_H_ */
