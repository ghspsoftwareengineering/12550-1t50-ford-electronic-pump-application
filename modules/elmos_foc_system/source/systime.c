/**
 * @ingroup HELP
 *
 * @{
 */

/**********************************************************************/
/*! @file systime.c
 * @brief generate a common timebase for the whole application
 *
 * @details
 * This whole thing is actually just a 32bit counter which is incremented on a <br>
 * regular timely basis. Here, it's the ISR (mc_isr_handler() ), hence the timebase is <br>
 * 50 us. Every other module can query the momentary value of the counter and by storing <br>
 * this value and continuously comparing it to the then-actual value, arbitrary delays can be <br>
 * measured or produced. <br>
 * module prefix: systime_
 *
 * @author       RHA
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <systime.h>
#include <defines.h>
#include <intrinsics.h>

static uint32_t systime_cur_time;

INLINE void systime_init( void )
{
  systime_cur_time = 0u;
}

/*! @brief get-function for the 32bit-value of the systime counter */
INLINE uint32_t systime_getU32( void )
{
  __disable_interrupt();
  uint32_t result = systime_cur_time;
  __enable_interrupt();
  return result;
}

/*! @brief get-function for the 16bit-value of the systime counter */
INLINE uint16_t systime_getU16( void )
{
  return (uint16_t) systime_cur_time;
}

/*! @brief increment the systime counter; must be regularly called by the timebase-creating entity (here: mc_isr_handler() ) */
INLINE void systime_incr( void )
{
  systime_cur_time++;
}

/*! @brief blocking wait()-function
 * @details the program execution is AT LEAST blocked for the time indicated by the argument (actually, a little longer)
 */
void systime_delay_blocking( uint16_t delay_us )
{
  uint16_t cnt = delay_us;

  while( 0u != cnt )
  {
    #if ( F_CPU_MHZ == INT64_C( 48 ))
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
      __no_operation();
    #elif  ( F_CPU_MHZ == INT64_C( 24 ))
    #error "F_CPU_MHZ must be 48 in order to run the FOC!"
    #else
    #error "Define F_CPU_MHZ is not set or has an invalid value!"
    #endif

    cnt--;
  }
}

INLINE uint32_t SYSTIME_MS_TO_CPU_CLKS( uint16_t ms )
{
  return (uint32_t) (int64_t) ( F_CPU * (int64_t) ms / INT64_C( 1000 ));
}

INLINE uint32_t SYSTIME_US_TO_CPU_CLKS( uint16_t us )
{
  return (uint32_t) (int64_t) ( F_CPU * (int64_t) us / INT64_C( 1000 ) / INT64_C( 1000 ));
}

INLINE uint32_t SYSTIME_NS_TO_CPU_CLKS( uint16_t ns )
{
  return (uint32_t) (int64_t) ( F_CPU * (int64_t) ns / INT64_C( 1000 ) / INT64_C( 1000 ) / INT64_C( 1000 ));
}

INLINE uint32_t SYSTIME_MS_TO_SYSTIME_TICKS( uint16_t ms )
{
  return (uint32_t) (int64_t) ( SYSTIME_TICKS_PER_SEC * (int64_t) ms / INT64_C( 1000 ));
}

INLINE uint32_t SYSTIME_US_TO_SYSTIME_TICKS( uint16_t us )
{
  return (uint32_t) (int64_t) ( SYSTIME_TICKS_PER_SEC * (int64_t) us / INT64_C( 1000 ) / INT64_C( 1000 ));
}

INLINE uint32_t SYSTIME_TICKS_TO_SYSTIME_MS(uint32_t ticks)
{
    return (uint32_t)((UINT64_C(1000) * (uint64_t)ticks) / (uint64_t)SYSTIME_TICKS_PER_SEC);
}
/* }@
 */
