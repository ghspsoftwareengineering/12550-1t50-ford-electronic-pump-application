
class UnitTestReqs
  # Parser module for pulling out requirement tags from test files.
  module Parser
    # Parses a set of files for requirement tags of a specific format.
    # The format is /* Reqs: REQ-01, REQ-02 */ or /* reqs: REQ-01 */
    #
    # @return [Array<String>] The requirement tags found in the file set
    def self.parse(files_to_parse)
      unit_test_reqs_found = []

      # read each file
      files_to_parse.each do |f|
        IO.readlines(f).each do |l|

          # match the requirement tag
          # comma separated list of tags with the Reqs: heading or Req: or reqs: or req:
          # found inside of a c style comment
          m = /\/\*\s*?[Rr]eq[s]?\:\s*?(.*)\*\//.match(l)
          unless m.nil?
            unless m[1].nil?
              unit_test_reqs_found << (m[1].gsub(/\s/, '')).split(',')
            end
          end
        end
      end
      unit_test_reqs_found.flatten!
      unit_test_reqs_found.uniq!
      unit_test_reqs_found
    end
  end
end
