include $(GHSP_MAKE_DIR)/pre_module.mak
####################################################
# BEGIN: User configurable portion of the makefile #
####################################################

# Module only supports the production build
#ifeq ($(_build_),production)
# Module only supports the IAR RH850 Toolchain
#ifeq ($($(_build_)_TOOLCHAIN),IAR-RH850)

#
# Create a whitespace separated list of source directories for this module
# All source files in these directories will be compiled and (probably) linked 
# into any resulting binary.
#
# The variable _module_path_ is available for use in this definition. This variable
# contains the directory where this module Makefile is located.
#
# Example: _src_dirs_ := $(_module_path_)/project_module_a_imp/_src
# Where the _src directory contains all of the source files for the implementation of 
# the module.
#
_src_dirs_ := $(_module_path_)/time_utils_imp/src

#
# Setup the creation of a useful artifact of a module.
#
# A module must produce either a binary or a library, but not both.
# All source files in the _src_dirs_ will be used to create either of these
# artifacts.
#
# Note: If a library is specified, it will take precedence over any binary specified

#
# If a library will be produced, provide a name for the library.
# This library name should be related to the module name.
#
_library_name_ := time_utils

####################################################
# END: user configurable section of the makefile   #
####################################################

include $(GHSP_MAKE_DIR)/post_module.mak

#endif #ifeq ($($(_build_)_TOOLCHAIN),IAR-RH850)
#endif #ifeq ($(_build_),production)
