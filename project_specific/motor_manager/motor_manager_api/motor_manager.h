#ifndef MOTOR_MANAGER_H
#define MOTOR_MANAGER_H

/**
 *  @file motor_manager.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup motor_manager_api Motor Manager Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/

/**
 * Initializes the Motor Manager module.
 *
 * @return true - Motor Manager was initialized
 *         false - Motor Manager failed to initialize
 */
bool_t MMGR_Init(void);

/**
 * Core functionality of the Motor Manager. Function must be called regularly.
 */
void MMGR_Task(void);

/**
 * Provides the Target Speed of the motor
 *
 * @return - Target Speed in RPM
 */
uint16_t MMGR_Get_Target_Speed(void);

/** @} doxygen end group */

#endif /* MOTOR_MANAGER_H */
