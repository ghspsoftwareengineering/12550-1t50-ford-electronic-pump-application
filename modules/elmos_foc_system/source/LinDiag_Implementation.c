/***************************************************************************//**
 * @file		LinDiag_Implementation.c
 *
 * @creator		sbai
 * @created		12.02.2015
 *
 * @brief  		TODO: Short description of this module
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#if LINDIAG_SUP_DATASTG == 1
#include "LinDataStg_Implementation.h"
#include "LinDataStg_DataValueIDs.h"
#endif
#include "Lin_Basictypes.h"
#include "el_helper.h"
#include "LinTrans_Implementation.h"
#include "LinDiag_Implementation.h"
//#include <intrinsics.h>

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#if LINDRVIMP_AQR_MOD_SZE == 1
#define DIAG_LAYER_CODE _Pragma("location=\"DIAG_LAYER_CODE\"")
#define DIAG_LAYER_DATA _Pragma("location=\"DIAG_LAYER_DATA\"")
#else
#define DIAG_LAYER_CODE
#define DIAG_LAYER_DATA
#endif

#define LOC_MAX_RBI_DATA_LEN 5u

#define LOC_SUB_FUNCTION_MSK           0x7Fu
#define LOC_SUPPRESS_POS_RESP_BIT_MSK  0x80u

#define LOC_TIMER_OVERFLOW_BIT  0x80000000
#define LOC_TIMER_MAX_VAL       0x7FFFFFFF

#if (LINDIAG_SUP_ASSIGN_NAD == 0) && (LINDIAG_SUP_ASSIGN_FRAME_IDENTIFIER == 0) && (LINDIAG_SUP_READ_BY_ID == 0) && \
    (LINDIAG_SUP_CONDITIONAL_CHANGE_NAD == 0) && (LINDIAG_SUP_SNPD == 0) && (LINDIAG_SUP_SAVE_CONFIGURATION == 0) && \
    (LINDIAG_SUP_ASSIGM_FRAME_ID_RANGE == 0)
   #error "No diagnostic service activated! No need for the LIN DIAG LAYER!"
#endif

#if (LINDIAG_SUP_ASSIGN_FRAME_IDENTIFIER == 1) || (LINDIAG_SUP_READ_BY_ID == 1)
    #define LOC_USE_APPENDDATATOMSGBUFFFER_FUN
#endif

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
struct loc_sTimer
{
  Lin_uint32_t                  Start;
  LinDiagIf_Timeout_t           Timeout;
  LinDiagIf_pGenericCbCtxData_t CbCtxData;
};

typedef struct loc_sTimer loc_sTimer_t;

/***************************************************************************//**
 *
 ******************************************************************************/
struct loc_sDiagEnvironmentData
{
  LinTransIf_NAD_t                   InitialNAD;
  LinTransIf_NAD_t                   NAD;
  LinDiagIf_sProductIdentification_t ProductIdentification;
  LinDiagIf_SerialNumber_t           SerialNumber;
  Lin_Bool_t                         InvalidReadByIDAnswered;
  LinTransIf_cpInterfaceFunctions_t  TransIfFuns;
  LinTransIf_pGenericEnvData_t       TransEnvData;
  LinLookupIf_sThis_t                LookupThisPtr;
  LinBusIf_sThis_t                   BusThisPtr;
#if LINDIAG_SUP_DATASTG == 1
  LinDataStgIf_sThis_t               DataStgThisPtr;
#endif
  LinDiagIf_cpCallbackFunctions_t    Callbacks;
  LinDiagIf_pGenericCbCtxData_t      CallbackCtxData;
  LinDiagIf_pRbiLookupEntry_t        RbiTbl[LINDIAG_MAX_RBI_TBL_CNT];
  LinDiagIf_Length_t                 UserAppendedDataLen;
#if LINDIAG_MAX_SUPPORTED_TIMER > 0
  loc_sTimer_t                       Timer[LINDIAG_MAX_SUPPORTED_TIMER];
#endif
#if LINDIAG_SUP_SNPD == 1
  LinSNPDIf_sThis_t                  LinSNPDIfThisPointer;
  Lin_Bool_t                         ImmediateNADUpdate;
  LinTransIf_NAD_t                   SNPDNewNAD;
  Lin_Bool_t                         SNPDNewNADValid;
  Lin_Bool_t                         SNPDStarted;
  Lin_Bool_t                         SNPDAddressed;
#endif    
};

typedef struct loc_sDiagEnvironmentData    loc_sDiagEnvironmentData_t;
typedef        loc_sDiagEnvironmentData_t* loc_pDiagEnvironmentData_t;

enum loc_eReqLenCompType
{
  loc_RLCT_GreaterThenOrEqual,
  loc_RLCT_Equal,
  loc_RLCT_LessThenOrEqual
};

typedef enum loc_eReqLenCompType loc_eReqLenCompType_t;

struct loc_sDiagServiceControlData
{
    loc_pDiagEnvironmentData_t DiagEnvData;
    LinTransIf_eSIDReaction_t  SIDReaction;
    LinTransIf_SID_t           RequestedSID;
    LinDiagIf_Length_t         RequestLength;
    loc_eReqLenCompType_t      ReqLenCompType;
    Lin_Bool_t                 SupportSubFunction;
    Lin_uint8_t                SubFunction;
    Lin_Bool_t                 SuppressPosRspMsg;
    Lin_Bool_t                 SendNegativeResponse;
    LinDiagIf_NRC_t            NegativeResponseCode;
};

typedef struct loc_sDiagServiceControlData    loc_sDiagServiceControlData_t;
typedef        loc_sDiagServiceControlData_t* loc_pDiagServiceControlData_t;

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static void loc_TransErrorCallback(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                                   LinTransIf_Error_t           error,               LinTransIf_SID_t                  SID,
                                                   LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);
  
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static void loc_TransSIDProcessedCallback(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                                          LinTransIf_SID_t             sid,                 LinTransIf_ErrorFlag_t            errorFlag,
                                                          LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static LinTransIf_eCheckNADResult_t loc_TransCheckNADCallback(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                                                              LinTransIf_NAD_t             nad,                 LinTransIf_SID_t             sid,
                                                                              LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static void loc_TransGoToSleepCallback(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                                       LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallGoToSleepCallback(loc_pDiagEnvironmentData_t diagEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallErrorCallback(loc_pDiagEnvironmentData_t diagEnvData, LinTransIf_SID_t SID, LinDiagIf_Error_t error);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallTimeoutCallback(loc_pDiagEnvironmentData_t diagEnvData, Lin_uint8_t timerNum, LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData);

#if (LINDIAG_SUP_SNPD == 1) && (LINDIAG_SUP_DATASTG == 0)
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallStoreNADCallback(loc_pDiagEnvironmentData_t diagEnvData, LinDiagIf_NAD_t nad, LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData);
#endif

#if LINDIAG_SUP_READ_BY_ID == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static LinDiagIf_pRbiLookupEntry_t loc_LookupRbi(loc_pDiagEnvironmentData_t diagEnvData, LinDiagIf_RbIdentifier_t rbIdentifier);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_CheckRbiTbl(LinDiagIf_pRbiLookupEntry_t rbiTbl);
#endif

#if LINDIAG_SUP_DATASTG == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinDataStgIf_eErrorCodes_t loc_DataStgGetValue(loc_pDiagEnvironmentData_t diagEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                                             LinDataStgIf_pData_t       buffer,      LinDataStgIf_Length_t      bufferLen);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinDataStgIf_eErrorCodes_t loc_DataStgSetValue(loc_pDiagEnvironmentData_t diagEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                                             LinDataStgIf_pData_t       buffer,      LinDataStgIf_Length_t      bufferLen);
#endif

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_AppendDataToMsgBuffer(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinTransIf_pData_t data, 
																																	 Lin_BufLength_t             dataLen);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_PrepareNegativeResponse(loc_pDiagServiceControlData_t diagSerCtrlData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_PreparePositiveResponse(loc_pDiagServiceControlData_t diagSerCtrlData, LinDiagIf_pData_t  pData,
                                                                     LinDiagIf_Length_t            complDataLen,    LinDiagIf_Length_t appendDataLen);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_InitSerCtrlData(loc_pDiagServiceControlData_t diagSerCtrlData, LinTransIf_pGenericCbCtxData_t genericTransCbCtxData,
                                                             LinTransIf_SID_t              requestedSID,    LinDiagIf_Length_t             requestedLen,
                                                             loc_eReqLenCompType_t         reqLenCompType,  Lin_Bool_t                     supportSubFunction);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_ValidateAndExtractServiceData(loc_pDiagServiceControlData_t diagSerCtrlData, LinDiagIf_pData_t pData,
                                                                           LinDiagIf_Length_t            dataLen);

#if LINDIAG_SUP_ASSIGN_NAD == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_AssignNAD(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                      LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                      LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                      LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

#endif

#if LINDIAG_SUP_ASSIGN_FRAME_IDENTIFIER == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_AssignFrameIdentifier(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                                  LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                                  LinTransIf_Length_t          dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                                  LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);
#endif

#if LINDIAG_SUP_READ_BY_ID == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_ReadByIdentifier(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                             LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                             LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                             LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static Lin_Bool_t loc_RbiCbProdIdent(LinDiagIf_pGenericEnvData_t   genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                                                     LinDiagIf_RbIdentifier_t      rbIdentifier,       LinDiagIf_AppendDataToMsgBuffer_t appendDataToMsgBufFun,
                                                     LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static Lin_Bool_t loc_RbiCbSerialNum(LinDiagIf_pGenericEnvData_t   genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                                                     LinDiagIf_RbIdentifier_t      rbIdentifier,       LinDiagIf_AppendDataToMsgBuffer_t appendDataToMsgBufFun,
                                                     LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData);
#endif

#if LINDIAG_SUP_CONDITIONAL_CHANGE_NAD == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_ConditionalChangeNAD(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                                 LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                                 LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                                 LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);
#endif

#if LINDIAG_SUP_DATA_DUMP == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_DataDump(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                     LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                     LinTransIf_Length_t          dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                     LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);
#endif

#if LINDIAG_SUP_SAVE_CONFIGURATION == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_SaveConfiguration(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                              LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                              LinTransIf_Length_t          dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                              LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);
#endif

#if LINDIAG_SUP_ASSIGM_FRAME_ID_RANGE == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_AssignFrameIdentifierRange(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                                       LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                                       LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                                       LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);
#endif

#if LINDIAG_SUP_SNPD == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_CallStartMeasurement( LinSNPDIf_pGenericEnvData_t genericSNPDEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallEndMeasurement ( LinSNPDIf_pGenericEnvData_t genericSNPDEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
//DIAG_LAYER_CODE static inline void loc_CallNextMeasurement (LinSNPDIf_pGenericEnvData_t genericSNPDEnvData, Lin_Bool_t addressed);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinSNPDIf_eState_t loc_CallSNPDGetState (LinSNPDIf_pGenericEnvData_t   genericLinSNPDIfEnvData);

#if 0
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_uint16_t loc_CallGetMeasurementCount (LinSNPDIf_pGenericEnvData_t   genericSNPDEnvData);
#endif

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_AutoAddrRequest(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                            LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                            LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                            LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);
#endif

#if ELMOS_BLTEST == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_ElmosBlTest(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_pData_t             data,
                                                                        LinTransIf_Length_t          dataLen,             LinTransIf_pGenericCbCtxData_t genericTransCbCtxData);
#endif /* ELMOS_BLTEST == 1 */

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINDIAGIMP_EXT_IFFUN_STRCT_ACCESS == 1
DIAG_LAYER_DATA const LinDiagIf_sInterfaceFunctions_t LinDiagImp_InterfaceFunctions =
#else
DIAG_LAYER_DATA static const LinDiagIf_sInterfaceFunctions_t LinDiagImp_InterfaceFunctions =
#endif
{
  .InterfaceVersion  = LINDIAG_INTERFACE_MODULE_API_VERSION,
  .Initialization    = &LinDiagImp_Initialization,
  .Task              = &LinDiagImp_Task,
  .GetSubInterface   = &LinDiagImp_GetSubInterface,
  .GetNAD            = &LinDiagImp_GetNad,
  .SetNAD            = &LinDiagImp_SetNad,
  .AddRbiTable       = &LinDiagImp_AddRbiTable,
  .RmvRbiTable       = &LinDiagImp_RmvRbiTable,
};

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/
#if ELMOS_BLTEST == 1
DIAG_LAYER_DATA static const LinTransIf_sSIDDescription_t loc_DiagSidLst[] =
{
    {.SID = 0xBB, .CallbackFnc = &loc_ElmosBlTest, .CallbackCtxType = LinTransIf_SIDDescLstCbCtxType_PER_LST, .CallbackCtx = LIN_NULL},
    {.CallbackFnc = LIN_NULL}
};
#else
DIAG_LAYER_DATA static const LinTransIf_sSIDDescription_t loc_DiagSidLst[] =
{
#if LINDIAG_SUP_ASSIGN_NAD == 1
    {.SID = (LinDiagIf_SID_t) LinDiagIf_SID_AssignNAD,            .CallbackFnc = &loc_AssignNAD,                      .CallbackCtxType = LinTransIf_SIDDescLstCbCtxType_PER_LST, .CallbackCtx = LIN_NULL},
#endif
#if LINDIAG_SUP_ASSIGN_FRAME_IDENTIFIER == 1
    {.SID = (LinDiagIf_SID_t) LinDiagIf_SID_AssignFrameID,        .CallbackFnc = &loc_AssignFrameIdentifier,         .CallbackCtxType = LinTransIf_SIDDescLstCbCtxType_PER_LST, .CallbackCtx = LIN_NULL},
#endif
#if LINDIAG_SUP_READ_BY_ID == 1
    {.SID = (LinDiagIf_SID_t) LinDiagIf_SID_ReadByIdentifier,     .CallbackFnc = &loc_ReadByIdentifier,               .CallbackCtxType = LinTransIf_SIDDescLstCbCtxType_PER_LST, .CallbackCtx = LIN_NULL},
#endif
#if LINDIAG_SUP_CONDITIONAL_CHANGE_NAD == 1
    {.SID = (LinDiagIf_SID_t) LinDiagIf_SID_ConditionalChangeNAD, .CallbackFnc = &loc_ConditionalChangeNAD,           .CallbackCtxType = LinTransIf_SIDDescLstCbCtxType_PER_LST, .CallbackCtx = LIN_NULL},
#endif
#if LINDIAG_SUP_SNPD == 1
    {.SID = (LinDiagIf_SID_t) LinDiagIf_SID_SNPD,                 .CallbackFnc = &loc_AutoAddrRequest,                .CallbackCtxType = LinTransIf_SIDDescLstCbCtxType_PER_LST, .CallbackCtx = LIN_NULL},
#endif
#if LINDIAG_SUP_SAVE_CONFIGURATION == 1
    {.SID = (LinDiagIf_SID_t) LinDiagIf_SID_SaveConfiguration,    .CallbackFnc = &loc_SaveConfiguration,             .CallbackCtxType = LinTransIf_SIDDescLstCbCtxType_PER_LST, .CallbackCtx = LIN_NULL},
#endif
#if LINDIAG_SUP_ASSIGM_FRAME_ID_RANGE
    {.SID = (LinDiagIf_SID_t) LinDiagIf_SID_AssignFrameIDRange,   .CallbackFnc = &loc_AssignFrameIdentifierRange,     .CallbackCtxType = LinTransIf_SIDDescLstCbCtxType_PER_LST, .CallbackCtx = LIN_NULL},
#endif
    {.CallbackFnc =  LIN_NULL}
};
#  endif

DIAG_LAYER_DATA static const LinTransIf_sCallbackFunctions_t loc_LinTransCallbacks =
{
  .CallbackVersion      = LINTRANS_INTERFACE_MODULE_API_VERSION,

  .Error                = &loc_TransErrorCallback,
  .SIDProcessed         = &loc_TransSIDProcessedCallback,
  .CheckNAD             = &loc_TransCheckNADCallback,
  .GoToSleep            = &loc_TransGoToSleepCallback
};

#if LINDIAG_SUP_READ_BY_ID == 1
DIAG_LAYER_DATA static const LinDiagIf_sRbiLookupEntry_t loc_RbiTbl[3] =
{
  {.FirstRbIdentifier = LINDIAGIF_RB_IDENTIFIER_PROD_IDENT, .LastRbIdentifier = LINDIAGIF_RB_IDENTIFIER_PROD_IDENT, .Callback = &loc_RbiCbProdIdent, .LengthType = LinDiagIf_RbiLenType_Value, .CbCtxData = LIN_NULL, .Length = (LinDiagIf_RbiLenght_t) LINDIAGIF_RB_IDENTIFIER_PROD_IDENT_LEN},
  {.FirstRbIdentifier = LINDIAGIF_RB_IDENTIFIER_SERIAL_NUM, .LastRbIdentifier = LINDIAGIF_RB_IDENTIFIER_SERIAL_NUM, .Callback = &loc_RbiCbSerialNum, .LengthType = LinDiagIf_RbiLenType_Value, .CbCtxData = LIN_NULL, .Length = (LinDiagIf_RbiLenght_t) LINDIAGIF_RB_IDENTIFIER_SERIAL_NUM_LEN},
  {.FirstRbIdentifier = 0u, .LastRbIdentifier = 0u, .Callback = LIN_NULL, .CbCtxData = LIN_NULL, .Length = 0u},
};
#endif

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE Lin_Bool_t LinDiagImp_Initialization(LinDiagIf_pGenericEnvData_t     genericDiagEnvData,      LinDiagIf_EnvDataSze_t         diagEnvDataSze,
                                                     LinDiagIf_cpCallbackFunctions_t diagCbFuns,              LinDiagIf_pGenericCbCtxData_t  genericDiagCbCtxData,
                                                     Lin_Bool_t                      invalidReadByIDAnswered, LinDiagIf_pGenericImpCfgData_t genericDiagImpCfgData)
{
  Lin_Bool_t returnValue = LIN_FALSE;
  
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;
  LinDiagImp_pCfgData_t diagImpCfgData = (LinDiagImp_pCfgData_t) genericDiagImpCfgData;

  if((genericDiagEnvData != LIN_NULL) &&
     (diagEnvDataSze >= sizeof(loc_sDiagEnvironmentData_t)) &&
     (diagCbFuns != LIN_NULL) &&
     (genericDiagImpCfgData != LIN_NULL) &&
     (diagCbFuns->CallbackVersion == LINDIAG_INTERFACE_MODULE_API_VERSION) &&
#if LINDIAG_SUP_SNPD == 1
     (diagImpCfgData->LinSNPDIfThisPointer.IfFunsTbl != LIN_NULL) &&
     (diagImpCfgData->LinSNPDIfThisPointer.IfFunsTbl->InterfaceVersion == LINSNPD_INTERFACE_MODULE_API_VERSION) &&
#endif
#if LINDIAG_SUP_DATASTG == 1
     (diagImpCfgData->LinDataStgIfThisPointer.IfFunsTbl != LIN_NULL) &&
     (diagImpCfgData->LinDataStgIfThisPointer.IfFunsTbl->InterfaceVersion == LINDATASTG_INTERFACE_MODULE_API_VERSION) &&
#endif
     (diagImpCfgData->LinBusIfThisPointer.IfFunsTbl != LIN_NULL) &&
     (diagImpCfgData->LinLookupIfThisPointer.IfFunsTbl != LIN_NULL) &&
     (diagImpCfgData->LinTransIfInitParam.IfFunsTbl != LIN_NULL) &&
     (diagImpCfgData->LinBusIfThisPointer.IfFunsTbl->InterfaceVersion == LINBUS_INTERFACE_MODULE_API_VERSION) &&
     (diagImpCfgData->LinLookupIfThisPointer.IfFunsTbl->InterfaceVersion == LINLOOKUP_INTERFACE_MODULE_API_VERSION) &&
     (diagImpCfgData->LinTransIfInitParam.IfFunsTbl->InterfaceVersion == LINTRANS_INTERFACE_MODULE_API_VERSION))
  {
#if LINDIAG_SUP_DATASTG == 1
    LinTransIf_NAD_t initialNad;
#endif
    Lin_uint8_t i;

    /* Initialize lookup environment data */
    for(i = 0; i < sizeof(loc_sDiagEnvironmentData_t); i++)
    {
      ((Lin_uint8_t*) genericDiagEnvData)[i] = 0;
    }

    diagEnvData->TransIfFuns = diagImpCfgData->LinTransIfInitParam.IfFunsTbl;
    diagEnvData->TransEnvData = diagImpCfgData->LinTransIfInitParam.EnvData;

    diagEnvData->BusThisPtr = diagImpCfgData->LinBusIfThisPointer;
    diagEnvData->LookupThisPtr = diagImpCfgData->LinLookupIfThisPointer;
#if LINDIAG_SUP_DATASTG == 1
    diagEnvData->DataStgThisPtr = diagImpCfgData->LinDataStgIfThisPointer;
#endif

    diagEnvData->Callbacks = diagCbFuns;

    diagEnvData->InvalidReadByIDAnswered = invalidReadByIDAnswered;

#if LINDIAG_SUP_SNPD == 1

    diagEnvData->LinSNPDIfThisPointer = diagImpCfgData->LinSNPDIfThisPointer;
    diagEnvData->ImmediateNADUpdate =  diagImpCfgData->ImmediateNADUpdate;
      
    diagEnvData->SNPDNewNAD = 0;
    diagEnvData->SNPDNewNADValid = LIN_FALSE;
    diagEnvData->SNPDStarted = LIN_FALSE;
    diagEnvData->SNPDAddressed = LIN_FALSE;

#endif      

#if LINDIAG_SUP_DATASTG == 1
    /* Get Product Identification */
    if(loc_DataStgGetValue(diagEnvData, LinDataStgIf_DVID_ProductIdentification,
                           (LinDataStgIf_pData_t) &(diagEnvData->ProductIdentification),
                           sizeof(diagEnvData->ProductIdentification)) == LinDataStgIf_ERR_NO_ERROR)
    {
      returnValue = LIN_TRUE;
    }
    else
    {
      #if LINDIAG_INIT_DEFAULT_VALUES == 1
      LinDiagIf_sProductIdentification_t defaultProductIdentification;

      defaultProductIdentification.SupplierID = LINDIAG_DEFAULT_SUPPLIERID;
      defaultProductIdentification.FunctionID = LINDIAG_DEFAULT_FUNCTIONID;
      defaultProductIdentification.VariantID = LINDIAG_DEFAULT_VARIANTID;

      if(loc_DataStgSetValue(diagEnvData, LinDataStgIf_DVID_ProductIdentification, (LinDataStgIf_pData_t) &defaultProductIdentification, sizeof(defaultProductIdentification)) == LinDataStgIf_ERR_NO_ERROR)
      {
        diagEnvData->ProductIdentification.SupplierID = LINDIAG_DEFAULT_SUPPLIERID;
        diagEnvData->ProductIdentification.FunctionID = LINDIAG_DEFAULT_FUNCTIONID;
        diagEnvData->ProductIdentification.VariantID  = LINDIAG_DEFAULT_VARIANTID;
        returnValue = LIN_TRUE;
      }
      else
      {
        returnValue = LIN_FALSE;
      }
      #endif // LINDIAG_INIT_DEFAULT_VALUES == 1
    }

    /* Get Serial Number */
    if((loc_DataStgGetValue(diagEnvData, LinDataStgIf_DVID_SerialNumber,
                            (LinDataStgIf_pData_t) &(diagEnvData->SerialNumber),
                            sizeof(diagEnvData->SerialNumber)) == LinDataStgIf_ERR_NO_ERROR) &&
                            (returnValue == LIN_TRUE))
    {
      returnValue = LIN_TRUE;
    }
    else
    {
      #if LINDIAG_INIT_DEFAULT_VALUES == 1
      LinDiagIf_SerialNumber_t defaultSerialNumber = LINDIAG_DEFAULT_SERIAL_NUMBER;

      if(loc_DataStgSetValue(diagEnvData, LinDataStgIf_DVID_SerialNumber, (LinDataStgIf_pData_t) &defaultSerialNumber, sizeof(defaultSerialNumber)) == LinDataStgIf_ERR_NO_ERROR)
      {
        diagEnvData->SerialNumber = LINDIAG_DEFAULT_SERIAL_NUMBER;
        returnValue = LIN_TRUE;
      }
      else
      {
        returnValue = LIN_FALSE;
      }
      #endif // LINDIAG_INIT_DEFAULT_VALUES == 1
    }

    /* Get initial NAD. */
    if((loc_DataStgGetValue(diagEnvData, LinDataStgIf_DVID_NAD,
                            (LinDataStgIf_pData_t) &(initialNad),
                            sizeof(diagEnvData->NAD)) == LinDataStgIf_ERR_NO_ERROR) &&
                            (returnValue == LIN_TRUE))
    {
      /* Set NADs */
      if(initialNad != LINTRANSIF_FUNCTIONAL_NAD &&
         initialNad != LINTRANSIF_GOTOSLEEP_NAD &&
         initialNad != LINTRANSIF_BROADCAST_NAD)
      {
        diagEnvData->InitialNAD = initialNad;
        diagEnvData->NAD        = initialNad;
        returnValue = LIN_TRUE;
      }
      else
      {
        /* ERROR: Invalid NAD */
        loc_CallErrorCallback(diagEnvData, 0xFF, LinDiagIf_ERR_INVALID_NAD);
      }
    }
    else
    {
      #if LINDIAG_INIT_DEFAULT_VALUES == 1
      LinTransIf_NAD_t defaultNad = LINDIAG_DEFAULT_NAD;

      if(defaultNad != LINTRANSIF_FUNCTIONAL_NAD &&
         defaultNad != LINTRANSIF_GOTOSLEEP_NAD &&
         defaultNad != LINTRANSIF_BROADCAST_NAD)
      {
        if(loc_DataStgSetValue(diagEnvData, LinDataStgIf_DVID_NAD, (LinDataStgIf_pData_t) &defaultNad, sizeof(defaultNad)) == LinDataStgIf_ERR_NO_ERROR)
        {
          diagEnvData->InitialNAD = defaultNad;
          diagEnvData->NAD        = defaultNad;
          returnValue = LIN_TRUE;
        }
        else
        {
          returnValue = LIN_FALSE;
        }
      }
      else
      {
        /* ERROR: Invalid NAD */
        loc_CallErrorCallback(diagEnvData, 0xFF, LinDiagIf_ERR_INVALID_NAD);
      }
      #endif // LINDIAG_INIT_DEFAULT_VALUES == 1
    }
#else
    /* Set Serial Number */
    diagEnvData->SerialNumber = diagImpCfgData->InitialSerialNumber;

    /* Set Product Identification */
    diagEnvData->ProductIdentification = diagImpCfgData->InitialProdIdent;

    /* Set NADs */
    if(diagImpCfgData->InitialNad != LINTRANSIF_FUNCTIONAL_NAD &&
       diagImpCfgData->InitialNad != LINTRANSIF_GOTOSLEEP_NAD &&
       diagImpCfgData->InitialNad != LINTRANSIF_BROADCAST_NAD)
    {
      diagEnvData->InitialNAD = diagImpCfgData->InitialNad;
      diagEnvData->NAD        = diagImpCfgData->InitialNad;
      returnValue = LIN_TRUE;
    }
    else
    {
      /* ERROR: Invalid NAD */
      loc_CallErrorCallback(diagEnvData, 0xFF, LinDiagIf_ERR_INVALID_NAD);
    }
#endif

    if(returnValue == LIN_TRUE)
    {
      returnValue = diagEnvData->TransIfFuns->Initialization(diagImpCfgData->LinTransIfInitParam.EnvData,          diagImpCfgData->LinTransIfInitParam.EnvDataLen,
                                                             &loc_LinTransCallbacks,                               diagEnvData,
                                                             diagImpCfgData->LinTransIfInitParam.ProtoFrmDescInfo, diagImpCfgData->LinTransIfInitParam.NasTimeout,
                                                             diagImpCfgData->LinTransIfInitParam.NcrTimeout,       diagImpCfgData->LinTransIfInitParam.ImpCfgData);
    }

    if(returnValue == LIN_TRUE)
    {
      returnValue = diagEnvData->TransIfFuns->AddSIDDescriptionList(diagEnvData->TransEnvData, (LinTransIf_pSIDDescription_t) loc_DiagSidLst, (LinTransIf_pGenericCbCtxData_t) genericDiagEnvData);

      #if LINDIAG_SUP_READ_BY_ID == 1
      if(returnValue == LIN_TRUE)
      {
        diagEnvData->UserAppendedDataLen = 0;
        returnValue = LinDiagImp_AddRbiTable(genericDiagEnvData, (LinDiagIf_pRbiLookupEntry_t) loc_RbiTbl);
      }
      #endif
    }
    else{}
  }
  else{}

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE void LinDiagImp_Task(LinDiagIf_pGenericEnvData_t genericDiagEnvData)
{
  if(genericDiagEnvData != LIN_NULL)
  {
    loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;
    Lin_uint8_t i;

    #if LINDIAG_MAX_SUPPORTED_TIMER > 0
    for(i = 0; i < LINDIAG_MAX_SUPPORTED_TIMER; i++)
    {
      if(diagEnvData->Timer[i].Start != 0 && diagEnvData->Timer[i].Timeout != 0)
      {
        istate_t istate = __get_interrupt_state();
        __disable_interrupt();
        Lin_Bool_t callCb = LIN_FALSE;
        LinBusIf_Tick_t tempTimerVal  = diagEnvData->BusThisPtr.IfFunsTbl->GetMilliseconds(diagEnvData->BusThisPtr.EnvData);
        Lin_uint32_t    savedTimerVal = diagEnvData->Timer[i].Start;
        Lin_uint32_t    tempCompVal   = tempTimerVal - savedTimerVal;
  
        if((savedTimerVal & LOC_TIMER_OVERFLOW_BIT) != (tempTimerVal & LOC_TIMER_OVERFLOW_BIT))
        {
          tempCompVal = tempTimerVal + (LOC_TIMER_MAX_VAL - savedTimerVal);
        }
        else{}
  
        if(tempCompVal >= diagEnvData->Timer[i].Timeout)
        {
          diagEnvData->Timer[i].Start = 0;
          diagEnvData->Timer[i].Timeout = 0;
          callCb = LIN_TRUE;
        }
        else{}
        
        __set_interrupt_state(istate);
        
        if(callCb == LIN_TRUE)
        {
          loc_CallTimeoutCallback(diagEnvData, i, diagEnvData->Timer[i].CbCtxData);
        }
        else{}
      }
      else{}
    }
    #endif
  }
  else{}
}

/***************************************************************************//**
 * TODO: Document exactly the meaning of return value LIN_TRUE and LIN_FALSE.
 ******************************************************************************/
DIAG_LAYER_CODE Lin_Bool_t LinDiagImp_GetSubInterface(LinDiagIf_pGenericEnvData_t genericDiagEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr)
{
  Lin_Bool_t retVal = LIN_FALSE;

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE LinTransIf_NAD_t LinDiagImp_GetNad(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_eNADType_t nadtype)
{
  LinTransIf_NAD_t nad;

  if(genericDiagEnvData != LIN_NULL)
  {
    loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

    if(nadtype == LinDiagIf_NADType_INITIAL)
    {
      nad = diagEnvData->InitialNAD;
    }
    else
    {
      nad = diagEnvData->NAD;
    }
  }

  return(nad);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static void LinDiagImp_SetNad(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinTransIf_NAD_t nad)
{
  if(genericDiagEnvData != LIN_NULL)
  {
    loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

    /* Set NADs */
    if(nad != LINTRANSIF_FUNCTIONAL_NAD &&
       nad != LINTRANSIF_GOTOSLEEP_NAD &&
       nad != LINTRANSIF_BROADCAST_NAD)
    {
      diagEnvData->NAD = nad;
    }
    else
    {
      /* ERROR: Invalid NAD */
      loc_CallErrorCallback(diagEnvData, 0xFF, LinDiagIf_ERR_INVALID_NAD);
    }
  }
  else{}
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE Lin_Bool_t LinDiagImp_AddRbiTable(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_pRbiLookupEntry_t rbiTbl)
{
  Lin_Bool_t retVal = LIN_FALSE;

  #if LINDIAG_SUP_READ_BY_ID == 1
  if(genericDiagEnvData != LIN_NULL)
  {
    loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;
    Lin_uint8_t i;

    if(loc_CheckRbiTbl(rbiTbl) == LIN_TRUE)
    {
      for(i = 0; i < LINDIAG_MAX_RBI_TBL_CNT; i++)
      {
        if(diagEnvData->RbiTbl[i] == LIN_NULL)
        {
          diagEnvData->RbiTbl[i] = rbiTbl;
          retVal = LIN_TRUE;
          break;
        }
        else{}
      }
    }
  }
  else{}
  #endif

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE Lin_Bool_t LinDiagImp_RmvRbiTable(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_pRbiLookupEntry_t rbiTbl)
{
  Lin_Bool_t retVal = LIN_FALSE;

  #if LINDIAG_SUP_READ_BY_ID == 1
  if(genericDiagEnvData != LIN_NULL)
  {
    loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;
    Lin_uint8_t i;

    if(loc_CheckRbiTbl(rbiTbl) == LIN_TRUE)
    {
      for(i = 0; i < LINDIAG_MAX_RBI_TBL_CNT; i++)
      {
        if(diagEnvData->RbiTbl[i] == LIN_NULL)
        {
          diagEnvData->RbiTbl[i] = rbiTbl;
          retVal = LIN_TRUE;
          break;
        }
        else{}
      }
    }
  }
  else{}
  #endif

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE Lin_Bool_t LinDiagImp_SetUpTimer(LinDiagIf_pGenericEnvData_t genericDiagEnvData, Lin_uint8_t                   timerNum,
                                                 LinDiagIf_Timeout_t         timeout,            LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData)
{
  Lin_Bool_t retVal = LIN_FALSE;

  #if LINDIAGIMP_MAX_SUPPORTED_TIMER > 0
  if((genericDiagEnvData != LIN_NULL) && (timeout <= LOC_TIMER_MAX_VAL))
  {
    loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

    diagEnvData->Timer[timerNum].Start = diagEnvData->BusThisPtr.IfFunsTbl->GetMilliseconds(diagEnvData->BusThisPtr.EnvData);
    diagEnvData->Timer[timerNum].Timeout = timeout;
    diagEnvData->Timer[timerNum].CbCtxData = genericDiagCbCtxData;

    retVal = LIN_TRUE;
  }
  #endif

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static void loc_TransErrorCallback(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                                   LinTransIf_Error_t           error,               LinTransIf_SID_t                  SID,
                                                   LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;

  loc_CallErrorCallback(diagEnvData, SID, error);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static void loc_TransSIDProcessedCallback(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                                          LinTransIf_SID_t             sid,                 LinTransIf_ErrorFlag_t            errorFlag,
                                                          LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
//  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static LinTransIf_eCheckNADResult_t loc_TransCheckNADCallback(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                                                              LinTransIf_NAD_t             nad,                 LinTransIf_SID_t             sid,
                                                                              LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  loc_pDiagEnvironmentData_t   diagEnvData = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;
  LinTransIf_eCheckNADResult_t returnValue = LinTransIf_ChkNADRes_Decline;

  if((sid == 0xB0 && diagEnvData->InitialNAD == nad) || (sid != 0xB0 && diagEnvData->NAD == nad) || (nad == 0x7F))
  {
    returnValue = LinTransIf_ChkNADRes_Accept;
  }
  else{}

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static void loc_TransGoToSleepCallback(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_cpInterfaceFunctions_t ifFunctions,
                                                       LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  loc_pDiagEnvironmentData_t   diagEnvData = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;

  loc_CallGoToSleepCallback(diagEnvData);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallGoToSleepCallback(loc_pDiagEnvironmentData_t diagEnvData)
{
  const LinDiagIf_cpCallbackFunctions_t callbacks = diagEnvData->Callbacks;
  if(callbacks != LIN_NULL)
  {
    const LinDiagIf_GoToSleepCbFun_t fun = callbacks->GoToSleep;
    if (fun != LIN_NULL)
    {
      fun(diagEnvData, &LinDiagImp_InterfaceFunctions, diagEnvData->CallbackCtxData);
    }
    else{}
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallErrorCallback(loc_pDiagEnvironmentData_t diagEnvData, LinTransIf_SID_t SID, LinDiagIf_Error_t error)
{
  const LinDiagIf_cpCallbackFunctions_t callbacks = diagEnvData->Callbacks;
  if(callbacks != LIN_NULL)
  {
    const LinDiagIf_ErrorCbFun_t fun = callbacks->Error;
    if (fun != LIN_NULL)
    {
      fun(diagEnvData, &LinDiagImp_InterfaceFunctions, error, SID, diagEnvData->CallbackCtxData);
    }
    else{}
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallTimeoutCallback(loc_pDiagEnvironmentData_t diagEnvData, Lin_uint8_t timerNum, LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData)
{
  const LinDiagIf_cpCallbackFunctions_t callbacks = diagEnvData->Callbacks;

  if(callbacks != LIN_NULL)
  {
    const LinDiagIf_TimeoutCbFun_t fun = callbacks->Timeout;
    if (fun != LIN_NULL)
    {
      fun(diagEnvData, &LinDiagImp_InterfaceFunctions, timerNum, genericDiagCbCtxData);
    }
    else{}
  }
}

#if (LINDIAG_SUP_SNPD == 1) && (LINDIAG_SUP_DATASTG == 0)
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallStoreNADCallback(loc_pDiagEnvironmentData_t diagEnvData, LinDiagIf_NAD_t nad, LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData)
{
  const LinDiagIf_cpCallbackFunctions_t callbacks = diagEnvData->Callbacks;

  if(callbacks != LIN_NULL)
  {
    const LinDiagIf_StoreNADCbFun_t fun = callbacks->StoreNAD;
    if (fun != LIN_NULL)
    {
      fun(diagEnvData, &LinDiagImp_InterfaceFunctions, nad, genericDiagCbCtxData);
    }
    else{}
  }
}
#endif

#if LINDIAG_SUP_READ_BY_ID == 1
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static LinDiagIf_pRbiLookupEntry_t loc_LookupRbi(loc_pDiagEnvironmentData_t diagEnvData, LinDiagIf_RbIdentifier_t rbIdentifier)
{
  LinDiagIf_pRbiLookupEntry_t retVal = LIN_NULL;
  Lin_uint8_t i = 0;

  /* Iterate through all tables */
  for(i = 0; i < LINDIAG_MAX_RBI_TBL_CNT; i++)
  {
    Lin_uint8_t j = 0;
  
    if(diagEnvData->RbiTbl[i] == LIN_NULL)
    {
      break;
    }

    while(diagEnvData->RbiTbl[i][j].Callback != LIN_NULL)
    {
      /* Check if there is a callback function for this RBIdentifier. */
      if(rbIdentifier >=  diagEnvData->RbiTbl[i][j].FirstRbIdentifier && rbIdentifier <=  diagEnvData->RbiTbl[i][j].LastRbIdentifier)
      {
        /* Return pointer to RBIdentifier entry. */
        retVal = &(diagEnvData->RbiTbl[i][j]);
        break;
      }
	  
      j++;
    }
  }

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_CheckRbiTbl(LinDiagIf_pRbiLookupEntry_t rbiTbl)
{
  Lin_Bool_t retVal = LIN_FALSE;
  Lin_uint8_t i = 0;

  while(rbiTbl[i].Callback != LIN_NULL)
  {
    //Check range
    if(rbiTbl[i].FirstRbIdentifier <= rbiTbl[i].LastRbIdentifier)
    {
      // Check length type parameter
      if(rbiTbl[i].LengthType == LinDiagIf_RbiLenType_Value)
      {
        retVal = LIN_TRUE;
      }
      else if(rbiTbl[i].LengthType == LinDiagIf_RbiLenType_Callback)
      {
        if(rbiTbl[i].Length != LIN_NULL)
        {
          retVal = LIN_TRUE;
        }
      }
      else{}
    }
    else{}
    
    i++;
  }

  return(retVal);
}
#endif

#if LINDIAG_SUP_DATASTG == 1
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinDataStgIf_eErrorCodes_t loc_DataStgGetValue(loc_pDiagEnvironmentData_t diagEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                                             LinDataStgIf_pData_t       buffer,      LinDataStgIf_Length_t      bufferLen)
{
  LinDataStgIf_eErrorCodes_t retVal = LinDataStgIf_ERR_DATA_VALUE_ACCESS_FAILED;

  retVal = diagEnvData->DataStgThisPtr.IfFunsTbl->GetDataValue(diagEnvData->DataStgThisPtr.EnvData, dataValueId,
                                                               buffer, bufferLen, LIN_NULL);

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinDataStgIf_eErrorCodes_t loc_DataStgSetValue(loc_pDiagEnvironmentData_t diagEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                                             LinDataStgIf_pData_t       buffer,      LinDataStgIf_Length_t      bufferLen)
{
  LinDataStgIf_eErrorCodes_t retVal = LinDataStgIf_ERR_DATA_VALUE_ACCESS_FAILED;

  retVal = diagEnvData->DataStgThisPtr.IfFunsTbl->SetDataValue(diagEnvData->DataStgThisPtr.EnvData, dataValueId,
                                                               buffer, bufferLen);

  return(retVal);
}
#endif

#ifdef LOC_USE_APPENDDATATOMSGBUFFFER_FUN
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_AppendDataToMsgBuffer(LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinTransIf_pData_t data, LinTransIf_BufLength_t dataLen)
{
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

  diagEnvData->UserAppendedDataLen += dataLen;

  return(diagEnvData->TransIfFuns->AppendDataToMsgBuffer((LinTransIf_pGenericEnvData_t) diagEnvData->TransEnvData, data, dataLen));
}
#endif

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_PrepareNegativeResponse(loc_pDiagServiceControlData_t diagSerCtrlData)
{
  Lin_Bool_t returnValue = LIN_FALSE;
  loc_pDiagEnvironmentData_t diagEnvData = diagSerCtrlData->DiagEnvData;

  returnValue = diagEnvData->TransIfFuns->InitResponse(diagEnvData->TransEnvData, diagEnvData->NAD, LINDIAGIF_NEG_RESP_RSID, LINDIAGIF_NEG_RESP_LEN);

  if(returnValue == LIN_TRUE)
  {
    LinDiagIf_Data_t buffer[2];

    buffer[0] = diagSerCtrlData->RequestedSID;
    buffer[1] = diagSerCtrlData->NegativeResponseCode;

    returnValue = diagEnvData->TransIfFuns->AppendDataToMsgBuffer((LinTransIf_pGenericEnvData_t) diagEnvData->TransEnvData, buffer, sizeof(buffer));

    if(returnValue != LIN_TRUE)
    {
      /* Appending data to buffer in Transport Layer failed.
       * Report error. */
      loc_CallErrorCallback(diagSerCtrlData->DiagEnvData, diagSerCtrlData->RequestedSID, LinDiagIf_ERR_TRANS_LAYER);
    }
    else{}
  }
  else
  {
    /* Initialization of response failed in Transport Layer. This function
     * only returns LIN_FALSE if the Transport Layer Environment Data pointer
     * is NULL, so report an error, but don't send a negative response because
     * communication via LIN Transport Layer is most likely impossible. */
    diagSerCtrlData->SIDReaction = LinTransIf_SIDRA_IGNORED;

    loc_CallErrorCallback(diagEnvData, diagSerCtrlData->RequestedSID, LinDiagIf_ERR_TRANS_LAYER);
  }

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_PreparePositiveResponse(loc_pDiagServiceControlData_t diagSerCtrlData, LinDiagIf_pData_t  pData,
                                                                     LinDiagIf_Length_t            complDataLen,    LinDiagIf_Length_t appendDataLen)
{
  Lin_Bool_t returnValue = LIN_FALSE;
  loc_pDiagEnvironmentData_t diagEnvData = diagSerCtrlData->DiagEnvData;

  /* Init response */
  returnValue = diagEnvData->TransIfFuns->InitResponse(diagEnvData->TransEnvData, diagEnvData->NAD,
                                                       diagSerCtrlData->RequestedSID + LINDIAGIF_RSID_OFFSET,
                                                       complDataLen);

  if(returnValue == LIN_TRUE)
  {
    if(appendDataLen > 0)
    {
      returnValue = diagEnvData->TransIfFuns->AppendDataToMsgBuffer((LinTransIf_pGenericEnvData_t) diagEnvData->TransEnvData,
                                                                    pData, appendDataLen);

      if(returnValue != LIN_TRUE)
      {
        /* Appending data to buffer in Transport Layer failed.
         * Report error and try to send a negative response. */
        diagSerCtrlData->SendNegativeResponse = LIN_TRUE;
        diagSerCtrlData->NegativeResponseCode = LinDiagIf_NRC_Sub_Function_Not_Supported;

        loc_CallErrorCallback(diagSerCtrlData->DiagEnvData, diagSerCtrlData->RequestedSID, LinDiagIf_ERR_TRANS_LAYER);
      }
      else{}
    }
    else{}
  }
  else
  {
    /* Initialization of response failed in Transport Layer. This function
     * only returns LIN_FALSE if the Transport Layer Environment Data pointer
     * is NULL, so report an error, but don't send a negative response because
     * communication via LIN Transport Layer is most likely impossible. */
    diagSerCtrlData->SIDReaction = LinTransIf_SIDRA_IGNORED;

    loc_CallErrorCallback(diagEnvData, diagSerCtrlData->RequestedSID, LinDiagIf_ERR_TRANS_LAYER);
  }

  return(returnValue);
}


/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_InitSerCtrlData(loc_pDiagServiceControlData_t diagSerCtrlData, LinTransIf_pGenericCbCtxData_t genericTransCbCtxData,
                                                             LinTransIf_SID_t              requestedSID,    LinDiagIf_Length_t             requestedLen,
                                                             loc_eReqLenCompType_t         reqLenCompType,  Lin_Bool_t                     supportSubFunction)
{
  Lin_Bool_t returnValue = LIN_FALSE;

  if(genericTransCbCtxData != LIN_NULL)
  {
    diagSerCtrlData->DiagEnvData          = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;
    diagSerCtrlData->SIDReaction          = LinTransIf_SIDRA_WAIT_FOR_RESPONSE_FRAME;
    diagSerCtrlData->RequestedSID         = requestedSID;
    diagSerCtrlData->RequestLength        = requestedLen;
    diagSerCtrlData->ReqLenCompType       = reqLenCompType;
    diagSerCtrlData->SupportSubFunction   = supportSubFunction;
    diagSerCtrlData->SubFunction          = 0;
    diagSerCtrlData->SuppressPosRspMsg    = LIN_FALSE;
    diagSerCtrlData->SendNegativeResponse = LIN_FALSE;
    diagSerCtrlData->NegativeResponseCode = LinDiagIf_NRC_Sub_Function_Not_Supported;

    /* Reset counter for data appended to buffer by user. */
    diagSerCtrlData->DiagEnvData->UserAppendedDataLen = 0;

    returnValue = LIN_TRUE;
  }
  else
  {
    diagSerCtrlData->SIDReaction = LinTransIf_SIDRA_IGNORED;
  }

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_ValidateAndExtractServiceData(loc_pDiagServiceControlData_t diagSerCtrlData, LinDiagIf_pData_t pData,
                                                                           LinDiagIf_Length_t            dataLen)
{
  Lin_Bool_t returnValue = LIN_FALSE;

  if(diagSerCtrlData->ReqLenCompType == loc_RLCT_GreaterThenOrEqual)
  {
    if(dataLen >= diagSerCtrlData->RequestLength)
    {
      returnValue = LIN_TRUE;
    }
    else{}
  }
  else if(diagSerCtrlData->ReqLenCompType == loc_RLCT_Equal)
  {
    if(dataLen == diagSerCtrlData->RequestLength)
    {
      returnValue = LIN_TRUE;
    }
    else{}
  }
  else if(diagSerCtrlData->ReqLenCompType == loc_RLCT_LessThenOrEqual)
  {
    if(dataLen <= diagSerCtrlData->RequestLength)
    {
      returnValue = LIN_TRUE;
    }
    else{}
  }
  else{}

  if(returnValue == LIN_TRUE)
  {
    if(diagSerCtrlData->SupportSubFunction == LIN_TRUE)
    {
      /* Suppress positive response message? */
      if(pData[0] & LOC_SUPPRESS_POS_RESP_BIT_MSK)
      {
        diagSerCtrlData->SuppressPosRspMsg = LIN_TRUE;
      }
      else{}

      diagSerCtrlData->SubFunction = pData[0] & LOC_SUB_FUNCTION_MSK;
    }
    else{}
  }
  else
  {
    /* Data length mismatch */
    diagSerCtrlData->SendNegativeResponse = LIN_TRUE;
    diagSerCtrlData->NegativeResponseCode = LinDiagIf_NRC_Sub_Function_Not_Supported;

    loc_CallErrorCallback(diagSerCtrlData->DiagEnvData, diagSerCtrlData->RequestedSID, LinDiagIf_ERR_IN_CORRECT_MESSAGE_LENGHT_INVALID_FORMAT);
  }

  return(returnValue);
}

#if LINDIAG_SUP_ASSIGN_NAD == 1
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_AssignNAD(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                      LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                      LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                      LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;
  LinTransIf_eSIDReaction_t  returnValue = LinTransIf_SIDRA_IGNORED;

  if(dataLen == LINDIAGIF_ASSIGNNAD_REQLEN)
  {
    LinDiagIf_SupplierID_t supplierID = 0;
    LinDiagIf_FunctionID_t functionID = 0;

    supplierID = data[0];
    supplierID |= data[1] << 8;
    functionID = data[2];
    functionID |= data[3] << 8;

    if(supplierID == diagEnvData->ProductIdentification.SupplierID && functionID == diagEnvData->ProductIdentification.FunctionID)
    {
      diagEnvData->TransIfFuns->InitResponse(diagEnvData->TransEnvData, diagEnvData->InitialNAD, LinDiagIf_SID_AssignNAD + LINDIAGIF_RSID_OFFSET,  0);
      diagEnvData->NAD = data[4];
      returnValue = LinTransIf_SIDRA_WAIT_FOR_RESPONSE_FRAME;
    }
    else{}
  }

  return(returnValue);
}
#endif

#if LINDIAG_SUP_ASSIGN_FRAME_IDENTIFIER == 1
/***************************************************************************//**
 *
 ******************************************************************************/
static inline LinTransIf_eSIDReaction_t loc_AssignFrameIdentifier(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                  LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                  LinTransIf_Length_t          dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                  LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  LinTransIf_eSIDReaction_t returnValue;

  return(returnValue);
}
# endif

#if LINDIAG_SUP_READ_BY_ID == 1
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_ReadByIdentifier(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                             LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                             LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                             LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;
  loc_sDiagServiceControlData_t diagSerCtrlData;

  /* Init Diagnostic Service Control Data. */
  if(loc_InitSerCtrlData(&diagSerCtrlData, genericTransCbCtxData, LinDiagIf_SID_ReadByIdentifier, LINDIAGIF_READBYIDENTIFIER_REQLEN,
                         loc_RLCT_Equal, LIN_FALSE) == LIN_TRUE)
  {
    if(loc_ValidateAndExtractServiceData(&diagSerCtrlData, data, dataLen) == LIN_TRUE)
    {
      LinDiagIf_SupplierID_t supplierID = 0;
      LinDiagIf_FunctionID_t functionID = 0;
      LinDiagIf_RbIdentifier_t rbiIdentifier = (LinDiagIf_RbIdentifier_t) data[0];
      LinDiagIf_Length_t sendDataLen = 0;
      LinDiagIf_pRbiLookupEntry_t pRbiLookupEntry = LIN_NULL;

      /* Read out Supplier and Function ID */
      supplierID = data[1];
      supplierID |= data[2] << 8;
      functionID = data[3];
      functionID |= data[4] << 8;

      /* Are Supplier and Function ID correct or are the wildcards used? */
      if((supplierID == diagEnvData->ProductIdentification.SupplierID ||
          supplierID == LINDIAGIF_SUPPLIER_ID_WILDCARD) &&
         (functionID == diagEnvData->ProductIdentification.FunctionID ||
          functionID == LINDIAGIF_FUNCTION_ID_WILDCARD))
      {
        /* Lookup RBI identifier. */
        pRbiLookupEntry = loc_LookupRbi(diagEnvData, rbiIdentifier);

        if(pRbiLookupEntry != LIN_NULL)
        {
          /* Acquire 'Read by identifier' data length. */
          if(pRbiLookupEntry->LengthType == LinDiagIf_RbiLenType_Value)
          {
            sendDataLen = (LinDiagIf_Length_t) pRbiLookupEntry->Length;
          }
          else if(pRbiLookupEntry->LengthType == LinDiagIf_RbiLenType_Callback)
          {
            /* Callback null-pointer check. */
            if(pRbiLookupEntry->Length != LIN_NULL)
            {
              sendDataLen = ((LinDiagIf_RbILenCbFun_t) pRbiLookupEntry->Length)((LinDiagIf_pGenericEnvData_t) diagEnvData,
                                                                                &LinDiagImp_InterfaceFunctions, rbiIdentifier);
            }
            else
            {
              /* Length-Callback function is a null-pointer,
               * report error and send negative response. */
              diagSerCtrlData.SendNegativeResponse = LIN_TRUE;
              diagSerCtrlData.NegativeResponseCode = LinDiagIf_NRC_Sub_Function_Not_Supported;
              loc_CallErrorCallback(diagEnvData, diagSerCtrlData.RequestedSID, LinDiagIf_ERR_RBI_DEF);
            }
          }
          else
          {
            /* Invalid length-type in RBI definition. */
            diagSerCtrlData.SendNegativeResponse = LIN_TRUE;
            diagSerCtrlData.NegativeResponseCode = LinDiagIf_NRC_Sub_Function_Not_Supported;
            loc_CallErrorCallback(diagEnvData, diagSerCtrlData.RequestedSID, LinDiagIf_ERR_RBI_DEF);
          }
        }
        else
        {
          /* No RBI identifier found in lookup tables, send
           * negative response and report an error. */
          diagSerCtrlData.SendNegativeResponse = LIN_TRUE;
          diagSerCtrlData.NegativeResponseCode = LinDiagIf_NRC_Sub_Function_Not_Supported;
          loc_CallErrorCallback(diagEnvData, diagSerCtrlData.RequestedSID, LinDiagIf_ERR_RBI_NOT_FOUND);
        }

        if(diagSerCtrlData.SendNegativeResponse == LIN_FALSE)
        {
          if(loc_PreparePositiveResponse(&diagSerCtrlData, LIN_NULL, sendDataLen, 0) == LIN_TRUE)
          {
            if(pRbiLookupEntry->Callback((LinDiagIf_pGenericEnvData_t) diagEnvData, &LinDiagImp_InterfaceFunctions,
                                         rbiIdentifier, &loc_AppendDataToMsgBuffer, pRbiLookupEntry->CbCtxData) != LIN_TRUE)
            {
              /* User returned LIN_FALSE, send negative
               * response but don't report and error. */
              diagSerCtrlData.SendNegativeResponse = LIN_TRUE;
            }
            else
            {
              if(diagEnvData->UserAppendedDataLen != sendDataLen)
              {
                /* RBI data length mismatch, report error
                 * and send negative response. */
                diagSerCtrlData.SendNegativeResponse = LIN_TRUE;
                diagSerCtrlData.NegativeResponseCode = LinDiagIf_NRC_Sub_Function_Not_Supported;
                loc_CallErrorCallback(diagEnvData, diagSerCtrlData.RequestedSID, LinDiagIf_ERR_RBI_DATA_LEN);
              }
              else{}
            }
          }
          else{}
        }
        else{}
      }
      else
      {
        /* Answer with a negative response according to LIN specification
         * or not according to the LIN test specification. */
        if(diagEnvData->InvalidReadByIDAnswered == LIN_TRUE)
        {
          /* Invalid Supplier or Function ID, don't
           * report error, but send negative response. */
          diagSerCtrlData.SendNegativeResponse = LIN_TRUE;
          diagSerCtrlData.NegativeResponseCode = LinDiagIf_NRC_Sub_Function_Not_Supported;
        }
        else
        {
          diagSerCtrlData.SIDReaction = LinTransIf_SIDRA_IGNORED;
        }
      }
    }

    /* If an error occurred and LIN communication is
     * still possible, send a negative response. */
    if(diagSerCtrlData.SendNegativeResponse == LIN_TRUE)
    {
      loc_PrepareNegativeResponse(&diagSerCtrlData);
    }
    else{}
  }
  else{}

  return(diagSerCtrlData.SIDReaction);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static Lin_Bool_t loc_RbiCbProdIdent(LinDiagIf_pGenericEnvData_t   genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                                                     LinDiagIf_RbIdentifier_t      rbIdentifier,       LinDiagIf_AppendDataToMsgBuffer_t appendDataToMsgBufFun,
                                                     LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData)
{
  Lin_Bool_t retVal = LIN_FALSE;
  LinDiagIf_Data_t buffer[5];

  if(genericDiagEnvData != LIN_NULL)
  {
    loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

    buffer[0] = (diagEnvData->ProductIdentification.SupplierID & 0x00FF)     ;
    buffer[1] = (diagEnvData->ProductIdentification.SupplierID & 0xFF00) >> 8;
    buffer[2] = (diagEnvData->ProductIdentification.FunctionID & 0x00FF)     ;
    buffer[3] = (diagEnvData->ProductIdentification.FunctionID & 0xFF00) >> 8;
    buffer[4] =  diagEnvData->ProductIdentification.VariantID;

    retVal = appendDataToMsgBufFun(genericDiagEnvData, (LinDiagIf_pData_t) buffer, sizeof(buffer));
  }

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static Lin_Bool_t loc_RbiCbSerialNum(LinDiagIf_pGenericEnvData_t   genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                                                     LinDiagIf_RbIdentifier_t      rbIdentifier,       LinDiagIf_AppendDataToMsgBuffer_t appendDataToMsgBufFun,
                                                     LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData)
{
  Lin_Bool_t retVal = LIN_FALSE;

  if(genericDiagEnvData != LIN_NULL)
  {
    loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

    /*
    buffer[0] = (diagEnvData->SerialNumber & 0x000000FF)      ;
    buffer[1] = (diagEnvData->SerialNumber & 0x0000FF00) >> 8 ;
    buffer[2] = (diagEnvData->SerialNumber & 0x00FF0000) >> 16;
    buffer[3] = (diagEnvData->SerialNumber & 0xFF000000) >> 24;*/

    retVal = appendDataToMsgBufFun(genericDiagEnvData, (LinDiagIf_pData_t) &(diagEnvData->SerialNumber), sizeof(diagEnvData->SerialNumber));
  }

  return(retVal);
}
#endif

#if LINDIAG_SUP_CONDITIONAL_CHANGE_NAD == 1
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_ConditionalChangeNAD(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                                 LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                                 LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                                 LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;
  LinTransIf_eSIDReaction_t  returnValue = LinTransIf_SIDRA_IGNORED;

  if(dataLen == LINDIAGIF_CONDITIONALCHANGENAD_REQLEN)
  {
    LinDiagIf_pConditionalChangeNADData_t pCondChgNADData = (LinDiagIf_pConditionalChangeNADData_t) data;
    LinDiagIf_Data_t result = 0xFF;
    LinDiagIf_pData_t pByte;
    Lin_uint8_t buffer[5];

    switch(pCondChgNADData->Id)
    {
      case LINDIAGIF_RB_IDENTIFIER_PROD_IDENT:
        if(pCondChgNADData->Byte < (sizeof(diagEnvData->ProductIdentification.SupplierID) +
                                    sizeof(diagEnvData->ProductIdentification.FunctionID) +
                                    sizeof(diagEnvData->ProductIdentification.VariantID)))
        {
          buffer[0] = diagEnvData->ProductIdentification.SupplierID & 0x00FF;
          buffer[1] = (diagEnvData->ProductIdentification.SupplierID & 0xFF00) >> 8;
          buffer[2] = diagEnvData->ProductIdentification.FunctionID & 0x00FF;
          buffer[3] = (diagEnvData->ProductIdentification.FunctionID & 0xFF00) >> 8;
          buffer[4] = diagEnvData->ProductIdentification.VariantID;

          result = buffer[(pCondChgNADData->Byte) - 1];
          result ^= pCondChgNADData->Invert;
          result &= pCondChgNADData->Mask;
        }
        else{}
        break;

      case LINDIAGIF_RB_IDENTIFIER_SERIAL_NUM:
        if(pCondChgNADData->Byte < sizeof(diagEnvData->SerialNumber))
        {
          pByte = (LinDiagIf_pData_t) &(diagEnvData->SerialNumber);
          result = pByte[(pCondChgNADData->Byte) - 1];
          result ^= pCondChgNADData->Invert;
          result &= pCondChgNADData->Mask;
        }
        else{}
        break;
      default:
        break;
    }

    if(result == 0)
    {
      diagEnvData->TransIfFuns->InitResponse(diagEnvData->TransEnvData, diagEnvData->NAD, 0xF3, 0);
      diagEnvData->NAD = pCondChgNADData->NewNAD;
      returnValue = LinTransIf_SIDRA_WAIT_FOR_RESPONSE_FRAME;
    }
  }

  return(returnValue);
}
#endif

#if LINDIAG_SUP_DATA_DUMP == 1
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_DataDump(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                     LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                     LinTransIf_Length_t          dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                     LinBusIf_pGenericCbCtxData_t genericTransCbCtxData);
{
  LinTransIf_eSIDReaction_t returnValue;

  return(returnValue);
}
#endif

#if LINDIAG_SUP_SAVE_CONFIGURATION == 1
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_SaveConfiguration(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                              LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                              LinTransIf_Length_t          dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                              LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  LinTransIf_eSIDReaction_t returnValue = LinTransIf_SIDRA_IGNORED;

  return(returnValue);
}
#endif

#if LINDIAG_SUP_ASSIGM_FRAME_ID_RANGE == 1
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_AssignFrameIdentifierRange(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                                       LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                                       LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                                       LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;
  LinTransIf_eSIDReaction_t  returnValue = LinTransIf_SIDRA_IGNORED;
  Lin_Bool_t assign = LIN_TRUE;

  static const Lin_uint8_t pids[LIN_MAX_FRAMEID_CNT] =
  {
      0x80u, 0xC1u, 0x42u, 0x03u, 0xC4u, 0x85u, 0x06u, 0x47u,
      0x08u, 0x49u, 0xCAu, 0x8Bu, 0x4Cu, 0x0Du, 0x8Eu, 0xCFu,
      0x50u, 0x11u, 0x92u, 0xD3u, 0x14u, 0x55u, 0xD6u, 0x97u,
      0xD8u, 0x99u, 0x1Au, 0x5Bu, 0x9Cu, 0xDDu, 0x5Eu, 0x1Fu,
      0x20u, 0x61u, 0xE2u, 0xA3u, 0x64u, 0x25u, 0xA6u, 0xE7u,
      0xA8u, 0xE9u, 0x6Au, 0x2Bu, 0xECu, 0xADu, 0x2Eu, 0x6Fu,
      0xF0u, 0xB1u, 0x32u, 0x73u, 0xB4u, 0xF5u, 0x76u, 0x37u,
      0x78u, 0x39u, 0xBAu, 0xFBu, 0x3Cu, 0x7Du, 0xFEu, 0xBFu
  };

  if(genericTransCbCtxData != LIN_NULL)
  {
    if(dataLen == LINDIAGIF_ASSIGNFRAMEID_REQLEN)
    {
      /* Convert the "Assign frame ID Range" values to internal values */
      Lin_uint8_t i;
      for(i = 1; i < dataLen; i++)
      {
        if(data[i] == 0)
        {
          data[i] = 0xFE;
        }
        else if(data[i] != 0xFF)
        {
          /* Convert PIDs to Frame IDs */
          if(pids[data[i] & LIN_PID_MASK] == data[i])
          {
            Lin_uint8_t tempFrameID;
            tempFrameID = data[i] & LIN_PID_MASK;
            if(tempFrameID < 0x3C)
            {
              data[i] = tempFrameID;
            }
            else
            {
              assign = LIN_FALSE;
            }
          }
          else{}
        }
        else{}
      }

      if(assign == LIN_TRUE)
      {
        if(diagEnvData->LookupThisPtr.IfFunsTbl->AssignFrameIDRange(diagEnvData->LookupThisPtr.EnvData, data[0], &data[1], 4) == LIN_TRUE)
        {
          diagEnvData->TransIfFuns->InitResponse(diagEnvData->TransEnvData, diagEnvData->NAD, 0xF7, 0);
          returnValue = LinTransIf_SIDRA_WAIT_FOR_RESPONSE_FRAME;
        }
      }
      else{}
    }
  }
  else
  {
    /* TODO: ERROR */
  }

  //LinTransImp_InterfaceFunctions.AppendDataToMsgBuffer(&LinTransEnvData, data, dataLen);

  return(returnValue);
}
#endif

#if LINDIAG_SUP_SNPD == 1
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_Bool_t loc_CallStartMeasurement(LinDiagIf_pGenericEnvData_t genericDiagEnvData)
{
  Lin_Bool_t rv = LIN_FALSE;
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

  LinSNPDIf_cpInterfaceFunctions_t ifuns = diagEnvData->LinSNPDIfThisPointer.IfFunsTbl;
  if(ifuns != LIN_NULL)
  {
    const LinSNPDIf_StartMeasurementIfFun_t fun = ifuns->StartMeasurement;
    if (fun != LIN_NULL)
    {
      rv = fun(diagEnvData->LinSNPDIfThisPointer.EnvData);
    }
    else{}
  }

  return rv;
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallEndMeasurement(LinDiagIf_pGenericEnvData_t genericDiagEnvData)
{
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

  LinSNPDIf_cpInterfaceFunctions_t ifuns = diagEnvData->LinSNPDIfThisPointer.IfFunsTbl;
  if(ifuns != LIN_NULL)
  {
    const LinSNPDIf_EndMeasurementIfFun_t fun = ifuns->EndMeasurement;
    if (fun != LIN_NULL)
    {
      fun(diagEnvData->LinSNPDIfThisPointer.EnvData);
    }
    else{}
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline void loc_CallNextMeasuremen(LinDiagIf_pGenericEnvData_t genericDiagEnvData, Lin_Bool_t addressed)
{
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

  LinSNPDIf_cpInterfaceFunctions_t ifuns = diagEnvData->LinSNPDIfThisPointer.IfFunsTbl;
  if(ifuns != LIN_NULL)
  {
    const LinSNPDIf_NextMeasurementIfFun_t fun = ifuns->NextMeasurement;
    if (fun != LIN_NULL)
    {
      fun(diagEnvData->LinSNPDIfThisPointer.EnvData,addressed);
    }
    else{}
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinSNPDIf_eState_t loc_CallSNPDGetState(LinDiagIf_pGenericEnvData_t genericDiagEnvData)
{
  LinSNPDIf_eState_t rv = LinSNPDIf_DISABLED;
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericDiagEnvData;

  LinSNPDIf_cpInterfaceFunctions_t ifuns = diagEnvData->LinSNPDIfThisPointer.IfFunsTbl;
  if(ifuns != LIN_NULL)
  {
    const LinSNPDIf_GetStateIfFun_t fun = ifuns->GetState;
    if (fun != LIN_NULL)
    {
      rv = fun(diagEnvData->LinSNPDIfThisPointer.EnvData);
    }
    else{}
  }

  return rv;
}

#if 0
/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline Lin_uint16_t loc_CallGetMeasurementCount (LinDiagIf_pGenericEnvData_t genericDiagEnvData)
{
  return 0;
}
#endif


/***************************************************************************//**
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_AutoAddrRequest(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_NAD_t       nad,
                                                                            LinTransIf_SID_t             sid,                 LinTransIf_pData_t     data,
                                                                            LinTransIf_BufLength_t       dataLen,             LinTransIf_ErrorFlag_t errorFlag,
                                                                            LinBusIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  loc_pDiagEnvironmentData_t diagEnvData = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;

  LinTransIf_eSIDReaction_t returnValue=LinTransIf_SIDRA_IGNORED;

  if(genericTransCbCtxData != LIN_NULL)
  {
    if(dataLen == LINDIAG_SNPD_REQ_MSG_LEN)
    {
      switch(data[LINDIAG_SNPD_CMD_POS])
      {
        case LINDIAG_SNPD_CMD_MEASURE:
        {
          diagEnvData->SNPDNewNAD = 0;
          diagEnvData->SNPDNewNADValid = LIN_FALSE;

          diagEnvData->SNPDStarted = loc_CallStartMeasurement(diagEnvData);
          diagEnvData->SNPDAddressed = LIN_FALSE;

          returnValue=LinTransIf_SIDRA_SUCCESSFULLY_PROC;
          break;
        }

        case LINDIAG_SNPD_CMD_KEEP_NAD:
        {
          if (diagEnvData->SNPDStarted != LIN_FALSE )
          {
            LinSNPDIf_eState_t st=loc_CallSNPDGetState(diagEnvData);

            switch(st)
            {
              case LinSNPDIf_DONE_LAST: // keep nad ...
              {
                LinTransIf_NAD_t tmpnad=data[LINDIAG_SNPD_NAD_POS];

                if ( tmpnad != LINTRANSIF_FUNCTIONAL_NAD &&
                     tmpnad != LINTRANSIF_GOTOSLEEP_NAD     &&
                     tmpnad != LINTRANSIF_BROADCAST_NAD  )
                {
                  diagEnvData->SNPDNewNAD = tmpnad;
                  diagEnvData->SNPDNewNADValid = LIN_TRUE;

                  if ( diagEnvData->ImmediateNADUpdate != LIN_FALSE )
                  {
                    diagEnvData->NAD = diagEnvData->SNPDNewNAD;
                  }
                  else {}

                }

                diagEnvData->SNPDAddressed = LIN_TRUE;
                loc_CallNextMeasuremen(diagEnvData,LIN_TRUE);  // TODO: eventually check return value for LIN_TRUE
                break;
              }

              case LinSNPDIf_DONE_NOT_LAST: // nothing to do...
              case LinSNPDIf_SKIPED:
              {
                loc_CallNextMeasuremen(diagEnvData,diagEnvData->SNPDAddressed); // TODO: eventually check return value for LIN_TRUE
                break;
              }


              case LinSNPDIf_DISABLED: // these are error cases ....
              case LinSNPDIf_WAITING:
              case LinSNPDIf_FAILED:
              default:
              {
                diagEnvData->SNPDNewNAD = 0;
                diagEnvData->SNPDNewNADValid = LIN_FALSE;
                diagEnvData->SNPDStarted = LIN_FALSE;
                diagEnvData->SNPDAddressed = LIN_FALSE;

                loc_CallEndMeasurement(diagEnvData);

                break;
              }
            }
          }

          returnValue=LinTransIf_SIDRA_SUCCESSFULLY_PROC;
          break;
        }


        case LINDIAG_SNPD_CMD_STORE_NAD:
        {
          if (diagEnvData->SNPDStarted != LIN_FALSE && diagEnvData->SNPDNewNADValid != LIN_FALSE)
          {
#if LINDIAG_SUP_DATASTG == 1
            if (diagEnvData->DataStgThisPtr.IfFunsTbl->SetDataValue(diagEnvData->DataStgThisPtr.EnvData, LinDataStgIf_DVID_NAD,
                                                                    &diagEnvData->SNPDNewNAD, sizeof(diagEnvData->SNPDNewNAD) == LinDataStgIf_ERR_NO_ERROR))
            {
              diagEnvData->InitialNAD = diagEnvData->SNPDNewNAD;
              diagEnvData->NAD = diagEnvData->SNPDNewNAD;
            }
            else {}
#else
            loc_CallStoreNADCallback(diagEnvData, diagEnvData->SNPDNewNAD, diagEnvData->CallbackCtxData);
            diagEnvData->InitialNAD = diagEnvData->SNPDNewNAD;
            diagEnvData->NAD = diagEnvData->SNPDNewNAD;
#endif
          }
          else {}

          returnValue=LinTransIf_SIDRA_SUCCESSFULLY_PROC;
          break;
        }


        case LINDIAG_SNPD_CMD_DISABLE:
        {
          diagEnvData->SNPDNewNAD = 0;
          diagEnvData->SNPDNewNADValid = LIN_FALSE;
          diagEnvData->SNPDStarted = LIN_FALSE;
          diagEnvData->SNPDAddressed = LIN_FALSE;

          loc_CallEndMeasurement(diagEnvData);

          returnValue = LinTransIf_SIDRA_SUCCESSFULLY_PROC;
          break;
        }

        default:
        {
          break;
        }
      }
    }
    else {}
  }
  else {}

  return(returnValue);
}
#endif

#if ELMOS_BLTEST == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
DIAG_LAYER_CODE static inline LinTransIf_eSIDReaction_t loc_ElmosBlTest(LinTransIf_pGenericEnvData_t genericTransEnvData, LinTransIf_pData_t             data,
                                                                        LinTransIf_Length_t          dataLen,             LinTransIf_pGenericCbCtxData_t genericTransCbCtxData)
{
  loc_pDiagEnvironmentData_t diagEnvData     = (loc_pDiagEnvironmentData_t) genericTransCbCtxData;
  LinTransIf_eSIDReaction_t  returnValue = LinTransIf_SIDRA_WAIT_FOR_RESPONSE_FRAME;

  diagEnvData->TransIfFuns->InitResponse(diagEnvData->TransEnvData, diagEnvData->NAD, 0xFB,  dataLen-1);
  diagEnvData->TransIfFuns->AppendDataToMsgBuffer(diagEnvData->TransEnvData, data+1, dataLen-1);

  return(returnValue);
}
#endif /* ELMOS_BLTEST == 1 */

