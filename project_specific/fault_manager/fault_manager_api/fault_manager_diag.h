#ifndef FAULT_MANAGER_DIAG_H
#define FAULT_MANAGER_DIAG_H

/**
 *  @file fault_manager_diag.h
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_manager_diag_api Fault Manager Diagnostics Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_manager_types.h"
#include "global.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
#define MMGR_FAULT_LOG_RECORDS_DID_SIZE (FLT_MGR_NUM_FAULTS * sizeof(FLT_Record_T))

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/

/** @} doxygen end group */

#endif /* FAULT_MANAGER_DIAG_H */
