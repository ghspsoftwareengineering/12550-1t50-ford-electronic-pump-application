/***************************************************************************//**
 * @file			LinDrvImp_Config.c
 *
 * @creator		sbai
 * @created		03.06.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "io_e52398a.h"
#include "Lin_Basictypes.h"
#include "LinDrvImp_Config.h"
#include "LinDrvImp_FrameAndSIDConfig.h"
#include "linsci_InterruptHandler.h"

/* ****************************************************************************/
/* ************************** FORWARD DECLARATIONS ****************************/
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t loc_CheckForROMIfCfg(void);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t loc_Init_Bus_ProtoLayer(void);

/***************************************************************************//**
 * Initialization of Data Storage layer.
 *
 * @param       none
 *
 * @return      LIN_TRUE , if initalization was successful
 *              LIN_FALSE, else
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
Lin_Bool_t loc_Init_DataStg(void);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t loc_Init_DiagLayer(void);

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

#pragma location="DATASTG_CONST"
__root static const struct LinDrvImp_sFLASHCfgData loc_DefaultConfigData  =
{
  .ConfigCookie                     = LINDRVIMP_CONFIG_COOKIE_VALID,
  
  .SerialNumber                     = LINDIAGIMP_SERIALNUMBER,
  .ProductIdentification.SupplierID = LINDIAGIMP_SUPPLIERID,
  .ProductIdentification.FunctionID = LINDIAGIMP_FUNCTIONID,
  .ProductIdentification.VariantID  = LINDIAGIMP_VARIANTID,
  .InitialNAD                       = LINDIAGIMP_INITIAL_NAD,
    
  .ADCSampleExt                     = 100,  // TBD ...
  .ADCSampleExtAA                   = 100,
  .ADCSampleExtVT                   = 100,

  .EnableHighSpeed                  = FALSE,
  .DebouncerValue                   = LINBUSIMP_DEFAULT_DEBOUNCER_VALUE,  // for std. LIN baudrates

  .PreInitHook                      = NULL,
  .PostInitHook                     = NULL,
  .TaskHook                         = NULL,

  .InitialBaudrate                  = LINBUSIMP_BAUDRATE
};

LinDrvImp_cpFLASHCfgData_t  LinDrvImp_ConfigData = &loc_DefaultConfigData;

LinBusIf_cpInterfaceFunctions_t     LinDrvImp_BusIfFuns;
LinDataStgIf_cpInterfaceFunctions_t LinDrvImp_DataStgIfFuns;
LinLookupIf_cpInterfaceFunctions_t  LinDrvImp_LookupIfFuns;
LinProtoIf_cpInterfaceFunctions_t   LinDrvImp_ProtoIfFuns;
LinTransIf_cpInterfaceFunctions_t   LinDrvImp_TransIfFuns;
LinDiagIf_cpInterfaceFunctions_t LinDrvImp_DiagIfFuns;
LinDiagIf_cpInterfaceFunctions_t LinDrvImp_UdsIfFuns;
#pragma data_alignment=2
Lin_uint8_t LinDrvImp_BusEnvData[LINBUSIMP_ENVIRONMENT_DATA_SZE];

#pragma data_alignment=2
Lin_uint8_t LinDrvImp_DataStgEnvData[LINDATASTGIMP_ENVIRONMENT_DATA_SZE];

#pragma data_alignment=2
Lin_uint8_t LinDrvImp_LookupEnvData[LINDRV_LOOKUP_ENVIRONMENT_DATA_SZE];

#pragma data_alignment=2
Lin_uint8_t LinDrvImp_ProtoEnvData[LINPROTOIMP_ENVIRONMENT_DATA_SZE(LINPROTOIMP_MSG_BUF_LEN)];
#pragma data_alignment=2
static Lin_uint8_t LinDrvImp_FrmDescLstEnvData[LIN_LOOKUP_FRM_DESC_ENV_DATA_SZE(LINPROTOIMP_FRAMEID_CNT)];

#pragma data_alignment=2
Lin_uint8_t LinDrvImp_TransEnvData[LINTRANSIMP_ENVIRONMENT_DATA_SZE(LINTRANSIMP_MSG_BUF_LEN)];

#pragma data_alignment=2
Lin_uint8_t LinDrvImp_DiagEnvData[LINDIAGIMP_ENVIRONMENT_DATA_SZE];
#define TRANS_FRM_DESC_CNT 3u
#pragma data_alignment=2
static Lin_uint8_t LinDrvImp_DiagFrmDescLstEnvData[LINPROTOIMP_PER_FRM_DESC_LST_ENV_DATA_SZE(TRANS_FRM_DESC_CNT)];
  
/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* Bus Layer */
  #define LOC_PTR_TO_BUS_ENV_DATA           &LinDrvImp_BusEnvData
  #define LOC_BUS_ENV_DATA_SZE              sizeof(LinDrvImp_BusEnvData)
  #define LOC_PTR_TO_BUS_IF_FUNS            LinDrvImp_BusIfFuns
  #define LOC_PTR_BUS_IMP_CF_DATA           &linBusImpCfgData

/* Data Storage Layer */
  #define LOC_PTR_TO_DATASTG_ENV_DATA       &LinDrvImp_DataStgEnvData
  #define LOC_DATASTG_ENV_DATA_SZE          sizeof(LinDrvImp_DataStgEnvData)
  #define LOC_PTR_TO_DATASTG_IF_FUNS        LinDrvImp_DataStgIfFuns

/* Lookup Layer */
  #define LOC_PTR_TO_LOOKUP_IF_FUNS         LinDrvImp_LookupIfFuns
  #define LOC_PTR_TO_LOOKUP_ENV_DATA        &LinDrvImp_LookupEnvData
  #define LOC_LOOKUP_ENV_DATA_SZE           sizeof(LinDrvImp_LookupEnvData)
  #define LOC_PTR_LOOKUP_IMP_CF_DATA        LIN_NULL

/* Protocol Layer */
  #define LOC_PTR_TO_BUS_ENV_DATA           &LinDrvImp_BusEnvData
  #define LOC_BUS_ENV_DATA_SZE              sizeof(LinDrvImp_BusEnvData)
  #define LOC_PTR_TO_BUS_IF_FUNS            LinDrvImp_BusIfFuns
  #define LOC_PTR_BUS_IMP_CF_DATA           &linBusImpCfgData

/* Transport Layer */

/* Diagnose Layer */

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/

static const LinProtoIf_sCallbackFunctions_t LinDrvImp_ProtoCallbacks =
{
  .CallbackVersion = LINDRVIMP_PROTO_USED_IF_VERSION,

  .Error          = LINDRVIMP_PROTO_ERROR_CALLBACK,
  .Restart        = LINDRVIMP_PROTO_RESTART_CALLBACK,
  .MeasDone       = LINDRVIMP_PROTO_MEASDONE_CALLBACK,
  .Idle           = LINDRVIMP_PROTO_IDLE_CALLBACK,
  .WakeUp         = LINDRVIMP_PROTO_WAKEUP_CALLBACK
};


static const LinDataStgIf_sCallbackFunctions_t LinDrvImp_DataStgCallbacks =
{
  .CallbackVersion = LINDRVIMP_DATASTG_USED_IF_VERSION,
  .Error           = LINDRVIMP_DATASTG_ERROR_CALLBACK
};

static const LinDiagIf_sCallbackFunctions_t LinDrvImp_DiagCallbacks =
{
  .CallbackVersion = LINDRVIMP_DIAG_USED_IF_VERSION,

  .GoToSleep       = LINDRVIMP_DIAG_GOTOSLEEP_CALLBACK,
  .Error           = LINDRVIMP_DIAG_ERROR_CALLBACK,
};

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void loc_adc_Init(void)
{      
  //ADC_CTRL_DMA_BASE_ADDR = (uint16_t) 0x0000;
  
  //ADC_CTRL_SAMPLE_EXT    = LinDrvImp_ConfigData->ADCSampleExt;
  //ADC_CTRL_SAMPLE_EXT_VT = LinDrvImp_ConfigData->ADCSampleExtAA;
  //ADC_CTRL_SAMPLE_EXT_AA = LinDrvImp_ConfigData->ADCSampleExtVT;
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
Lin_Bool_t LinDrvImp_Init(vic_cpInterfaceFunctions_t vicIfFuns)
{
  Lin_Bool_t retVal = LIN_TRUE;
  
  LinDrvImp_DataStgIfFuns = &LinDataStgImp_InterfaceFunctions; 

  retVal = loc_Init_DataStg();
  
  loc_adc_Init();  
  
  if(retVal == LIN_TRUE)
  {
    retVal = loc_CheckForROMIfCfg();
  }

  if (LinDrvImp_ConfigData->PreInitHook != NULL) { LinDrvImp_ConfigData->PreInitHook(); }
  else {}
  

  retVal = loc_Init_Bus_ProtoLayer();
    
  if (retVal == LIN_TRUE)
  {
    retVal = loc_Init_DiagLayer();
  }
  else{}
  
  return(retVal);
}

/***************************************************************************//**
 * Loads EEPROM cfg data, if available and assigns ROMIF pointer, if selected as such.
 *
 * @param       none
 *
 * @return      LIN_TRUE , if initalization was successful
 *              LIN_FALSE, else
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
Lin_Bool_t loc_CheckForROMIfCfg()
{
  Lin_Bool_t retVal = LIN_TRUE;
  
  if(LinDrvImp_DataStgIfFuns->SetDataValue(LinDrvImp_DataStgEnvData, LinDataStgImp_DVID_EECfgData, (Lin_uint8_t *) &loc_DefaultConfigData, sizeof(loc_DefaultConfigData)) != LinDataStgIf_ERR_NO_ERROR)
  {
    retVal = LIN_FALSE;
  }
  else{}
  
  /* ROM API INIT */
  if(romIf_MainInterface.Interface_Get(ROMIF_LIN_BUS, (romIf_cpGenericInterface_t *) &LinDrvImp_BusIfFuns, LIN_NULL) != TRUE)
  {
    retVal = LIN_FALSE;
  }
  
  if(romIf_MainInterface.Interface_Get(ROMIF_LIN_LOOKUP_SEQUENTIAL, (romIf_cpGenericInterface_t *) &LinDrvImp_LookupIfFuns, LIN_NULL) != TRUE)
  {
    retVal = LIN_FALSE;
  }
  
  if(romIf_MainInterface.Interface_Get(ROMIF_LIN_PROTO, (romIf_cpGenericInterface_t *) &LinDrvImp_ProtoIfFuns, LIN_NULL) != TRUE)
  {
    retVal = LIN_FALSE;
  }
  
  if(romIf_MainInterface.Interface_Get(ROMIF_LIN_TRANS, (romIf_cpGenericInterface_t *) &LinDrvImp_TransIfFuns, LIN_NULL)  != TRUE)
  {
    retVal = LIN_FALSE;
  }
  
  LinDrvImp_DiagIfFuns = &LinDiagImp_InterfaceFunctions; 
  
  return retVal;
}

/***************************************************************************//**
 * Initialization of Bus, Lookup and/or Protocol layer.
 *
 * @param       none
 *
 * @return      LIN_TRUE , if initalization was successful
 *              LIN_FALSE, else
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
Lin_Bool_t loc_Init_Bus_ProtoLayer()
{
  Lin_Bool_t retVal = LIN_FALSE;

  /* LIN Bus Layer Configuration */
  LinBusImp_sCfgData_t linBusImpCfgData =
  {
    .Version               = LINBUSIMP_CONFIG_DATA_VERSION,
    .ClockFrequency        = LINBUSIMP_CLK_FREQUENZY,
    .ConfigFlags           = 
	{
      .CallMeasDone             = LIN_FALSE,
      .CheckBaudrate            = TRUE,
      .DetectPostPIDCollisions  = TRUE
    },
    .DebouncerValue        = LinDrvImp_ConfigData->DebouncerValue,
    .IgnoreMsgTimeouts     = LINBUSIMP_DEFAULT_IGNORE_MSGTOUT,
    .SendHeaderBreakLen    = LINBUSIMP_SEND_HEADER_BREAK_LEN,
  };  

  
  /* LIN Protocol Layer Configuration */
  LinProtoImp_sCfgData_t linProtoImpCfgData =
  {
    .Version = LINPROTOIMP_CONFIG_DATA_VERSION,
    .LinBusIfInitParam =
    {
      .IfFunsTbl  = LOC_PTR_TO_BUS_IF_FUNS,
      .EnvData    = LOC_PTR_TO_BUS_ENV_DATA,
      .EnvDataLen = LOC_BUS_ENV_DATA_SZE,
      .Baudrate   = LinDrvImp_ConfigData->InitialBaudrate,      
      .ImpCfgData = LOC_PTR_BUS_IMP_CF_DATA
    },
    .LinLookupIfInitParam  =
    {
      .IfFunsTbl  = LOC_PTR_TO_LOOKUP_IF_FUNS,
      .EnvData    = LOC_PTR_TO_LOOKUP_ENV_DATA,
      .EnvDataLen = LOC_LOOKUP_ENV_DATA_SZE,
      .ImpCfgData = LOC_PTR_LOOKUP_IMP_CF_DATA
    },
  };  
 
  retVal = LinDrvImp_ProtoIfFuns->Initialization((LinProtoIf_pGenericEnvData_t) LinDrvImp_ProtoEnvData, sizeof(LinDrvImp_ProtoEnvData),
                                                 &LinDrvImp_ProtoCallbacks, LINDRVIMP_PROTO_CALLBACK_CTX_DATA, &linProtoImpCfgData);
  if(retVal == LIN_TRUE)
  {
    retVal = LinDrvImp_ProtoIfFuns->AddFrameDescLst((LinProtoIf_pGenericEnvData_t) LinDrvImp_ProtoEnvData, (LinProtoIf_pGenericFrameDescriptionList_t) LinDrvImp_FrameDescriptionList,
                                                    LIN_TRUE, (LinProtoIf_pGenericFrmDescLstEnvData_t) LinDrvImp_FrmDescLstEnvData, sizeof(LinDrvImp_FrmDescLstEnvData), LINDRVIMP_PROTO_FRM_DESC_LST_CB_CTX_DATA);
  }
  
  return retVal;
}


/***************************************************************************//**
 * Initialization of Data Storage layer.
 *
 * @param       none
 *
 * @return      LIN_TRUE , if initalization was successful
 *              LIN_FALSE, else
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
Lin_Bool_t loc_Init_DataStg()
{
  Lin_Bool_t retVal = LIN_FALSE;
  
  retVal = LinDrvImp_DataStgIfFuns->Initialization((LinDataStgIf_pGenericEnvData_t) LinDrvImp_DataStgEnvData, sizeof(LinDrvImp_DataStgEnvData),
                                                   &LinDrvImp_DataStgCallbacks, LINDRVIMP_DATASTG_CALLBACK_CTX_DATA,LIN_NULL);

  return retVal;
}

/***************************************************************************//**
 * Initialization of Diagnostic layer.
 *
 * @param       none
 *
 * @return      LIN_TRUE , if initalization was successful
 *              LIN_FALSE, else
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
Lin_Bool_t loc_Init_DiagLayer()
{
  Lin_Bool_t retVal = LIN_FALSE;
 
  /* LIN Transport Layer Configuration */
  LinTransImp_sCfgData_t linTransImpCfgData =
  {
      .Version = LINTRANSIMP_CONFIG_DATA_VERSION,
      .LinProtoInsThisPointer =
      {
          .IfFunsTbl  = LinDrvImp_ProtoIfFuns,
          .EnvData    = &LinDrvImp_ProtoEnvData,
      },
  };
  
  const LinTransIf_sProtoFrameDescriptionInfo_t protoFrmDescInfo =
  {
    .ReqFrameInfo =
    {
      .MsgID   = LINDIAGIMP_REQST_CH_FRM_ID,
      .Length  = LINDIAGIMP_REQST_CH_MSG_SZE,
      .CrcType = LINDIAGIMP_REQST_CH_CRC_TYPE
    },
    .RespFrameInfo =
    {
      .MsgID   = LINDIAGIMP_RESP_CH_FRM_ID,
      .Length  = LINDIAGIMP_RESP_CH_MSG_SZE,
      .CrcType = LINDIAGIMP_RESP_CH_CRC_TYPE
    },
    
    .FrameDescListEnvData = LinDrvImp_DiagFrmDescLstEnvData,
    .FrameDescListEnvDataSze = sizeof(LinDrvImp_DiagFrmDescLstEnvData),

    .LdfRelevance = LIN_FALSE
  };
  
  LinDiagImp_sCfgData_t linDiagImpCfgData =
  {
    .Version = LINDIAGIMP_CONFIG_DATA_VERSION,

    .LinTransIfInitParam =
    {
      .IfFunsTbl              = LinDrvImp_TransIfFuns,
      .EnvData                = &LinDrvImp_TransEnvData,
      .EnvDataLen             = sizeof(LinDrvImp_TransEnvData),
      .ProtoFrmDescInfo       = &protoFrmDescInfo,
      .NasTimeout             = LINDIAGIMP_NAS_TIMEOUT,
      .NcrTimeout             = LINDIAGIMP_NCR_TIMEOUT,
      .ImpCfgData             = &linTransImpCfgData
    },

    .LinBusIfThisPointer =
    {
      .IfFunsTbl = LinDrvImp_BusIfFuns,
      .EnvData =  &LinDrvImp_BusEnvData
    },

    .LinLookupIfThisPointer =
    {
      .IfFunsTbl = LinDrvImp_LookupIfFuns,
      .EnvData =  &LinDrvImp_LookupEnvData
    },


#if LINDIAG_SUP_DATASTG == 1
    .LinDataStgIfThisPointer =
    {
      .IfFunsTbl = LinDrvImp_DataStgIfFuns,
      .EnvData =  &LinDrvImp_DataStgEnvData
    }
#else
    .InitialProdIdent =
    {
      .SupplierID = LINDIAGIMP_SUPPLIERID,
      .FunctionID = LINDIAGIMP_FUNCTIONID,
      .VariantID = LINDIAGIMP_VARIANTID
    },
    
    .InitialNad = LINDIAGIMP_INITIAL_NAD,
    .InitialSerialNumber = LINDIAGIMP_SERIALNUMBER,
#endif
  };

 
  if(LinDrvImp_ProtoIfFuns->GetSubInterface(LinDrvImp_ProtoEnvData, Lin_IfId_LOOKUP, (Lin_pThis_t) &(linDiagImpCfgData.LinLookupIfThisPointer)) == LIN_FALSE)
  {
    linDiagImpCfgData.LinLookupIfThisPointer.IfFunsTbl = LIN_NULL;
    linDiagImpCfgData.LinLookupIfThisPointer.EnvData   = LIN_NULL;
  }
    
         
  retVal = LinDrvImp_DiagIfFuns->Initialization((LinDiagIf_pGenericEnvData_t) LinDrvImp_DiagEnvData, sizeof(LinDrvImp_DiagEnvData),
                                                &LinDrvImp_DiagCallbacks, LINDRVIMP_DIAG_CALLBACK_CTX_DATA,
                                                LIN_FALSE, &linDiagImpCfgData);
                                                                                                
  if(retVal == LIN_TRUE)
  {
    #if (LINDIAG_UDS_SUP_RDBI == 1) || (LINDIAG_UDS_SUP_WDBI == 1) || (LINDIAG_UDS_SUP_IO_CTRL_BY_ID == 1)
    retVal = LinDrvImp_DiagIfFuns->AddDidTable((LinDiagIf_pGenericEnvData_t) LinDrvImp_DiagEnvData, (LinDiagIf_pDidLookupEntry_t) LinDrvImp_DidTbl);
    #endif
  }
      
  if(retVal == LIN_TRUE)
  {
    #if (LINDIAG_UDS_SUP_ROUTINE_CTRL == 1)
    retVal = LinDrvImp_DiagIfFuns->AddRoutineIdTable((LinDiagIf_pGenericEnvData_t) LinDrvImp_DiagEnvData, (LinDiagIf_pRoutineIdLookupEntry_t) LinDrvImp_RoutineIdTbl);
    #endif
  }
 
  return retVal;
}

/***************************************************************************//**
 * TODO
 *
 * @param       none
 *
 * @return      void
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_Task(void)
{
  if (LinDrvImp_ConfigData->TaskHook != NULL) { LinDrvImp_ConfigData->TaskHook(); }
  else {}        
}
