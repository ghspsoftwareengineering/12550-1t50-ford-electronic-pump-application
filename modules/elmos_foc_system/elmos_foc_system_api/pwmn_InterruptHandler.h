/***************************************************************************//**
 * @file			pwmn_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef PWMN_INTERRUPTHANDLER_H_          
#define PWMN_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * PWMN IRQ vector numbers
 ******************************************************************************/
typedef enum {
  pwm_IRQ_OC                       = 0u,
  pwm_IRQ_START_EVT                = 1u,
  pwm_IRQ_MIDDLE_EVT               = 2u,
  pwm_IRQ_DEAD_TIME_EVT_0          = 3u,
  pwm_IRQ_DEAD_TIME_EVT_1          = 4u,
  pwm_IRQ_DEAD_TIME_EVT_2          = 5u,
  pwm_IRQ_DEAD_TIME_EVT_3          = 6u,
  pwm_IRQ_DEAD_TIME_EVT_4          = 7u,
  pwm_IRQ_DEAD_TIME_EVT_5          = 8u,
  pwm_IRQ_DEAD_TIME_EVT_6          = 9u,
  pwm_IRQ_DEAD_TIME_EVT_7          = 10u,

  pwmn_INTERRUPT_VECTOR_CNT        = 11u  /**< Number of available interrupt vectors */
} pwmn_eInterruptVectorNum_t;

/***************************************************************************//**
 * Pointer to PWMN context data
 ******************************************************************************/
typedef void * pwmn_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*pwmn_InterruptCallback_t) (pwmn_eInterruptVectorNum_t irqsrc, pwmn_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * PWMN environment data
 ******************************************************************************/
typedef struct pwmn_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module             */
    pwmn_InterruptCallback_t InterrupVectorTable[pwmn_INTERRUPT_VECTOR_CNT];
    
    /** PWMN module context data */
    pwmn_pInterruptContextData_t ContextData;
    
} pwmn_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to PWMN environment data
 ******************************************************************************/
typedef pwmn_sInterruptEnvironmentData_t * pwmn_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to PWMN module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with pwmn_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The PWMN IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void pwmn_InterruptEnable(pwmn_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to PWMN module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The PWMN IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void pwmn_InterruptDisable(pwmn_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to PWMN module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void pwmn_InterruptRegisterCallback(pwmn_eInterruptVectorNum_t irqvecnum, pwmn_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to PWMN module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void pwmn_InterruptDeregisterCallback(pwmn_eInterruptVectorNum_t irqvecnum);

/***************************************************************************//**
 * Handles the PWMN related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void pwmn_InterruptHandler(void);

/***************************************************************************//**
 * @brief Initialize PWMN module
 *
 * @param environmentdata  Pointer to Environment data for PWMN module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and PWMN (pwmn_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       PWMN module is configured for use.
 *
 * @detaildesc
 * Initializes the PWMN software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as PWMNs or not.
 *
 ******************************************************************************/
void pwmn_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, pwmn_pInterruptEnvironmentData_t environmentdata, pwmn_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

