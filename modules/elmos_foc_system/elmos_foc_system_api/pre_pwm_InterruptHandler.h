/***************************************************************************//**
 * @file			pre_pwm_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef PRE_PWM_INTERRUPTHANDLER_H_          
#define PRE_PWM_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * PRE_PWM IRQ vector numbers
 ******************************************************************************/
typedef enum {  
  pre_pwm_IRQ_BPS0_EVT               = 0u,
  pre_pwm_IRQ_BPS1_EVT               = 1u,
  pre_pwm_IRQ_U_RELOAD_EVT           = 2u,
  pre_pwm_IRQ_V_RELOAD_EVT           = 3u,
  pre_pwm_IRQ_W_RELOAD_EVT           = 4u,
  pre_pwm_IRQ_RELOAD_ERROR_EVT       = 5u,
  pre_pwm_IRQ_U_DMA_RDY              = 6u,
  pre_pwm_IRQ_V_DMA_RDY              = 7u,
  pre_pwm_IRQ_W_DMA_RDY              = 8u,

  pre_pwm_INTERRUPT_VECTOR_CNT       = 9u  /**< Number of available interrupt vectors */
} pre_pwm_eInterruptVectorNum_t;


/***************************************************************************//**
 * Pointer to PRE_PWM context data
 ******************************************************************************/
typedef void * pre_pwm_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*pre_pwm_InterruptCallback_t) (pre_pwm_eInterruptVectorNum_t irqsrc, pre_pwm_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * PRE_PWM environment data
 ******************************************************************************/
typedef struct pre_pwm_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module */
    pre_pwm_InterruptCallback_t InterrupVectorTable[pre_pwm_INTERRUPT_VECTOR_CNT];
    
    /** PRE_PWM module context data */
    pre_pwm_pInterruptContextData_t ContextData;
    
} pre_pwm_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to PRE_PWM environment data
 ******************************************************************************/
typedef pre_pwm_sInterruptEnvironmentData_t * pre_pwm_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to PRE_PWM module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with pre_pwm_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The PRE_PWM IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void pre_pwm_InterruptEnable(pre_pwm_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to PRE_PWM module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The PRE_PWM IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void pre_pwm_InterruptDisable(pre_pwm_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to PRE_PWM module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void pre_pwm_InterruptRegisterCallback(pre_pwm_eInterruptVectorNum_t irqvecnum, pre_pwm_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to PRE_PWM module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void pre_pwm_InterruptDeregisterCallback(pre_pwm_eInterruptVectorNum_t irqvecnum);

/***************************************************************************//**
 * Handles the PRE_PWM related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void pre_pwm_InterruptHandler(void);

/***************************************************************************//**
 * @brief Initialize PRE_PWM module
 *
 * @param environmentdata  Pointer to Environment data for PRE_PWM module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and PRE_PWM (pre_pwm_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       PRE_PWM module is configured for use.
 *
 * @detaildesc
 * Initializes the PRE_PWM software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as PRE_PWMs or not.
 *
 ******************************************************************************/
void pre_pwm_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, pre_pwm_pInterruptEnvironmentData_t environmentdata, pre_pwm_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

