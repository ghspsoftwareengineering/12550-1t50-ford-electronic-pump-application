
#include <string.h>
#include <stdint.h>
#include "flash_api.h"
#include "flash_api_ram.h"
/* todo: changed! */
#include "defines.h"

#pragma inline = forced
inline void RestoreIRQ(__istate_t state)
{
    __set_interrupt_state(state);
}

#pragma inline = forced
inline __istate_t SaveAndDisableIRQ(void)
{
    __istate_t ret = __get_interrupt_state();
    __disable_interrupt();
    return ret;
}

/*Save & Disable interrupts*/
#define CLI() __istate_t irqstate = SaveAndDisableIRQ()
/*Restore & Enable interrupts*/
#define SEI() RestoreIRQ(irqstate)

#pragma segment="RAMCODE"
#pragma segment="ROMCODE"

#define ROW_SIZE 0x40u
#define ROW_MASK (ROW_SIZE-1u)

static flash_status_t flash_set_mode(flash_modes_t mode);
static uint8_t flash_is_busy(void);

#define RAM_FUNCTION_SETTING _Pragma ("optimize=medium") _Pragma ("location=\"RAMCODE\"")


RAM_FUNCTION_SETTING
static flash_status_t flash_set_mode(flash_modes_t mode){
    uint16_t tmp;
    flash_status_t fstatus;

    uint16_t value = 0xA500u | ((uint16_t)mode & 0x00FFu);
    FLASH_CTRL_MODE = value;
    tmp = FLASH_CTRL_MODE;
    if (tmp != (0x9600u | ((uint16_t)mode & 0x00FFu))) {
        fstatus = flash_failed;
    } else {
        fstatus = flash_ok;
    }
    return fstatus;
}

RAM_FUNCTION_SETTING
static uint8_t flash_is_busy(void){
    return (uint8_t)((FLASH_CTRL_STATUS) & E_FLASH_CTRL_STATUS_BUSY);

}

RAM_FUNCTION_SETTING
flash_status_t ram_write_words_to_flash (flash_block_t  block, uint16_t flash_addr , const uint16_t word_buf[], uint16_t word_count)
{
    flash_modes_t mode;
    flash_status_t fstatus;
    uint16_t i;
    CLI();

    if ( block == FLASH_MAIN ) {
        mode = MB_ROW_PROG;
    } else {
        mode = IB_ROW_PROG;
    }
    if ( flash_set_mode(mode) == flash_failed ) {
        fstatus = flash_failed;
    } else {

        i = 0u;
        while ( word_count > 0u ) {
            uint8_t wordsperrow = ROW_SIZE - ((uint8_t)(flash_addr >> 1u) & ROW_MASK );
            if ( wordsperrow > word_count ) {
                wordsperrow = (uint8_t)word_count;
            }
            uint16_t value = 0xA500u | ((uint16_t)wordsperrow - 1u);
            FLASH_CTRL_WORD_CONFIG = value;
            word_count -= wordsperrow;
            while ( wordsperrow > 0u ) {
                *(volatile uint16_t *)(flash_addr) = (word_buf[i]);
                i++;
                flash_addr+=2u;
                wordsperrow--;
                while (flash_is_busy()) {
                    /* loop forever */
                }
            }
        }

        while ( flash_set_mode(MB_WORD_READ) == flash_failed ) {
            /* loop forever */
        }

        fstatus = flash_ok;
    }
    SEI();
    return fstatus;
}


RAM_FUNCTION_SETTING
flash_status_t ram_flash_page_erase (flash_block_t  block, uint16_t flash_addr )
{
    flash_modes_t mode;
    flash_status_t fstatus;
    CLI();

    if ( block == FLASH_MAIN ) {
        mode = MB_PAGE_ER;
    } else {
        mode = IB_PAGE_ER;
    }
    if ( flash_set_mode(mode) == flash_failed ) {
        fstatus = flash_failed;
    } else {

        *(volatile uint16_t *)(flash_addr) = (0x0u);
        while (flash_is_busy()) {
            /* loop forever */
        }

        while ( flash_set_mode(MB_WORD_READ) == flash_failed ) {
            /* loop forever */
        }
        fstatus = flash_ok;
    }
    SEI();
    return fstatus;
}


RAM_FUNCTION_SETTING
flash_status_t ram_flash_ib_read(uint16_t addr, uint16_t data[], uint16_t anzahl)
{
    flash_status_t fstatus;
    uint16_t i;

    CLI();
    if ( flash_set_mode(IB_WORD_READ) == flash_failed ) {
        fstatus = flash_failed;
    } else {

        for ( i = 0u; i < anzahl; i++ ) {
            data[i] = (uint16_t)(*(volatile uint16_t *)(addr));
            addr += 2u;
        }

        while ( flash_set_mode(MB_WORD_READ) == flash_failed ) {
            /* loop forever */
        }
        fstatus = flash_ok;
    }
    SEI();
    return fstatus;
}

#pragma language=extended
void init_ram_code(void)
{
    void * ram_start = __sfb("RAMCODE");  /* start of RAMCODE */
    void * ram_end = __sfe("RAMCODE");    /* end of RAMCODE   */
    void * rom_start = __sfb("ROMCODE");  /* start of ROMCODE */
    uint16_t size = (uint16_t)(ram_end) - (uint16_t)(ram_start); /* compute the number of bytes to copy */
    memcpy( ram_start, rom_start, (uint16_t)size );       /* copy the contents of ROMCODE to RAMCODE */
}
/* restore the previous mode */

#pragma language=default
