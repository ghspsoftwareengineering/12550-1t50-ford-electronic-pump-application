#ifndef FLT_ENVIRONMENTAL_LOGGER_H
#define FLT_ENVIRONMENTAL_LOGGER_H

/**
 *  @file fault_environmental_logger.h
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_environmental_logger_api Fault Environmental Logger Interface Documentation
 *  @{
 *      @details Logs environmental data to permanent storage when a fault occurs
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_logger.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
void fltel_Init(void);

/**
 * Logs the occurrence of a fault to non-volatile memory fault buffer along with
 * the environmental conditions present when the fault occurred.
 *
 * @param Fault_ID - ID of the fault that has occurred
 */
void fltel_Log_Fault(FLT_LOG_Faults_T Fault_ID);

/** @} doxygen end group */

#endif /* FLT_ENVIRONMENTAL_LOGGER_H */
