/***************************************************************************//**
 * @file			LinProto_Implementation.c
 *
 * @creator		sbai
 * @created		13.01.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id$
 *
 * $Revision$
 *
 * @misra{M3CM Dir-1.2. and 11.1. - PRQA Msg 307 and 310,
 * The cast between a pointer-to-function or pointer-to-object and (another) pointer-to-object type
 * is dangerous\, but basically they are both pointers. This cast is necessary\, because some parameter
 * for the Elmos LIN Driver have to be determinable during runtime and/or via a concrete values.
 * I.e. the determination of some length values. To save memory\, callbacks to determine such a value
 * during runtime and a pointer to a variable share the same pointer and another parameter configures
 * what is behind this pointer.,
 * Can be dangerous, if the conditions aren't strictly checked.,
 * Alignment and NULL-pointer checks are performed and the functionality of this code
 * is precisely tested.}
 * _____
 *
 * @misra{M3CM Rule-21.2. and 11.1. - PRQA Msg 4603 and 4604,
 * A real time operating system is not used in this project and the
 * signal.h library, either. Signal is also an item in the LIN protocol
 * layer specification.,
 * Could cause conflicts in functions with the parameter signal.,
 * None.}
 * _____
 *
 * @misra{M3CM Dir-2.7. - PRQA Msg 3206,
 * The Elmos LIN Driver defines interfaces for every module of it. This interfaces consist
 * of typedefinitions of function pointers combined in struct. This struct has to be 
 * implemented by every implementation of every module. Because of this matter of fact\,
 * there can be different implementations for the same module and for the same reason one
 * implementation use and/or write to a parameter defined in the function pointer typedefinition
 * and another one not. So the parameter of the interface function are binding\, but must not
 * be used and/or not written to them.,
 * Unnecessary code.,
 * None}
 * _____
 *
 * @misra{M3CM Rule-8.9. - PRQA Msg 3218,
 * The interface and callback functions struct are the main junction of a LIN driver module\,
 * so it declared and initialized as uppermost of the file as possible. Eventhough they are
 * only directly accessed once at every 'Initialization' function\, they are thenceforth still
 * accessed via a pointer in the environment data structs.,
 * Properly access to data you should not have.,
 * None}
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ********************** MODULE MISRA RULE EXCEPTIONS ***************************/
/* ****************************************************************************/
// PRQA S 4603 ++
// PRQA S 4604 ++

/* ****************************************************************************/
/* ******************************** INCLUDES ***********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "el_helper.h"

#include "LinProto_FrameDescription.h"

#if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
#include "LinProto_Signal.h"
#endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

#include "LinProto_Implementation.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#if LINDRVIMP_AQR_MOD_SZE == 1
#define PROTO_LAYER_CODE _Pragma("location=\"PROTO_LAYER_CODE\"")
#define PROTO_LAYER_DATA _Pragma("location=\"PROTO_LAYER_DATA\"")
#else
#define PROTO_LAYER_CODE 
#define PROTO_LAYER_DATA 
#endif

#ifndef LINPROTO_INTERFACE_MAX_CALLBACK_TABLES
# error Maximal count of callback table is undefinied! (LINPROTO_INTERFACE_MAX_CALLBACK_TABLES)
#endif

#define LOC_RXRX_CB_DATA_MSK 0x0Cu

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

struct loc_sProtoTaskControlFlags    /**< Data struct to conrol LIN driver task  **/
{
  Lin_uint8_t ErrorCbRecvd        :1; /**< TODO: Add description **/
  Lin_uint8_t RestartCbRecvd      :1; /**< TODO: Add description **/
  Lin_uint8_t ReceiveDoneCbRecvd  :1; /**< TODO: Add description **/
  Lin_uint8_t TransmitDoneCbRecvd :1; /**< TODO: Add description **/
  Lin_uint8_t IdleCbRecvd         :1; /**< TODO: Add description **/
  Lin_uint8_t WakupCbRecvd        :1; /**< TODO: Add description **/
};

union loc_uProtoTaskControlData              /**< TODO: Add description **/
{
  Lin_uint8_t                  Byte;  /**< TODO: Add description **/
  struct loc_sProtoTaskControlFlags Flags; /**< TODO: Add description **/
};

typedef union loc_uProtoTaskControlData loc_uProtoTaskControlData_t; /**< TODO: Add description **/


struct loc_sFrameInfo /**< TODO: Concrete description. **/
{
  LinBusIf_FrameID_t              FrameID;     /**< TODO: Add description **/
  LinProtoIf_cpFrameDescription_t FrameDesc;   /**< TODO: Add description **/
  LinBusIf_Error_t                Error;       /**< TODO: Add description **/
};

typedef struct loc_sFrameInfo    loc_sFrameInfo_t; /**< TODO: Add description **/
typedef        loc_sFrameInfo_t* loc_pFrameInfo_t; /**< TODO: Add description **/

struct loc_sProtoEnvironmentData  /**<< TODO: Concrete description. */
{
    loc_uProtoTaskControlData_t        TaskControl;                                             /**< TODO: Concrete description. */
    loc_sFrameInfo_t                   ActlFrameInfo;                                           /**< TODO: Concrete description. */
    loc_sFrameInfo_t                   ErrCallbackData;                                         /**< TODO: Concrete description. */
    loc_sFrameInfo_t                   RxTxCallbackData;                                        /**< TODO: Concrete description. */
    LinProtoIf_cpCallbackFunctions_t   Callbacks[LINPROTO_INTERFACE_MAX_CALLBACK_TABLES];       /**< TODO: Concrete description. */
    LinProtoIf_pGenericCbCtxData_t     CallbackCtxData[LINPROTO_INTERFACE_MAX_CALLBACK_TABLES]; /**< The callback context data, to pass back information back to the using implementation */
    LinBusIf_pGenericEnvData_t         BusEnvData;                                              /**< Pointer to bus module environment data */
    LinBusIf_cpInterfaceFunctions_t    BusIfFuncTbl;                                            /**< TODO: Concrete description. */
    LinLookupIf_pGenericEnvData_t      LookupEnvData;                                           /**< Pointer to lookup module environment data */
    LinLookupIf_cpInterfaceFunctions_t LookupIfFuncTbl;                                         /**< TODO: Concrete description. */
    LinProtoIf_BufLength_t             MsgBufLen;                                               /**< Length of the LIN message buffer */
    LinProtoIf_pData_t                 MsgBuf;                                                  /**< Pointer to message buffer */
};

enum loc_eCopyProcessResult /**< TODO: Add description **/
{
  loc_CPR_RUNNING = 0,      /**< TODO: Add description **/
  loc_CPR_OK      = 1,      /**< TODO: Add description **/
  loc_CPR_ERR     = 2,      /**< TODO: Add description **/
};

typedef enum loc_eCopyProcessResult loc_eCopyProcessResult_t; /**< TODO: Add description **/

typedef struct loc_sProtoEnvironmentData    loc_sProtoEnvironmentData_t;  /**<< TODO: Concrete description. */
typedef        loc_sProtoEnvironmentData_t* loc_pProtoEnvironmentData_t;  /**<< TODO: Concrete description. */


struct loc_sFrameIdLookupTableEntry       /**<< TODO: Concrete description. */
{
    LinBusIf_FrameID_t              FrameID;      /**<< TODO: Concrete description. */
    LinProtoIf_cpFrameDescription_t PtrToFrmDesc; /**<< TODO: Concrete description. */
};

typedef struct loc_sFrameIdLookupTableEntry    loc_sFrameIfLookupTableEntry_t; /**<< TODO: Concrete description. */
typedef        loc_sFrameIfLookupTableEntry_t* loc_pFrameIdLookupTable_t;      /**<< TODO: Concrete description. */

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* *****************************************************************************
 * Local helper functions
 *
 ******************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline Lin_Bool_t loc_VerifyFrameDesciption(LinProtoIf_cpFrameDescription_t frameDesc, LinProtoIf_BufLength_t msgBufLen);

#if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline Lin_Bool_t loc_VerifySignalDefinition(LinProtoIf_cpSignal_t signal, LinProtoIf_FrameLenght_t frameLength);
#endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

#if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline loc_eCopyProcessResult_t loc_CopySignalToMsgBuf(loc_pProtoEnvironmentData_t protoEnvData, LinProtoIf_pSignal_t   signal,
                                                                               LinProtoIf_pData_t          msgBuf,       LinProtoIf_BufLength_t msgBufLen,
                                                                               LinProtoIf_pCtxData_t       perFrmDescLstCtxData);
#endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

#if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline loc_eCopyProcessResult_t loc_CopySignalToMemory(loc_pProtoEnvironmentData_t protoEnvData, LinProtoIf_pSignal_t   signal,
                                                                               LinProtoIf_pData_t          msgBuf,       LinProtoIf_BufLength_t msgBufLen,
                                                                               LinProtoIf_pCtxData_t       perFrmDescLstCtxData);
#endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline void loc_SetCallbackData(loc_pProtoEnvironmentData_t protoEnvData, loc_pFrameInfo_t cbdata);

/* *****************************************************************************
 * Caller for LIN driver callbacks
 *
 ******************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline void loc_CallErrorCallback(loc_pProtoEnvironmentData_t protoEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline void loc_RestartCallback(loc_pProtoEnvironmentData_t protoEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline LinProtoIf_eMsgAction_t loc_CallFrameIdProcessedCallback(loc_pProtoEnvironmentData_t protoEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline LinProtoIf_eMeasDoneRetVal_t loc_CallMeasDoneCallback(loc_pProtoEnvironmentData_t protoEnvData, LinProtoIf_Baudrate_t exp_div,
                                                                                     LinProtoIf_Baudrate_t       prev_div,     LinProtoIf_Baudrate_t meas_div,
                                                                                     LinBusIf_BreakLen_t         meas_break_len);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline LinBusIf_eIdleAction_t loc_CallIdleCallback(loc_pProtoEnvironmentData_t protoEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline void loc_CallWakeupCallback(loc_pProtoEnvironmentData_t protoEnvData);

/* *****************************************************************************
 * Bus module callback functions
 *
 ******************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static void loc_BusErrorCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                  LinBusIf_Error_t             error,             LinBusIf_FrameID_t              frameID,
                                                  LinBusIf_pGenericCbCtxData_t genericBusCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static void loc_BusRestartCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                    LinBusIf_pGenericCbCtxData_t genericBusCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static LinBusIf_eMeasAction_t loc_BusMeasDoneCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                       LinBusIf_Baudrate_t          exp_baudrate,      LinBusIf_Baudrate_t             prev_baudrate,
                                                                       LinBusIf_Baudrate_t          meas_baudrate,     LinBusIf_BreakLen_t             meas_break_len,
                                                                       LinBusIf_pGenericCbCtxData_t genericBusCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static LinBusIf_eFrameIdAction_t loc_BusFrameIdReceivedCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                                 LinBusIf_FrameID_t           frameID,           LinBusIf_ppData_t               rxtx_buf,
                                                                                 LinBusIf_pBufLength_t           buf_len,           LinProtoIf_pCrcType_t           crcType,
                                                                                 LinBusIf_pGenericCbCtxData_t genericBusCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static LinBusIf_eMsgAction_t loc_BusReceiveDoneCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                         LinBusIf_FrameID_t           frameID,           LinBusIf_pData_t                rx_buf,
                                                                         LinBusIf_BufLength_t         buf_len,           LinBusIf_Error_t                error,
                                                                         LinBusIf_pGenericCbCtxData_t genericBusCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static LinBusIf_eMsgAction_t loc_BusTransmitDoneCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                          LinBusIf_FrameID_t           frameID,           LinBusIf_pData_t                tx_buf,
                                                                          LinBusIf_BufLength_t         buf_len,           LinBusIf_Error_t                error,
                                                                          LinBusIf_pGenericCbCtxData_t genericBusCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static LinBusIf_eIdleAction_t loc_BusIdleCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                   LinBusIf_pGenericCbCtxData_t genericBusCbCtxData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static void loc_BusWakeupCallback_t(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                     LinBusIf_pGenericCbCtxData_t genericBusCbCtxData);

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/
/* See justification at the file comment header */
// PRQA S 3218 ++
PROTO_LAYER_DATA static const LinBusIf_sCallbackFunctions_t loc_LinBusCallbacks =
{
    .CallbackVersion = LINBUS_INTERFACE_MODULE_API_VERSION,

    .Error         	 = &loc_BusErrorCallback,
    .Restart       	 = &loc_BusRestartCallback,
    .MeasDone        = &loc_BusMeasDoneCallback,
    .FrameIdReceived = &loc_BusFrameIdReceivedCallback,
    .ReceiveDone     = &loc_BusReceiveDoneCallback,
    .TransmitDone    = &loc_BusTransmitDoneCallback,
    .Idle            = &loc_BusIdleCallback,
    .Wakeup          = &loc_BusWakeupCallback_t
};
// PRQA S 3218 --

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINPROTOIMP_EXT_IFFUN_STRCT_ACCESS == 1
PROTO_LAYER_DATA const LinProtoIf_sInterfaceFunctions_t LinProtoImp_InterfaceFunctions =
#else
PROTO_LAYER_DATA static const LinProtoIf_sInterfaceFunctions_t LinProtoImp_InterfaceFunctions =
#endif
{
  .InterfaceVersion  = LINPROTO_INTERFACE_MODULE_API_VERSION,

  .Initialization     = &LinProtoImp_Initialization,
  .GetSubInterface    = &LinProtoImp_GetSubInterface,
  .Task               = &LinProtoImp_Task,
  .AppendCallbacks    = &LinProtoImp_AppendCallbacks,
  .RemoveCallbacks    = &LinProtoImp_RemoveCallbacks,
  .AddFrameDescLst    = &LinProtoImp_AddFrameDescriptionList,
  .RmvFrameDescLst    = &LinProtoImp_RemoveFrameDescriptionList,
};

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/***************************************************************************//**
 *  TODO: Concrete implementation description.
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline Lin_Bool_t loc_VerifyFrameDesciption(LinProtoIf_cpFrameDescription_t frameDesc, LinProtoIf_BufLength_t msgBufLen)
{
  Lin_Bool_t returnValue = FALSE;

  if((frameDesc->Lenght <= msgBufLen) &&
     (frameDesc->DefaultFrameID <= LIN_MAX_FRAMEID) &&
    ((frameDesc->CrcType == LinBusIf_CRC_Classic) ||
     (frameDesc->CrcType == LinBusIf_CRC_Extended)) && (
     #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
     (frameDesc->Kind == LinProtoIf_FrameKind_PUBLISH_WITH_SGNL_LST) ||
     (frameDesc->Kind == LinProtoIf_FrameKind_SUBSCRIBE_WITH_SGNL_LST) ||
     #endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */
     (frameDesc->Kind == LinProtoIf_FrameKind_PUBLISH_WITH_FUN) ||
     (frameDesc->Kind == LinProtoIf_FrameKind_SUBSCRIBE_WITH_FUN) ||
     (frameDesc->Kind == LinProtoIf_FrameKind_NON_BUFFERED)) &&
     #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
     (frameDesc->PtrToSgnlLst != LIN_NULL) &&
     #else
     (frameDesc->PtrToFun != LIN_NULL) &&
     #endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */
    ((frameDesc->FrameIdProcCbHandling == LinProtoIf_TxRxCbProc_DIRECT) ||
     (frameDesc->FrameIdProcCbHandling == LinProtoIf_TxRxCbProc_SCHEDULED)) &&
    ((frameDesc->FrameIdProcessedCbCtxType == LinProtoIf_FrmIdPrcCbCtxDataType_NONE) ||
     (frameDesc->FrameIdProcessedCbCtxType == LinProtoIf_FrmIdPrcCbCtxDataType_PER_CALLBACK) ||
     (frameDesc->FrameIdProcessedCbCtxType == LinProtoIf_FrmIdPrcCbCtxDataType_PER_DESCLST)))
  {
    returnValue = TRUE;
  }
  else{}

  return(returnValue);
}

#if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
/***************************************************************************//**
 *  NULL-pointer of 'signal' check already performed in
 *  'loc_VerifyFrameDesciption' function.
 *
 *  Length check of byte-arrays is omitted since frame lengths
 *  greater then 8 byte are supported.
 *
 *  TODO: Eventually replace magic number.
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline Lin_Bool_t loc_VerifySignalDefinition(LinProtoIf_cpSignal_t signal, LinProtoIf_FrameLenght_t frameLength)
{
  Lin_Bool_t returnValue = FALSE;

  if((signal->Length != 0u) &&
     (signal->PtrToData != LIN_NULL))
  {
    switch(signal->Type)
    {
      case LinProtoIf_SignalType_Scalar_MEM8:
      case LinProtoIf_SignalType_Scalar_MEM16:
      case LinProtoIf_SignalType_Scalar_FNC_WITH_OWN_CTX:
      case LinProtoIf_SignalType_Scalar_FNC_WITH_DESCLST_CTX:
        if((signal->Length <= 16u) &&
           ((signal->Offset + signal->Length) <= (frameLength << 3u)))
        {
          returnValue = TRUE;
        }
        break;
      case LinProtoIf_SignalType_ByteArray_MEM:
      case LinProtoIf_SignalType_ByteArray_FNC_WITH_OWN_CTX:
      case LinProtoIf_SignalType_ByteArray_FNC_WITH_DESCLST_CTX:
        /* Length check of byte-arrays is omitted since frame lengths
         *  greater then 8 byte are supported. */
        if((signal->Offset + signal->Length) <= frameLength)
        {
          returnValue = TRUE;
        }
        break;
      default:
        break;
    }

  }
  else{}

  return(returnValue);
}
#endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

#if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline loc_eCopyProcessResult_t loc_CopySignalToMsgBuf(loc_pProtoEnvironmentData_t protoEnvData, LinProtoIf_pSignal_t   signal,
                                                                               LinProtoIf_pData_t          msgBuf,       LinProtoIf_BufLength_t msgBufLen,
                                                                               LinProtoIf_pCtxData_t       perFrmDescLstCtxData)
{
  loc_eCopyProcessResult_t returnValue = loc_CPR_RUNNING;
  Lin_uint32_t             longSignal = 0;
  Lin_uint8_t              offset = signal->Offset;
  Lin_uint8_t              offsetModulu = offset & 7u; // Modulo 8 of bit offset
  Lin_uint8_t              byteOffset = offset >> 3u;  // Calculate byte offset in message buffer - / 8 == >> 3
  Lin_uint8_t              length = signal->Length;
  LinProtoIf_eSignalType_t signalType = signal->Type;
  Lin_uint32_t             tmpSignal;
  Lin_uint16_t             tempData;

  /* Pointer is checked in initialization against NULL */

  switch(signalType)
  {
    case LinProtoIf_SignalType_Scalar_MEM16:
    case LinProtoIf_SignalType_Scalar_MEM8:
    case LinProtoIf_SignalType_Scalar_FNC_WITH_OWN_CTX:
    case LinProtoIf_SignalType_Scalar_FNC_WITH_DESCLST_CTX:
      /* Check if byte offset is in range of the message buffer length */
      if(((LinProtoIf_BufLength_t) offset + (LinProtoIf_BufLength_t) length) <= (msgBufLen << 3u))
      {
        if(signalType == LinProtoIf_SignalType_Scalar_MEM16)
        {
          /* Copy data from pointer */
          longSignal = (Lin_uint32_t) *((LinProtoIf_pScalarDataMem16_t) signal->PtrToData);
        }
        else if(signalType == LinProtoIf_SignalType_Scalar_MEM8)
        {
          /* Copy data from pointer */
          longSignal = (Lin_uint32_t) *((LinProtoIf_pScalarDataMem8_t) signal->PtrToData);
        }
        else if(signalType == LinProtoIf_SignalType_Scalar_FNC_WITH_OWN_CTX)
        {
          /* Call function and save return value */
          /* NULL-pointer check of pointer to data/function is checked in 'loc_VerifySignalDefinition'. */
          /* See justification at the file comment header */
          // PRQA S 307 ++
          if((((LinProtoIf_pPublScalarDataFunction_t) signal->PtrToData) (protoEnvData, signal, &tempData, signal->CtxData)) == LIN_TRUE)
          // PRQA S 307 --
          {
            longSignal = tempData;
          }
          else
          {
            returnValue = loc_CPR_ERR;
          }
        }
        /** if(signalType == LinProtoIf_SignalType_Scalar_FNC_WITH_DESCLST_CTX) */
        else
        {
          /* Call function and save return value */
          /* NULL-pointer check of pointer to data/function is checked in 'loc_VerifySignalDefinition'. */
          /* See justification at the file comment header */
          // PRQA S 307 ++
          if((((LinProtoIf_pPublScalarDataFunction_t) signal->PtrToData) (protoEnvData, signal, &tempData, perFrmDescLstCtxData)) == LIN_TRUE)
          // PRQA S 307 --
          {
            longSignal = tempData;
          }
          else
          {
            returnValue = loc_CPR_ERR;
          }
        }

        /* Shift signal to fit bit offset with byte alignment */
        longSignal = longSignal << offsetModulu;

        /* Create bit mask for signal and invert it, because
         * unused bits in LIN messages have to be 1 */
        tmpSignal = (Lin_uint32_t) ((1ULL <<  length) - 1ULL);
        tmpSignal = tmpSignal << offsetModulu;

        /* Mask only unused bits in signal. */
        longSignal &= tmpSignal;

        /* Invert signal mask for remaining data. */
        tmpSignal = ~tmpSignal;

        /* Read out affected bytes out of message buffer
         * and join it with the inverted bit mask */
        /* May read out of message buffer boundary. Take care of message
         * buffer placement. */
        tmpSignal &= ((Lin_uint32_t) (msgBuf[byteOffset]))
                   | ((Lin_uint32_t) (msgBuf[byteOffset + 1u]) << 8u)
                   | ((Lin_uint32_t) (msgBuf[byteOffset + 2u]) << 16u);

        /* Join signal with data read out of message buffer */
        longSignal |= tmpSignal;

        /* Write signal to message buffer */
        msgBuf[byteOffset] = (LinProtoIf_Data_t) (longSignal & 0xFFu);
        if(msgBufLen > ((LinProtoIf_BufLength_t) byteOffset + 1u))
        {
          msgBuf[byteOffset + 1u] = (LinProtoIf_Data_t) ((longSignal >> 8u) & 0xFFu);
          if(msgBufLen > ((LinProtoIf_BufLength_t) byteOffset + 2u))
          {
           /* @misra{M3CM Rule-2.2. - PRQA Msg 2985,
            * Even though the AND'ing with 0xFF of the 3rd byte is not necessary
            * after the left shift of 16 bit, because the 4th byte is always zero\,
            * it's much more understand- and readable\, then skipping this instruction. ,
            * Could produce dead code if there is no compiler optimization.,
            * None.} */
            // PRQA S 2985 ++
            msgBuf[byteOffset + 2u] = (LinProtoIf_Data_t) ((longSignal >> 16u) & 0xFFu);
            // PRQA S 2985 --
          }
          else{}
        }
        else{}
      }
      else{}
      break;

    case LinProtoIf_SignalType_ByteArray_MEM:
    case LinProtoIf_SignalType_ByteArray_FNC_WITH_OWN_CTX:
    case LinProtoIf_SignalType_ByteArray_FNC_WITH_DESCLST_CTX:
      /* Check if byte offset is in range of the message buffer length */
      if(((LinProtoIf_BufLength_t) offset + (LinProtoIf_BufLength_t) length) <= msgBufLen)
      {
        if(signalType == LinProtoIf_SignalType_ByteArray_MEM)
        {
          /* Copy data from pointer to message buffer*/
          Lin_uint8_t i;
          for(i = 0; i < length; i++)
          {
            msgBuf[offset + i] = ((Lin_uint8_t*) signal->PtrToData)[i];
          }
        }
        else if(signalType == LinProtoIf_SignalType_ByteArray_FNC_WITH_OWN_CTX)
        {
          /* Call function and pass buffer as parameter */
          /* See justification at the file comment header */
          // PRQA S 307 ++
          if((((LinProtoIf_pByteArrayDataFunction_t) signal->PtrToData) (protoEnvData, signal, &(msgBuf[offset]), length, signal->CtxData)) != LIN_TRUE)
          // PRQA S 307 --
          {
            returnValue = loc_CPR_ERR;
          }
          else{}
        }
        /** if(signalType == LinProtoIf_SignalType_ByteArray_FNC_WITH_DESCLST_CTX) */
        else 
        {
          /* Call function and pass buffer as parameter */
          /* See justification at the file comment header */
          // PRQA S 307 ++
          if((((LinProtoIf_pByteArrayDataFunction_t) signal->PtrToData) (protoEnvData, signal, &(msgBuf[offset]), length, perFrmDescLstCtxData)) != LIN_TRUE)
          // PRQA S 307 --
          {
            returnValue = loc_CPR_ERR;
          }
          else{}
        }
      }
      else{}
      break;

    case LinProtoIf_SignalType_Invalid:
      returnValue = loc_CPR_OK;
      break;

    default:
      break;
  }

  return(returnValue);
}
#endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

#if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline loc_eCopyProcessResult_t loc_CopySignalToMemory(loc_pProtoEnvironmentData_t protoEnvData, LinProtoIf_pSignal_t   signal,
                                                                               LinProtoIf_pData_t          msgBuf,       LinProtoIf_BufLength_t msgBufLen,
                                                                               LinProtoIf_pCtxData_t       perFrmDescLstCtxData)
{
  loc_eCopyProcessResult_t returnValue = loc_CPR_RUNNING;
  Lin_uint8_t              offset = signal->Offset;
  Lin_uint8_t              offsetModulu = offset & 7u; // Modulo 8 of bit offset
  Lin_uint8_t              byteOffset = offset >> 3u;  // Calculate byte offset in message buffer - / 8 == >> 3
  Lin_uint8_t              length = signal->Length;
  LinProtoIf_eSignalType_t signalType = signal->Type;
  Lin_uint32_t             tmpSignal = 0 ;

  /* Pointer is checked in initialization against NULL */
  switch(signalType)
  {
    case LinProtoIf_SignalType_Scalar_MEM16:
    case LinProtoIf_SignalType_Scalar_MEM8:
    case LinProtoIf_SignalType_Scalar_FNC_WITH_OWN_CTX:
    case LinProtoIf_SignalType_Scalar_FNC_WITH_DESCLST_CTX:
      /* Check if byte offset is in range of the message buffer length */
      if(((LinProtoIf_BufLength_t) offset + (LinProtoIf_BufLength_t) length) <= (msgBufLen << 3u))
      {
        /* Read out affected bytes out of message buffer */
        tmpSignal = (Lin_uint32_t) (msgBuf[byteOffset]);
        tmpSignal |= (Lin_uint32_t) (msgBuf[byteOffset + 1u]) << 8u;
        tmpSignal |= (Lin_uint32_t) (msgBuf[byteOffset + 2u]) << 16u;

        /* Shift signal to bit offset 0 */
        tmpSignal = tmpSignal >> offsetModulu;

        /* AND with bit mask */
        tmpSignal &= (Lin_uint32_t) ((1ULL << length) - 1ULL);

        if(signalType == LinProtoIf_SignalType_Scalar_MEM16)
        {
          /* Copy data to pointer */
          *((LinProtoIf_pScalarDataMem16_t) signal->PtrToData) = (Lin_uint16_t) tmpSignal;
        }
        else if(signalType == LinProtoIf_SignalType_Scalar_MEM8)
        {
          /* Copy data to pointer */
          *((LinProtoIf_pScalarDataMem8_t) signal->PtrToData) = (Lin_uint8_t) tmpSignal;
        }
        else if(signalType == LinProtoIf_SignalType_Scalar_FNC_WITH_OWN_CTX)
        {
          /* Call function and save return value */
          /* See justification at the file comment header */
          // PRQA S 307 ++
          if((((LinProtoIf_pSubscrScalarDataFunction_t) signal->PtrToData) (protoEnvData, signal, (Lin_uint16_t) tmpSignal, signal->CtxData)) != LIN_TRUE)
          // PRQA S 307 --
          {
            returnValue = loc_CPR_ERR;
          }
          else{}
        }
        /* if(signalType == LinProtoIf_SignalType_Scalar_FNC_WITH_DESCLST_CTX) */
        else 
        {
          /* Call function and save return value */
          /* See justification at the file comment header */
          // PRQA S 307 ++
          if((((LinProtoIf_pSubscrScalarDataFunction_t) signal->PtrToData) (protoEnvData, signal, (Lin_uint16_t) tmpSignal, perFrmDescLstCtxData)) != LIN_TRUE)
          // PRQA S 307 --
          {
            returnValue = loc_CPR_ERR;
          }
          else{}
        }
      }
      break;

    case LinProtoIf_SignalType_ByteArray_MEM:
    case LinProtoIf_SignalType_ByteArray_FNC_WITH_OWN_CTX:
    case LinProtoIf_SignalType_ByteArray_FNC_WITH_DESCLST_CTX:
      /* Check if byte offset is in range of the message buffer length */
      if(((LinProtoIf_BufLength_t) offset + (LinProtoIf_BufLength_t) length) <= msgBufLen)
      {
        if(signalType == LinProtoIf_SignalType_ByteArray_MEM)
        {
          /* Copy data from pointer to message buffer*/
          Lin_uint8_t i;
          for(i = 0; i < length; i++)
          {
            ((Lin_uint8_t*) signal->PtrToData)[i] = msgBuf[offset + i];
          }
        }
        else if(signalType == LinProtoIf_SignalType_ByteArray_FNC_WITH_OWN_CTX)
        {
          /* Call function and pass buffer as parameter */
          /* See justification at the file comment header */
          // PRQA S 307 ++
          if((((LinProtoIf_pByteArrayDataFunction_t) signal->PtrToData) (protoEnvData, signal, &(msgBuf[offset]), length, signal->CtxData)) != LIN_TRUE)
          // PRQA S 307 --
          {
            returnValue = loc_CPR_ERR;
          }
          else{}
        }
        /* if(signalType == LinProtoIf_SignalType_ByteArray_FNC_WITH_DESCLST_CTX) */
        else 
        {
          /* Call function and pass buffer as parameter */
          /* See justification at the file comment header */
          // PRQA S 307 ++
          if((((LinProtoIf_pByteArrayDataFunction_t) signal->PtrToData) (protoEnvData, signal, &(msgBuf[offset]), length, perFrmDescLstCtxData)) != LIN_TRUE)
          // PRQA S 307 --
          {
            returnValue = loc_CPR_ERR;
          }
          else{}
        }
      }
      break;
    
    case LinProtoIf_SignalType_Invalid:
      returnValue = loc_CPR_OK;
      break;

    default:
      break;
  }

  return(returnValue);
}
#endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline void loc_SetCallbackData(loc_pProtoEnvironmentData_t protoEnvData, loc_pFrameInfo_t cbdata)
{
  (*cbdata).FrameID   = protoEnvData->ActlFrameInfo.FrameID;
  (*cbdata).FrameDesc = protoEnvData->ActlFrameInfo.FrameDesc;
  (*cbdata).Error     = protoEnvData->ActlFrameInfo.Error;
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline void loc_CallErrorCallback(loc_pProtoEnvironmentData_t protoEnvData)
{
  Lin_uint8_t i;

  for(i = 0; i < LINPROTO_INTERFACE_MAX_CALLBACK_TABLES; i++)
  {
    const LinProtoIf_cpCallbackFunctions_t callbacks = protoEnvData->Callbacks[i];
    if(callbacks != LIN_NULL)
    {
      const LinProtoIf_ErrorCbFun_t fun = callbacks->Error;
      if(fun != LIN_NULL)
      {
        fun(protoEnvData, &LinProtoImp_InterfaceFunctions, protoEnvData->ErrCallbackData.Error, protoEnvData->ErrCallbackData.FrameID, protoEnvData->ErrCallbackData.FrameDesc, protoEnvData->CallbackCtxData[i]);
      }
      else{}
    }
    else{}
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline void loc_RestartCallback(loc_pProtoEnvironmentData_t protoEnvData)
{
  Lin_uint8_t i;

  for(i = 0; i < LINPROTO_INTERFACE_MAX_CALLBACK_TABLES; i++)
  {
    const LinProtoIf_cpCallbackFunctions_t callbacks = protoEnvData->Callbacks[i];
    if(callbacks != LIN_NULL)
    {
      const LinProtoIf_RestartCbFun_t fun = callbacks->Restart;
      if (fun != LIN_NULL)
      {
        fun(protoEnvData, &LinProtoImp_InterfaceFunctions, protoEnvData->CallbackCtxData[i]);
      }
      else{}
    }
    else{}
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline LinProtoIf_eMsgAction_t loc_CallFrameIdProcessedCallback(loc_pProtoEnvironmentData_t protoEnvData)
{
  const LinProtoIf_cpFrameDescription_t frameDesc = protoEnvData->RxTxCallbackData.FrameDesc;
  const LinProtoIf_FrameIdProcCbFun_t fun = frameDesc->FrameIdProcessedCallbackFnc;
  LinProtoIf_eMsgAction_t retVal = LinBusIf_MSGACT_CONTINUNE;
  LinProtoIf_pGenericCbCtxData_t cbCtx;
  
  if (fun != LIN_NULL)
  {
    switch(frameDesc->FrameIdProcessedCbCtxType)
    {
      case LinProtoIf_FrmIdPrcCbCtxDataType_PER_CALLBACK:
        cbCtx = frameDesc->FrameIdProcessedCallbackCtx;
        break;
      case LinProtoIf_FrmIdPrcCbCtxDataType_PER_DESCLST:
        cbCtx = protoEnvData->LookupIfFuncTbl->GetPerFrmDescLstCbCtxData(protoEnvData->LookupEnvData, frameDesc->FrameIdProcessedCallbackCtx);
        break;
      case LinProtoIf_FrmIdPrcCbCtxDataType_NONE:
      default:
        cbCtx = LIN_NULL;
        break;
    }

    retVal = fun(protoEnvData, &LinProtoImp_InterfaceFunctions, protoEnvData->RxTxCallbackData.FrameID, frameDesc, protoEnvData->RxTxCallbackData.Error, cbCtx);
  }
  else{}

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline LinProtoIf_eMeasDoneRetVal_t loc_CallMeasDoneCallback(loc_pProtoEnvironmentData_t protoEnvData, LinProtoIf_Baudrate_t exp_div,
                                                                                     LinProtoIf_Baudrate_t       prev_div,     LinProtoIf_Baudrate_t meas_div,
                                                                                     LinBusIf_BreakLen_t         meas_break_len)
{
  LinProtoIf_eMeasDoneRetVal_t returnValue = LinProtoIf_MeasDoneRetVal_DISMISS;
  Lin_uint8_t i;

  for(i = 0; i < LINPROTO_INTERFACE_MAX_CALLBACK_TABLES; i++)
  {
    const LinProtoIf_cpCallbackFunctions_t callbacks = protoEnvData->Callbacks[i];
    if(callbacks != LIN_NULL)
    {
      const LinProtoIf_MeasDoneCbFun_t fun = callbacks->MeasDone;
      if (fun != LIN_NULL)
      {
        returnValue = fun(protoEnvData, &LinProtoImp_InterfaceFunctions, exp_div, prev_div, meas_div, meas_break_len, protoEnvData->CallbackCtxData[i]);
        if(returnValue != LinProtoIf_MeasDoneRetVal_DOESNT_CARE)
        {
          break;
        }
      }
      else{}
    }
    else{}
  }

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline LinBusIf_eIdleAction_t loc_CallIdleCallback(loc_pProtoEnvironmentData_t protoEnvData)
{
  LinBusIf_eIdleAction_t retVal = LinBusIf_IDLEACT_CONTINUNE;
  Lin_uint8_t i;
  
  for(i = 0; i < LINPROTO_INTERFACE_MAX_CALLBACK_TABLES; i++)
  {
    const LinProtoIf_cpCallbackFunctions_t callbacks = protoEnvData->Callbacks[i];
    if(callbacks != LIN_NULL)
    {
      const LinProtoIf_IdleCbFun_t fun = callbacks->Idle;
      if (fun != LIN_NULL)
      {
        if(fun(protoEnvData, &LinProtoImp_InterfaceFunctions, protoEnvData->CallbackCtxData[i]) == LinBusIf_IDLEACT_SLEEP)
        {
          retVal = LinBusIf_IDLEACT_SLEEP;
        }
        else{}
      }
      else{}
    }
    else{}
  }

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE static inline void loc_CallWakeupCallback(loc_pProtoEnvironmentData_t protoEnvData)
{
  Lin_uint8_t i;
  for(i = 0; i < LINPROTO_INTERFACE_MAX_CALLBACK_TABLES; i++)
  {
    const LinProtoIf_cpCallbackFunctions_t callbacks = protoEnvData->Callbacks[i];
    if(callbacks != LIN_NULL)
    {
      const LinProtoIf_WakeupCbFun_t fun = callbacks->WakeUp;
      if (fun != LIN_NULL)
      {
        fun(protoEnvData, &LinProtoImp_InterfaceFunctions, protoEnvData->CallbackCtxData[i]);
      }
      else{}
    }
    else{}
  }
}

/***************************************************************************//**
 * 'protoEnvData/genericBusCbCtxData' is not tested against NULL,
 * because the BUS layer get's this pointer from the higher layer.
 * It does not work on that data, but passes it back to the
 * higher layer in this callback functions.
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
PROTO_LAYER_CODE static void loc_BusErrorCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                  LinBusIf_Error_t             error,             LinBusIf_FrameID_t              frameID,
                                                  LinBusIf_pGenericCbCtxData_t genericBusCbCtxData)
// PRQA S 3206 --
{
  loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericBusCbCtxData;

  if(protoEnvData->TaskControl.Flags.ErrorCbRecvd != 1u)
  {
    protoEnvData->ActlFrameInfo.Error = error;
    loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
    protoEnvData->TaskControl.Flags.ErrorCbRecvd = 1u;
  }
  else{}
}

/***************************************************************************//**
 * 'protoEnvData/genericBusCbCtxData' is not tested against NULL,
 * because the BUS layer get's this pointer from the higher layer.
 * It does not work on that data, but passes it to the back to the
 * higher layer in this callback functions.
 *

 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
PROTO_LAYER_CODE static void loc_BusRestartCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                    LinBusIf_pGenericCbCtxData_t genericBusCbCtxData)
// PRQA S 3206 --
{
  loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericBusCbCtxData;

  if(protoEnvData->TaskControl.Flags.RestartCbRecvd != 1u)
  {
    protoEnvData->TaskControl.Flags.RestartCbRecvd = 1u;
  }
  else
  {
    protoEnvData->ErrCallbackData.Error = LinProtoIf_ERR_TSK_TO_SLOW;
    loc_CallErrorCallback(genericBusCbCtxData);
  }
}


/***************************************************************************//**
 * This function can't be scheduled because the the BUS layer needs
 * the decision about how to proceed with the auto-measured baud-rate
 * Immediately.
 *
 * 'protoEnvData/genericBusCbCtxData' is not tested against NULL,
 * because the BUS layer get's this pointer from the higher layer.
 * It does not work on that data, but passes it to the back to the
 * higher layer in this callback functions.
 * 
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
PROTO_LAYER_CODE static LinBusIf_eMeasAction_t loc_BusMeasDoneCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                       LinBusIf_Baudrate_t          exp_baudrate,      LinBusIf_Baudrate_t             prev_baudrate,
                                                                       LinBusIf_Baudrate_t          meas_baudrate,     LinBusIf_BreakLen_t             meas_break_len,
                                                                       LinBusIf_pGenericCbCtxData_t genericBusCbCtxData)
// PRQA S 3206 --
{
  /**
   * @misra{M3CM Dir-10.5. - PRQA Msg 4322,
   * To ensure module independence every module defines his own data types\,
   * because of overlaps of some types\, a redefinition of some types is
   * necessary. This enum is such a redefinition.,
   * Not traceable code and unexpected behavior.,
   * The enums a basically identical, because it is just a redefinition.}
   */
  // PRQA S 4322 ++
  return((LinBusIf_eMeasAction_t) loc_CallMeasDoneCallback(genericBusCbCtxData, exp_baudrate, prev_baudrate, meas_baudrate, meas_break_len));
  // PRQA S 4322 --
}

/***************************************************************************//**
 * 'protoEnvData/genericBusCbCtxData' is not tested against NULL,
 * because the BUS layer get's this pointer from the higher layer.
 * It does not work on that data, but passes it to the back to the
 * higher layer in this callback functions.
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
PROTO_LAYER_CODE static LinBusIf_eFrameIdAction_t loc_BusFrameIdReceivedCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                                 LinBusIf_FrameID_t           frameID,           LinBusIf_ppData_t               rxtx_buf,
                                                                                 LinBusIf_pBufLength_t        buf_len,           LinProtoIf_pCrcType_t           crcType,
                                                                                 LinBusIf_pGenericCbCtxData_t genericBusCbCtxData)
// PRQA S 3206--
{
  loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericBusCbCtxData;
  LinBusIf_eFrameIdAction_t   returnValue = LinBusIf_PIDACT_IGNORE;
	
  if(frameID != LIN_INVALID_FRAMEID)
  {
    LinProtoIf_cpFrameDescription_t frameDesc = protoEnvData->LookupIfFuncTbl->GetFrameDescription(protoEnvData->LookupEnvData, frameID);

    if(frameDesc != LIN_NULL)
    {
      /* Save  Frame ID and pointer to Frame Description in environment data. */
      protoEnvData->ActlFrameInfo.FrameID   = frameID;
      protoEnvData->ActlFrameInfo.FrameDesc = frameDesc;
      protoEnvData->ActlFrameInfo.Error     = LinProtoIf_ERR_NO_ERROR;
      
      if((protoEnvData->TaskControl.Byte & LOC_RXRX_CB_DATA_MSK) == 0u)
      {
        #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
        /* 'signalLst' can't be NULL, because this is checked
         * in 'loc_VerifyFrameDesciption' function */
        LinProtoIf_pSignal_t signalLst = frameDesc->PtrToSgnlLst;
        #endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */
  
        /* NULL-pointer checks */
        if(crcType != LIN_NULL)
        {
          *crcType = frameDesc->CrcType;
        }
        else{}
  
        if (frameDesc->Kind != LinProtoIf_FrameKind_NON_BUFFERED)
	      {							
	        if(rxtx_buf != LIN_NULL)
	        {
	          *rxtx_buf = protoEnvData->MsgBuf;
	        }
	        else{}
  
          if(buf_len != LIN_NULL)
          {
            *buf_len = frameDesc->Lenght;
          }
          else{}
        }
  
        switch(frameDesc->Kind)
        {
          #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
          case LinProtoIf_FrameKind_PUBLISH_WITH_SGNL_LST:
            {
              loc_eCopyProcessResult_t cprResult;
              Lin_uint8_t i = 0;
  
              /* Reset the frame length in message buffer with 0xFF, because default
               * value for unused bits in a LIN message is 1*/
              el_FillBytes(0xFF, (Lin_uint8_t*) protoEnvData->MsgBuf, frameDesc->Lenght);
  
              do
              {
                cprResult = loc_CopySignalToMsgBuf(genericBusCbCtxData, &signalLst[i], protoEnvData->MsgBuf, frameDesc->Lenght,
                                                   protoEnvData->LookupIfFuncTbl->GetPerFrmDescLstCbCtxData(protoEnvData->LookupEnvData, frameDesc->FrameIdProcessedCallbackCtx));
                i++;
              }while(cprResult == loc_CPR_RUNNING);
  
              if(cprResult == loc_CPR_OK)
              {
                returnValue = LinBusIf_PIDACT_TRANSMIT;
              }
              else
              {
                /* Set error code and callback data. */
                protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_SGN_COPY;
                loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
                loc_SetCallbackData(protoEnvData, &(protoEnvData->RxTxCallbackData));
                            
                protoEnvData->TaskControl.Flags.ReceiveDoneCbRecvd = 1;         
                
                /* Call error callback */
                loc_CallErrorCallback(protoEnvData);
                
                if(frameDesc->FrameIdProcCbHandling == LinProtoIf_TxRxCbProc_DIRECT)
                {
                  loc_CallFrameIdProcessedCallback(genericBusCbCtxData);
                  protoEnvData->TaskControl.Flags.ReceiveDoneCbRecvd = 0;
                }
                else if(frameDesc->FrameIdProcCbHandling == LinProtoIf_TxRxCbProc_SCHEDULED){}
                else{}
  
                returnValue = LinBusIf_PIDACT_IGNORE;
              }
            }
            break;

  
          case LinProtoIf_FrameKind_SUBSCRIBE_WITH_SGNL_LST:
            returnValue = LinBusIf_PIDACT_RECEIVE;
            break;
          #endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */
  
          case LinProtoIf_FrameKind_PUBLISH_WITH_FUN:
            /* The function has to take care by his self,  *
             * that the message buffer is reset to default */
            /**
             * @misra{M3CM Dir-9.1. - PRQA Msg 2961,
             * QAC is making a mistake here. The variable "signalLst" is set to "frameDesc->PtrToSgnlLst"
             * after "frameDesc" has been checked against NULL and "frameDesc->PtrToSgnlLst" is not NULL\,
             * because it is checked against NULL in the local function "loc_VerifyFrameDesciption". So
             * "signalLst" is definitely not uninitialized and not NULL.,
             * None.,
             * None.}
             */
            // PRQA S 2961 ++
            /* See justification at the file comment header */
            // PRQA S 307 ++
            #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
            returnValue = ((LinProtoIf_pBuildFrameFun_t) signalLst)((LinProtoIf_pGenericEnvData_t) protoEnvData, frameID, frameDesc, protoEnvData->MsgBuf, frameDesc->Lenght,
                                                                    frameDesc->FrameIdProcessedCallbackCtx);
            #else
            returnValue = ((LinProtoIf_pBuildFrameFun_t) frameDesc->PtrToFun)((LinProtoIf_pGenericEnvData_t) protoEnvData, frameID, frameDesc, protoEnvData->MsgBuf, frameDesc->Lenght,
                                                                              frameDesc->FrameIdProcessedCallbackCtx);
            #endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */
            // PRQA S 307 --
            // PRQA S 2961 --
            break;
  
          case LinProtoIf_FrameKind_SUBSCRIBE_WITH_FUN:
            returnValue = LinBusIf_PIDACT_RECEIVE;
            break;
				
        case LinProtoIf_FrameKind_NON_BUFFERED:
          /* The function basically forwards bus-layer interface */
          #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
          returnValue = ((LinProtoIf_pBuildNonBufferedFrameFun_t) signalLst)((LinProtoIf_pGenericEnvData_t) protoEnvData, frameID, frameDesc, rxtx_buf, buf_len,
                                                                             frameDesc->FrameIdProcessedCallbackCtx);
          #else
          returnValue = ((LinProtoIf_pBuildNonBufferedFrameFun_t) frameDesc->PtrToFun)((LinProtoIf_pGenericEnvData_t) protoEnvData, frameID, frameDesc, rxtx_buf, buf_len,
                                                                                       frameDesc->FrameIdProcessedCallbackCtx);
          #endif
          break;
  
          default:
            break;
        }
      }
      else
      {
        protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_TSK_TO_SLOW;
        loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
        loc_CallErrorCallback(genericBusCbCtxData);
      }
    }
    else{}
  }
  else {}
  
  return(returnValue);
}

/***************************************************************************//**
 * 'protoEnvData/genericBusCbCtxData' is not tested against NULL,
 * because the BUS layer get's this pointer from the higher layer.
 * It does not work on that data, but passes it to the back to the
 * higher layer in this callback functions.
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
PROTO_LAYER_CODE static LinBusIf_eMsgAction_t loc_BusReceiveDoneCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                         LinBusIf_FrameID_t           frameID,           LinBusIf_pData_t                rx_buf,
                                                                         LinBusIf_BufLength_t         buf_len,           LinBusIf_Error_t                error,
                                                                         LinBusIf_pGenericCbCtxData_t genericBusCbCtxData)
// PRQA S 3206 --
{
  loc_pProtoEnvironmentData_t  protoEnvData = (loc_pProtoEnvironmentData_t) genericBusCbCtxData;
  LinBusIf_eMsgAction_t retVal = LinBusIf_MSGACT_CONTINUNE;
  
  if((protoEnvData->TaskControl.Byte & LOC_RXRX_CB_DATA_MSK) == 0u)
  {
    if(frameID != LIN_INVALID_FRAMEID)
    {
      LinProtoIf_cpFrameDescription_t frameDesc = protoEnvData->ActlFrameInfo.FrameDesc;
      loc_eCopyProcessResult_t cprResult = loc_CPR_ERR;

      if(frameDesc != LIN_NULL)
      {
        #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
        /* 'signalLst' can't be NULL, because this checked
         * in 'loc_VerifyFrameDesciption' function */
        LinProtoIf_pSignal_t signalLst = frameDesc->PtrToSgnlLst;
        #endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

        switch(frameDesc->Kind)
        {
          #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
          case LinProtoIf_FrameKind_SUBSCRIBE_WITH_SGNL_LST:
          {
            Lin_uint8_t i = 0;

            if((rx_buf != LIN_NULL) && (buf_len > 0u))
            {
              do
              {
                cprResult = loc_CopySignalToMemory(genericBusCbCtxData, &signalLst[i], rx_buf, buf_len, protoEnvData->LookupIfFuncTbl->GetPerFrmDescLstCbCtxData(protoEnvData->LookupEnvData, frameDesc->FrameIdProcessedCallbackCtx));
                i++;
              }while(cprResult == loc_CPR_RUNNING);
            }
            else{}
          }
          break;
          #endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

          case LinProtoIf_FrameKind_SUBSCRIBE_WITH_FUN:
          {
            /**
             * @misra{M3CM Dir-9.1. - PRQA Msg 2961,
             * QAC is making a mistake here. The variable "signalLst" is set to "frameDesc->PtrToSgnlLst"
             * after "frameDesc" has been checked against NULL and "frameDesc->PtrToSgnlLst" is not NULL\,
             * because it is checked against NULL in the local function "loc_VerifyFrameDesciption". So
             * "signalLst" is definitely not uninitialized and not NULL.,
             * None.,
             * None.}
             */
            // PRQA S 2961 ++
            /* See justification at the file comment header */
            // PRQA S 307 ++
            #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
            ((LinProtoIf_pBuildFrameFun_t) signalLst)((LinProtoIf_pGenericEnvData_t) protoEnvData, frameID, frameDesc, rx_buf, buf_len, frameDesc->FrameIdProcessedCallbackCtx);
            #else
            ((LinProtoIf_pBuildFrameFun_t) frameDesc->PtrToFun)((LinProtoIf_pGenericEnvData_t) protoEnvData, frameID, frameDesc, rx_buf, buf_len, frameDesc->FrameIdProcessedCallbackCtx);
            #endif
            // PRQA S 307 --
            // PRQA S 2961 --
            cprResult = loc_CPR_OK;
          }
          break;

          default:
          break;
        }
      }
      else{}

      if(cprResult == loc_CPR_ERR)
      {
        protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_SGN_COPY;
        loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
        loc_CallErrorCallback(protoEnvData);
      }
      else{}

      protoEnvData->TaskControl.Flags.ReceiveDoneCbRecvd = 1;
      loc_SetCallbackData(protoEnvData, &(protoEnvData->RxTxCallbackData));

      if(frameDesc->FrameIdProcCbHandling == LinProtoIf_TxRxCbProc_DIRECT)
      {
        retVal = loc_CallFrameIdProcessedCallback(genericBusCbCtxData);
        protoEnvData->TaskControl.Flags.ReceiveDoneCbRecvd = 0;
      }
      else if(frameDesc->FrameIdProcCbHandling == LinProtoIf_TxRxCbProc_SCHEDULED){}
      else{}
    }
    else{}
  }
  else
  {
    protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_TSK_TO_SLOW;
    loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
    loc_CallErrorCallback(genericBusCbCtxData);
  }

  /* Reset data about current processed Frame ID and Frame Description */
  protoEnvData->ActlFrameInfo.FrameID   = LIN_INVALID_FRAMEID;
  protoEnvData->ActlFrameInfo.FrameDesc = LIN_NULL;
  protoEnvData->ActlFrameInfo.Error     = LinProtoIf_ERR_NO_ERROR;

  return(retVal);
}

/***************************************************************************//**
 * 'protoEnvData/genericBusCbCtxData' is not tested against NULL,
 * because the BUS layer get's this pointer from the higher layer.
 * It does not work on that data, but passes it to the back to the
 * higher layer in this callback functions.
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
PROTO_LAYER_CODE static LinBusIf_eMsgAction_t loc_BusTransmitDoneCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                          LinBusIf_FrameID_t           frameID,           LinBusIf_pData_t                tx_buf,
                                                                          LinBusIf_BufLength_t         buf_len,           LinBusIf_Error_t                error,
                                                                          LinBusIf_pGenericCbCtxData_t genericBusCbCtxData)
// PRQA S 3206 --
{
  loc_pProtoEnvironmentData_t  protoEnvData = (loc_pProtoEnvironmentData_t) genericBusCbCtxData;
  LinBusIf_eMsgAction_t retVal = LinBusIf_MSGACT_CONTINUNE;
  
  if((protoEnvData->TaskControl.Byte & LOC_RXRX_CB_DATA_MSK) == 0u)
  {
    if(frameID != LIN_INVALID_FRAMEID)
    {
      LinProtoIf_cpFrameDescription_t frameDesc = protoEnvData->ActlFrameInfo.FrameDesc;

      if(frameDesc != LIN_NULL)
      {
        protoEnvData->TaskControl.Flags.TransmitDoneCbRecvd = 1;
        loc_SetCallbackData(protoEnvData, &(protoEnvData->RxTxCallbackData));
        
        if(frameDesc->FrameIdProcCbHandling == LinProtoIf_TxRxCbProc_DIRECT)
        {
          retVal = loc_CallFrameIdProcessedCallback(genericBusCbCtxData);
          protoEnvData->TaskControl.Flags.TransmitDoneCbRecvd = 0;
        }
        else if(frameDesc->FrameIdProcCbHandling == LinProtoIf_TxRxCbProc_SCHEDULED){} 
        else{}
      }
      else{}
    }
    else{}
  }
  else
  {
    protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_TSK_TO_SLOW;
    loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
    loc_CallErrorCallback(genericBusCbCtxData);
  }

  /* Reset data about current processed Frame ID and Frame Description */
  protoEnvData->ActlFrameInfo.FrameID   = LIN_INVALID_FRAMEID;
  protoEnvData->ActlFrameInfo.FrameDesc = LIN_NULL;
  protoEnvData->ActlFrameInfo.Error     = LinProtoIf_ERR_NO_ERROR;

  return(retVal);
}

/***************************************************************************//**
 * 'protoEnvData/genericBusCbCtxData' is not tested against NULL,
 * because the BUS layer get's this pointer from the higher layer.
 * It does not work on that data, but passes it to the back to the
 * higher layer in this callback functions.
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
PROTO_LAYER_CODE static LinBusIf_eIdleAction_t loc_BusIdleCallback(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                                   LinBusIf_pGenericCbCtxData_t genericBusCbCtxData)
// PRQA S 3206 --
{
  loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericBusCbCtxData;

  if(protoEnvData->TaskControl.Flags.IdleCbRecvd != 1u)
  {
    protoEnvData->TaskControl.Flags.IdleCbRecvd = 1u;
  }
  else
  {
    protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_TSK_TO_SLOW;
    loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
    loc_CallErrorCallback(genericBusCbCtxData);
  }

  return(LinBusIf_IDLEACT_CONTINUNE);
}

/***************************************************************************//**
 * 'protoEnvData/genericBusCbCtxData' is not tested against NULL,
 * because the BUS layer get's this pointer from the higher layer.
 * It does not work on that data, but passes it to the back to the
 * higher layer in this callback functions.
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
PROTO_LAYER_CODE static void loc_BusWakeupCallback_t(LinBusIf_pGenericEnvData_t   genericBusEnvData, LinBusIf_cpInterfaceFunctions_t busIfFuns,
                                                     LinBusIf_pGenericCbCtxData_t genericBusCbCtxData)
// PRQA S 3206 --
{
  loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericBusCbCtxData;

  if(protoEnvData->TaskControl.Flags.WakupCbRecvd != 1u)
  {
    protoEnvData->TaskControl.Flags.WakupCbRecvd = 1;
  }
  else
  {
    protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_TSK_TO_SLOW;
    loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
    loc_CallErrorCallback(genericBusCbCtxData);
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE Lin_Bool_t LinProtoImp_Initialization(LinProtoIf_pGenericEnvData_t     genericProtoEnvData, LinLookupIf_EnvDataSze_t       protoEnvDataSze,
                                                       LinProtoIf_cpCallbackFunctions_t protoCbFuns,         LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData,
                                                       LinProtoIf_pGenericImpCfgData_t  genericProtoImpCfgData)
{
  Lin_Bool_t returnValue = FALSE;
  loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t)  genericProtoEnvData;
  LinProtoImp_pCfgData_t protoImpCfgData = (LinProtoImp_pCfgData_t) genericProtoImpCfgData;

  if ((protoEnvData != LIN_NULL) &&
      (protoEnvDataSze >= (sizeof(loc_sProtoEnvironmentData_t) + LINPROTOIMP_MIN_LIN_MSG_SZE)) &&
      (protoCbFuns != LIN_NULL) &&
      (genericProtoImpCfgData != LIN_NULL) &&
      (protoCbFuns->CallbackVersion == LINPROTO_INTERFACE_MODULE_API_VERSION) &&
      (protoImpCfgData->LinBusIfInitParam.IfFunsTbl != LIN_NULL) &&
      (protoImpCfgData->LinLookupIfInitParam.IfFunsTbl != LIN_NULL) &&
      (protoImpCfgData->LinBusIfInitParam.IfFunsTbl->InterfaceVersion == LINBUS_INTERFACE_MODULE_API_VERSION) &&
      (protoImpCfgData->LinLookupIfInitParam.IfFunsTbl->InterfaceVersion == LINLOOKUP_INTERFACE_MODULE_API_VERSION))
  {
    /* Initialize LIN driver environment data with 0 */
    /* See justification at the file comment header */
    // PRQA S 310 ++
    el_FillBytes(0x00, (uint8_t*) protoEnvData, sizeof(loc_sProtoEnvironmentData_t));
    // PRQA S 310 --

    protoEnvData->TaskControl.Byte = 0;

    if(LinProtoImp_AppendCallbacks(genericProtoEnvData, protoCbFuns, genericProtoCbCtxData) == LIN_TRUE)
    {
      LinProtoIf_BufLength_t msgBufLen = 0;
     /**
      * @misra{M3CM Rule-18.4. - PRQA Msg 488,
      * This arithmetic on a pointer value is necessary to support variable message buffer lengths
      * for the LIN driver proto layer. The pointer is set to the first address behind the environment
      * data. The reserved memory for the proto module minus the length of the environment data is the
      * message buffer length.,
      * Double memory usage.,
      * Verified and tested that the pointer is always set correctly.}
      */
      // PRQA S 488 ++

      /* See justification at the file comment header */
      // PRQA S 310 ++
      protoEnvData->MsgBuf = (LinProtoIf_pData_t) ((LinProtoIf_EnvDataSze_t) protoEnvData + (LinProtoIf_EnvDataSze_t) sizeof(loc_sProtoEnvironmentData_t));
      // PRQA S 310 --
      // PRQA S 488 --

      msgBufLen = (LinProtoIf_BufLength_t) (protoEnvDataSze - (LinProtoIf_EnvDataSze_t) sizeof(loc_sProtoEnvironmentData_t));

      if(msgBufLen >= LINPROTOIMP_MAX_LIN_MSG_SZE)
      {
        protoEnvData->MsgBufLen = LINPROTOIMP_MAX_LIN_MSG_SZE;
      }
      else
      {
        protoEnvData->MsgBufLen = msgBufLen;
      }

      /* Initialize message buffer with 0xFF, because default value for unused bits in a LIN message is 1*/
      el_FillBytes(0xFF, (Lin_uint8_t*) protoEnvData->MsgBuf, protoEnvData->MsgBufLen);

      protoEnvData->BusIfFuncTbl    = protoImpCfgData->LinBusIfInitParam.IfFunsTbl;
      protoEnvData->LookupIfFuncTbl = protoImpCfgData->LinLookupIfInitParam.IfFunsTbl;

      /* Initialize LIN bus layer */
      if(protoEnvData->BusIfFuncTbl->Initialization(protoImpCfgData->LinBusIfInitParam.EnvData, protoImpCfgData->LinBusIfInitParam.EnvDataLen,
                                                    &loc_LinBusCallbacks, protoEnvData,
                                                    protoImpCfgData->LinBusIfInitParam.Baudrate,
                                                    protoImpCfgData->LinBusIfInitParam.ImpCfgData) == LIN_TRUE)
      {
        protoEnvData->BusEnvData = protoImpCfgData->LinBusIfInitParam.EnvData;

        /* Initialize PID lookup module */
        if(protoEnvData->LookupIfFuncTbl->Initialization(protoImpCfgData->LinLookupIfInitParam.EnvData,    protoImpCfgData->LinLookupIfInitParam.EnvDataLen,
                                                         protoImpCfgData->LinLookupIfInitParam.ImpCfgData) == LIN_TRUE)
        {
          protoEnvData->LookupEnvData   = protoImpCfgData->LinLookupIfInitParam.EnvData;

          returnValue = TRUE;
        }
        else{}
      }
      else{}
    }
    else{}
  }
  else
  {
    protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_INIT;
    loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
    loc_CallErrorCallback(genericProtoEnvData);
  }

  return (returnValue);
}

/***************************************************************************//**
 * TODO: Document exactly the meaning of return value LIN_TRUE and LIN_FALSE.
 ******************************************************************************/
PROTO_LAYER_CODE Lin_Bool_t LinProtoImp_GetSubInterface(LinProtoIf_pGenericEnvData_t genericProtoEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr)
{
  Lin_Bool_t retVal = LIN_FALSE;

  if(genericProtoEnvData != LIN_NULL)
  {
    loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericProtoEnvData;

    switch(interfaceId)
    {
      case Lin_IfId_SNPD:
        {
          retVal = protoEnvData->BusIfFuncTbl->GetSubInterface(protoEnvData->BusEnvData, interfaceId, ifThisPtr);
        }
        break;
      case Lin_IfId_BUS:
        {
          if (ifThisPtr != LIN_NULL)
          {
            ifThisPtr->IfFunsTbl = (Lin_cpGenericInterfaceFunctions_t) protoEnvData->BusIfFuncTbl;
            ifThisPtr->EnvData = protoEnvData->BusEnvData;
          }
          else{}

          retVal = LIN_TRUE;
        }
        break;
      case Lin_IfId_LOOKUP:
        {
          if (ifThisPtr != LIN_NULL)
          {
            ifThisPtr->IfFunsTbl = (Lin_cpGenericInterfaceFunctions_t) protoEnvData->LookupIfFuncTbl;
            ifThisPtr->EnvData = protoEnvData->LookupEnvData;
          }
          else{}

          retVal = LIN_TRUE;
        }
        break;
      default:
        break;
    }
  }
  else{}

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE void LinProtoImp_Task(LinProtoIf_pGenericEnvData_t genericProtoEnvData)
{
  if(genericProtoEnvData != LIN_NULL)
  {
    loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericProtoEnvData;
    protoEnvData->BusIfFuncTbl->Task(protoEnvData->BusEnvData);
    
    if(protoEnvData->TaskControl.Byte != 0u)
    {
      if(protoEnvData->TaskControl.Flags.ErrorCbRecvd == 1u)
      {
        loc_CallErrorCallback(protoEnvData);

        protoEnvData->TaskControl.Flags.ErrorCbRecvd = 0u;
      }

      if(protoEnvData->TaskControl.Flags.RestartCbRecvd == 1u)
      {
        loc_RestartCallback(genericProtoEnvData);
        protoEnvData->TaskControl.Flags.RestartCbRecvd = 0u;
      }

      if(protoEnvData->TaskControl.Flags.ReceiveDoneCbRecvd == 1u)
      {
        LinBusIf_eMsgAction_t msgAction = loc_CallFrameIdProcessedCallback(genericProtoEnvData);
        protoEnvData->TaskControl.Flags.ReceiveDoneCbRecvd = 0u;

        if(msgAction == LinBusIf_MSGACT_SLEEP)
        {
          protoEnvData->BusIfFuncTbl->GoToSleep(protoEnvData->BusEnvData);
        }
      }
      else{}

      if(protoEnvData->TaskControl.Flags.TransmitDoneCbRecvd == 1u)
      {
        LinBusIf_eMsgAction_t msgAction = loc_CallFrameIdProcessedCallback(genericProtoEnvData);
        protoEnvData->TaskControl.Flags.TransmitDoneCbRecvd = 0u;

        if(msgAction == LinBusIf_MSGACT_SLEEP)
        {
          protoEnvData->BusIfFuncTbl->GoToSleep(protoEnvData->BusEnvData);
        }
      }
      else{}

      if(protoEnvData->TaskControl.Flags.IdleCbRecvd == 1u)
      {
        LinBusIf_eIdleAction_t idleAction = loc_CallIdleCallback(genericProtoEnvData);
        protoEnvData->TaskControl.Flags.IdleCbRecvd = 0u;

        if(idleAction == LinBusIf_IDLEACT_SLEEP)
        {
          protoEnvData->BusIfFuncTbl->GoToSleep(protoEnvData->BusEnvData);
        }
      }
      else{}

      if(protoEnvData->TaskControl.Flags.WakupCbRecvd == 1u)
      {
        loc_CallWakeupCallback(genericProtoEnvData);
        protoEnvData->TaskControl.Flags.WakupCbRecvd = 0u;
      }
      else{}
    }
    else{}
  }
  else{}
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
PROTO_LAYER_CODE Lin_Bool_t LinProtoImp_AppendCallbacks(LinProtoIf_pGenericEnvData_t   genericProtoEnvData, LinProtoIf_cpCallbackFunctions_t protoCbFuns,
                                                        LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData)
{
  Lin_Bool_t returnValue = LIN_FALSE;
  
  if ((genericProtoEnvData != LIN_NULL) &&
      (protoCbFuns != LIN_NULL) &&
      (protoCbFuns->CallbackVersion == LINPROTO_INTERFACE_MODULE_API_VERSION))
  {
    loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericProtoEnvData;
    Lin_uint8_t i;

    /* Append callback table if there is free space */
    for(i = 0; i < LINPROTO_INTERFACE_MAX_CALLBACK_TABLES; i++)
    {
      if(protoEnvData->Callbacks[i] == protoCbFuns)
      {
        break;
      }
      else if(protoEnvData->Callbacks[i] == LIN_NULL)
      {
        protoEnvData->Callbacks[i]       = protoCbFuns;
        protoEnvData->CallbackCtxData[i] = genericProtoCbCtxData;
        returnValue = LIN_TRUE;
        break;
      }
      else{}
    }
  }

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE Lin_Bool_t LinProtoImp_RemoveCallbacks(LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpCallbackFunctions_t protoCbFuns)
{
  Lin_Bool_t returnValue = LIN_FALSE;

  if(genericProtoEnvData != LIN_NULL)
  {
    loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericProtoEnvData;
    Lin_uint8_t i;
    Lin_uint8_t j;

    /* Search for callback table address and delete it */
    for(i = 0; i < LINPROTO_INTERFACE_MAX_CALLBACK_TABLES; i++)
    {
      if(protoEnvData->Callbacks[i] == protoCbFuns)
      {
        for(j = i; j < (LINPROTO_INTERFACE_MAX_CALLBACK_TABLES - 1u); j++)
        {
          protoEnvData->Callbacks[j] = protoEnvData->Callbacks[j + 1u];
        }

        protoEnvData->Callbacks[LINPROTO_INTERFACE_MAX_CALLBACK_TABLES - 1u]       = LIN_NULL;
        protoEnvData->CallbackCtxData[LINPROTO_INTERFACE_MAX_CALLBACK_TABLES - 1u] = LIN_NULL;
        returnValue = LIN_TRUE;
        break;
      }
      else{}
    }
  }
  else{}

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE Lin_Bool_t LinProtoImp_AddFrameDescriptionList(LinProtoIf_pGenericEnvData_t genericProtoEnvData,  LinProtoIf_cpGenericFrameDescriptionList_t genericFrmDscLst,
                                                                Lin_Bool_t                   ldfRelevance,         LinProtoIf_pGenericFrmDescLstEnvData_t     genericFrmDescLstEnvData,
                                                                LinProtoIf_EnvDataSze_t      frmDescLstEnvDataSze, LinProtoIf_pGenericCbCtxData_t             genericProtoPerDescLstCbCtxData)
{
  Lin_Bool_t returnValue  = LIN_FALSE;

  /* TODO: Die Frame Desciprtion Liste darauf ueberpruefen ob es Elemente enthaelt, die das Unterelement "FrameIdProcessedCbCtxType"
     auf LinDrv_FrmIdPrcCbCtxDataType_PER_DESCLST stehen haben und wenn ja, ob der "PerFrmDescLstCbCtxData" Pointer nicht NULL ist */

  if((genericProtoEnvData != LIN_NULL) && (genericFrmDscLst != LIN_NULL))
  {
    loc_pProtoEnvironmentData_t     protoEnvData = (loc_pProtoEnvironmentData_t) genericProtoEnvData;
    LinProtoIf_cpFrameDescription_t  frameDescLst = (LinProtoIf_cpFrameDescription_t) genericFrmDscLst;
    Lin_Bool_t                      verifyResult = LIN_TRUE;
    Lin_uint8_t                     i = 0;

    /* Verify frame description list and signals */
    while((frameDescLst[i].Kind != LinProtoIf_FrameKind_INVALID) && (verifyResult == LIN_TRUE))
    {
      if(loc_VerifyFrameDesciption(&frameDescLst[i], protoEnvData->MsgBufLen) != LIN_TRUE)
      {
        verifyResult = LIN_FALSE;
        protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_FRM_DEF;
        loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
        loc_CallErrorCallback(genericProtoEnvData);
        break;
      }
      else{}

      #if LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1
      if((frameDescLst[i].Kind != LinProtoIf_FrameKind_PUBLISH_WITH_FUN) && 
	     (frameDescLst[i].Kind != LinProtoIf_FrameKind_SUBSCRIBE_WITH_FUN) &&
         (frameDescLst[i].Kind != LinProtoIf_FrameKind_NON_BUFFERED))
      {
        Lin_uint8_t j = 0;
        LinProtoIf_pSignal_t pSignal = (LinProtoIf_pSignal_t) frameDescLst[i].PtrToSgnlLst;
        while(pSignal[j].Type != LinProtoIf_SignalType_Invalid)
        {
          if(loc_VerifySignalDefinition(&pSignal[j], frameDescLst[i].Lenght) != LIN_TRUE)
          {
            verifyResult = LIN_FALSE;
            protoEnvData->ActlFrameInfo.Error = LinProtoIf_ERR_SGN_DEF;
            loc_SetCallbackData(protoEnvData, &(protoEnvData->ErrCallbackData));
            loc_CallErrorCallback(genericProtoEnvData);
            break;
          }
          else{}
          j++;
        }
      }
      else{}
      #endif /* LINPROTOIMP_SUPPORT_SIGNAL_LISTS == 1 */

      i++;
    }

    if(verifyResult == LIN_TRUE)
    {
      returnValue = protoEnvData->LookupIfFuncTbl->AddFrameDescriptionList(protoEnvData->LookupEnvData, genericFrmDscLst, ldfRelevance, genericFrmDescLstEnvData, frmDescLstEnvDataSze, genericProtoPerDescLstCbCtxData);
    }
  }
  
  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
PROTO_LAYER_CODE Lin_Bool_t LinProtoImp_RemoveFrameDescriptionList(LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpGenericFrameDescriptionList_t genericFrmDscLst)
{
  Lin_Bool_t retVal = LIN_FALSE;

  if(genericProtoEnvData != LIN_NULL)
  {
    loc_pProtoEnvironmentData_t protoEnvData = (loc_pProtoEnvironmentData_t) genericProtoEnvData;

    retVal = protoEnvData->LookupIfFuncTbl->RemoveFrameDescriptionList(protoEnvData->LookupEnvData, genericFrmDscLst);
  }

  return(retVal);
}
