/**
 *  @file test_VCOMM_Signal_Set_TrnOilPump_U_Meas.c
 *
 */
#include "unity.h"
#include "lin_vehicle_comm.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_fault_manager.h"
#include "mock_foc_stubs.h"
#include "mock_lin_transport.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_time_utils.h"
/* STATIC's */
uint8_t vcomm_U_Meas;

void setUp(void)
{
    vcomm_U_Meas = 0;
}

void tearDown(void)
{

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_U_Meas() sets signal value to
 * 0 when the measured voltage is 0
 *
 */
/* Reqs: SWREQ_1936 */
void test_VCOMM_Signal_Set_TrnOilPump_U_Meas_sets_signal_to_0_When_voltage_is_0(void)
{
    /* Ensure known test state */
    vcomm_U_Meas = 255;

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_U_Meas(0);
    
    /* Verify test results */
    
    TEST_ASSERT_UINT8_WITHIN(1, 0, vcomm_U_Meas);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_U_Meas() sets signal value to
 * 255 when the measured voltage is > 3493 (25.5 V) which is the maximum value
 * than can be represented by the signal.
 *
 */
/* Reqs: SWREQ_1936 */
void test_VCOMM_Signal_Set_TrnOilPump_U_Meas_sets_signal_to_255_When_voltage_is_above_3494(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_U_Meas(3494);

    /* Verify test results */

    TEST_ASSERT_UINT8_WITHIN(1, 255, vcomm_U_Meas);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_U_Meas() sets signal value to
 * 255 when the measured voltage is 3493 (25.5 V)
 *
 */
/* Reqs: SWREQ_1936 */
void test_VCOMM_Signal_Set_TrnOilPump_U_Meas_sets_signal_to_255_When_voltage_is_3493(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_U_Meas(3493);

    /* Verify test results */

    TEST_ASSERT_UINT8_WITHIN(1, 255, vcomm_U_Meas);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_U_Meas() sets signal value to
 * 132 when the measured voltage is 1821 (13.3 V)
 *
 */
/* Reqs: SWREQ_1936 */
void test_VCOMM_Signal_Set_TrnOilPump_U_Meas_sets_signal_to_132_When_voltage_is_1821(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_U_Meas(1821);

    /* Verify test results */

    TEST_ASSERT_UINT8_WITHIN(1, 132, vcomm_U_Meas);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_U_Meas() sets signal value to
 * 99 when the measured voltage is 1369 (10.0 V)
 *
 */
/* Reqs: SWREQ_1936 */
void test_VCOMM_Signal_Set_TrnOilPump_U_Meas_sets_signal_to_99_When_voltage_is_1369(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_U_Meas(1369);

    /* Verify test results */

    TEST_ASSERT_UINT8_WITHIN(1, 99, vcomm_U_Meas);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_U_Meas() sets signal value to
 * 179 when the measured voltage is 2465 (18.0 V)
 *
 */
/* Reqs: SWREQ_1936 */
void test_VCOMM_Signal_Set_TrnOilPump_U_Meas_sets_signal_to_179_When_voltage_is_2465(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_U_Meas(2465);

    /* Verify test results */

    TEST_ASSERT_UINT8_WITHIN(1, 179, vcomm_U_Meas);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPump_U_Meas() sets signal value to
 * 89 when the measured voltage is 1232 (9.0 V)
 *
 */
/* Reqs: SWREQ_1936 */
void test_VCOMM_Signal_Set_TrnOilPump_U_Meas_sets_signal_to_89_When_voltage_is_1232(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPump_U_Meas(1232);

    /* Verify test results */

    TEST_ASSERT_UINT8_WITHIN(1, 89, vcomm_U_Meas);

}
