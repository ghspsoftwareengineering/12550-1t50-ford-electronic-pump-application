/**
 * @defgroup PI Digital PI controller
 * @ingroup PI
 *
 * @{
 */

#ifndef PID_CONTROLLER_H
#define PID_CONTROLLER_H

#include "global.h"

/* ---------------
   global defines
   --------------- */

/* limitation of pi_controller_t->err_sum */
#define PI_CONTROLLER_MAX_ERR_SUM ( S32_MAX / (s32) 4 )

/* prescaler of Kp and Ki -> 2^PI_CONTROLLER_KI_KP_SCALE_BITS */
#define PI_CONTROLLER_KI_KP_SCALE_BITS ( 12u )

#define PI_CONTROLLER_SATURATION_INCREMENTAL   ( 1 )
#define PI_CONTROLLER_SATURATION_COMPUTATIONAL ( 2 )

#define PI_CONTROLLER_SATURATION_TYPE PI_CONTROLLER_SATURATION_COMPUTATIONAL

#define PI_CONTROLLER_SATURATION_RECOVER_RATIO_INIT ( 100 )

/**! @typedef pi_controller_t
 * @brief structure of a PI controller, all pi_controller-functions need a reference to an instance of that type for executing
 */
typedef struct
{
    int16_t Kp;                        /**< gain of the P-part; keep in mind that the output is scaled down by PI_CONTROLLER_KI_KP_SCALE_BITS (right-shift) */
    uint16_t Ki;                        /**< gain of the I-part; keep in mind that the output is scaled down by PI_CONTROLLER_KI_KP_SCALE_BITS (right-shift) */
    int16_t set_value;                 /**< output of the PI controller */
    int32_t err_sum;                   /**< accumulator for the I-part */
    int16_t saturation_recovery_ratio; /**< the higher this value, the faster the controller returns from saturation; however a higher value means tendency to oscillate */

} pi_controller_t;

/* --------------------
   global declarations
   -------------------- */
void pi_controller_init( pi_controller_t * controller, const int16_t Kp, const uint16_t Ki );
void pi_controller_exec_unsafe( pi_controller_t * controller, const int16_t target_value, const int16_t current_value );
void pi_controller_exec_w_saturation_unsafe( pi_controller_t * controller, const int16_t target_value, const int16_t current_value, const int16_t output_max, const int16_t output_min );
void pi_controller_reset( pi_controller_t * controller );

#endif /* _#ifndef PID_CONTROLLER_H_ */

/* }@
 */
