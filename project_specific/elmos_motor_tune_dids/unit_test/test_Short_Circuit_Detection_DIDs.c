/**
 *  @file test_Short_Circuit_Detection_DIDs.c
 *
 */
#include "unity.h"
#include "elmos_motor_tune_dids.h"
#include "52307_test_regs.h"
#include "mock_52307_driver_stubs.h"
#include "mock_foc.h"
#include "mock_motor_ctrl.h"
#include "mock_motor_manager_diag.h"
#include "mock_serial_data_buffer.h"

uint16_t Test_Regs[TEST_REG_END] = {0};

/* STATIC's */

static Serial_Data_Buffer_T Test_SDB = {0};

void setUp(void)
{
}

void tearDown(void)
{

}

/**
 * Verifies that the function MTDIDS_Get_Short_Circuit_HS_Thresh reads the
 * SCTH_HS register and serializes the value.
 */
void test_MTDIDS_Get_Short_Circuit_HS_Thresh_retrieves_SCTH_HS_and_serializes(void)
{
    uint8_t expected_reg_value = 31;

    /* Ensure known test state */

    /* Setup expected call chain */
    d52307_read_reg_Expect(0x28u, NULL);
    d52307_read_reg_IgnoreArg_data();
    d52307_read_reg_ReturnThruPtr_data(&expected_reg_value);
    SDB_Serialize_U8_ExpectAndReturn(&Test_SDB, expected_reg_value, SDB_EOK);

    /* Call function under test */
    MTDIDS_Get_Short_Circuit_HS_Thresh(&Test_SDB);
    
    /* Verify test results */
}

/**
 * Verifies that the function MTDIDS_Set_Short_Circuit_HS_Thresh writes the
 * SCTH_HS register using the value deserialized from the data buffer
 */
void test_MTDIDS_Set_Short_Circuit_HS_Thresh_retrieves_deserializes_and_writes_SCTH_HS(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 45);
    d52307_write_reg_Expect(0x28u, 45);

    /* Call function under test */
    MTDIDS_Set_Short_Circuit_HS_Thresh(&Test_SDB);

    /* Verify test results */
}

/**
 * Verifies that the function MTDIDS_Get_Short_Circuit_LS_Thresh reads the
 * SCTH_LS register and serializes the value.
 */
void test_MTDIDS_Get_Short_Circuit_LS_Thresh_retrieves_SCTH_LS_and_serializes(void)
{
    uint8_t expected_reg_value = 31;

    /* Ensure known test state */

    /* Setup expected call chain */
    d52307_read_reg_Expect(0x29u, NULL);
    d52307_read_reg_IgnoreArg_data();
    d52307_read_reg_ReturnThruPtr_data(&expected_reg_value);
    SDB_Serialize_U8_ExpectAndReturn(&Test_SDB, expected_reg_value, SDB_EOK);

    /* Call function under test */
    MTDIDS_Get_Short_Circuit_LS_Thresh(&Test_SDB);

    /* Verify test results */
}

/**
 * Verifies that the function MTDIDS_Set_Short_Circuit_LS_Thresh writes the
 * SCTH_LS register using the value deserialized from the data buffer
 */
void test_MTDIDS_Set_Short_Circuit_LS_Thresh_retrieves_deserializes_and_writes_SCTH_LS(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 45);
    d52307_write_reg_Expect(0x29u, 45);

    /* Call function under test */
    MTDIDS_Set_Short_Circuit_LS_Thresh(&Test_SDB);

    /* Verify test results */
}

/**
 * Verifies that the function MTDIDS_Get_SC_Masking_Time reads the
 * SCMT register and serializes the value.
 */
void test_MTDIDS_Get_SC_Masking_Time_retrieves_SCMT_and_serializes(void)
{
    uint8_t expected_reg_value = 55;

    /* Ensure known test state */

    /* Setup expected call chain */
    d52307_read_reg_Expect(0x26u, NULL);
    d52307_read_reg_IgnoreArg_data();
    d52307_read_reg_ReturnThruPtr_data(&expected_reg_value);
    SDB_Serialize_U8_ExpectAndReturn(&Test_SDB, expected_reg_value, SDB_EOK);

    /* Call function under test */
    MTDIDS_Get_SC_Masking_Time(&Test_SDB);

    /* Verify test results */
}

/**
 * Verifies that the function MTDIDS_Set_SC_Masking_Time writes the
 * SCMT register using the value deserialized from the data buffer
 */
void test_MTDIDS_Set_SC_Masking_Time_retrieves_deserializes_and_writes_SCMT(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 45);
    d52307_write_reg_Expect(0x26u, 45);

    /* Call function under test */
    MTDIDS_Set_SC_Masking_Time(&Test_SDB);

    /* Verify test results */
}

/**
 * Verifies that the function MTDIDS_Get_SC_Debounce_Time reads the
 * SCDEB register and serializes the value.
 */
void test_MTDIDS_Get_SC_Debounce_Time_retrieves_SCDEB_and_serializes(void)
{
    uint8_t expected_reg_value = 55;

    /* Ensure known test state */

    /* Setup expected call chain */
    d52307_read_reg_Expect(0x2Du, NULL);
    d52307_read_reg_IgnoreArg_data();
    d52307_read_reg_ReturnThruPtr_data(&expected_reg_value);
    SDB_Serialize_U8_ExpectAndReturn(&Test_SDB, expected_reg_value, SDB_EOK);

    /* Call function under test */
    MTDIDS_Get_SC_Debounce_Time(&Test_SDB);

    /* Verify test results */
}

/**
 * Verifies that the function MTDIDS_Set_SC_Debounce_Time writes the
 * SCDEB register using the value deserialized from the data buffer
 */
void test_MTDIDS_Set_SC_Debounce_Time_retrieves_deserializes_and_writes_SCDEB(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */
    SDB_Deserialize_U8_ExpectAndReturn(&Test_SDB, 45);
    d52307_write_reg_Expect(0x2Du, 45);

    /* Call function under test */
    MTDIDS_Set_SC_Debounce_Time(&Test_SDB);

    /* Verify test results */
}
