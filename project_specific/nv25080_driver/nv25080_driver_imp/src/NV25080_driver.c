/**
 *  @file NV25080_driver.c
 *
 *  @ref NV25080_driver api
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup drivers_imp NV2508 EEPROM using SPI_1 Implementation
 *  @{
 *
 *      System specific implementation NV2508 EEPROM using SPI_1
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/**********************************************************************/
/*! @file NV25080_driver_driver.c
 * @brief module that interfaces IC NV25080 On semi SPI EEPROM
 *
 * @details
 * contains functions to access NV25080 on SPI_1
 *
 * @author       KJL @GHSP
 */
/**********************************************************************/
/*   Note that using bit bang for NSS with a GPIO port does not work, internal NSS must be used. 
*/
/**********************************************************************/
#include "NV25080_driver.h"
#include <adc.h>
/* -------------
   local defines
   ------------- */
#define CLEAR_ALL_FIFO ((u16) ( E_SPI_FIFO_CLEAR_RX_FIFO_CLEAR ) | (u16) ( E_SPI_FIFO_CLEAR_TX_FIFO_CLEAR  | (u16) (E_SPI_FIFO_CLEAR_RESET)))
#define CLEAR_JUST_FIFO ((u16) ( E_SPI_FIFO_CLEAR_RX_FIFO_CLEAR ) | (u16) ( E_SPI_FIFO_CLEAR_TX_FIFO_CLEAR))

/* ------------------
   local declarations
   ------------------ */
#define SPI_FIFO_DEPTH 4  /* this is depth of the rx and tx buffers */

/*! @brief init/configure  NV25080 SPI_1 as EEPROM interface_driver 
 *
 * @details
 * initialize SPI_1 interface for use with ON Semi NV25080
 */
#pragma optimize=balanced
void NV25080_Init( void )
{
    spi_config_bf_t spi_ee_config;

    spi_ee_config.order        = 1u;        /* MSB_First */
    spi_ee_config.phase        = 1u;        /* phase = 1 --> 1st edge sample */
    spi_ee_config.polarity     = 0u;        /* polarity = 0 --> clock_off _level 0 */
    spi_ee_config.slave        = 0u;        /* !MASTER */
    spi_ee_config.length       = 8u - 1u;   /* All transmits are 8 bits */
    spi_ee_config.slave_high_z = 0u;        /* high_z = 0 --> doesn't matter (master_mode) */
    spi_ee_config.invert_nss   = 0u;
    spi_ee_config.invert_data  = 0u;
    spi_ee_config.pause_nss    = 1u;
    spi_ee_config.sdi_irq_pol  = 0u;
    spi_ee_config.swap_sdi_sdo = 0u;
    spi_ee_config.enable       = 1u;

    SPI_1_CONFIG_bit     = spi_ee_config;
    SPI_1_BAUD_CONFIG    = (u16) ((u32) F_CPU_MHZ / 2uL / (u32) ( SPI0_CLK_MHZ )); /* Divider = f_clk/(2*baudrate) */
    SPI_1_TIMEOUT_CONFIG = 0xFFFFu;
    SYS_STATE_MODULE_ENABLE_bit.spi_1=1u;
    SPI_1_RX_FIFO_TIMEOUT = 60u;
 }

#pragma optimize=balanced
void NV25080_Enable_Write( BOOL enable )
{
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );


    if(enable)
    {
        SPI_1_DATA = NV25080_WREN;  
    }
    else
    {
        SPI_1_DATA = NV25080_WRDI; 
    }
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u); 
    (uint8_t)SPI_1_DATA;

    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
}

#pragma optimize=balanced
uint8_t NV25080_Read_Byte(uint16_t address)
{
    uint8_t rv = 0xffu;

    SPI_1_FIFO_CLEAR = CLEAR_ALL_FIFO;
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level > (u16) 0u ) );
    SPI_1_FIFO_CLEAR = (u16) ( E_SPI_FIFO_CLEAR_RX_FIFO_CLEAR ) | (u16) ( E_SPI_FIFO_CLEAR_TX_FIFO_CLEAR );

    SPI_1_DATA_KEEP_NSS = (uint8_t)NV25080_READ;
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u); 
    (uint8_t)SPI_1_DATA;

    /* send address */
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address >> 8u);
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);  
    (uint8_t)SPI_1_DATA;
   
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address & 0x00ffu);
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);  
    (uint8_t)SPI_1_DATA;

    SPI_1_DATA_KEEP_NSS = (uint8_t)0xffu;
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);  
    rv = (uint8_t)SPI_1_DATA;

    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
    SPI_1_DATA = 0xffu;
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);  
    (uint8_t)SPI_1_DATA;
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
    return(rv);
}

#pragma optimize=balanced
uint8_t NV25080_Read_Status_Reg(void)
{
    uint8_t rv = 0xffu;

    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level > (u16) 0u ) );
   
    SPI_1_FIFO_CLEAR = CLEAR_ALL_FIFO;
    SPI_1_DATA_KEEP_NSS = NV25080_RDSR;
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);
    (uint8_t)SPI_1_DATA;
    SPI_1_DATA = 0xffu;
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);
    rv = (uint8_t)SPI_1_DATA;
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
    return(rv);
}
    
#pragma optimize=balanced
void NV25080_Read_Multiple_Bytes(uint16_t address, uint8_t * const data, uint8_t numbytes)
{
    uint8_t i;

    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level > (u16) 0u ) );
    SPI_1_FIFO_CLEAR = CLEAR_ALL_FIFO;


    SPI_1_DATA_KEEP_NSS = (uint8_t)NV25080_READ;
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u); 
    (uint8_t)SPI_1_DATA;

    /* send address */
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address >> 8u);
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);  
    (uint8_t)SPI_1_DATA;
   
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address & 0x00ffu);
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);  
    (uint8_t)SPI_1_DATA;

    SPI_1_DATA_KEEP_NSS = (uint8_t)0xffu;
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);  
     
    for (i = 0u; i < (numbytes - (uint8_t)1); i++)
    {
        data[i] = (uint8_t)SPI_1_DATA;  
        do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
        SPI_1_DATA_KEEP_NSS = (uint8_t)0xffu;
        do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);  
    }
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
    SPI_1_DATA = (uint8_t)0xffu;
    do {} while(SPI_1_FIFO_LEVELS_bit.rx_fifo_level == 0u);
    data[numbytes - (uint8_t)1] = (uint8_t)SPI_1_DATA;
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
}

#pragma optimize=balanced
bool_t NV25080_Write_Byte(uint16_t address, uint8_t data)
{
    bool_t rv;
    
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level > 0u ) );

    if((NV25080_Read_Status_Reg() & NV25080_WEL) != NV25080_WEL)
    {
        NV25080_Enable_Write(true);
    }
    SPI_1_DATA_KEEP_NSS = NV25080_WRITE;
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address >>  8u);
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address & 0x00ffu);
    SPI_1_DATA = data;
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
    rv = 1;
    return(rv);
}

#pragma optimize=balanced
void NV25080_Write_Multiple_Bytes(uint16_t address, uint8_t const *data, uint8_t numbytes)
{
    uint8_t i;

    SPI_1_FIFO_CLEAR = CLEAR_ALL_FIFO;

    if((NV25080_Read_Status_Reg() & NV25080_WEL) != NV25080_WEL)
    {
        NV25080_Enable_Write(true);
    }

    SPI_1_FIFO_CLEAR = CLEAR_JUST_FIFO;
    SPI_1_DATA_KEEP_NSS = NV25080_WRITE;
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address >>  8u);
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address & 0x00ffu);

    for (i = 0u; i < (numbytes - (uint8_t)1); i++)
    {
        do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
        SPI_1_DATA_KEEP_NSS = data[i]; 
    }
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
    SPI_1_DATA = data[numbytes - 1u]; 
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
}

#pragma optimize=balanced
void NV25080_Erase(uint16_t address,  uint8_t numbytes)
{
    uint8_t i;

    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level > 0u ) );
    SPI_1_FIFO_CLEAR = CLEAR_JUST_FIFO;

    if((NV25080_Read_Status_Reg() & NV25080_WEL) != NV25080_WEL)
    {
        NV25080_Enable_Write(true);
    }

    SPI_1_FIFO_CLEAR = CLEAR_JUST_FIFO;
    SPI_1_DATA_KEEP_NSS = NV25080_WRITE;
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address >>  8u);
    SPI_1_DATA_KEEP_NSS = (uint8_t)(address & 0x00ffu);

    for (i = 0u; i < (numbytes - (uint8_t)1); i++)
    {
        do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
        SPI_1_DATA_KEEP_NSS = (uint8_t)0xff; 
    }
 
    SPI_1_DATA = (uint8_t)0xff;
    do {}  while(( SPI_1_FIFO_LEVELS_bit.tx_fifo_level >= (u16) SPI_FIFO_DEPTH ) );
}

#pragma optimize=balanced
bool_t NV25080_Is_Busy(void)
{
    bool_t rv = false;
    uint8_t status;

    /* Things causing an eeprom is busy return (true) */
    /* EEPROM status regiser NV25080_NOT_RDY (Bit 0) is 1 indicating a write cycle in progress */
    /* OR tx_fifo_level > 0 indicating SPI is sending queued bytes to the eeprom */
    /* OR tx_fifo_empty == 0 which indicates the transmit fifo is not empty */
    status = NV25080_Read_Status_Reg(); 
  
    if((SPI_1_FIFO_LEVELS_bit.tx_fifo_level > 0u) || (SPI_1_IRQ_STATUS_bit.tx_fifo_empty == 0u) || ((status & NV25080_NOT_RDY) != 0u))
    {
        rv = true;
    }
    return(rv);
}
