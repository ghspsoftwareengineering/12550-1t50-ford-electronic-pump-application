/**
 *  @file stall_logger.c
 *
 *  @ref stall_logger_api
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup stall_logger_imp Stall Event Logger Implementation
 *  @{
 *
 *      System specific implementation of the @ref stall_logger_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_environmental_logger.h"
#include "fault_logger.h"
#include "fault_logger_imp_defines.h"
#include "fault_logger_imp_types.h"
#include "global.h"
#include "lin_vehicle_comm.h"
#include "motor_ctrl.h"
#include "nvm_mgr.h"
#include <string.h>
#include "system_signals.h"

#ifdef _TEST_
#    include "52307_test_regs.h"
#endif
/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
STATIC flt_log_General_Info_T flt_log_General_Info =
{
    .Serial_Number = "",
    .Max_Lifetime_Voltage = 0u,
    .Max_Lifetime_CPU_Temperature = 0u,
    .Power_Cycles_Since_Stall = 0u,
    .Time_Since_Stall = 0u,
    .Run_Time = 0u,
    .Lifetime_Fault_Count = 0u,
    .Fault_Index = 0u
};

STATIC flt_Environmental_Log_Block_T fltel_Blocks[FLT_LOG_NVM_BLOCK_COUNT] =
{
    {
        .NVM_Block_ID = 3u
    },
    {
        .NVM_Block_ID = 4u
    },
    {
        .NVM_Block_ID = 5u
    },
    {
        .NVM_Block_ID = 6u
    },
    {
        .NVM_Block_ID = 7u
    },
    {
        .NVM_Block_ID = 8u
    },
    {
        .NVM_Block_ID = 9u
    },
    {
        .NVM_Block_ID = 10u
    },
};

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
void fltel_Init(void)
{
    /* Register fault logging shadow RAM for each fault */
    NVMMGR_Register_Shadow(fltel_Blocks[0].NVM_Block_ID, (uint8_t *)&fltel_Blocks[0].Fault_Env_Record[0], NULL);
    NVMMGR_Register_Shadow(fltel_Blocks[1].NVM_Block_ID, (uint8_t *)&fltel_Blocks[1].Fault_Env_Record[0], NULL);
    NVMMGR_Register_Shadow(fltel_Blocks[2].NVM_Block_ID, (uint8_t *)&fltel_Blocks[2].Fault_Env_Record[0], NULL);
    NVMMGR_Register_Shadow(fltel_Blocks[3].NVM_Block_ID, (uint8_t *)&fltel_Blocks[3].Fault_Env_Record[0], NULL);
    NVMMGR_Register_Shadow(fltel_Blocks[4].NVM_Block_ID, (uint8_t *)&fltel_Blocks[4].Fault_Env_Record[0], NULL);
    NVMMGR_Register_Shadow(fltel_Blocks[5].NVM_Block_ID, (uint8_t *)&fltel_Blocks[5].Fault_Env_Record[0], NULL);
    NVMMGR_Register_Shadow(fltel_Blocks[6].NVM_Block_ID, (uint8_t *)&fltel_Blocks[6].Fault_Env_Record[0], NULL);
    NVMMGR_Register_Shadow(fltel_Blocks[7].NVM_Block_ID, (uint8_t *)&fltel_Blocks[7].Fault_Env_Record[0], NULL);

    NVMMGR_Register_Shadow(15u, (uint8_t *)&flt_log_General_Info, NULL);
}

#pragma optimize=balanced
void fltel_Log_Fault(FLT_LOG_Faults_T Fault_ID)
{
    uint16_t pwmn_cnt = PWMN_CNT;
    uint16_t pwmn_cnt_max = PWMN_CNT_MAX;
    uint8_t Duty_Cycle_Percent = (uint8_t)((pwmn_cnt * 100u) / pwmn_cnt_max);
    uint8_t Fault_Block_Index = (flt_log_General_Info.Fault_Index) & 0x01u;
    uint8_t Fault_Log_Block = (flt_log_General_Info.Fault_Index & 0x0Eu) >> 1;
    flt_Environmental_Record_T * Environmental_Record = &fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index];

    Environmental_Record->Incident_Number = flt_log_General_Info.Lifetime_Fault_Count;
    Environmental_Record->Fault_ID = Fault_ID;
    Environmental_Record->CPU_Temperature = SYSIG_Get_CPU_Temp();
    Environmental_Record->Supply_Voltage = SYSIG_Get_Voltage();
    Environmental_Record->Supply_Current = SYSIG_Get_Current();
    Environmental_Record->Commanded_RPM = VCOMM_Signal_Get_TrnOilPumpV_Pc_Rq();
    Environmental_Record->IRQSTAT = mc_get_irqstat();
    Environmental_Record->PWM_Duty_Cycle = Duty_Cycle_Percent;

    flt_log_General_Info.Lifetime_Fault_Count++;
    flt_log_General_Info.Fault_Index++;

    if (flt_log_General_Info.Fault_Index >= FLT_MAX_INCIDENTS_TO_LOG)
    {
        flt_log_General_Info.Fault_Index = 0u;
    }

    NVMMGR_Write_NVM_Block(15u); /* Write the General block to update the lifetime count and the index */
    NVMMGR_Write_NVM_Block(fltel_Blocks[Fault_Log_Block].NVM_Block_ID); /* Update the fault incident log buffer */
}

#pragma optimize=balanced
bool_t FLTEL_Clear_Environmental_Log(void)
{
    bool_t Success = true;

    flt_log_General_Info.Lifetime_Fault_Count = 0u;
    flt_log_General_Info.Fault_Index = 0u;

    if (NVM_STATUS_SUCCESS != NVMMGR_Write_NVM_Block(15u)) /* Write the General block to update the lifetime count and the index */
    {
        Success = false;
    }
    else
    {
        uint8_t Fault_Log_Block = 0u;
        uint8_t Fault_Block_Index = 0u;

        for (Fault_Log_Block = 0u; Fault_Log_Block < FLT_LOG_NVM_BLOCK_COUNT; Fault_Log_Block++)
        {
            for (Fault_Block_Index = 0u; Fault_Block_Index < 2u; Fault_Block_Index++)
            {
                fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index].Incident_Number = 0u;
                fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index].Fault_ID = FLT_LOG_NO_ERROR;
                fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index].PWM_Duty_Cycle = 0u;
                fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index].CPU_Temperature = 0;
                fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index].Supply_Voltage = 0u;
                fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index].Supply_Current = 0;
                fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index].Commanded_RPM = 0u;
                fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index].IRQSTAT = 0u;
                fltel_Blocks[Fault_Log_Block].Fault_Env_Record[Fault_Block_Index].Reserved[0] = 0u;
            }

            fltel_Blocks[Fault_Log_Block].Pad = 0u;

            if (NVM_STATUS_SUCCESS != NVMMGR_Write_NVM_Block(fltel_Blocks[Fault_Log_Block].NVM_Block_ID)) /* Update the fault incident log buffer */
            {
                Success = false;
                break;
            }
        }
    }
    return Success;
}

#pragma optimize=balanced
UDS_Response_Code_T FLTEL_Clear_Environmental_Log_DID(Serial_Data_Buffer_T * const p_Req_SDB)
{
    bool_t Success = false;
    UDS_Response_Code_T Status = UDS_RESP_POSITIVE;

    (void)SDB_Deserialize_U8(p_Req_SDB);

    Success = FLTEL_Clear_Environmental_Log();

    if(!Success)
    {
        Status = UDS_RESP_GENERAL_PROGRAMMING_FAILURE;
    }
    return Status;
}

/** @} doxygen end group */
