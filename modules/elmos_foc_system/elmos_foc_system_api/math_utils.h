/**
 * @defgroup HELP Helper functions (math calculations etc.)
 * @ingroup HELP
 *
 * @{
 */

#ifndef MATH_UTILS_H_
#define MATH_UTILS_H_

#include <defines.h>

/* --------------
   global defines
   -------------- */

/** @define the internal numerical value for 360 degrees (one full rotation) */
#define MATH_ANGLE_MAX U16_MAX

#define MATH_SINE_LUT_AMPLITUDE ( 32726 )

#define SQRT2 ( 1.4142135623730950488016887242097 )
#define SQRT3 ( 1.7320508075688772935274463415059 )
#define PI    ( 3.1415926535897932384626433832795 )

/* -------------------
   global declarations
   ------------------- */

/* MUL/DIV */
void math_mulU16_S32_unsafe( u16 m1, s32 m2 );

void math_mulS16_unsafe( s16 m1, s16 m2 );
void math_nmulS16_unsafe( s16 m1, s16 m2 );
void math_mulS16_acc_unsafe( s16 m1, s16 m2 );
void math_nmulS16_acc_unsafe( s16 m1, s16 m2 );

void math_mulU16_unsafe ( u16 m1, u16 m2 );
void math_mulU16_acc_unsafe ( u16 m1, u16 m2 );

void math_mul_shiftright_result( u16 cnt );
void math_mul_shiftleft_result ( u16 cnt );
s32 math_mul_get_result ( BOOL return32Bit );

u32 math_divU32_unsafe( u32 dividend, u16 divisor, BOOL return32Bit );
s32 math_divS32_unsafe( s32 dividend, s16 divisor, BOOL return32Bit );

void math_unblockedDivU32_unsafe( u32 dividend, u16 divisor );
void math_unblockedDivS32_unsafe( s32 dividend, s16 divisor );

u32 math_unblockedDivReadResult_unsafe ( BOOL return32Bit );
u16 math_unblockedDivReadRemainder_unsafe ( void );

/* trigonometrics */
s16 math_sine_unsafe( const u16 alpha );
s16 math_cosine_unsafe( const u16 alpha );

s16 math_svm_waveform_unsafe ( const u16 alpha );

u16 math_get_angle_unsafe( s16 y, s16 x );

u16 math_arcsine_unsafe ( const s16 sine_val );

/* MIN/MAX */
u16 math_max_U16( u16 val_1, u16 val_2 );
u16 math_min_U16( u16 val_1, u16 val_2 );
s16 math_max_S16( s16 val_1, s16 val_2 );
s16 math_min_S16( s16 val_1, s16 val_2 );

u32 math_max_U32( u32 val_1, u32 val_2 );
u32 math_min_U32( u32 val_1, u32 val_2 );
s32 math_max_S32( s32 val_1, s32 val_2 );
s32 math_min_S32( s32 val_1, s32 val_2 );

#define MATH_DEGREE_TO_MATH_ANGLE_COEF ((((dbl) MATH_ANGLE_MAX ) + 1.0 ) / 360.0 )
INLINE u16 MATH_DEGREE_TO_MATH_ANGLE( dbl angle );

#endif /* _#ifndef MATH_UTILS_H_ */

/* }@
 */
