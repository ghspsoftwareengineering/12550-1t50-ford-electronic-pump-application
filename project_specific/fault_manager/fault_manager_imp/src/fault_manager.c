/**
 *
 *
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_manager_imp Fault Manager Implementation
 *  @{
 *
 *      System specific implementation of the @ref fault_manager_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "fault_logger.h"
#include "global.h"
#include "lin_vehicle_comm.h"
#include "LinBus_Types.h"
#include "motor_ctrl.h"
#include "motor_manager.h"
#include "nvm_mgr.h"
#include <string.h>
#include "system_signals.h"
#include "time_utils.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/


/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/
STATIC void FLT_MGR_Check_Voltage(void);
STATIC void FLT_MGR_Check_Temperature(void);
STATIC void FLT_MGR_Check_Current(void);
STATIC void FLT_MGR_Check_Speed(void);
STATIC void FLT_MGR_Check_Irqstat( void );

STATIC void flt_mgr_Check_LIN_Fault(void);

STATIC FLT_LOG_Faults_T flt_mgr_Translate_LIN_To_Fault_Log(FLT_MGR_LIN_J2602_Err_T LIN_Fault);

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
STATIC const flt_mgr_Fault_Params_T Fault_Parameters[FLT_MGR_NUM_FAULTS] =
{   
    /* Fault Index,                 Set ms,   Clear ms   */
    {FLT_OVER_VOLTAGE,              1000u,   500u  },
    {FLT_UNDER_VOLTAGE,             1000u,   500u  }, 
    {FLT_UNDER_VOLT_SELF_PROTECT,   1000u,   500u  },
    {FLT_OVER_CURRENT,              1000u,   1000u  },
    {FLT_UNDER_CURRENT,             1000u,   1000u  },
    {FLT_OVER_TEMPERATURE,          1000u,   1000u  },
    {FLT_OVER_SPEED,                4000u,   1000u  },
    {FLT_UNDER_SPEED,               4000u,   1000u  }    

    /*  There are parts of the code that depend on the addresses in this table being in order, from low to high  */
};

STATIC bool_t FLT_MGR_Irqstat_Fault = false;
STATIC bool_t Fault_Active[FLT_MGR_NUM_FAULTS];
STATIC FLT_MGR_LIN_J2602_Err_T Lin_Fault_J2602;

/* timestamps for each fault type */
STATIC Time_MS_T FLT_Voltage_Timestamp = 0u;
STATIC Time_MS_T FLT_Under_Voltage_Self_Protect_Timestamp = 0u;
STATIC Time_MS_T FLT_Temperature_Timestamp = 0u;
STATIC Time_MS_T FLT_Current_Timestamp = 0u;
STATIC Time_MS_T FLT_Speed_Timestamp = 0u;
STATIC Time_MS_T FLT_LIN_Fault_Timestamp = 0u;

/* fault status flags */
STATIC bool_t detected_over_temp = false;
STATIC bool_t clearing_over_temp = false;

STATIC bool_t detected_over_voltage = false;
STATIC bool_t clearing_over_voltage = false;
STATIC bool_t detected_under_voltage = false;
STATIC bool_t clearing_under_voltage = false;
STATIC bool_t detected_under_voltage_self_p = false;
STATIC bool_t clearing_under_voltage_self_p = false;

STATIC bool_t detected_over_current = false;
STATIC bool_t clearing_over_current = false;
STATIC bool_t detected_under_current = false;
STATIC bool_t clearing_under_current = false;

STATIC bool_t detected_over_speed = false;
STATIC bool_t clearing_over_speed = false;
STATIC bool_t detected_under_speed = false;
STATIC bool_t clearing_under_speed = false;
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
bool_t FLT_MGR_Init(void)
{
    FLT_LOG_Init();

    return true;
}

#pragma optimize=balanced
void FLT_MGR_Task(void)
{
    if (FLT_LOG_Is_Enabled())
    {
        FLT_LOG_Scan();
    }
    else
    {
        if (NVMMGR_Is_NVM_Ready())
        {
            FLT_LOG_Enable();
        }
    }

    FLT_MGR_Check_Temperature();
    FLT_MGR_Check_Voltage();
    FLT_MGR_Check_Current();
    FLT_MGR_Check_Speed();
    FLT_MGR_Check_Irqstat();
    flt_mgr_Check_LIN_Fault();
}

#pragma optimize=balanced
STATIC void FLT_MGR_Check_Temperature(void)
{
    int16_t temperature = SYSIG_Get_CPU_Temp();

    if(temperature >= FLT_OVER_TEMP_LMT)
    {
        if(!detected_over_temp)
        {
            detected_over_temp = true;
            FLT_Temperature_Timestamp = TMUT_Get_Base_Time_MS();
        }
        else if(TMUT_Has_Time_Elapsed_MS(FLT_Temperature_Timestamp,  Fault_Parameters[FLT_OVER_TEMPERATURE].set_ms))
        {
            if (!Fault_Active[FLT_OVER_TEMPERATURE])
            {
                Fault_Active[FLT_OVER_TEMPERATURE] = true;
                FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_TEMPERATURE);
                clearing_over_temp = false;
            }
        }
        else{;}
    }
    else if((Fault_Active[FLT_OVER_TEMPERATURE]) && (temperature <= FLT_OVER_TEMP_LMTCLR))
    {
        if(!clearing_over_temp)
        {
            clearing_over_temp = true;
            FLT_Temperature_Timestamp = TMUT_Get_Base_Time_MS();
        }
        else if(TMUT_Has_Time_Elapsed_MS(FLT_Temperature_Timestamp, Fault_Parameters[FLT_OVER_TEMPERATURE].clear_ms))
        {
            detected_over_temp = false;
            Fault_Active[FLT_OVER_TEMPERATURE] = false;
            FLT_LOG_Record_Fault_Inactive(FLT_LOG_OVER_TEMPERATURE);
        }
        else{;}
    }
    else
    {
        /* in the case of this being detected then goes away before the mature time,
        this will cause new base time to be loaded if it comes back */
        detected_over_temp = false;
    }
}

#pragma optimize=balanced
STATIC void FLT_MGR_Check_Voltage(void)
{
    uint16_t volts = SYSIG_Get_Voltage();

    /* Check Over Voltage */
    if(volts > FLT_OVER_VOLTAGE_LMT)
    {
        if(!detected_over_voltage)
        {
            detected_over_voltage = true;
            FLT_Voltage_Timestamp = TMUT_Get_Base_Time_MS();
        }
        else if (TMUT_Has_Time_Elapsed_MS(FLT_Voltage_Timestamp, Fault_Parameters[FLT_OVER_VOLTAGE].set_ms))
        {
            if (!Fault_Active[FLT_OVER_VOLTAGE])
            {
                Fault_Active[FLT_OVER_VOLTAGE] = true;
                FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_VOLTAGE);
                clearing_over_voltage = false;
            }
        }
        else{;}
    }
    else if ((Fault_Active[FLT_OVER_VOLTAGE]) && (volts < FLT_OVER_VOLTAGE_CLEAR_LMT)) 
    {
        if(!clearing_over_voltage)
        {
            clearing_over_voltage = true;
            FLT_Voltage_Timestamp = TMUT_Get_Base_Time_MS();
        }
        else if(TMUT_Has_Time_Elapsed_MS(FLT_Voltage_Timestamp, Fault_Parameters[FLT_OVER_VOLTAGE].clear_ms))
        {
            detected_over_voltage = false;
            Fault_Active[FLT_OVER_VOLTAGE] = false;
            FLT_LOG_Record_Fault_Inactive(FLT_LOG_OVER_VOLTAGE);
        }
        else{;}
    }
    else 
    {
        /* in the case of this being detected then goes away before the mature time,
        this will cause new base time to be loaded if it comes back */
        detected_over_voltage = false; 
    }
    /* Check Under Voltage */
    if((volts < FLT_UNDER_VOLTAGE_LMT) && (volts >= FLT_UNDER_VOLT_SELF_P_LMT))
    {
        if(!detected_under_voltage)
        {
            detected_under_voltage = true;
            FLT_Voltage_Timestamp = TMUT_Get_Base_Time_MS();

        }
        else if (TMUT_Has_Time_Elapsed_MS(FLT_Voltage_Timestamp, Fault_Parameters[FLT_UNDER_VOLTAGE].set_ms))
        {
            if (!Fault_Active[FLT_UNDER_VOLTAGE])
            {
                Fault_Active[FLT_UNDER_VOLTAGE] = true;
                FLT_LOG_Record_Fault_Active(FLT_LOG_UNDER_VOLTAGE);
                clearing_under_voltage = false;
            }
        }
        else{;}
    }            
    else if ((volts >= FLT_UNDER_VOLTAGE_CLR) || (volts <= FLT_UNDER_VOLT_SELF_P_LMT)) 
    {
        if(Fault_Active[FLT_UNDER_VOLTAGE])
        {
            if(!clearing_under_voltage)
            {
                clearing_under_voltage = true;
                FLT_Voltage_Timestamp = TMUT_Get_Base_Time_MS();
            }
            else if(TMUT_Has_Time_Elapsed_MS(FLT_Voltage_Timestamp, Fault_Parameters[FLT_UNDER_VOLTAGE].clear_ms))
            {
                Fault_Active[FLT_UNDER_VOLTAGE] = false;
                detected_under_voltage = false; 
                FLT_LOG_Record_Fault_Inactive(FLT_LOG_UNDER_VOLTAGE);
            }
            else{;}
        }
        else
        {
            detected_under_voltage = false; /* force getting new base time if goes back to being undervoltage */
        }
    }
    else 
    {
        /* in the case of this being detected then goes away before the mature time,
        this will cause new base time to be loaded if it comes back */
        detected_under_voltage = false;
    }
    /* Check Under Voltage Self Protect */
    if(volts <= FLT_UNDER_VOLT_SELF_P_LMT)
    {
        if(!detected_under_voltage_self_p)
        {
            detected_under_voltage_self_p = true;
            FLT_Under_Voltage_Self_Protect_Timestamp = TMUT_Get_Base_Time_MS();
        }
        else if (TMUT_Has_Time_Elapsed_MS(FLT_Under_Voltage_Self_Protect_Timestamp, Fault_Parameters[FLT_UNDER_VOLT_SELF_PROTECT].set_ms))
        {
            if (!Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT])
            {
                Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = true;
                FLT_LOG_Record_Fault_Active(FLT_LOG_UNDER_VOLT_SELF_PROTECT);
                clearing_under_voltage_self_p = false;
            }
        }
        else{;}
    }            
    else if ((Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT]) && (volts >= FLT_UNDER_VOLT_SELF_P_CLR))
    {
        if(!clearing_under_voltage_self_p)
        {
            clearing_under_voltage_self_p = true;
            FLT_Under_Voltage_Self_Protect_Timestamp = TMUT_Get_Base_Time_MS();
        }
        else if(TMUT_Has_Time_Elapsed_MS(FLT_Under_Voltage_Self_Protect_Timestamp, Fault_Parameters[FLT_UNDER_VOLT_SELF_PROTECT].clear_ms))
        {
            Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = false;
            detected_under_voltage_self_p = false; 
            FLT_LOG_Record_Fault_Inactive(FLT_LOG_UNDER_VOLT_SELF_PROTECT);
        }
        else{;}
    }
    else 
    {
        /* in the case of this being detected then goes away before the mature time,
        this will cause new base time to be loaded if it comes back */
        detected_under_voltage_self_p = false;
    }
}

#pragma optimize=balanced
STATIC void FLT_MGR_Check_Current(void)
{
    int16_t current;
    VCOMM_RPM_T local_act_speed = SYSIG_Get_Actual_RPM();


    if (MC_CLOSED_LOOP == mc_get_comm_mode())
    {
        current = SYSIG_Get_Current();

        /* Check Over Current */
        if(current >= (int16_t)FLT_OVER_CURRENT_LMT)
        {
            if(!detected_over_current)
            {
                detected_over_current = true;
                FLT_Current_Timestamp = TMUT_Get_Base_Time_MS();
            }
            else if (TMUT_Has_Time_Elapsed_MS(FLT_Current_Timestamp, Fault_Parameters[FLT_OVER_CURRENT].set_ms))
            {
                Fault_Active[FLT_UNDER_CURRENT] = false;
                if (!Fault_Active[FLT_OVER_CURRENT])
                {
                    Fault_Active[FLT_OVER_CURRENT] = true;
                    FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_CURRENT);
                    clearing_over_current = false;
                }
            }
            else{;}
        }
        else if ((Fault_Active[FLT_OVER_CURRENT]) && (current <= (int16_t)FLT_OVER_CURRENT_CLR))
        {
            if(!clearing_over_current)
            {
                clearing_over_current = true;
                FLT_Current_Timestamp = TMUT_Get_Base_Time_MS();
            }
            else if (TMUT_Has_Time_Elapsed_MS(FLT_Current_Timestamp, Fault_Parameters[FLT_OVER_CURRENT].clear_ms))
            {
                Fault_Active[FLT_OVER_CURRENT] = false;
                detected_over_current = false;
                FLT_LOG_Record_Fault_Inactive(FLT_LOG_OVER_CURRENT);
            }
        }
        else
        {
            /* in the case of this being detected then goes away before the mature time,
            this will cause new base time to be loaded if it comes back */
            detected_over_current = false;
        }
        /* Check Under Current */
        if((current <= (int16_t)FLT_UNDER_CURRENT_LMT) && (local_act_speed >= (VCOMM_RPM_T)FLT_UNDER_CURRENT_ENABLE_SPEED)) 
        {
            if(!detected_under_current)
            {
                detected_under_current = true;
                FLT_Current_Timestamp = TMUT_Get_Base_Time_MS();
            }
            else if (TMUT_Has_Time_Elapsed_MS(FLT_Current_Timestamp, Fault_Parameters[FLT_UNDER_CURRENT].set_ms))
            {
                Fault_Active[FLT_OVER_CURRENT] = false;
                if (!Fault_Active[FLT_UNDER_CURRENT])
                {
                    Fault_Active[FLT_UNDER_CURRENT] = true;
                    FLT_LOG_Record_Fault_Active(FLT_LOG_UNDER_CURRENT);
                    clearing_under_current = false;
                }
            }
            else{;}
        }
        else if ((Fault_Active[FLT_UNDER_CURRENT]) && ((current >= (int16_t)FLT_UNDER_CURRENT_CLR) ||  
                (local_act_speed < (VCOMM_RPM_T)FLT_UNDER_CURRENT_ENABLE_SPEED)))
        {
            if(!clearing_under_current)
            {
                clearing_under_current = true;
                FLT_Current_Timestamp = TMUT_Get_Base_Time_MS();
            }
            else if (TMUT_Has_Time_Elapsed_MS(FLT_Current_Timestamp, Fault_Parameters[FLT_UNDER_CURRENT].clear_ms))
            {
                Fault_Active[FLT_UNDER_CURRENT] = false;
                detected_under_current = false;
                FLT_LOG_Record_Fault_Inactive(FLT_LOG_UNDER_CURRENT);
            }
            else{;}
        }
        else
        {
            /* in the case of this being detected then goes away before the mature time,
            this will cause new base time to be loaded if it comes back */
            detected_under_current = false;  
        }
    }
    else
    {
        /* Not running, clear all the current variables and faults */
        Fault_Active[FLT_UNDER_CURRENT] = false;
        Fault_Active[FLT_OVER_CURRENT] = false;
        detected_under_current = false;
        detected_over_current = false;
        clearing_over_current = false;
        clearing_under_current = false;
    }
}

#pragma optimize=balanced
STATIC void FLT_MGR_Check_Speed(void)
{
    VCOMM_RPM_T actual_speed = SYSIG_Get_Actual_RPM();
    VCOMM_RPM_T target_speed = MMGR_Get_Target_Speed();
    uint32_t temp_u32;
    VCOMM_RPM_T tolerance = 0u;
    VCOMM_RPM_T speed_diff = 0u;
      
    if (MC_CLOSED_LOOP == mc_get_comm_mode())
    {
        if(actual_speed > target_speed)
        {
            /* Over Speed */
            temp_u32 = ((uint32_t)target_speed * (uint32_t)FLT_SPEED_PCT_TOLERANCE);
            temp_u32 /= 1000u;
            tolerance = (VCOMM_RPM_T)temp_u32;  /* 1000 is for integer math to get back to .1 % units */

            speed_diff = actual_speed - target_speed;
            if(speed_diff >= tolerance)
            {
                if(!detected_over_speed)
                {
                    detected_over_speed = true;
                    FLT_Speed_Timestamp = TMUT_Get_Base_Time_MS();
                }
                else if(TMUT_Has_Time_Elapsed_MS(FLT_Speed_Timestamp, Fault_Parameters[FLT_OVER_SPEED].set_ms))
                {
                    Fault_Active[FLT_UNDER_SPEED] = false;
                    detected_under_speed = false;
                    if (!Fault_Active[FLT_OVER_SPEED])
                    {
                        Fault_Active[FLT_OVER_SPEED] = true;
                        FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_SPEED);
                        clearing_over_speed = false;
                        clearing_under_speed = false;
                    }
                }
                else{;}
            }
            else if((!clearing_over_speed) && (Fault_Active[FLT_OVER_SPEED]))
            {
                clearing_over_speed = true;
                detected_over_speed = false;
                FLT_Speed_Timestamp = TMUT_Get_Base_Time_MS();
            }
            else if(clearing_over_speed)
            {
                if(TMUT_Has_Time_Elapsed_MS(FLT_Speed_Timestamp, Fault_Parameters[FLT_OVER_SPEED].clear_ms))
                {
                    Fault_Active[FLT_OVER_SPEED] = false;
                    clearing_over_speed = false;
                    detected_over_speed = false;
                    FLT_LOG_Record_Fault_Inactive(FLT_LOG_OVER_SPEED);
                }
            }
            else
            {
                detected_over_speed = false;  
                clearing_over_speed = false;
            }
        }
        else if(target_speed > actual_speed)
        {
            /* Under Speed */
            temp_u32 = ((uint32_t)target_speed * (uint32_t)FLT_SPEED_PCT_TOLERANCE);
            temp_u32 /= 1000u;
            tolerance = (VCOMM_RPM_T)temp_u32;  /* 1000 is for integer math to get back to .1 % units */
            speed_diff = target_speed - actual_speed;
            if(speed_diff >= tolerance)
            {
                if(!detected_under_speed)
                {
                    detected_under_speed = true;
                    FLT_Speed_Timestamp = TMUT_Get_Base_Time_MS();
                }
                else if(TMUT_Has_Time_Elapsed_MS(FLT_Speed_Timestamp, Fault_Parameters[FLT_UNDER_SPEED].set_ms))
                {
                    Fault_Active[FLT_OVER_SPEED] = false;
                    detected_over_speed = false;
                    if (!Fault_Active[FLT_UNDER_SPEED])
                    {
                        Fault_Active[FLT_UNDER_SPEED] = true;
                        FLT_LOG_Record_Fault_Active(FLT_LOG_UNDER_SPEED);
                        clearing_under_speed = false;
                        clearing_over_speed = false;
                    }
                }
            }
            else if((!clearing_under_speed) && (Fault_Active[FLT_UNDER_SPEED]))
            {
                clearing_under_speed = true;
                detected_under_speed = false;
                FLT_Speed_Timestamp = TMUT_Get_Base_Time_MS();
            }
            else if(clearing_under_speed)
            {
                if(TMUT_Has_Time_Elapsed_MS(FLT_Speed_Timestamp, Fault_Parameters[FLT_UNDER_SPEED].clear_ms))
                {
                    Fault_Active[FLT_UNDER_SPEED] = false;
                    clearing_under_speed = false;
                    detected_under_speed = false;
                    FLT_LOG_Record_Fault_Inactive(FLT_LOG_UNDER_SPEED);
                }
            }
            else
            {
                /* in the casee of this being detected then goes away before the mature time, 
                   this will cause new base time to be loaded if it comes back */
                detected_under_speed = false;
                clearing_under_speed = false; 
            }
        }   
        /* below handles when actual and target speed are ==, and there's a fault to clear */
        /* in there unlikely event that a fault has been set and speed stays equal forever */ 
        else if(Fault_Active[FLT_OVER_SPEED])
        {
            if(!clearing_over_speed)
            {
                clearing_over_speed = true;
                detected_over_speed = false;
                FLT_Speed_Timestamp = TMUT_Get_Base_Time_MS();
            }
            else if(TMUT_Has_Time_Elapsed_MS(FLT_Speed_Timestamp, Fault_Parameters[FLT_OVER_SPEED].clear_ms))
            {
                {
                    Fault_Active[FLT_OVER_SPEED] = false;
                    clearing_over_speed = false;
                    detected_over_speed = false;
                    FLT_LOG_Record_Fault_Inactive(FLT_LOG_OVER_SPEED);
                }
            }
            else{;}
        }
        else if(Fault_Active[FLT_UNDER_SPEED])
        {
            if(!clearing_under_speed)
            {
                clearing_under_speed = true;
                detected_under_speed = false;
                FLT_Speed_Timestamp = TMUT_Get_Base_Time_MS();
            }
            else if(TMUT_Has_Time_Elapsed_MS(FLT_Speed_Timestamp, Fault_Parameters[FLT_UNDER_SPEED].clear_ms))
            {
                Fault_Active[FLT_UNDER_SPEED] = false;
                clearing_under_speed = false;
                detected_under_speed = false;
                FLT_LOG_Record_Fault_Inactive(FLT_LOG_UNDER_SPEED);
            }
            else{;}            
        }
        else{;}
    }
    else
    {
        /* Not running, clear all the speed variables and faults */
        Fault_Active[FLT_OVER_SPEED] = false;
        Fault_Active[FLT_UNDER_SPEED] = false;
        detected_under_speed = false;
        detected_over_speed = false;
        clearing_under_speed = false;
        clearing_over_speed = false;
    }
}

#pragma optimize=balanced
bool_t FLT_MGR_Is_In_Self_Protect(void)
{
    bool_t return_val = false;

    if(    (Fault_Active[FLT_OVER_VOLTAGE])
        || (Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT])
        || (FLT_MGR_Irqstat_Fault))
    {
        return_val = true;
    }
   
    return (return_val);
}

bool_t FLT_MGR_Is_Fault_Active(uint8_t index)
{
    return(Fault_Active[index]);
}

#pragma optimize=balanced
bool_t FLT_MGR_Is_Any_Fault_Active(void)
{
    uint8_t i;
    bool_t return_val = false;

    for(i = 0u; i < FLT_MGR_NUM_FAULTS; i++)
    {
        if(Fault_Active[i])
        {
            return_val = true; 
            break; 
        }
    }
    return (return_val);
}

/* Check IRQSTAT1 and IRQSTAT2  flags for shorted, over temp, over current,  SPI_0 fault
   Vbatt, VG, or Vcc faults.  */
#pragma optimize=balanced
STATIC void FLT_MGR_Check_Irqstat( void )
{
    uint16_t irqstat_reqs; 

    irqstat_reqs = mc_get_irqstat();
    if(irqstat_reqs != 0u) /* Set fault if any bits are set in mc_data.irqstat1 or mc_data.irqstat2 */
    {
        FLT_MGR_Irqstat_Fault = true;
        FLT_LOG_Record_Fault_Active(FLT_LOG_SHORT_CIRCUIT);
    }
    else
    {
        FLT_MGR_Irqstat_Fault = false;
        FLT_LOG_Record_Fault_Inactive(FLT_LOG_SHORT_CIRCUIT);
    }
}

/**
 * Periodic monitoring of the LIN Fault timer to determine if the LIN faults have cleared
 */
STATIC void flt_mgr_Check_LIN_Fault(void)
{
    if (    (Lin_Fault_J2602 != FLT_MGR_J2602_NO_ERROR)
         && (TMUT_Has_Time_Elapsed_MS(FLT_LIN_Fault_Timestamp, FLT_MGR_LIN_FAULT_TIMEOUT_MS)))
    {
        FLT_MGR_Clear_Lin_Fault_J2602();
    }
}

#pragma optimize=balanced
void FLT_MGR_Set_Lin_Fault_Callback(uint16_t lin_fault)
{
    FLT_MGR_LIN_J2602_Err_T Pending_LIN_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;

    FLT_LIN_Fault_Timestamp = TMUT_Get_Base_Time_MS();

    if(lin_fault != 0u)
    {
        switch (lin_fault)
        {
            case LinBusIf_ERR_SYNC_FAIL:
                Pending_LIN_Fault_J2602 = FLT_MGR_J2602_ID_PARITY_ERROR;
            break;

            case LinBusIf_ERR_BUS_COLLISION:
                Pending_LIN_Fault_J2602 = FLT_MGR_J2602_DATA_ERROR;
            break;

            case  LinBusIf_ERR_PID_PARITY:
                Pending_LIN_Fault_J2602 = FLT_MGR_J2602_ID_PARITY_ERROR;
            break;

            case LinBusIf_ERR_CRC:
                Pending_LIN_Fault_J2602 = FLT_MGR_J2602_CHECKSUM_ERROR;
            break;

            case LinBusIf_ERR_INIT:
                Pending_LIN_Fault_J2602 = FLT_MGR_J2602_RESET;   /* not sure if this is right, but could be a clue of something */
            break;

            case LinBusIf_ERR_FRAMING_ERROR:
                Pending_LIN_Fault_J2602 = FLT_MGR_J2602_FRAMING_ERROR;
            break;

            default:
                Pending_LIN_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;
            break;    
        }
    }

    if (Pending_LIN_Fault_J2602 != Lin_Fault_J2602)
    {
        /* Deactivate the previous fault */
        FLT_LOG_Record_Fault_Inactive(flt_mgr_Translate_LIN_To_Fault_Log(Lin_Fault_J2602));

        Lin_Fault_J2602 = Pending_LIN_Fault_J2602;

        /* Activate the new fault */
        FLT_LOG_Record_Fault_Active(flt_mgr_Translate_LIN_To_Fault_Log(Lin_Fault_J2602));
    }
    
}

FLT_MGR_LIN_J2602_Err_T FLT_MGR_Get_Lin_Fault_J2602(void)
{
    return (Lin_Fault_J2602);
}

void FLT_MGR_Clear_Lin_Fault_J2602(void)
{
    FLT_LOG_Record_Fault_Inactive(flt_mgr_Translate_LIN_To_Fault_Log(Lin_Fault_J2602));

    Lin_Fault_J2602 = FLT_MGR_J2602_NO_ERROR;
}

bool_t FLT_MGR_IRQSTAT_Fault_Present(void)
{
    return(FLT_MGR_Irqstat_Fault);
}

/**
 * Translates the Elmos LIN Fault to the Faults logged by the Fault Logger
 *
 * @param LIN_Fault - fault to translate
 *
 * @return Fault Logger fault id that relates to the LIN Fault
 */
#pragma optimize=balanced
STATIC FLT_LOG_Faults_T flt_mgr_Translate_LIN_To_Fault_Log(FLT_MGR_LIN_J2602_Err_T LIN_Fault)
{
    FLT_LOG_Faults_T Log_Fault_ID;

    switch (LIN_Fault)
    {
        case FLT_MGR_J2602_ID_PARITY_ERROR:
            Log_Fault_ID = FLT_LOG_LIN_ID_PARITY_ERROR;
            break;

        case FLT_MGR_J2602_DATA_ERROR:
            Log_Fault_ID = FLT_LOG_LIN_DATA_ERROR;
            break;

        case FLT_MGR_J2602_CHECKSUM_ERROR:
            Log_Fault_ID = FLT_LOG_LIN_CHECKSUM_ERROR;
            break;

        case FLT_MGR_J2602_FRAMING_ERROR:
            Log_Fault_ID = FLT_LOG_LIN_BIT_FIELD_FRAMING_ERROR;
            break;

        default:
            Log_Fault_ID = FLT_LOG_NO_ERROR;
            break;
    }

    return Log_Fault_ID;
}

/** @} doxygen end group */
