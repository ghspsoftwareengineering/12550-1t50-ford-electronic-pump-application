/**
 * @ingroup HW
 *
 * @{
 */

/**********************************************************************/
/*! @file diag_pwm_in.c
 * @brief measurement of a PWM input signal
 *
 * @details
 * configuration and control functions for measuring a PWM input signal <br>
 * at a PORTC GPIO pin via CCTIMER2 module <br>
 *
 * module prefix: diagi_
 *
 * @author       RHA
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <diag_pwm_in.h>
#include <defines.h>

#include <debug.h>

#if ( USE_PWM_DIAG_INPUT == 1 )

/*! @brief initialize diagnostic PWM input
 *
 * @details
 * Initialize CCTIMER module (prescaler!) and set the IOMUX of the input pin <br>
 * NOTE: diagi_enable(True) must be called before the measurement actually starts!
 */
  void diagi_init( const u16 prescaler )
  {
    SYS_STATE_MODULE_ENABLE_bit.cctimer_2 = 1u;

    CCTIMER_2_PRESCALER = prescaler - 1u;

    CCTIMER_2_CONFIG_A_bit.mode        = 3u; /* capture mode */
    CCTIMER_2_CONFIG_A_bit.capture_sel = 2u; /* capt. evt. on neg. edge */
    CCTIMER_2_CONFIG_A_bit.restart_sel = 2u; /* restart on pos. edge */

    CCTIMER_2_CONFIG_B_bit.mode        = 3u; /* capture mode */
    CCTIMER_2_CONFIG_B_bit.capture_sel = 1u; /* capt. evt. on pos. edge */
    CCTIMER_2_CONFIG_B_bit.restart_sel = 2u; /* restart on pos. edge */

    #if ( PWM_DIAG_INPUT_PORT != GPIO_C )
    #error Diagnostic PWM input is only supported at PORT C!
    #endif

    #if ( PWM_DIAG_INPUT_PIN == GPIO_IO_0 )
      IOMUX_CTRL_PC0123_IO_SEL_bit.sel_0 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_2_MEAS;
    #elif ( PWM_DIAG_INPUT_PIN == GPIO_IO_1 )
      IOMUX_CTRL_PC0123_IO_SEL_bit.sel_1 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_2_MEAS;
    #elif ( PWM_DIAG_INPUT_PIN == GPIO_IO_2 )
      IOMUX_CTRL_PC0123_IO_SEL_bit.sel_2 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_2_MEAS;
    #elif ( PWM_DIAG_INPUT_PIN == GPIO_IO_3 )
      IOMUX_CTRL_PC0123_IO_SEL_bit.sel_3 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_2_MEAS;
    #elif ( PWM_DIAG_INPUT_PIN == GPIO_IO_4 )
      IOMUX_CTRL_PC4567_IO_SEL_bit.sel_4 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_2_MEAS;
    #elif ( PWM_DIAG_INPUT_PIN == GPIO_IO_5 )
      IOMUX_CTRL_PC4567_IO_SEL_bit.sel_5 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_2_MEAS;
    #elif ( PWM_DIAG_INPUT_PIN == GPIO_IO_6 )
      IOMUX_CTRL_PC4567_IO_SEL_bit.sel_6 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_2_MEAS;
    #elif ( PWM_DIAG_INPUT_PIN == GPIO_IO_7 )
      IOMUX_CTRL_PC4567_IO_SEL_bit.sel_7 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_2_MEAS;
    #else
    #error PWM_DIAG_INPUT_PIN has an invalid value!
    #endif

    #warning Diagnostic PWM input enabled. PWM_DIAG_INPUT_PIN functionality overridden! CCTIMER_2 in use!  /*  PRQA S 1008, 3115 */
  }

/*! @brief enable (and restart) CCTIMER or disable it */
  INLINE void diagi_enable( const BOOL enable )
  {
    if( enable )
    {
      CCTIMER_2_CONTROL = (u16) E_CCTIMER_CONTROL_ENABLE_A | (u16) E_CCTIMER_CONTROL_ENABLE_B | (u16) E_CCTIMER_CONTROL_RESTART_A | (u16) E_CCTIMER_CONTROL_RESTART_B | (u16) E_CCTIMER_CONTROL_RESTART_P;
    }
    else
    {
      CCTIMER_2_CONTROL = 0u;
    }
  }

/*! @brief when function returns True, a new PWM period has passed -> new values for duty cycle and period available */
  INLINE BOOL diagi_new_meas_available( void )
  {
    BOOL loc_return_value = False;

    if( CCTIMER_2_IRQ_STATUS_bit.capture_b != 0u )
    {
      /* reset capture_b flag */
      CCTIMER_2_IRQ_STATUS_bit.capture_b = 1u;
      loc_return_value                   = True;
    }

    return loc_return_value;
  }

/*! @brief retrieve most recent duty cycle value (actually, the capture value for falling edge; the counter is reset on rising edge) */
  INLINE u16 diagi_get_dutycycle( void )
  {
    return CCTIMER_2_CAPCMP_A;
  }

/*! @brief retrieve most recent period value (actually, the counter value between the last two rising edges) */
  INLINE u16 diagi_get_period( void )
  {
    return CCTIMER_2_CAPCMP_B;
  }

#endif /* _#if ( USE_PWM_DIAG_INPUT == 1 ) */

/* }@
 */
