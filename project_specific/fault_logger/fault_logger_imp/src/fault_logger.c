/**
 *  @file fault_logger.c
 *
 *  @ref fault_logger_api
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_logger_imp template Implementation
 *  @{
 *
 *      System specific implementation of the @ref fault_logger_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_environmental_logger.h"
#include "fault_logger.h"
#include "fault_logger_imp_types.h"
#include "global.h"
#include "lin_vehicle_comm.h"
#include "nvm_mgr.h"
#include "serial_data_buffer.h"
#include "time_utils.h"
#include "uds_types.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/
STATIC bool_t flt_log_Clear_Fault_Record(FLT_LOG_Faults_T Fault_ID, bool_t Force_Update);

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
STATIC Time_MS_T flt_log_Fault_Start_Times[FLT_LOG_NUM_FAULTS] = {0u};
STATIC bool_t flt_log_Fault_Is_Active[FLT_LOG_NUM_FAULTS] = {false};

STATIC flt_log_Occurence_Block_T flt_log_Occurence_Blocks[FLT_LOG_NUM_DATA_BLOCKS] =
{
    {
        .Dirty = false,
        .NVM_Block_ID = 0u
    },
    {
        .Dirty = false,
        .NVM_Block_ID = 1u
    },
    {
        .Dirty = false,
        .NVM_Block_ID = 2u
    }
};

STATIC flt_log_Data_Map_T flt_log_Data_Map[FLT_LOG_NUM_FAULTS] =
{
    {0u, 0u}, /**< FLT_LOG_OVER_VOLTAGE */
    {0u, 1u}, /**< FLT_LOG_UNDER_VOLTAGE */
    {0u, 2u}, /**< FLT_LOG_UNDER_VOLT_SELF_PROTECT */
    {0u, 3u}, /**< FLT_LOG_OVER_CURRENT */
    {0u, 4u}, /**< FLT_LOG_UNDER_CURRENT */
    {0u, 5u}, /**< FLT_LOG_OVER_TEMPERATURE */
    {0u, 6u}, /**< FLT_LOG_OVER_SPEED */
    {1u, 0u}, /**< FLT_LOG_UNDER_SPEED */
    {1u, 1u}, /**< FLT_LOG_LIN_DATA_ERROR */
    {1u, 2u}, /**< FLT_LOG_LIN_BIT_FIELD_FRAMING_ERROR */
    {1u, 3u}, /**< FLT_LOG_LIN_ID_PARITY_ERROR */
    {1u, 4u}, /**< FLT_LOG_LIN_CHECKSUM_ERROR */
    {1u, 5u}, /**< FLT_LOG_LIN_MASTER_REQUEST_MISSING */
    {1u, 6u}, /**< FLT_LOG_LIN_MASTER_REQUEST_DENIED */
    {2u, 0u}, /**< FLT_LOG_BLOCKED_GEROTOR */
    {2u, 1u}, /**< FLT_LOG_SHORT_CIRCUIT */
};

STATIC bool_t flt_log_Enabled = false;

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
#pragma optimize=balanced
void FLT_LOG_Scan(void)
{
    uint8_t Block_ID = 0u;
    for (Block_ID = 0u; Block_ID < FLT_LOG_NUM_DATA_BLOCKS; Block_ID++)
    {
        if (flt_log_Occurence_Blocks[Block_ID].Dirty)
        {
            if (NVM_STATUS_SUCCESS == NVMMGR_Write_NVM_Block(flt_log_Occurence_Blocks[Block_ID].NVM_Block_ID))
            {
                flt_log_Occurence_Blocks[Block_ID].Dirty = false;
            }
        }
    }
}

#pragma optimize=balanced
void FLT_LOG_Init(void)
{
    /* Register fault logging shadow RAM for each fault */
    NVMMGR_Register_Shadow(0u, (uint8_t *)&flt_log_Occurence_Blocks[0].Block_Data, NULL);
    NVMMGR_Register_Shadow(1u, (uint8_t *)&flt_log_Occurence_Blocks[1].Block_Data, NULL);
    NVMMGR_Register_Shadow(2u, (uint8_t *)&flt_log_Occurence_Blocks[2].Block_Data, NULL);

    fltel_Init();
}

void FLT_LOG_Enable(void)
{
    flt_log_Enabled = true;
}

void FLT_LOG_Disable(void)
{
    flt_log_Enabled = false;
}

bool_t FLT_LOG_Is_Enabled(void)
{
    return flt_log_Enabled;
}

#pragma optimize=balanced
void FLT_LOG_Record_Fault_Active(FLT_LOG_Faults_T Fault_ID)
{
    if (    (FLT_LOG_NO_ERROR != Fault_ID)
         && (flt_log_Enabled)
         && (!flt_log_Fault_Is_Active[Fault_ID]))
    {
        uint8_t Block_ID = flt_log_Data_Map[Fault_ID].Block_ID;
        uint8_t Index    = flt_log_Data_Map[Fault_ID].Index;

        flt_log_Fault_Start_Times[Fault_ID] = TMUT_Get_Base_Time_MS();
        flt_log_Fault_Is_Active[Fault_ID] = true;

        if (VCOMM_MODE_IDLE == VCOMM_Signal_Get_TrnOilPumpMde_D_Stat())
        {
            if (flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].IDLE_State_Count < (uint8_t)UINT8_MAX)
            {
                flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].IDLE_State_Count++;
            }
        }
        else
        {
            if (flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].RUN_State_Count < (uint8_t)UINT8_MAX)
            {
                flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].RUN_State_Count++;
            }
        }

        flt_log_Occurence_Blocks[Block_ID].Dirty = true;

        fltel_Log_Fault(Fault_ID);
    }
}

#pragma optimize=balanced
void FLT_LOG_Record_Fault_Inactive(FLT_LOG_Faults_T Fault_ID)
{
    if (    (FLT_LOG_NO_ERROR != Fault_ID)
         && (flt_log_Enabled)
         && (flt_log_Fault_Is_Active[Fault_ID]))
    {
        uint8_t Block_ID = flt_log_Data_Map[Fault_ID].Block_ID;
        uint8_t Index    = flt_log_Data_Map[Fault_ID].Index;

        flt_log_Fault_Is_Active[Fault_ID] = false;

        flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].Total_Duration_Sec += (uint16_t)(TMUT_Get_Elapsed_Time_MS(flt_log_Fault_Start_Times[Fault_ID]) / (uint16_t)1000u);
        flt_log_Occurence_Blocks[Block_ID].Dirty = true;
    }
}

#pragma optimize=balanced
UDS_Response_Code_T FLT_LOG_Get_Fault_Log_Records_DID(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    UDS_Response_Code_T Status = UDS_RESP_POSITIVE;
    FLT_LOG_Faults_T Fault_ID = (FLT_LOG_Faults_T)0u;

    if (flt_log_Enabled)
    {
        SDB_Serialize_U8(p_Resp_SDB, FLT_LOG_NUM_FAULTS);

        for (Fault_ID = (FLT_LOG_Faults_T)0u; Fault_ID < FLT_LOG_NUM_FAULTS; Fault_ID++)
        {
            uint8_t Block_ID = flt_log_Data_Map[Fault_ID].Block_ID;
            uint8_t Index    = flt_log_Data_Map[Fault_ID].Index;

            SDB_Serialize_U16(p_Resp_SDB, flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].Total_Duration_Sec);
            SDB_Serialize_U8(p_Resp_SDB, flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].IDLE_State_Count);
            SDB_Serialize_U8(p_Resp_SDB, flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].RUN_State_Count);
        }
    }
    else
    {
        Status = UDS_RESP_CONDITIONS_NOT_CORRECT;
    }

    return Status;
}

#pragma optimize=balanced
UDS_Response_Code_T FLT_LOG_Clear_Fault_Record_DID(Serial_Data_Buffer_T * const p_Req_SDB)
{
    UDS_Response_Code_T Status = UDS_RESP_POSITIVE;
    FLT_LOG_Faults_T Fault_ID = (FLT_LOG_Faults_T)SDB_Deserialize_U8(p_Req_SDB);

    if (Fault_ID < FLT_LOG_NUM_FAULTS)
    {
        if (!flt_log_Clear_Fault_Record(Fault_ID, true))
        {
            Status = UDS_RESP_GENERAL_PROGRAMMING_FAILURE;
        }
    }
    else
    {
        Status = UDS_RESP_REQ_OUT_OF_RANGE;
    }

    return Status;
}

#pragma optimize=balanced
bool_t FLT_LOG_Clear_Fault_Records(void)
{
    bool_t Success = true;
    uint8_t Data_Block = 0u;
    FLT_LOG_Faults_T Fault_ID = (FLT_LOG_Faults_T)0u;

    for (Fault_ID = (FLT_LOG_Faults_T)0u; Fault_ID < FLT_LOG_NUM_FAULTS; Fault_ID++)
    {
        Success = flt_log_Clear_Fault_Record(Fault_ID, false);
        if (!Success)
        {
            break;
        }
    }

    /* Write all the blocks used for the Fault Log */
    for (Data_Block = 0u; Data_Block < FLT_LOG_NUM_DATA_BLOCKS; Data_Block++)
    {
        if (NVM_STATUS_SUCCESS != NVMMGR_Write_NVM_Block(flt_log_Occurence_Blocks[Data_Block].NVM_Block_ID))
        {
            Success = false;
            break;
        }
    }

    return Success;
}

#pragma optimize=balanced
STATIC bool_t flt_log_Clear_Fault_Record(FLT_LOG_Faults_T Fault_ID, bool_t Force_Update)
{
    bool_t Status = true;

    if (Fault_ID < FLT_LOG_NUM_FAULTS)
    {
        uint8_t Block_ID = flt_log_Data_Map[Fault_ID].Block_ID;
        uint8_t Index    = flt_log_Data_Map[Fault_ID].Index;

        flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].Total_Duration_Sec = 0u;
        flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].IDLE_State_Count = 0u;
        flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Index].RUN_State_Count = 0u;

        if (Force_Update)
        {
            if (NVM_STATUS_SUCCESS != NVMMGR_Write_NVM_Block(Block_ID)) /* Update the fault record EEPROM block */
            {
                Status = false;
            }
        }
    }

    return Status;
}
/** @} doxygen end group */
