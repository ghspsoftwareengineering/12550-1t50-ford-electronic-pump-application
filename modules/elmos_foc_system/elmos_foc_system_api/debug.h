/*$6*/

#ifndef DEBUG_H
#define DEBUG_H

#include "global.h"

#if ( DEBUG == 1 )

  #define DBG_HALT_ON_IC_TYPE_MISMATCH 0               /* IC revision set in defines.h is checked against the values from the INFO flash area */

/* debug pins */
  #define DBG_PORT GPIO_C

  #define DBG_PIN_MUX_FUNC IOMUX_CTRL_FUNC_C_GPIO

 #if ( USR_CONFIGURATION != USR_CFG_DEMOBOARD_52306_DUAL_SHUNT )
    #define DBG_EN_PIN3 ( 1 )
 #else
    #define DBG_EN_PIN3 ( 0 )
 #endif

#define DBG_PIN3     GPIO_IO_6
#define DBG_PIN3_MUX IOMUX_CTRL_PC4567_IO_SEL_bit.sel_6

/***********************************/

 #if 0
    #define DBG_CONST_VBAT_MV               ( 13500u ) /* if defined, the measurement of the battery voltage is ignored and this value is used instead */
    #define USR_PHASE_VOLTAGE_DIVIDER_RATIO ( 9u )     /* todo: optimize */
 #else
    #define DBG_CONST_VBAT_MV               ( 0u )     /* if defined, the measurement of the battery voltage is ignored and this value is used instead */
    #define USR_PHASE_VOLTAGE_DIVIDER_RATIO ( 9u )     /* todo: optimize */
 #endif
#else
  #define DBG_CONST_VBAT_MV ( 0u )
#endif

/***********************************/

#define DBG_SHOW_NOTHING                      ( 0xFFFF )
#define DBG_ISR_DURATION                      ( 1 )
#define DBG_ENABLE_PWM3_PB7                   ( 2 )
#define DBG_MOT_CTRL_DURATION                 ( 3 )
#define DBG_PI_EXEC_DURATION                  ( 4 )
#define DBG_PI_SHOW_OVERFLOW                  ( 5 )
#define DBG_SHOW_WD_TRIGGER_ACCESS            ( 6 )
#define DBG_SHOW_SPI_FRAME                    ( 7 )
#define DBG_SHOW_SINE_CALC_DURATION           ( 8 )
#define DBG_SHOW_HALL_CALC_DURATION           ( 9 )
#define DBG_ESTIMATED_ROTOR_ANGLE_IS_ZERO     ( 10 )
#define DBG_HALL_ROTOR_ANGLE_IS_ZERO          ( 11 )
#define DBG_SHOW_DEBUG_MAIN_DURATION          ( 12 )
#define DBG_SHOW_LUENBERGER_CALC_DURATION     ( 13 )
#define DBG_SHOW_LUENBERGER_CALC_OVERFLOW     ( 14 )
#define DBG_SHOW_ROTOR_ANGLE_TICK             ( 15 )
#define DBG_SHOW_DRIVER_ERROR                 ( 16 )
#define DBG_SHOW_CURRENT_RAMP                 ( 17 )
#define DBG_SHOW_SPEED_RAMP                   ( 18 )
#define DBG_ADC_SHOW_SET_NEW_LIST             ( 19 )
#define DBG_SHOW_SUPPLY_CURRENT_CALC_DURATION ( 20 )
#define DBG_SHOW_CLOSED_LOOP                  ( 21 )
#define DBG_SHOW_STALLED_ROTOR                ( 22 )
#define DBG_SHOW_SYNC_TO_TURNING_ROTOR        ( 23 )
#define DBG_SHOW_FAST_STARTUP                 ( 24 )
#define DBG_SHOW_HALL_CCT_PIN_U_STATE         ( 25 )
#define DBG_SHOW_HALL_CCT_CAPTURE_ENABLED     ( 26 )
#define DBG_MAIN_LOOP_DURATION                ( 27 )

/*  set the debugging signal which is then output at DBG_PIN_3 */
#define DBG_PIN3_FUNCTION DBG_ISR_DURATION

/* debug function declarations */
void dbg_init ( void );
void dbg_main( void );

void dbg_pin3_pulse( void );
void dbg_pin3_set( void );
void dbg_pin3_clear( void );
void dbg_pin3_code ( uint8_t code );

#endif
