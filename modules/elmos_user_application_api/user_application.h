#ifndef USER_APPLICATION_H
#define USER_APPLICATION_H

/**
 *  @file template.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup template_api template Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/**
 * User application initialization
 */
void app_init( void );

/**
 * User application main point of entry. Function must return.
 */
void app_main( void );

void usr_output_voltage_overflow_handler( void );

/** @} doxygen end group */

#endif /* USER_APPLICATION_H */

