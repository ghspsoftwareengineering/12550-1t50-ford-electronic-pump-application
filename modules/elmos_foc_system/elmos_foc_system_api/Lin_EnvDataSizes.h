/***************************************************************************//**
 * @file    Lin_EnvDataSizes_H430.h
 *
 * @creator SBAI
 * @created 06.04.2017
 *
 * @brief
 *
 * @purpose
 *
 * $Id: Lin_EnvDataSizes_H430.h 2421 2017-05-11 12:52:48Z sbai $
 *
 * $Revision: 2421 $
 *
 ******************************************************************************/
#ifndef LIN_ENVDATASIZES_H430_H_
#define LIN_ENVDATASIZES_H430_H_

#pragma system_include

/* ****************************************************************************/
/* ***************************** INCLUDES *************************************/
/* ****************************************************************************/
#include "LinDrvImp_CompilationConfig.h"
#include "Lin_DevEnvDataSizes.h"
#include "LinDiag_Interface.h"
#include "LinUDS_Interface.h"

/* ****************************************************************************/
/* ************************ DEFINES AND MACROS ********************************/
/* ****************************************************************************/

/* ****************************************************************************/
/*                               LOOKUP LAYER                                 */
/* ****************************************************************************/
#define LINDRV_LOOKUP_FIX_ENVIRONMENT_DATA_SZE 162u                 /**< LIN LOOKUP Layer environment data size */
#define LIN_LOOKUP_FIX_FRM_DESC_ENV_DATA_SZE(FRAME_CNT) (FRAME_CNT) /**< LIN LOOKUP Layer per'Frame Description List' environment data size */

#define LINDRV_LOOKUP_SEQ_ENVIRONMENT_DATA_SZE 34u                  /**< LIN LOOKUP Layer environment data size w/o lookup table (64*2 byte) */
#define LIN_LOOKUP_SEQ_FRM_DESC_ENV_DATA_SZE(FRAME_CNT) (FRAME_CNT) /**< LIN LOOKUP Layer per'Frame Description List' environment data size */

#if LINLOOKUP_USE_FIX_LOOKUP == 1

#define LINDRV_LOOKUP_ENVIRONMENT_DATA_SZE LINDRV_LOOKUP_FIX_ENVIRONMENT_DATA_SZE                     /**< LIN LOOKUP Layer environment data size */
#define LIN_LOOKUP_FRM_DESC_ENV_DATA_SZE(FRAME_CNT) LIN_LOOKUP_FIX_FRM_DESC_ENV_DATA_SZE(FRAME_CNT)   /**< LIN LOOKUP Layer per'Frame Description List' environment data size */

#elif LINLOOKUP_USE_SEQ_LOOKUP == 1

#define LINDRV_LOOKUP_ENVIRONMENT_DATA_SZE LINDRV_LOOKUP_SEQ_ENVIRONMENT_DATA_SZE                     /**< LIN LOOKUP Layer environment data size w/o lookup table (64*2 byte) */
#define LIN_LOOKUP_FRM_DESC_ENV_DATA_SZE(FRAME_CNT) LIN_LOOKUP_SEQ_FRM_DESC_ENV_DATA_SZE(FRAME_CNT)   /**< LIN LOOKUP Layer per'Frame Description List' environment data size */

#endif

/* ****************************************************************************/
/*                                PROTO LAYER                                 */
/* ****************************************************************************/
#define LINPROTOIMP_ENVIRONMENT_DATA_SZE(MSG_BUF_SZE)         ((44u)+ (MSG_BUF_SZE))  /**< LIN PROTO Layer environment data size */

#define LINPROTOIMP_PER_FRM_DESC_LST_ENV_DATA_SZE(FRAME_CNT)  (LIN_LOOKUP_FRM_DESC_ENV_DATA_SZE(FRAME_CNT)) /**< LIN PROTO Layer per 'Frame Description List' environment data size */

/* ****************************************************************************/
/*                               TRANS LAYER                                  */
/* ****************************************************************************/
#if LINTRANSIMP_MULTI_PDU_SUPPORT == 1
#define LINTRANSIMP_ENVIRONMENT_DATA_SZE(MSG_BUF_SZE) (116u + (LINTRANSIMP_MAX_SID_DESCRIPTION_LISTS * 4) + (MSG_BUF_SZE)) /**< LIN TRANS Layer environment data size. */
#else
#define LINTRANSIMP_ENVIRONMENT_DATA_SZE(MSG_BUF_SZE) (114u + (LINTRANSIMP_MAX_SID_DESCRIPTION_LISTS * 4) + (MSG_BUF_SZE)) /**< LIN TRANS Layer environment data size. */
#endif

/* ****************************************************************************/
/*                                DIAG LAYER                                  */
/* ****************************************************************************/
#if LINDIAG_SUP_SNPD == 1 && LINDIAG_SUP_DATASTG == 1
  #define LINDIAGIMP_ENVIRONMENT_DATA_SZE (46u + (LINDIAG_MAX_RBI_TBL_CNT * 2u) + (LINDIAG_MAX_SUPPORTED_TIMER * 10u))
#elif LINDIAG_SUP_SNPD == 0 && LINDIAG_SUP_DATASTG == 1
  #define LINDIAGIMP_ENVIRONMENT_DATA_SZE (36u + (LINDIAG_MAX_RBI_TBL_CNT * 2u) + (LINDIAG_MAX_SUPPORTED_TIMER * 10u))
#elif LINDIAG_SUP_SNPD == 1 && LINDIAG_SUP_DATASTG == 0
  #define LINDIAGIMP_ENVIRONMENT_DATA_SZE (42u + (LINDIAG_MAX_RBI_TBL_CNT * 2u) + (LINDIAG_MAX_SUPPORTED_TIMER * 10u))
#else
  #define LINDIAGIMP_ENVIRONMENT_DATA_SZE (32u + (LINDIAG_MAX_RBI_TBL_CNT * 2u) + (LINDIAG_MAX_SUPPORTED_TIMER * 10u))
#endif

/* ****************************************************************************/
/*                                UDS LAYER                                  */
/* ****************************************************************************/
#if LINUDS_SUP_SNPD == 1 && LINUDS_SUP_DATASTG == 1
  #define LINUDSIMP_ENVIRONMENT_DATA_SZE (46u + (LINUDS_MAX_RBI_TBL_CNT * 2u) + (LINUDS_MAX_DID_TBL_CNT * 2u) + (LINUDS_MAX_ROUTINEID_TBL_CNT * 2u) + (LINUDS_MAX_SUPPORTED_TIMER * 10u))
#elif LINUDS_SUP_SNPD == 0 && LINUDS_SUP_DATASTG == 1
  #define LINUDSIMP_ENVIRONMENT_DATA_SZE (36u + (LINUDS_MAX_RBI_TBL_CNT * 2u) + (LINUDS_MAX_DID_TBL_CNT * 2u) + (LINUDS_MAX_ROUTINEID_TBL_CNT * 2u) + (LINUDS_MAX_SUPPORTED_TIMER * 10u))
#elif LINUDS_SUP_SNPD == 1 && LINUDS_SUP_DATASTG == 0
  #define LINUDSIMP_ENVIRONMENT_DATA_SZE (42u + (LINUDS_MAX_RBI_TBL_CNT * 2u) + (LINUDS_MAX_DID_TBL_CNT * 2u) + (LINUDS_MAX_ROUTINEID_TBL_CNT * 2u) + (LINUDS_MAX_SUPPORTED_TIMER * 10u))
#else
  #define LINUDSIMP_ENVIRONMENT_DATA_SZE (32u + (LINUDS_MAX_RBI_TBL_CNT * 2u) + (LINUDS_MAX_DID_TBL_CNT * 2u) + (LINUDS_MAX_ROUTINEID_TBL_CNT * 2u) + (LINUDS_MAX_SUPPORTED_TIMER * 10u))
#endif

#endif /* LIN_ENVDATASIZES_H430_H_ */
