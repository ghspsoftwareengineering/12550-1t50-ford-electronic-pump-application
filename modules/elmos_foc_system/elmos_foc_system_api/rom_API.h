/***************************************************************************//**
 * @file		rom_API.h
 *
 * @creator		RPY
 * @created		20.04.2015
 *
 * @brief  		ROM API
 *
 * Provides the access to the different software modules in ROM.
 *
 * $Id: $
 *
 * $Revision: 1.1 $
 *
 ******************************************************************************/

#ifndef ROM_API_H_
#define ROM_API_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

#define ROMIF_MAIN_INTERFACE_VERSION    0x0100 /**< Interface version of the ROM API */

/** @addtogroup RomApi */
/**@{*/

/* *****************************************************************************
 * Interface identifiers
 ******************************************************************************/

#define ROMIF_MAIN                  0     /**< Main ROM interface. */

#define ROMIF_API_VIC               1     /**< Interface ID for the VIC. */
// ... more APIs to come...

#define ROMIF_BOOTLOADER            30    /**< Interface ID for the bootloader. */

#define ROMIF_LIN_BUS               35    /**< Interface ID for the LIN BUS layer module. */
#define ROMIF_LIN_PROTO             36    /**< Interface ID for the LIN PROTO layer module. */
#define ROMIF_LIN_TRANS             37    /**< Interface ID for the LIN TRANS layer module. */
//#define ROMIF_LIN_DIAG              38  
//#define ROMIF_LIN_LOOKUP_FIXED      39
#define ROMIF_LIN_LOOKUP_SEQUENTIAL 40
// ... more lookups to be added


#define ROMIF_INVALID               0xFFFFu /**< Invalid interface ID. */

/***************************************************************************//**
 * Data structure encoding ROM version
 ******************************************************************************/
typedef struct 
{
  uint8_t  Major; /**< Major version byte. */
  uint8_t  Minor; /**< Minor version byte. */
  uint16_t Build; /**< 2 Byte build revision. */
} rom_Version_t;

typedef uint16_t romIf_Version_t;                 /**< Data type for ROM API version. */

typedef uint16_t romIf_InterfaceId_t;             /**< Data type for ROM API interface ID. */

typedef uint16_t romIf_Length_t;                  /**< Data type for length values. */

typedef        void*  romIf_pGenericInterface_t;  /**< Generic pointer to an interface. */
typedef const  void*  romIf_cpGenericInterface_t; /**< Generic const pointer to an interface. */

/***************************************************************************//**
 * Typedef of fucntion to get the interface of the desired module in ROM.
 *
 * @param id                  Interface ID.
 * @param interfaceFunctions  @copydoc romIf_cpGenericInterface_t
 * @param envSize             Pointer to variable to store the needed
 *                            environment data of the requested module.
 *
 * @return      TRUE if the interface has been returned successfully.
 *
 ******************************************************************************/
typedef bool_t (*romIf_Interface_Get_t) (romIf_InterfaceId_t id, romIf_cpGenericInterface_t *interfaceFunctions, romIf_Length_t *envSize);


/***************************************************************************//**
 * Main ROM application related entry point type declaration
 ******************************************************************************/
struct romIf_MainInterfaceFunctions
{
    romIf_Version_t           InterfaceVersion; /**< ROM API version. */
    
    romIf_Interface_Get_t     Interface_Get;    /**< @brief Pointer to 'Interface Get' function as member of ROM API interface function struct. @see romIf_Interface_Get_t @copydetails romIf_Interface_Get_t */
};

typedef struct romIf_MainInterfaceFunctions     romIf_MainInterfaceFunctions_t;   /**< Typedef for romIf_MainInterfaceFunctions. */
typedef        romIf_MainInterfaceFunctions_t*  romIf_pMainInterfaceFunctions_t;  /**< Typedef of pointer to romIf_MainInterfaceFunctions. */
typedef const  romIf_MainInterfaceFunctions_t*  romIf_cpMainInterfaceFunctions_t; /**< Typedef of const pointer to romIf_MainInterfaceFunctions. */

/***************************************************************************//**
 * Main ROM application related entry point
 *
 * @misra{M3CM Rule-1.1. and 1.2. - PRQA Msg 289 and 1019,
 * The @ command is a IAR specific c language extension. It is necessary here to
 * map the ROM interface functiosn into the ROM\, to be accessed from the user
 * application.,
 * Only compatible to IAR compiler.,
 * None}
 ******************************************************************************/
// PRQA S 289,1019 ++
#ifndef BUILD_ROM
__root __no_init const romIf_MainInterfaceFunctions_t romIf_MainInterface @ 0x2010;  /**< Instance of the ROM API interface struct at address 0x4010 in ROM. */
#endif
// PRQA S 289,1019 --

/**@} RomApi */

#endif /* ROM_API_H_ */




