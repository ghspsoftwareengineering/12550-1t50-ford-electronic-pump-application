#ifndef FAULT_MANAGER_H
#define FAULT_MANAGER_H

/**
 *  @file fault_manager.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_manager_api Fault Manager Interface Documentation
 *  @{
 *      @details **Place Your Description Here**
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_manager_types.h"
#include "global.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/


/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
bool_t FLT_MGR_Init(void);

void FLT_MGR_Task(void);

/**
 * Getter for the self protect status.
 *
 * @return self protect status
 */

bool_t FLT_MGR_Is_In_Self_Protect(void);

/**
 * Getter for the fault active status.
 *
 * @return BOOL active status, using FLT_MGR_General_Faults_T
 */
bool_t FLT_MGR_Is_Fault_Active(uint8_t index);

/**
 * Getter for ANY fault(s) active status.
 *
 * @return BOOL active status, using FLT_MGR_General_Faults_T
 */

bool_t FLT_MGR_Is_Any_Fault_Active(void);

/**
 * Callback from Lin Driver level, sets lin fault type LinDiagIf_Error_t
 * Then converts it to SAE J2602 Status byte lin fault enum 
 *
 * @return BOOL active status, using FLT_MGR_General_Faults_T
 */

void FLT_MGR_Set_Lin_Fault_Callback(uint16_t lin_fault);

/**
 * Getter for lin faults based on SAE J2602 Status byte 0 specification 
 * 
 *
 * @return FLT_MGR_LIN_J2602_Err_T, the lin errors we are required to report on linbus status byte 0 */


FLT_MGR_LIN_J2602_Err_T FLT_MGR_Get_Lin_Fault_J2602(void);

/**
 * Clear lin faults that were set by the Lin driver callback. 
 * The callback does not snd a no error status, only faults. Clear after setting up Tx on lin
 *
 * @return FLT_MGR_LIN_J2602_Err_T, the lin errors we are required to report on linbus status byte 0 */


void FLT_MGR_Clear_Lin_Fault_J2602(void);

/**
 * Get status of registers irqstat1 and irqstat2
 *
 * @return bool_t true if there is a short circuit was detected */


bool_t FLT_MGR_IRQSTAT_Fault_Present(void);

/** @} doxygen end group */

#endif /* FAULT_MANAGER_H */
