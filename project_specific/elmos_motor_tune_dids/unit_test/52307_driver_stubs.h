/**
 * @defgroup HW Hardware driver
 * @ingroup HW
 *
 * @{
 */

#ifndef DRIVER_52307_STUBS_H
#define DRIVER_52307_STUBS_H

#include "global.h"

/* -------------------
   global declarations
   ------------------- */
/* blocking write IO register of 523.07 via SPI */
void d52307_write_reg( uint8_t adr, uint8_t data );

/* blocking read IO register of 523.07 via SPI */
void d52307_read_reg( uint8_t adr, uint8_t * data );

#endif /* "#ifndef DRIVER_52307_STUBS_H" */

/* }@
 */
