/**
 * @defgroup UART UART data logger
 * @ingroup UART
 *
 * @{
 */

#ifndef UART_H
#define UART_H

#include <defines.h>

/* ---------------
   global defines
   --------------- */

#define USR_SHUNT_MEASUREMENT_TYPE_MSK        UINT16_C( 7 )
#define USR_EN_SINGLE_SHUNT_SILENT_MODE_MSK   UINT16_C( 1 )
#define FOC_EN_HALL_SENS_MSK                  UINT16_C( 1 )
#define USR_USE_HALL_SPEED_CALC_MSK           UINT16_C( 1 )
#define HALL_CCT_MODE_MSK                     UINT16_C( 1 )
#define USR_EN_INITIAL_POSITION_DETECTION_MSK UINT16_C( 1 )
#define FOC_EN_VBAT_COMPENSATION_MSK          UINT16_C( 1 )
#define FOC_EN_OBSERVER_MSK                   UINT16_C( 1 )
#define FOC_USE_SVM_MSK                       UINT16_C( 1 )
#define USE_PWM_DIAG_INPUT_MSK                UINT16_C( 1 )
#define USE_PWM_DIAG_OUTPUT_MSK               UINT16_C( 1 )

#define USR_SHUNT_MEASUREMENT_TYPE_SHFT        UINT16_C( 0 )
#define USR_EN_SINGLE_SHUNT_SILENT_MODE_SHFT   UINT16_C( 3 )
#define FOC_EN_HALL_SENS_SHFT                  UINT16_C( 4 )
#define USR_USE_HALL_SPEED_CALC_SHFT           UINT16_C( 5 )
#define HALL_CCT_MODE_SHFT                     UINT16_C( 6 )
#define USR_EN_INITIAL_POSITION_DETECTION_SHFT UINT16_C( 7 )
#define FOC_EN_VBAT_COMPENSATION_SHFT          UINT16_C( 8 )
#define FOC_EN_OBSERVER_SHFT                   UINT16_C( 9 )
#define FOC_USE_SVM_SHFT                       UINT16_C( 10 )
#define USE_PWM_DIAG_INPUT_SHFT                UINT16_C( 11 )
#define USE_PWM_DIAG_OUTPUT_SHFT               UINT16_C( 12 )

typedef struct
{
  s16 ic_major_id;
  s16 ic_minor_id;
  s16 ic_major_version;
  s16 ic_minor_version;

  s16 pcb_major_id;
  s16 pcb_minor_id;
  s16 pcb_major_version;
  s16 pcb_minor_version;

  s16 firmware_major_id;
  s16 firmware_minor_id;
  s16 firmware_major_version;
  s16 firmware_minor_version;

  u16 firmware_features;

  s16 uart_protocol_major_version;
  s16 uart_protocol_minor_version;

  u8 firmware_date_of_compilation[ 12 ]; /* "Oct 30 2010" */
  u8 firmware_time_of_compilation[ 9 ];  /* "hh:mm:ss" */
} uart_system_info_t;

/* -------------------
   global declarations
   ------------------- */
void uart_init( void );
void uart_main( void );

void uart_set_data_logger_trigger( void );
void uart_datalogger_exec( void );

#endif /* _#ifndef UART_H */

/* }@
 */
