/*@!Encoding:1252*/
variables
{
  dword dwBusContextLIN1;

  byte gBitStream[35];  // array of data to be sent as bit stream
  int  gCurrentBitPos;  // "pointer" inside of data array fir bit stream
  linFrame 0x0 gBitStreamFrame; // frame used as a place holder
  
  char gCallerName[20] = "LINStress.can"; // name of this CAPL program
  const byte  gChannel = 1;   // LIN channel to apply stress functionality
  const	word	ID_VALUE_RANGE 		= 0x3F;

  const dword gByte_NotToSend = 0xFFF;   // in bit stream don't send byte
  const dword gByte_MaskInvertStopBit = 0x100; // in bit stream invert stop bit
  const dword gByte_MaskInvertStartBit = 0x1000; // in bit stream invert start bit
  const dword gCS_Autom = 0xFFC;  // in bit stream calculate CS byte automatically 

  const	dword	MAX_DISTURBANCE_TIME = 360000; // time in ms
  const	byte	MAX_RESP_COUNT		= 254;
  const	word	MAX_INTERBYTE_VAL 	= 0xFF;
  const	byte	MAX_DLC		= 8;

  const dword wrCAPLSink = 1;
  const dword wrSYSSink = 0;
  const dword wrInfo = 1;
  const dword wrError = 3;
  
  msTimer headerErrorTimer;
  msTimer headerTimer;
  int setHeaderError = -1;
  dword	global_id;
  byte	global_syncByte;
  byte global_StopAfterError;
}

on envVar EnvErrBitStreamAny
{
  byte aBitStream[5];
  dword byteVal;
  long numBits;
  byte ret;

  // react on push only
  if( !getValue( this ) )
    return;
  
  numBits = 0;

  byteVal=getValue(EnvErrBSArbitraryByte0);
  if (gByte_NotToSend != byteVal)
  {
    aBitStream[0]=byteVal;
    numBits += 8;
    byteVal=getValue(EnvErrBSArbitraryByte1);
    if (gByte_NotToSend != byteVal)
    {
      aBitStream[1]=byteVal;
      numBits += 8;
      byteVal=getValue(EnvErrBSArbitraryByte2);
      if (gByte_NotToSend != byteVal)
      {
        aBitStream[2]=byteVal;
        numBits += 8;
        byteVal=getValue(EnvErrBSArbitraryByte3);
        if (gByte_NotToSend != byteVal)
        {
          aBitStream[3]=byteVal;
          numBits += 8;
          byteVal=getValue(EnvErrBSArbitraryByte4);
          if (gByte_NotToSend != byteVal)
          {
            aBitStream[4]=byteVal;
            numBits += 8;
          }
        }
      }
    }
  }

  // LINstress 
  ret = LINSendBitStream(aBitStream, numBits);
  writeLineEx(wrCAPLSink, wrInfo,  "LINSendBitStream( d[0]=0x%x; d[1]=0x%x; d[2]=0x%x; d[3]=0x%x; d[4]=0x%x, length=%d )=%d", 
         aBitStream[0], aBitStream[1], aBitStream[2], aBitStream[3], aBitStream[4], numBits, ret );

}

on envVar EnvErrInvertRespSw
{
  long	_id,ret,_dlc,byteIndex;
  byte	bitIndex,level;

  // react on push only
  if( !getValue( this ) )
    return;
  
  // get FrameId
  _id = getValue( EnvErrInvertRespID );

  // validate frameId
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrInvertRespID, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }
  
  // get ByteIndex
  byteIndex = getValue( EnvErrInvertRespByteIndex );
  // get DLC
  _dlc = LINGetDlc( _id );

  // Validate byte index
  if( byteIndex > _dlc )
  {
    putValue( EnvErrInvertRespByteIndex, _dlc );
    byteIndex = _dlc;
  }
  else if( byteIndex < 0 )
  {
    putValue( EnvErrInvertRespByteIndex, 0 );
    byteIndex = 0;
  }

  // get bitIndex
  bitIndex = getValue( EnvErrInvertRespBitIndex );

  // get level
  level = getValue( EnvErrInvertRespLevel );

  // LINstress
  ret = LINInvertRespBit( _id, byteIndex, bitIndex, level );
  writeLineEx(wrCAPLSink, wrInfo,  "LINInvertRespBit( 0x%x, %d, %d, %d )=%d", _id, byteIndex, bitIndex, level, ret );

}

on envVar EnvErrInvertRespSw_255
{
  long	_id,ret,_dlc,byteIndex;
  byte	bitIndex,level;

  // react on push only
  if( !getValue( this ) )
    return;
  
  // get FrameId
  _id = getValue( EnvErrInvertRespID );

  // validate frameId
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrInvertRespID, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }
  
  // get ByteIndex
  byteIndex = getValue( EnvErrInvertRespByteIndex );
  // get DLC
  _dlc = LINGetDlc( _id );

  // Validate byte index
  if( byteIndex > _dlc )
  {
    putValue( EnvErrInvertRespByteIndex, _dlc );
    byteIndex = _dlc;
  }
  else if( byteIndex < 0 )
  {
    putValue( EnvErrInvertRespByteIndex, 0 );
    byteIndex = 0;
  }

  // get bitIndex
  bitIndex = getValue( EnvErrInvertRespBitIndex );

  // get level
  level = getValue( EnvErrInvertRespLevel );

  // LINstress
  ret = LINInvertRespBit( _id, byteIndex, bitIndex, level, 255);
  writeLineEx(wrCAPLSink, wrInfo,  "LINInvertRespBit( 0x%x, %d, %d, %d )=%d", _id, byteIndex, bitIndex, level, ret );

}

on envVar EnvErrInvertHeaderSw
{
  byte	byteIndex,bitIndex, level,ret;

  // react on push only
  if( !getValue( this ) )
    return;

  // ByteIndex value
  byteIndex = getValue( EnvErrInvertHeaderByteIndex );

  // bitIndex value
  bitIndex = getValue( EnvErrInvertHeaderBitIndex );

  // Level value
  level = getValue( EnvErrInvertHeaderLevel );

  // LINstress 
  ret = LINInvertHeaderBit( byteIndex, bitIndex, level );
  writeLineEx(wrCAPLSink, wrInfo,  "LINInvertHeaderBit( %d, %d, %d )=%d", byteIndex, bitIndex, level, ret );

}

on envVar EnvErrRespLenApply
{
  dword	_id, lDLC, ret;
  int		count;

  // react on push only
  if( !getValue( this ) )
    return;

  // Get FrameId
  _id = getValue( EnvErrRespLenFrameId );
  // Validate FrameId 
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrRespLenFrameId, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }

  // Get requested DLC
  lDLC = getValue( EnvErrRespLength );

  // Validate
  if( lDLC > MAX_DLC+1 )
  {
    lDLC = MAX_DLC+1;
    putValue( EnvErrRespLength, lDLC );
  }

  // LINstress
  ret = LinSetRespLength( _id,  lDLC );
  writeLineEx(wrCAPLSink, wrInfo,  "LinSetRespLength( 0x%x, %d )=%d", _id, lDLC, ret );
}

on envVar EnvErrHeaderApply
{
  dword	_id;
  byte	syncByte,StopAfterError;
  int		ret;

  // react on push only
  if( !getValue( this ) )
    return;

  // Get SyncByte
  syncByte = getValue( EnvErrHeaderSyncByte );

  // Get FrameId
  _id = getValue( EnvErrHeaderID );

  // Validate FrameId 
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrHeaderID, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }

  // Calculate protected ID
  _id = LINGetProtectedID( _id );

  // Query if parity error has to be applied
  if( getValue( EnvErrHeaderParitySw ) )
  {
    // Calculate PID with parity error
    _id ^= 0xC0;
  }

  // Query if StopAfterError has to be set
  StopAfterError = getValue( EnvErrHeaderStopAfterError );

  // LINstress
  ret = LINSendHeaderError( syncByte, _id, StopAfterError );
  writeLineEx(wrCAPLSink, wrInfo,  "LINSendHeaderError( 0x%x, 0x%x, %d )=%d", syncByte, _id, StopAfterError, ret );
}

on envVar EnvErrHeaderApply_20s
{
  dword	_id;
  byte	syncByte,StopAfterError;
  int		ret;

  // react on push only
  if( !getValue( this ) )
    return;

  // Get SyncByte
  syncByte = getValue( EnvErrHeaderSyncByte );

  // Get FrameId
  _id = getValue( EnvErrHeaderID );

  // Validate FrameId 
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrHeaderID, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }

  // Calculate protected ID
  _id = LINGetProtectedID( _id );

  // Query if parity error has to be applied
  if( getValue( EnvErrHeaderParitySw ) )
  {
    // Calculate PID with parity error
    _id ^= 0xC0;
  }

  // Query if StopAfterError has to be set
  StopAfterError = getValue( EnvErrHeaderStopAfterError );
  
  setHeaderError = 1;
  
  // Send the error for 20s.
  setTimer(headerErrorTimer, 20000);
 
  // The header for frame 0x08 is sent every 70ms.  The error will need to be reset before the timer expires.
  setTimerCyclic(headerTimer, 70);
  
  //The timer will need these variables.
  global_id = _id;
  global_syncByte = syncByte;
  global_StopAfterError = StopAfterError;
  
  // LINstress
  //Deactivate the normal (good) message 0x08.
  linDeactivateSlot(0, 2);

  ret = LINSendHeaderError( syncByte, _id, StopAfterError );
 
  writeLineEx(wrCAPLSink, wrInfo,  "LINSetHeaderError( 0x%x, 0x%x, %d )=%d", syncByte, _id, StopAfterError, ret );
}

on timer headerTimer
{
  if(setHeaderError > 0)
  {
    LINSendHeaderError( global_syncByte, global_id, global_StopAfterError );
  }
}

on timer headerErrorTimer
{
  // this code runs when we want to stop the wait time
  setHeaderError = -1;
  cancelTimer(headerTimer);
  
  //Reactivate the good message.
  linActivateSlot(0, 2);
}

// This function sends LIN header as a bit stream
int SendSpecialHeader(int breakLength, int delimiterLength, byte syncByte, byte pidByte)
{
  byte data[25], ret;
  int headerLength;
  int currentPosition;
  int i;

  headerLength = breakLength + delimiterLength + 20; // Header consists of Break, Sync and PID bytes

  if (headerLength > 200)
  {
    return 0; // maximum is exceeded
  }

  currentPosition = 0;

  for (i = 0; i < 25; ++i)
  {
    data[i] = 0xff; // initialize 
  }

  for (i = breakLength; i > 0;) // set sync break
  {
    if (i >= 8)
    {
      data[currentPosition >> 3] = 0;
      i -= 8;
      currentPosition += 8;
    }
    else
    {
      data[currentPosition >> 3] &= ~(1 << (currentPosition & 0x7));
      --i;
      ++currentPosition;
    }
  }

  currentPosition += delimiterLength; // set synch delimiter

  // set sync byte
  data[currentPosition >> 3] &= ~(1 << (currentPosition & 0x7)); // set dominant start bit
  ++currentPosition;
  for (i = 0; i < 8; ++i)
  {
    if ((syncByte & (1 << i)) == 0) // dominant bit in syncByte
    {
      data[currentPosition >> 3] &= ~(1 << (currentPosition & 0x7));
    }
    ++currentPosition;
  }
  ++currentPosition; // set recessive stop bit

  // set protected id
  data[currentPosition >> 3] &= ~(1 << (currentPosition & 0x7)); // set dominant start bit
  ++currentPosition;
  for (i = 0; i < 8; ++i)
  {
    if ((pidByte & (1 << i)) == 0) // dominant bit in pidByte
    {
      data[currentPosition >> 3] &= ~(1 << (currentPosition & 0x7));
    }
    ++currentPosition;
  }
  ++currentPosition; // set recessive stop bit

  ret = LINSendBitStream(data, headerLength); // send created header
  writeLineEx(wrCAPLSink, wrInfo,  "LINSendBitStream( data, length=%d )=%d", headerLength, ret );
  writeLineEx(wrCAPLSink, wrInfo,  "data[0]=0x%x, data[1]=0x%x, data[2]=0x%x, data[3]=0x%x, data[4]=0x%x", 
           data[0], data[1], data[2], data[3], data[4] );

  return headerLength;
}

on envVar EnvErrBitStreamSpecial
{
  int ret;
  dword d0, d1, d2, d3, d4, d5, d6, d7, cs;
  dword d0_d1, d1_d2, d2_d3, d3_d4, d4_d5, d5_d6, d6_d7, d7_CS;

  // react on push only
  if( !getValue( this ) )
    return;

  SetBitStream_Init();
  SetBitStream_Header (getValue(EnvErrBSHdrBreakLength), getValue(EnvErrBSHdrDelLength), getValue(EnvErrBSHdrSyncByte), 0, getValue(EnvErrBSHdrPID));

  SetBitStream_ResponseSpace(getValue(EnvErrBSRespSpace));

  d0 = getValue(EnvErrBSRespD0);
  d1 = getValue(EnvErrBSRespD1);
  d2 = getValue(EnvErrBSRespD2);
  d3 = getValue(EnvErrBSRespD3);
  
  d4 = getValue(EnvErrBSRespD4);
  d5 = getValue(EnvErrBSRespD5);
  d6 = getValue(EnvErrBSRespD6);
  d7 = getValue(EnvErrBSRespD7);
  cs = getValue(EnvErrBSRespCS);

  d0_d1 = getValue(EnvErrBSRespD0_D1);
  d1_d2 = getValue(EnvErrBSRespD1_D2);
  d2_d3 = getValue(EnvErrBSRespD2_D3);
  d3_d4 = getValue(EnvErrBSRespD3_D4);
  d4_d5 = getValue(EnvErrBSRespD4_D5);
  d5_d6 = getValue(EnvErrBSRespD5_D6);
  d6_d7 = getValue(EnvErrBSRespD6_D7);
  d7_CS = getValue(EnvErrBSRespD7_CS);

  if ( SetBitStream_ResponseLow (d0, d0_d1, d1, d1_d2, d2, d2_d3, d3, d3_d4) > 0)
  {
    // 4 data bytes are defined -> next 4 (or less) can be set
    SetBitStream_ResponseHigh (d4, d4_d5, d5, d5_d6, d6, d6_d7, d7, d7_CS);
    SetBitStream_ResponseChecksum(cs); // set checksum only if response is required
  }

  ret = LINSendBitStream(gBitStream, gCurrentBitPos); // send created bit stream
  writeLineEx(wrCAPLSink, wrInfo,  "LINSendBitStream( data, length=%d )=%d", gCurrentBitPos, ret );
  writeLineEx(wrCAPLSink, wrInfo,  "data[0]=0x%x, data[1]=0x%x, data[2]=0x%x, data[3]=0x%x, data[4]=0x%x, data[5]=0x%x, data[6]=0x%x, data[7]=0x%x", 
           gBitStream[0], gBitStream[1], gBitStream[2], gBitStream[3], gBitStream[4], gBitStream[5], gBitStream[6], gBitStream[7] );

}

on envVar EnvErrDisturbanceStart
{
  dword level,length;
  int64 msTime;
  dword ret;

  // react on push only
  if( !getValue( this ) )
    return;

  // get disturbance length in ms
  msTime = getValue( EnvErrDisturbanceLength );

  // valdiate  disturbance length
  if( msTime > MAX_DISTURBANCE_TIME )
  {
    msTime = MAX_DISTURBANCE_TIME;
    putValue( EnvErrDisturbanceLength, MAX_DISTURBANCE_TIME );
  }

  // convert seconds to bit times (if it's not permanent)
  if( msTime > 0 )
    length = linTime2Bits_ns( msTime * 1000000 ); 
  else if( msTime < 0 ) // permanent disturbance if -1 is set
  {
    length = -1;
    putValue( EnvErrDisturbanceLength, -1 );
  }

  // get Level value
  level = getValue( EnvErrDisturbanceMode );

  // Apply disturbance
  ret = LINStartDisturbance(length,level);
  writeLineEx(wrCAPLSink, wrInfo, "LINStartDisturbance(%d, %d)=%d",length,level,ret);
}

on envVar EnvErrDisturbanceStop
{
  byte ret;

  // react on push only
  if( !getValue( this ) )
    return;

  ret = LINStopDisturbance();
  writeLineEx(wrCAPLSink, wrInfo, "LINStopDisturbance()=%d", ret);
}

on envVar EnvErrIBHeaderApply
{
  byte ret, _id, length;

  // react on push only
  if( !getValue( this ) )
    return;

  // Get FrameId
  _id = getValue( EnvErrIBHeaderFrameId );
  // Validate FrameId 
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrIBHeaderFrameId, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }

  // Get interbyte length
  length = getValue( EnvErrIBHeaderLength );
  // Validate Length 
  if( length > MAX_INTERBYTE_VAL )
  {
    putValue( EnvErrIBHeaderLength, MAX_INTERBYTE_VAL );
    length = MAX_INTERBYTE_VAL;
  }
 
  // LINstress
  ret = linSetInterByteSpace( _id, 0, length );
  writeLineEx(wrCAPLSink, wrInfo,  "linSetInterByteSpace( 0x%x, 0, %d)=%d", _id, length, ret );
}

on envVar EnvErrIBRespSpaceApply
{
  byte ret, _id, length;

  // react on push only
  if( !getValue( this ) )
    return;

  // Get FrameId
  _id = getValue( EnvErrIBRespSpaceFrameId );
  // Validate FrameId 
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrIBRespSpaceFrameId, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }

  // Get interbyte length
  length = getValue( EnvErrIBRespSpaceLength );
  // Validate Length 
  if( length > MAX_INTERBYTE_VAL )
  {
    putValue( EnvErrIBRespSpaceLength, MAX_INTERBYTE_VAL );
    length = MAX_INTERBYTE_VAL;
  }
 
  // LINstress
  ret = linSetInterByteSpace( _id, 1, length );
  writeLineEx(wrCAPLSink, wrInfo,  "linSetInterByteSpace( 0x%x, 1, %d)=%d", _id, length, ret );
}

on envVar EnvErrIBInterFrameApply
{
  byte ret, length;

  // react on push only
  if( !getValue( this ) )
    return;

  // Get interbyte length
  length = getValue( EnvErrIBInterFrameLength );
  // Validate Length 
  if( length > MAX_INTERBYTE_VAL )
  {
    putValue( EnvErrIBInterFrameLength, MAX_INTERBYTE_VAL );
    length = MAX_INTERBYTE_VAL;
  }
 
  // LINstress
  ret = linSetInterframeSpace( length );
  writeLineEx(wrCAPLSink, wrInfo,  "linSetInterframeSpace( %d )=%d",length, ret );
}

on envVar EnvErrIBRespApply
{
  byte ret, _id, sixteenthBits, byteIndex, i;
  dword arrayOfSixteenthBits[9];


  // react on push only
  if( !getValue( this ) )
    return;

  // Initialize
  for (i=0; i < 9; ++i)
  {
    arrayOfSixteenthBits[i] = 0;
  }

  // Get interbyte length
  sixteenthBits = getValue( EnvErrIBRespLength );
  // Validate Length 
  if( sixteenthBits > MAX_INTERBYTE_VAL )
  {
    putValue( EnvErrIBRespLength, MAX_INTERBYTE_VAL );
    sixteenthBits = MAX_INTERBYTE_VAL;
  }
  // Get FrameId Type
  if (getValue(EnvErrIBRespFrameChoice) == 0)
  {
    _id = 0xFF;
  }
  else
  {
    // Get FrameId
    _id = getValue( EnvErrIBRespFrameId );
    // Validate FrameId 
    if( _id > ID_VALUE_RANGE )
    {
      putValue( EnvErrIBRespFrameId, ID_VALUE_RANGE );
      _id = ID_VALUE_RANGE;
    }
  }

  // Get Byte index
  if (getValue(EnvErrIBRespByteChoice) == 0)
  {
    // all bytes get the requested interbyte space
    byteIndex = 0xFF;
  }
  else
  {
    // Get Byte Index
    byteIndex = getValue( EnvErrIBRespByteIndex );
    // Validate FrameId 
    if( byteIndex > 7 )
    {
      putValue( EnvErrIBRespByteIndex, 7 );
      byteIndex = 7;
    }
  }

  // Perform LIN Stress
  if (0xFF == _id && 0xFF==byteIndex)
  {
    // all frame ids and all bytes
    ret = LINSetGlobalInterByteSpace( sixteenthBits );
    writeLineEx(wrCAPLSink, wrInfo, "LINSetGlobalInterByteSpace( %d )=%d", sixteenthBits, ret );
  }
  else if (0xFF == _id && 0xFF !=byteIndex)
  {
    // all frame ids and certain bytes -> THIS CAN WORK FOR ALL FRAMES PUBLISHED BY A SLAVE ONLY
    msgBeep (3); 
    writeLineEx(wrCAPLSink, wrError, "This combination is possible only when this CAPL program runs in the Slave context!");
  }
  else if (0xFF != _id && 0xFF==byteIndex)
  {
    // certain frame + all bytes
    for (i=0; i < 9; ++i)
    {
      arrayOfSixteenthBits[i] = sixteenthBits;
    }
    ret = linSetInterByteSpaces(_id, arrayOfSixteenthBits );
    writeLineEx(wrCAPLSink, wrInfo,  "linSetInterByteSpaces( %d, arrayOfSixteenthBits )=%d", _id, ret );
    for (i=0; i < 9; ++i)
    {
      writeLineEx(wrCAPLSink, wrInfo,  "arrayOfSixteenthBits[%d]==%d", i, arrayOfSixteenthBits[i] );
    }
  } 
  else
  {
    // Certain frame + certain byte index
    ret = linSetInterByteSpace( _id, byteIndex+2, sixteenthBits );
    writeLineEx(wrCAPLSink, wrInfo,  "linSetInterByteSpace( 0x%x, %d, %d)=%d", _id, byteIndex+2, sixteenthBits, ret );
  }
}

on envVar EnvErrRespCountApply
{
  dword	_id, lDLC, ret;
  int		count;

  // react on push only
  if( !getValue( this ) )
    return;

  // Get FrameId
  _id = getValue( EnvErrRespCountFrameId );
  // Validate FrameId 
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrRespCountFrameId, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }

  // Get number of responses to set
  count = getValue( EnvErrRespCount );

  // Validate
  if( count > MAX_RESP_COUNT )
  {
    count = MAX_RESP_COUNT;
    putValue( EnvErrRespCount, MAX_RESP_COUNT );
  }
  else if( count < 0 )
  {
    count = -1;
    putValue( EnvErrRespCount, -1 );
  }

  // LINstress
  ret = LINSetRespCounter( _id,  count );
  writeLineEx(wrCAPLSink, wrInfo,  "LINSetRespCounter( 0x%x, %d )=%d", _id, count, ret );
}

on envVar EnvErrCSApply
{
  dword	_id, ret;
  byte csErrorChoice, csManualByte;
  linFrame 0x0 mylinFrame;

  // react on push only
  if( !getValue( this ) )
    return;

  // Get FrameId
  _id = getValue( EnvErrCSFrameId );
  // Validate FrameId 
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrCSFrameId, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }


  // Get requested error type
  csErrorChoice = getValue( EnvErrCSChoice );

  if (0 == csErrorChoice)
  {
    // Automatic Error
    ret = linSetChecksumError(_id, 1);
    writeLineEx(wrCAPLSink, wrInfo,  "linSetChecksumError( 0x%x, 1 )=%d", _id, ret );
  }
  else
  {
    // Manual Error
    mylinFrame.id = _id;
    mylinFrame.dlc = linGetDlc(_id);
    mylinFrame.MsgChannel = gChannel;
    // data bytes
    mylinFrame.byte(0) = getValue( EnvErrCSData1 );
    mylinFrame.byte(1) = getValue( EnvErrCSData2 );
    mylinFrame.byte(2) = getValue( EnvErrCSData3 );
    mylinFrame.byte(3) = getValue( EnvErrCSData4 );
    mylinFrame.byte(4) = getValue( EnvErrCSData5 );
    mylinFrame.byte(5) = getValue( EnvErrCSData6 );
    mylinFrame.byte(6) = getValue( EnvErrCSData7 );
    mylinFrame.byte(7) = getValue( EnvErrCSData8 );

    csManualByte = getValue( EnvErrCSByteRQ );

    linSetManualChecksum(mylinFrame, csManualByte);
    output(mylinFrame); // it is important to call output() to make the changes active
    writeLineEx(wrCAPLSink, wrInfo,  "linSetManualChecksum( linFrame, 0x%x ); linFrame => ID=0x%x, Length=%d", csManualByte, mylinFrame.id, mylinFrame.dlc);
//  	writeLineEx(wrCAPLSink, wrInfo,  "      linFrame = ID=x%x, DLC=%d, MsgChannel=%d )", linFrame.id, linFrame.dlc, linFrame.MsgChannel);
  }
}

on envVar EnvErrCSReset
{
  dword	_id, ret;
  byte csErrorChoice, csManualByte;
  linFrame 0x0 mylinFrame;

  // react on push only
  if( !getValue( this ) )
    return;

  // Get FrameId
  _id = getValue( EnvErrCSFrameId );
  // Validate FrameId 
  if( _id > ID_VALUE_RANGE )
  {
    putValue( EnvErrCSFrameId, ID_VALUE_RANGE );
    _id = ID_VALUE_RANGE;
  }


  // Get requested error type
  csErrorChoice = getValue( EnvErrCSChoice );

  if (0 == csErrorChoice)
  {
    // Automatic Error
    ret = linSetChecksumError(_id, 0);
    writeLineEx(wrCAPLSink, wrInfo,  "linSetChecksumError( 0x%x, 0 )=%d", _id, ret );
  }
  else
  {
    // Manual Error
    mylinFrame.id = _id;
    mylinFrame.dlc = linGetDlc(_id);
    // data bytes
    mylinFrame.byte(0) = getValue( EnvErrCSData1 );
    mylinFrame.byte(1) = getValue( EnvErrCSData2 );
    mylinFrame.byte(2) = getValue( EnvErrCSData3 );
    mylinFrame.byte(3) = getValue( EnvErrCSData4 );
    mylinFrame.byte(4) = getValue( EnvErrCSData5 );
    mylinFrame.byte(5) = getValue( EnvErrCSData6 );
    mylinFrame.byte(6) = getValue( EnvErrCSData7 );
    mylinFrame.byte(7) = getValue( EnvErrCSData8 );

    linResetManualChecksum(mylinFrame);
    output(mylinFrame); // it is important to call output() to make the changes active
    writeLineEx(wrCAPLSink, wrInfo,  "linResetManualChecksum( linFrame )" );
    writeLineEx(wrCAPLSink, wrInfo,  "linFrame = ID=x%x, DLC=%d )", mylinFrame.id, mylinFrame.dlc );
  }
}

on start
{
  dword ret;

  dwBusContextLIN1 = GetBusNameContext("LIN");
  ret = SetBusContext(dwBusContextLIN1);
  //writeLineEx(wrCAPLSink, wrInfo,  "SetBusContext( LIN )=%d", ret); //EVAL0006243
}

int SetBitStream_Header (int breakLength, int delimiterLength, dword syncByte, dword interByteLength, dword pidByte)
{
  int headerLength, i;

  headerLength = breakLength + delimiterLength + 20 + interByteLength; // Header consists of Break, Sync and PID bytes + interbyte btw. Sync and PID

  if (headerLength > 8*elCount(gBitStream))
  {
    return 0; // maximum is exceeded
  }

  for (i = breakLength; i > 0;) // set sync break
  {
    if (i >= 8)
    {
      gBitStream[gCurrentBitPos >> 3] = 0; // set the whole byte to be dominant
      i -= 8;
      gCurrentBitPos += 8;
    }
    else
    {
      gBitStream[gCurrentBitPos >> 3] &= ~(1 << (gCurrentBitPos & 0x7)); // set signle dominant bit
      --i;
      ++gCurrentBitPos;
    }
  }

  gCurrentBitPos += delimiterLength; // set synch delimiter

  SetBitStream_SetByteAtCurrentPosition(syncByte, interByteLength); // set sync byte and interbyte after it

  SetBitStream_SetByteAtCurrentPosition(pidByte, 0); // set protected ID byte 
 
  return headerLength;
}

void SetBitStream_Init ()
{
  int i;
  
  gCurrentBitPos = 0;

  for (i = 0; i < elCount(gBitStream); ++i)
  {
    gBitStream[i] = 0xff; // initialize with recessive bits
  }

}

int SetBitStream_ResponseSpace (int responseSpaceLength)
{
  if (responseSpaceLength + gCurrentBitPos + 1 > 8*elCount(gBitStream))
  {
    return 0; // maximum is exceeded
  }
  
  if (responseSpaceLength > 0)
  {
    gCurrentBitPos += responseSpaceLength; // set response space
  }

  return responseSpaceLength;
}

int SetBitStream_ResponseLow (dword d1, dword d1_d2, dword d2, dword d2_d3, dword d3, dword d3_d4, dword d4, dword d4_d5)
{
  byte frameLength;

  frameLength=0;

  if (0 != SetBitStream_SetByteAtCurrentPosition(d1, d1_d2))
  {
    gBitStreamFrame.byte(0) = d1;
    ++frameLength;
    if (0 != SetBitStream_SetByteAtCurrentPosition(d2, d2_d3))
    {
      gBitStreamFrame.byte(1) = d2;
      ++frameLength;
      if (0 != SetBitStream_SetByteAtCurrentPosition(d3, d3_d4))
      {
        gBitStreamFrame.byte(2) = d3;
        ++frameLength;
        if (0 != SetBitStream_SetByteAtCurrentPosition(d4, d4_d5))
        {
          gBitStreamFrame.byte(3) = d4;
          ++frameLength;
        }
      }
    }
  }
  gBitStreamFrame.DLC = frameLength;

  return frameLength; // current frame length in bytes
}

int SetBitStream_SetByteAtCurrentPosition (dword byteAsGiven, int followedByInterbyte)
{
  int i;
  byte byteValue;

  if (10 + followedByInterbyte + gCurrentBitPos + 1 > 8*elCount(gBitStream))
  {
    return 0; // maximum is exceeded
  }
  if (gByte_NotToSend == byteAsGiven)
    return 0; 

  byteValue = byteAsGiven & 0xFF; // take byte value only

  {
    { // set dominant start bit
      if ( !(byteAsGiven & gByte_MaskInvertStartBit))
        gBitStream[gCurrentBitPos >> 3] &= ~(1 << (gCurrentBitPos & 0x7)); 
      ++gCurrentBitPos;
    }

    for (i = 0; i < 8; ++i)
    {
      if ((byteValue & (1 << i)) == 0) // is dominant bit in byteValue ? 
      {
        gBitStream[gCurrentBitPos >> 3] &= ~(1 << (gCurrentBitPos & 0x7)); // set dominant bit
      }
      ++gCurrentBitPos;
    }

    {  // set recessive stop bit
      if ( byteAsGiven & gByte_MaskInvertStopBit)
        gBitStream[gCurrentBitPos >> 3] &= ~(1 << (gCurrentBitPos & 0x7)); 
      ++gCurrentBitPos;
    }

    if (followedByInterbyte > 0)
    {
      gCurrentBitPos += followedByInterbyte; // add interbyte space
    }

    return 10 + followedByInterbyte; // 10 bit length + interbyte length
  }

  return 0; // byte value is ignored
}

int SetBitStream_ResponseHigh (dword d5, dword d5_d6, dword d6, dword d6_d7, dword d7, dword d7_d8, dword d8, dword d8_CS)
{
  byte frameLength;


  if (0 != SetBitStream_SetByteAtCurrentPosition(d5, d5_d6))
  {
    frameLength=4;
    gBitStreamFrame.byte(0) = d5;
    ++frameLength;
    if (0 != SetBitStream_SetByteAtCurrentPosition(d6, d6_d7))
    {
      gBitStreamFrame.byte(1) = d6;
      ++frameLength;
      if (0 != SetBitStream_SetByteAtCurrentPosition(d7, d7_d8))
      {
        gBitStreamFrame.byte(2) = d7;
        ++frameLength;
        if (0 != SetBitStream_SetByteAtCurrentPosition(d8, d8_CS))
        {
          gBitStreamFrame.byte(3) = d8;
          ++frameLength;
        }
      }
    }
    gBitStreamFrame.DLC = frameLength;

    return frameLength; // current frame length in bytes
  }
  return gBitStreamFrame.DLC;
}

int SetBitStream_ResponseChecksum (dword csByte)
{
  byte csValue;
  if (gCS_Autom == csByte)
  {
    gBitStreamFrame.id = getValue(EnvErrBSHdrPID) & 0x3F;
    gBitStreamFrame.msgChannel = gChannel;
    csValue = linGetChecksum(gBitStreamFrame);
//writeLineEx(wrCAPLSink, wrInfo, "linGetChecksum() = 0x%x, id=0x%x, length=%d, data_low = 0x%x, data_high = 0x%x", 
//     csValue, gBitStreamFrame.id, gBitStreamFrame.dlc, gBitStreamFrame.dword(0), gBitStreamFrame.dword(4));
    return SetBitStream_SetByteAtCurrentPosition(csValue, 0); // set automatically calculated checksum
  }
  return SetBitStream_SetByteAtCurrentPosition(csByte, 0); // set checksum as specified
}

