/***************************************************************************//**
 * @file			LinProto_Implementation.h
 *
 * @creator		sbai
 * @created		13.01.2015
 *
 * @brief  		Implementation of the LIN Driver PROTO layer.
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINPROTO_IMPLEMENTATION_H_
#define LINPROTO_IMPLEMENTATION_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinLookup_Interface.h"
#include "LinBus_Interface.h"
#include "LinProto_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINPROTOIMP_VERSION                                   0x0110u                 /**< LIN PROTO implementation version */

#define LINPROTOIMP_CONFIG_DATA_VERSION                       0x0100                  /* Expected config data version */

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief Struct for PROTO layer implementation specific configuration data.
 ******************************************************************************/
struct LinProtoImp_sCfgData
{
    Lin_Version_t            Version;              /* Config data struct version */

    LinBusIf_sInitParam_t    LinBusIfInitParam;    /**<< @copydoc LinBusIf_sInitParam_t */
    LinLookupIf_sInitParam_t LinLookupIfInitParam; /**<< @copydoc LinLookupIf_sInitParam_t */
};

typedef struct LinProtoImp_sCfgData    LinProtoImp_sCfgData_t; /**< Typedef of LinProtoImp_sCfgData. */
typedef        LinProtoImp_sCfgData_t* LinProtoImp_pCfgData_t; /**< Typedef of pointer to LinProtoImp_sCfgData. */

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINPROTOIMP_EXT_IFFUN_STRCT_ACCESS == 1
extern const LinProtoIf_sInterfaceFunctions_t LinProtoImp_InterfaceFunctions;
#endif /* LINPROTOIMP_EXT_IFFUN_STRCT_ACCESS == 1 */

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/** @addtogroup LinProtoImpInterfaceFunctions */
/**@{*/

/***************************************************************************//**
 * @brief Implementation of LIN PROTO layer 'Initialization' function.
 *
 * @copydetails LinProtoIf_InitializationIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinProtoImp_Initialization(LinProtoIf_pGenericEnvData_t     genericProtoEnvData, LinLookupIf_EnvDataSze_t       protoEnvDataSze,
                                      LinProtoIf_cpCallbackFunctions_t protoCbFuns,         LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData,
                                      LinProtoIf_pGenericImpCfgData_t  genericProtoImpCfgData);

/***************************************************************************//**
 * @brief Implementation of LIN PROTO layer 'Get Sub-Interface' function.
 *
 * @copydetails LinProtoIf_GetSubInterfaceIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinProtoImp_GetSubInterface(LinProtoIf_pGenericEnvData_t genericProtoEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * @brief Implementation of LIN PROTO layer 'Task' function.
 *
 * @copydetails LinProtoIf_TaskIfFun_t
 *
 ******************************************************************************/
void LinProtoImp_Task(LinProtoIf_pGenericEnvData_t genericProtoEnvData);

/***************************************************************************//**
 * @brief Implementation of LIN PROTO layer 'Append Callbacks' function.
 *
 * @copydetails LinProtoIf_AppendCallbacksIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinProtoImp_AppendCallbacks(LinProtoIf_pGenericEnvData_t   genericProtoEnvData, LinProtoIf_cpCallbackFunctions_t protoCbFuns,
                                       LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData);

/***************************************************************************//**
 * @brief Implementation of LIN PROTO layer 'Remove Callbacks' function.
 *
 * @copydetails LinProtoIf_RemoveCallbacksIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinProtoImp_RemoveCallbacks(LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpCallbackFunctions_t protoCbFuns);

/***************************************************************************//**
 * @brief Implementation of LIN PROTO layer 'Add Frame Description List' function.
 *
 * @copydetails LinProtoIf_AddFrameDescLstIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinProtoImp_AddFrameDescriptionList(LinProtoIf_pGenericEnvData_t genericProtoEnvData,  LinProtoIf_cpGenericFrameDescriptionList_t genericFrmDscLst,
                                               Lin_Bool_t                   ldfRelevance,         LinProtoIf_pGenericFrmDescLstEnvData_t     genericFrmDescLstEnvData,
                                               LinProtoIf_EnvDataSze_t      frmDescLstEnvDataSze, LinProtoIf_pGenericCbCtxData_t             genericProtoPerDescLstCbCtxData);

/***************************************************************************//**
 * @brief Implementation of LIN PROTO layer 'Remove Frame Description List' function.
 *
 * @copydetails LinProtoIf_RmvFrameDescLstIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinProtoImp_RemoveFrameDescriptionList(LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpGenericFrameDescriptionList_t genericFrmDscLst);

/**@} LinProtoImpInterfaceFunctions */

#endif /* LINPROTO_IMPLEMENTATION_H_ */
