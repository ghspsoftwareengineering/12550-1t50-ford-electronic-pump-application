/***************************************************************************//**
 * @file			gpio_InterruptHandler.c
 *
 * @creator		rpy
 * @created		26.01.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 * @misra{M3CM Dir-8.9. - PRQA Msg 3218,
 * This arrays are register lookup tables for the different GPIO modules.
 * Even though they are defined at file scope and only used in one function\,
 * it is more useful to define them at file scope.,
 * None,
 * None}
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "io_e52398a.h"
#include <intrinsics.h>

#include "vic_InterruptHandler.h"
#include "gpio_InterruptHandler.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/
/* See justification at the file comment header */
// PRQA S 3218 ++
static volatile unsigned short * const loc_GPIO_x_IRQ_VENABLE[gpio_INSTANCE_CNT]  = 
{
  &GPIO_A_IRQ_VENABLE,
  &GPIO_B_IRQ_VENABLE,
  &GPIO_C_IRQ_VENABLE
};
// PRQA S 3218 --

static volatile unsigned short * const loc_GPIO_x_IRQ_VDISABLE[gpio_INSTANCE_CNT]  = 
{
  &GPIO_A_IRQ_VDISABLE,
  &GPIO_B_IRQ_VDISABLE,
  &GPIO_C_IRQ_VDISABLE
};

/* See justification at the file comment header */
// PRQA S 3218 ++
static volatile unsigned short * const loc_GPIO_x_IRQ_VMAX[gpio_INSTANCE_CNT]  = 
{
  &GPIO_A_IRQ_VMAX,
  &GPIO_B_IRQ_VMAX,
  &GPIO_C_IRQ_VMAX
};
// PRQA S 3218 --

/* See justification at the file comment header */
// PRQA S 3218 ++
static volatile unsigned short * const loc_GPIO_x_IRQ_VNO[gpio_INSTANCE_CNT]  = 
{
  &GPIO_A_IRQ_VNO,
  &GPIO_B_IRQ_VNO,
  &GPIO_C_IRQ_VNO
};
// PRQA S 3218 --

static const vic_eInterruptVectorNum_t loc_GPIO_VIC_VNO[gpio_INSTANCE_CNT] = 
{
  vic_IRQ_GPIO_A,
  vic_IRQ_GPIO_B,
  vic_IRQ_GPIO_C
};

/* See justification at the file comment header */
// PRQA S 3218 ++
static const vic_InterruptCallback_t loc_GPIO_InterruptHandler[gpio_INSTANCE_CNT] = 
{
  gpio_InterruptHandlerA,
  gpio_InterruptHandlerB,
  gpio_InterruptHandlerC
};
// PRQA S 3218 --

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VENABLE register, but with the difference
 * to gpio_SetIRQVEnable, that a special enum (gpio_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void gpio_InterruptEnable(gpio_InstanceNum_t instanceNum, gpio_eInterruptVectorNum_t irqsrc)
{
  if (instanceNum < gpio_INSTANCE_CNT)
  {
    *(loc_GPIO_x_IRQ_VENABLE[instanceNum]) = (uint16_t) irqsrc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VDISABLE register, but with the difference
 * to gpio_SetIRQVDisable, that a special enum (gpio_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void gpio_InterruptDisable(gpio_InstanceNum_t instanceNum, gpio_eInterruptVectorNum_t irqsrc)
{
  if (instanceNum < gpio_INSTANCE_CNT)
  {
    *(loc_GPIO_x_IRQ_VDISABLE[instanceNum]) = (uint16_t) irqsrc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * At first the module number is looked up in a table, because module number
 * and module instance base address have a logical connection. Then the
 * interrupt callback function pointer is saved in module interrupt vector
 * table.
 *
 ******************************************************************************/
void gpio_InterruptRegisterCallback(gpio_InstanceNum_t instanceNum, gpio_eInterruptVectorNum_t irqvecnum, gpio_InterruptCallback_t cbfnc)
{
  if (instanceNum < gpio_INSTANCE_CNT)
  {
    gpio_sInterruptEnvironmentData_t* evironmentData = (gpio_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(loc_GPIO_VIC_VNO[instanceNum]);

    if ((irqvecnum < gpio_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
    {
      evironmentData->InterrupVectorTable[irqvecnum] = cbfnc;
    }
    else {}
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Reverse function of "gpio_RegisterInterruptCallback". Sets the corresponding
 * entry in the module interrupt vector table back to NULL. Very imported,for
 * security reasons the corresponding interrupt is disabled. Otherwise the
 * program could possibly call NULL.
 *
 ******************************************************************************/
void gpio_InterruptDeregisterCallback(gpio_InstanceNum_t instanceNum, gpio_eInterruptVectorNum_t irqvecnum)
{
  if (instanceNum < gpio_INSTANCE_CNT)
  {
    gpio_sInterruptEnvironmentData_t* evironmentData = (gpio_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(loc_GPIO_VIC_VNO[instanceNum]);

    if ((irqvecnum < gpio_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
    {
      gpio_InterruptDisable(instanceNum,irqvecnum);
      evironmentData->InterrupVectorTable[irqvecnum] = NULL;
    }
    else {}
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Every hardware GPIO module has it's own interrupt handler, also because each
 * module has it's own interrupt vector. So the module base address is fixed.
 * The interrupt vector table is provided by the user-application from RAM. The
 * module specific vector tables lie behind the VIC interrupt vector table, which
 * address is saved in the "TABLE_BASE". The module interrupt handler
 * gets the address from the function "vic_GetPointerToEviornmentData".
 * The interrupt vector number is saved in a local variable, because the value
 * of IRQ_VECTOR could possible change during processing of
 * gpio_GPIO0_InterruptHandler. Next the registered interrupt callback function
 * (gpio_RegisterInterruptCallback) is copied into a local function pointer,
 * checked if it's not NULL and at least called. At the end the interrupt
 * request flag is cleared, by writing back the interrupt vector number to
 * IRQ_VNO register.
 *
 ******************************************************************************/
static void loc_InterruptHandler(gpio_InstanceNum_t instanceNum)
{
  gpio_eInterruptVectorNum_t irqvecnum = (gpio_eInterruptVectorNum_t) (*(loc_GPIO_x_IRQ_VNO[instanceNum]));
  
  if (irqvecnum < gpio_INTERRUPT_VECTOR_CNT) /* ensure the IRQ trigger is not already gone away until processing starts */
  {
    gpio_sInterruptEnvironmentData_t* evironmentData = (gpio_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(loc_GPIO_VIC_VNO[instanceNum]);

    gpio_InterruptCallback_t fptr;
  
    uint16_t vicVmaxBackup = 0;
    uint16_t gpioVmaxBackup = 0;
  
    /* IRQ nesting on VIC level: enabled by default */
    vicVmaxBackup = VIC_IRQ_VMAX;
    VIC_IRQ_VMAX = (uint16_t) loc_GPIO_VIC_VNO[instanceNum];

    /* IRQ nesting on module level: by default disabled, handler may override this later on */
    gpioVmaxBackup = *(loc_GPIO_x_IRQ_VMAX[instanceNum]);
    *(loc_GPIO_x_IRQ_VMAX[instanceNum]) = 0;
  
    fptr = evironmentData->InterrupVectorTable[irqvecnum];
          
    if(fptr != NULL)
    {          
      __enable_interrupt();
        
      /* Call interrupt callback function */
      fptr(irqvecnum,  (void*) evironmentData->ContextData);      
      
      __disable_interrupt();
    }
    else 
    {
      /* if there is no handler function for a particular IRQ, disable this IRQ  */ 
      *(loc_GPIO_x_IRQ_VDISABLE[instanceNum]) = (uint16_t) irqvecnum;
    }

    /* Clear interrupt request flag */
    *(loc_GPIO_x_IRQ_VNO[instanceNum]) = (uint16_t) irqvecnum;
  
    /* IRQ nesting on module level */
    *(loc_GPIO_x_IRQ_VMAX[instanceNum]) = gpioVmaxBackup;
  
    /* IRQ nesting on VIC level */
    VIC_IRQ_VMAX = vicVmaxBackup;
  }
  else {}  
} 

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
__interrupt static void gpio_InterruptHandlerA(void)
{
  loc_InterruptHandler(gpio_MOD_A);
}

__interrupt static void gpio_InterruptHandlerB(void)
{
  loc_InterruptHandler(gpio_MOD_B);
}

__interrupt static void gpio_InterruptHandlerC(void)
{
  loc_InterruptHandler(gpio_MOD_C);
}


/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void gpio_InterruptInitialisation(gpio_InstanceNum_t instanceNum, vic_cpInterfaceFunctions_t vicIf, gpio_pInterruptEnvironmentData_t environmentdata, gpio_pInterruptContextData_t contextdata)
{
  if ( (vicIf != NULL) &&
       (vicIf->InterfaceVersion == VIC_INTERFACE_VERSION) &&
       (environmentdata != NULL) &&
       (instanceNum < gpio_INSTANCE_CNT) )
  {
    environmentdata->ContextData = contextdata;

    /* Register module at interrupt handler */
    vicIf->RegisterModule(loc_GPIO_VIC_VNO[instanceNum], loc_GPIO_InterruptHandler[instanceNum], environmentdata);
    vicIf->EnableModule(loc_GPIO_VIC_VNO[instanceNum]);
  }
  else {}
}
