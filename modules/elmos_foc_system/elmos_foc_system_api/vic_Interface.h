/***************************************************************************//**
 * @file			vic_Interface.h
 *
 * @creator		sbai
 * @created		16.06.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef VIC_INTERFACE_H_
#define VIC_INTERFACE_H_

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define VIC_INTERFACE_VERSION           0x0100u

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

typedef unsigned short vic_InterfaceVersion_t;         /**< Data type for interface version representation **/

typedef void * vic_pInterruptModuleEnvironmentData_t;  /**< Generic pointer type to module specific environment data **/

/***************************************************************************//**
 * @brief Interrupt vector table type
 *
 * Auto combine vector number and table base to create vector number
 * related CPU interrupt pointer.
 ******************************************************************************/
typedef enum vic_eInterruptTableType
{
  /** base value is combined with vector number to be used as CPU
   * interrupt pointer (an interrupt service routine per module)*/
  vic_TT_COMBINE_WITH_VECTOR_NUM = 0,
  
  /** base value is directly used as CPU interrupt pointer
   * (one common interrupt service routine) */
  vic_TT_DIRECTLY_USED           = 1
    
}vic_eInterruptTableType_t;

/***************************************************************************//**
 * @brief VIC IRQ vector numbers
 ******************************************************************************/
typedef enum vic_eInterruptVectorNum
{  
  vic_IRQ_MEM_PROT                = 0u,
  vic_IRQ_SYS_STATE               = 1u,
  vic_IRQ_WDOG                    = 2u,
  vic_IRQ_DIVIDER                 = 3u,
  vic_IRQ_ADC_CTRL                = 4u,
  vic_IRQ_PWMN                    = 5u,
  vic_IRQ_PRE_PWM                 = 6u,
  vic_IRQ_SPI_0                   = 7u,
  vic_IRQ_SPI_1                   = 8u,
  vic_IRQ_SCI                     = 9u,
  vic_IRQ_CCTIMER_0               = 10u,
  vic_IRQ_CCTIMER_1               = 11u,
  vic_IRQ_CCTIMER_2               = 12u,
  vic_IRQ_CCTIMER_3               = 13u,
  vic_IRQ_GPIO_A                  = 14u,
  vic_IRQ_GPIO_B                  = 15u,
  vic_IRQ_GPIO_C                  = 16u,
    
  vic_INTERRUPT_VECTOR_CNT    = 17u  /**< Number of available interrupt vectors */
    
}vic_eInterruptVectorNum_t;

/***************************************************************************//**
 * @brief Interrupt handler function typedef
 *
 * This function type represents the actual interrupt handler function as 
 * implemented in each module. 
 *
 ******************************************************************************/
typedef void (__interrupt *vic_InterruptCallback_t) (void);

/***************************************************************************//**
 * @brief VIC environment data type
 ******************************************************************************/
typedef struct vic_sInterruptEnvironmentData
{
    /** Interrupt vector table to module interrupt handler */
    vic_InterruptCallback_t InterrupVectorTable[vic_INTERRUPT_VECTOR_CNT];
    
    /** Pointer to module vector table */
    vic_pInterruptModuleEnvironmentData_t ModuleEnvironmentData[vic_INTERRUPT_VECTOR_CNT];
    
} vic_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * @brief Pointer to VIC environment data.
 ******************************************************************************/
typedef vic_sInterruptEnvironmentData_t * vic_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief Returns current module specific environment data
 *
 * @param module  Module number 
 *
 * @return        Pointer to module data (may be NULL)
 *
 ******************************************************************************/
typedef vic_pInterruptModuleEnvironmentData_t (*vic_GetPointerToEnvironmentData_t)(vic_eInterruptVectorNum_t module);

/***************************************************************************//**
 * @brief Introduces a module into the global VIC based IRQ handling
 *
 * @param module                  Module number
 *
 * @param interrupthandler        Pointer to module specific interrupt handler
 *
 * @param moduleenvironmentdata   Pointer to module specific interrupt handler runtime data
 *
 *
 ******************************************************************************/
typedef void (*vic_RegisterModule_t)(vic_eInterruptVectorNum_t    module, vic_InterruptCallback_t interrupthandler,
                                     vic_pInterruptModuleEnvironmentData_t moduleenvironmentdata);

/***************************************************************************//**
 * @brief Removes module handler for a particular module. 
 *
 * @param module                  Module number
 *
 * A default IRQ handler is installed for the selected module.
 *
 ******************************************************************************/
typedef void (*vic_DeregisterModule_t)(vic_eInterruptVectorNum_t module);

/***************************************************************************//**
 * @brief Enables interrupt processing for specified module
 *
 * @param module                  Module number
 *
 ******************************************************************************/
typedef void (*vic_EnableModule_t)(vic_eInterruptVectorNum_t moduleIRQNum);

/***************************************************************************//**
 * @brief Disables interrupt processing for specified module
 *
 * @param module                  Module number
 *
 ******************************************************************************/
typedef void (*vic_DisableModule_t)(vic_eInterruptVectorNum_t moduleIRQNum);

/***************************************************************************//**
 * @brief Enables general interrupt processing
 *
 ******************************************************************************/
typedef void (*vic_EnableMain_t)(void);

/***************************************************************************//**
 * @brief Disables general interrupt processing
 *
 ******************************************************************************/
typedef void (*vic_DisableMain_t)(void);

/***************************************************************************//**
 * @brief Initializes global interrupt handling
 *
 * @param environmentdata  Pointer to Environment data for VIC module in
 *                         user RAM
 *
 ******************************************************************************/
typedef void (*vic_IRQInitialisation_t)(vic_pInterruptEnvironmentData_t penvironmentdata);

/***************************************************************************//**
 * @brief LIN driver interface function pointer
 ******************************************************************************/
struct vic_sInterfaceFunctions
{
  vic_InterfaceVersion_t            InterfaceVersion;

  vic_IRQInitialisation_t           IRQInitialisation;

  vic_GetPointerToEnvironmentData_t GetPointerToEnvironmentData;

  vic_RegisterModule_t              RegisterModule;
  vic_DeregisterModule_t            DeregisterModule;
  vic_EnableModule_t                EnableModule;
  vic_DisableModule_t               DisableModule;

  vic_EnableMain_t                  EnableMain;
  vic_DisableMain_t                 DisableMain;
};

typedef struct vic_sInterfaceFunctions    vic_sInterfaceFunctions_t;
typedef        vic_sInterfaceFunctions_t* vic_pInterfaceFunctions_t;
typedef const  vic_sInterfaceFunctions_t* vic_cpInterfaceFunctions_t;

#endif /* VIC_INTERFACE_H_ */




