/***************************************************************************//**
 * @file			wdog_InterruptHandler.c
 *
 * @creator		rpy
 * @created		26.01.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "io_e52398a.h"
#include <intrinsics.h>

#include "vic_InterruptHandler.h"
#include "wdog_InterruptHandler.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VENABLE register, but with the difference
 * to wdog_SetIRQVEnable, that a special enum (wdog_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void wdog_InterruptEnable(wdog_eInterruptVectorNum_t irqsrc)
{
  WDOG_IRQ_VENABLE = (uint16_t) irqsrc;
}

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VDISABLE register, but with the difference
 * to wdog_SetIRQVDisable, that a special enum (wdog_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void wdog_InterruptDisable(wdog_eInterruptVectorNum_t irqsrc)
{
  WDOG_IRQ_VDISABLE = (uint16_t) irqsrc;
}

/***************************************************************************//**
 * @implementation
 * At first the module number is looked up in a table, because module number
 * and module instance base address have a logical connection. Then the
 * interrupt callback function pointer is saved in module interrupt vector
 * table.
 *
 ******************************************************************************/
void wdog_InterruptRegisterCallback(wdog_eInterruptVectorNum_t irqvecnum, wdog_InterruptCallback_t cbfnc)
{
  wdog_sInterruptEnvironmentData_t* evironmentData = (wdog_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_WDOG);

  if ((irqvecnum < wdog_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
  {
    evironmentData->InterrupVectorTable[irqvecnum] = cbfnc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Reverse function of "wdog_RegisterInterruptCallback". Sets the corresponding
 * entry in the module interrupt vector table back to NULL. Very imported,for
 * security reasons the corresponding interrupt is disabled. Otherwise the
 * program could possibly call NULL.
 *
 ******************************************************************************/
void wdog_InterruptDeregisterCallback(wdog_eInterruptVectorNum_t irqvecnum)
{
  wdog_sInterruptEnvironmentData_t* evironmentData = (wdog_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_WDOG);

  if ((irqvecnum < wdog_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
  {
    wdog_InterruptDisable(irqvecnum);
    evironmentData->InterrupVectorTable[irqvecnum] = NULL;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Every hardware WDOG module has it's own interrupt handler, also because each
 * module has it's own interrupt vector. So the module base address is fixed.
 * The interrupt vector table is provided by the user-application from RAM. The
 * module specific vector tables lie behind the VIC interrupt vector table, which
 * address is saved in the "TABLE_BASE". The module interrupt handler
 * gets the address from the function "vic_GetPointerToEviornmentData".
 * The interrupt vector number is saved in a local variable, because the value
 * of IRQ_VECTOR could possible change during processing of
 * wdog_WDOG0_InterruptHandler. Next the registered interrupt callback function
 * (wdog_RegisterInterruptCallback) is copied into a local function pointer,
 * checked if it's not NULL and at least called. At the end the interrupt
 * request flag is cleared, by writing back the interrupt vector number to
 * IRQ_VNO register.
 *
 ******************************************************************************/
 __interrupt void wdog_InterruptHandler(void)
{
  wdog_eInterruptVectorNum_t irqvecnum = (wdog_eInterruptVectorNum_t) WDOG_IRQ_VNO;
  
  if (irqvecnum < wdog_INTERRUPT_VECTOR_CNT) /* ensure the IRQ trigger is not already gone away until processing starts */
  {
    wdog_sInterruptEnvironmentData_t* evironmentData = (wdog_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_WDOG);

    wdog_InterruptCallback_t fptr;
  
    uint16_t vicVmaxBackup = 0;
    uint16_t wdogVmaxBackup = 0;
  
    /* IRQ nesting on VIC level: enabled by default */
    vicVmaxBackup = VIC_IRQ_VMAX;
    VIC_IRQ_VMAX = (uint16_t) vic_IRQ_WDOG;

    /* IRQ nesting on module level: by default disabled, handler may override this later on */
    wdogVmaxBackup = WDOG_IRQ_VMAX;
    WDOG_IRQ_VMAX = 0;
  
    fptr = evironmentData->InterrupVectorTable[irqvecnum];
          
    if(fptr != NULL)
    {          
      __enable_interrupt();
        
      /* Call interrupt callback function */
      fptr(irqvecnum,  (void*) evironmentData->ContextData);      
      
      __disable_interrupt();
    }
    else 
    {
      /* if there is no handler function for a particular IRQ, disable this IRQ  */ 
      WDOG_IRQ_VDISABLE = (uint16_t) irqvecnum;
    }

    /* Clear interrupt request flag */
    WDOG_IRQ_VNO = (uint16_t) irqvecnum;
  
    /* IRQ nesting on module level */
    WDOG_IRQ_VMAX = wdogVmaxBackup;
  
    /* IRQ nesting on VIC level */
    VIC_IRQ_VMAX = vicVmaxBackup;
  }
  else {}
  
  /* RETI will reenable IRQs here */ 
} 

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void wdog_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, wdog_pInterruptEnvironmentData_t environmentdata, wdog_pInterruptContextData_t contextdata)
{
  if ((vicIf != NULL) && (vicIf->InterfaceVersion == VIC_INTERFACE_VERSION) && (environmentdata != NULL))
  {
    environmentdata->ContextData = contextdata;

    /* Register module at interrupt handler */
    vicIf->RegisterModule(vic_IRQ_WDOG, &wdog_InterruptHandler, environmentdata);
    vicIf->EnableModule(vic_IRQ_WDOG);
  }
  else {}
}
