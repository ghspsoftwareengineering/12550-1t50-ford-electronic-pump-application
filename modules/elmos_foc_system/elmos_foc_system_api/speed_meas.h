/**
 * @ingroup FOC
 *
 * @{
 */

#ifndef SPEED_MEAS_H_
#define SPEED_MEAS_H_

#include "global.h"
#include "motor_ctrl.h"

typedef struct                              /* PRQA S 3630 */ /* justification: typedef necessary for exporting function spdmeas_get_data() */
{
  uint16_t cur_phase_voltage_order;              /**< the order of the phase voltages (by their current value) coded into one literal number (see description of function spdmeas_set_voltage_uvw() ) */
  uint16_t last_phase_voltage_order;             /**< value of cur_phase_voltage_order of last iteration */

  uint16_t phase_voltage_pattern;                /**< cur_phase_voltage_order and last_phase_voltage_order coded into one literal number; thus certain, known values indicate a change of oder of the phase voltages */

  int16_t cur_phase_voltage_order_change;       /**< value indicating that either no change of order occurred (0), a forward change of order (1) or a backward change of order (-1), actually just: direction_lut[phase_voltage_pattern] */
  int16_t last_phase_voltage_order_change;      /**< value of cur_phase_voltage_order_change of last iteration */

  int16_t phase_voltage_order_change_evnt_cntr; /**< (signed) counter which simply counts the changes of the phase voltage order; thus a value of 6 indicates a full forward rotation, a -6 indicates a full reverse rotation */

  uint16_t speed_recalc_cntr_thrshld;            /**< amount of sectors (of the voltage hexagon, i.e. phase_voltage_order_change_evnt_cntr) after which the speed is recalculated */
  uint16_t min_meas_duration;                    /**< this amount of time has to pass between two (re)calculations of the speed; thus at higher speeds, more than 'speed_recalc_cntr_thrshld' sectors are needed for recalulation (to still achieve acceptable accuracy) [systime ticks] */

  uint16_t speed_recalc_timestamp;               /**< timestamp of last (re)calculation of the speed */

  uint16_t speed;                                /**< the actual rotor speed [eRPM] */

  motor_direction_t cur_direction;          /**< the measured direction of rotation */

  uint16_t struct_signature;

} speed_meas_data_t;

void spdmeas_init( void );
void spdmeas_generate_struct_signature ( void );
const speed_meas_data_t * spdmeas_get_data ( void );
void spdmeas_reset_data( void );
uint16_t spdmeas_get_volt_uvw_pattern( void );
uint16_t spdmeas_get_cur_speed( void );
motor_direction_t spdmeas_get_cur_direction( void );
void spdmeas_set_voltage_uvw( const int16_t u, const int16_t v, const int16_t w );

#endif /* _#ifndef SPEED_MEAS_H_ */

/* }@
 */
