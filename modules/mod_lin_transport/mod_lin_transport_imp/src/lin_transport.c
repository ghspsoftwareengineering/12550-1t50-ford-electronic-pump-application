/**
 * @file lin_transport.c
 *
 * Handles LIN diagnostic frames, which contain messages using the LIN
 * Tranport protocol as defined in LIN Transport Layer spec Revision 2.2A
 *
 *------------------------------------------------------------------------------
 *
 * Copyright 2014 GHSP, Inc., All Rights Reserved.
 * GHSP Confidential
 * 
 * NOTICE:  
 * All information contained herein is, and remains the property of GHSP, Inc. 
 * The intellectual and technical concepts contained herein are proprietary to 
 * GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in 
 * process, and are protected by trade secret or copyright law. Dissemination 
 * of this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from GHSP, Inc.
 *
 *------------------------------------------------------------------------------
 *
 * @section DESC DESCRIPTION:
 *    Add a detailed description of the functionality implemented in this file.
 *
 * @section ABBR ABBREVIATIONS:
 *   - None
 *
 * @section TRACE TRACEABILITY INFO:
 *   - Design Document(s):
 *     - None
 *
 *   - Requirements Document(s):
 *     - None
 *
 *   - Applicable Standards (in order of precedence: highest first):
 *     - <a href="http://sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx">
 *       "GHSP Coding Standard" [12-Mar-2006]</a>
 *
 * @section DFS DEVIATIONS FROM STANDARDS:
 *   - None
 *
 * @defgroup homebrew_lin_trans_imp LIN Transport Layer 2.2A Implementation
 * @{
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "lin_transport.h"
#include "lin_cfg.h"
#include "serial_data_buffer.h"
#include <string.h>
#include "uds.h"
#include "uds_cot.h"

/*===========================================================================*
 * Local Preprocessor #define Constants
 *===========================================================================*/
#if (LINT_MAX_MSG_SIZE > 4096u)
#    error "Maximum allowed payload of a LIN Transport message is 4096"
#endif

#define LINT_MAX_SINGLE_FRAME_LEN (6u)

#define LINT_FIRST_FRAME_DATA_LENGTH (5u)

#define LINT_MIN_MULTI_FRAME_LEN (6u)

#define LINT_START_FRAME_COUNTER (1u)

#define LINT_MAX_FRAME_COUNTER (15u)

#define LINT_MAX_CONSEC_FRAME_LEN (6u)

/**
 * First consecutive frame always has a frame counter of 1
 */
#define LINT_FIRST_CONSEC_FRAME_COUNTER (1u)

#define LINT_NAD_BYTE (0u)
#define LINT_PCI_BYTE (1u)
#define LINT_LEN_BYTE (2u)
#define LINT_SF_DATA_START (2u)
#define LINT_FF_DATA_START (3u)
#define LINT_CF_DATA_START (2u)

/*===========================================================================*
 * Local Preprocessor #define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/
typedef enum
{
    LIN_TRANSPORT_SINGLE_FRAME      = 0,
    LIN_TRANSPORT_FIRST_FRAME       = 1,
    LIN_TRANSPORT_CONSECUTIVE_FRAME = 2
} LIN_Transport_PCI_T;

typedef struct
{
    uint8_t NAD;
    LIN_Transport_PCI_T PCI;
    uint16_t Length;
    uint8_t Frame_Counter;
    uint8_t * Data;
} LIN_Transport_Frame_T;

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
static uint8_t LINT_Rx_Diag_Msg[LINT_MAX_MSG_SIZE];
static uint16_t LINT_Rx_Diag_Msg_Index = 0u;
static uint16_t LINT_Rx_Diag_Msg_Len = 0u;
static bool_t LINT_Rx_Multi_Frame_Msg_Started = false;
static uint8_t LINT_Rx_Expected_Frame_Counter = 0u;

static uint8_t LINT_Tx_Diag_Msg[LINT_MAX_MSG_SIZE];
static uint16_t LINT_Tx_Diag_Msg_Index = 0u;
static size_t LINT_Tx_Diag_Msg_Len = 0u;
static uint8_t LINT_Tx_Frame_Counter = LINT_FIRST_CONSEC_FRAME_COUNTER;

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Inline Function Definitions and Function-Like Macros
 *===========================================================================*/

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
void LIN_Diagnostic_Request(Serial_Data_Buffer_T * Rx_Data_Buffer, Serial_Data_Buffer_T * Tx_Data_Buffer)
{
    LIN_Transport_Frame_T Rx_Frame;
    uint8_t Raw_Bytes[8u];

    SDB_Deserialize_Bytes(Rx_Data_Buffer, Raw_Bytes, 8u); /* Diagnostic frames always have 8 bytes */

    Rx_Frame.NAD = Raw_Bytes[LINT_NAD_BYTE];

    if (Rx_Frame.NAD == LIN_NETWORK_ADDRESS)
    {
        Rx_Frame.PCI = (LIN_Transport_PCI_T)((Raw_Bytes[LINT_PCI_BYTE] & 0xF0u) >> 4u);

        switch (Rx_Frame.PCI)
        {
            case LIN_TRANSPORT_SINGLE_FRAME:
            {
                Rx_Frame.Length = (uint16_t)Raw_Bytes[LINT_PCI_BYTE] & 0x0Fu;

                if (Rx_Frame.Length <= LINT_MAX_SINGLE_FRAME_LEN)
                {
                    Rx_Frame.Data = &Raw_Bytes[LINT_SF_DATA_START];

                    for (LINT_Rx_Diag_Msg_Index = 0u; LINT_Rx_Diag_Msg_Index < Rx_Frame.Length; LINT_Rx_Diag_Msg_Index++)
                    {
                        LINT_Rx_Diag_Msg[LINT_Rx_Diag_Msg_Index] = Rx_Frame.Data[LINT_Rx_Diag_Msg_Index];
                    }

                    /* Send frame off to Frame Handler */
                    UDS_Handle_Message(LINT_Rx_Diag_Msg, Rx_Frame.Length);
                }
            }
            break;

            case LIN_TRANSPORT_FIRST_FRAME:
            {
                Rx_Frame.Length = (((uint16_t)Raw_Bytes[LINT_PCI_BYTE] & 0x0Fu) << 8u) | Raw_Bytes[LINT_LEN_BYTE];

                if (Rx_Frame.Length > LINT_MIN_MULTI_FRAME_LEN)
                {
                    uint8_t Rx_Frame_Index = 0u;

                    Rx_Frame.Data = &Raw_Bytes[LINT_FF_DATA_START];

                    LINT_Rx_Multi_Frame_Msg_Started = true;
                    LINT_Rx_Diag_Msg_Len = Rx_Frame.Length;

                    LINT_Rx_Diag_Msg_Index = 0u;

                    for (Rx_Frame_Index = 0u; Rx_Frame_Index < LINT_FIRST_FRAME_DATA_LENGTH; Rx_Frame_Index++)
                    {
                        LINT_Rx_Diag_Msg[LINT_Rx_Diag_Msg_Index] = Rx_Frame.Data[Rx_Frame_Index];
                        LINT_Rx_Diag_Msg_Index++;
                    }

                    LINT_Rx_Expected_Frame_Counter = LINT_START_FRAME_COUNTER;
                }
            }
            break;

            case LIN_TRANSPORT_CONSECUTIVE_FRAME:
            {
                Rx_Frame.Frame_Counter = Raw_Bytes[LINT_PCI_BYTE] & 0x0Fu;

                if (LINT_Rx_Multi_Frame_Msg_Started)
                {
                    if (Rx_Frame.Frame_Counter == LINT_Rx_Expected_Frame_Counter)
                    {
                        uint8_t Rx_Frame_Index = 0u;

                        Rx_Frame.Data = &Raw_Bytes[LINT_CF_DATA_START];

                        while (    (Rx_Frame_Index < LINT_MAX_CONSEC_FRAME_LEN)
                                && (LINT_Rx_Diag_Msg_Index < LINT_Rx_Diag_Msg_Len))
                        {
                            LINT_Rx_Diag_Msg[LINT_Rx_Diag_Msg_Index] = Rx_Frame.Data[Rx_Frame_Index];
                            Rx_Frame_Index++;
                            LINT_Rx_Diag_Msg_Index++;
                        }

                        if (LINT_Rx_Diag_Msg_Index == LINT_Rx_Diag_Msg_Len)
                        {
                            /* Send Frame off to Handler */
                            UDS_Handle_Message(LINT_Rx_Diag_Msg, LINT_Rx_Diag_Msg_Len);

                            /* Finished Multi-Frame Message */
                            LINT_Rx_Multi_Frame_Msg_Started = false;
                        }
                        else
                        {
                            LINT_Rx_Expected_Frame_Counter = (LINT_Rx_Expected_Frame_Counter + 1u) % (LINT_MAX_FRAME_COUNTER + 1u);
                        }
                    }
                    else
                    {
                        /* Corrupted Multi Frame message. Drop any consecutive frames */
                        LINT_Rx_Multi_Frame_Msg_Started = false;
                    }
                }
            }
            break;

            default:
            {

            }
            break;
        }
    }
    else
    {
        /* Message not addressed to this node. Cancel any pending Multi-Frame messages */
        LINT_Rx_Multi_Frame_Msg_Started = false;
    }
}

void LIN_Diagnostic_Response(Serial_Data_Buffer_T * Rx_Data_Buffer, Serial_Data_Buffer_T * Tx_Data_Buffer)
{
    if (LINT_Tx_Diag_Msg_Len > 0u)
    {
        /* NAD Byte */
        SDB_Serialize_U8(Tx_Data_Buffer, LIN_NETWORK_ADDRESS);

        if (LINT_Tx_Diag_Msg_Len >= LINT_MIN_MULTI_FRAME_LEN)
        {
            /* Multi Frame Response is pending */

            if (0u == LINT_Tx_Diag_Msg_Index)
            {
                /* First frame of multi frame message */

                /* PCI Byte */
                SDB_Serialize_U8(Tx_Data_Buffer, (LIN_TRANSPORT_FIRST_FRAME << 4u) | (uint8_t)(((LINT_Tx_Diag_Msg_Len & 0x0F00u) >> 8u)));

                /* LEN Byte */
                SDB_Serialize_U8(Tx_Data_Buffer, ((uint8_t)(LINT_Tx_Diag_Msg_Len & 0x00FFu)));

                SDB_Serialize_Bytes(Tx_Data_Buffer, LINT_Tx_Diag_Msg, LINT_FIRST_FRAME_DATA_LENGTH);

                LINT_Tx_Diag_Msg_Index += LINT_FIRST_FRAME_DATA_LENGTH;
            }
            else
            {
                uint8_t Tx_Frame_Byte_Count = 0u;

                /* Consecutive frame of multi frame message */

                /* PCI Byte */
                SDB_Serialize_U8(Tx_Data_Buffer, (LIN_TRANSPORT_CONSECUTIVE_FRAME << 4u) | LINT_Tx_Frame_Counter);

                while (    (Tx_Frame_Byte_Count < LINT_MAX_CONSEC_FRAME_LEN)
                        && (LINT_Tx_Diag_Msg_Index < LINT_Tx_Diag_Msg_Len))
                {
                    SDB_Serialize_U8(Tx_Data_Buffer, LINT_Tx_Diag_Msg[LINT_Tx_Diag_Msg_Index]);
                    LINT_Tx_Diag_Msg_Index++;
                    Tx_Frame_Byte_Count++;
                }

                if (LINT_Tx_Diag_Msg_Index == LINT_Tx_Diag_Msg_Len)
                {
                    /* Multi frame message is complete */
                    LINT_Tx_Diag_Msg_Len = 0u;
                    LINT_Tx_Diag_Msg_Index = 0u;
                    LINT_Tx_Frame_Counter = LINT_FIRST_CONSEC_FRAME_COUNTER;

                    /* Fill any remaining bytes in the frame with 0xFF */
                    while (Tx_Frame_Byte_Count < LINT_MAX_CONSEC_FRAME_LEN)
                    {
                        SDB_Serialize_U8(Tx_Data_Buffer, 0xFFu);
                        Tx_Frame_Byte_Count++;
                    }
                }
                else
                {
                    LINT_Tx_Frame_Counter = (LINT_Tx_Frame_Counter + 1u) % (LINT_MAX_FRAME_COUNTER + 1u);
                }
            }
        }
        else
        {
            uint8_t Tx_Frame_Byte_Count = 0u;

            /* Single Frame response is pending */

            /* PCI Byte */
            SDB_Serialize_U8(Tx_Data_Buffer, (LIN_TRANSPORT_SINGLE_FRAME << 4u) | ((uint8_t)LINT_Tx_Diag_Msg_Len));

            for (Tx_Frame_Byte_Count = 0u; Tx_Frame_Byte_Count < LINT_MAX_SINGLE_FRAME_LEN; Tx_Frame_Byte_Count++)
            {
                if (Tx_Frame_Byte_Count < LINT_Tx_Diag_Msg_Len)
                {
                    SDB_Serialize_U8(Tx_Data_Buffer, LINT_Tx_Diag_Msg[LINT_Tx_Diag_Msg_Index]);
                    LINT_Tx_Diag_Msg_Index++;
                }
                else
                {
                    /* Fill with 0xFF */
                    SDB_Serialize_U8(Tx_Data_Buffer, 0xFFu);
                }
            }

            LINT_Tx_Diag_Msg_Len = 0u; /* Mark message as sent */
            LINT_Tx_Diag_Msg_Index = 0u;
        }
    }
}

int32_t UDS_Transmit_Response(uint8_t const * Message_Buffer,
                              size_t const Len,
                              UDS_Response_Complete_Callback_T const fp_Response_Complete)
{
    memcpy(LINT_Tx_Diag_Msg, Message_Buffer, Len);
    LINT_Tx_Diag_Msg_Len = Len;

    return 0;
}

/** @} doxygen end group */
