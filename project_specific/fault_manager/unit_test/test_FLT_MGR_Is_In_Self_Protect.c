/**
 *  @file test_FLT_MGR_Is_In_Self_Protect.c
 *
 */
#include "unity.h"
#include "global.h"
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "speed_meas.h"
#include "motor_manager.h"

#include <string.h>


/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_system_signals.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_motor_ctrl.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"

/* STATIC's */
void FLT_MGR_Check_Irqstat( void );

bool_t FLT_MGR_Irqstat_Fault;
bool_t Fault_Active[FLT_MGR_NUM_FAULTS];

void setUp(void)
{
    memset(Fault_Active, 0, sizeof(Fault_Active));
}

void tearDown(void)
{

}


/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return true if voltage is over the limit
 * required to be in self protect
 */
/* Reqs: SWREQ_1941, SWREQ_1944, SWREQ_2049, SWREQ_2051, SWREQ_2053 */
void test_FLT_MGR_Is_In_Self_Protect_returns_true_when_Overvoltage_Fault_Is_Present(void)
{
    bool_t self_protect_state = false;

    /* Ensure known test state */
    Fault_Active[FLT_OVER_VOLTAGE] = true;

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_EQUAL(true, self_protect_state);
 }

/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return true when both temperature and voltage
 * are over the limit required to be in self protect
 */
/* Reqs: SWREQ_1941, SWREQ_1944, SWREQ_1997, SWREQ_2000, SWREQ_2003, SWREQ_2049, SWREQ_2051, SWREQ_2053 */
void test_FLT_MGR_Is_In_Self_Protect_returns_true_when_Overtemp_And_Overvoltage_Faults_Are_Present(void)
{
    bool_t self_protect_state = false;

    /* Ensure known test state */
    Fault_Active[FLT_OVER_VOLTAGE] = true;
    Fault_Active[FLT_OVER_TEMPERATURE] = true;

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_EQUAL(true, self_protect_state);
 }

/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return false if temperature and
 * voltage is within operating range
 */
/* Reqs: SWREQ_2048, SWREQ_2050 */
void test_FLT_MGR_Is_In_Self_Protect_returns_false_when_No_Fault_Present(void)
{
    bool_t self_protect_state = true;

    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_EQUAL(false, self_protect_state);
}

/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return false if the temperature is too high
 */
/* Reqs: SWREQ_1998, SWREQ_2001, SWREQ_2048, SWREQ_2050 */
void test_FLT_MGR_Is_In_Self_Protect_returns_false_when_only_FLT_OVER_TEMPERATURE_present(void)
{
    bool_t self_protect_state = true;

    /* Ensure known test state */
    Fault_Active[FLT_OVER_TEMPERATURE] = true;

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_EQUAL(false, self_protect_state);
}

/**
 * @test Verifies that FLT_MGR_Is_In_Self_Protect() will return true if the voltage is too low
 */
/* Reqs: SWREQ_2055 */
void test_FLT_MGR_Is_In_Self_Protect_returns_true_when_under_voltage_present(void)
{
    bool_t self_protect_state = false;


    /* Ensure known test state */
    memset(Fault_Active, 0, sizeof(Fault_Active));
    Fault_Active[FLT_UNDER_VOLT_SELF_PROTECT] = true;

    /* Setup expected call chain */

    /* Call function under test */
    self_protect_state = FLT_MGR_Is_In_Self_Protect();

    /* Verify test results */
    TEST_ASSERT_TRUE(self_protect_state);
}


/**
 * Verifies that FLT_MGR_Check_Irqstat() will set FLT_MGR_Irqstat_Fault to true if a short is detected in IRQSTAT1
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_sets_FLT_MGR_Irqstat_Fault_true_when_short_in_IRQSTAT1(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = false;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(1u);
    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_SHORT_CIRCUIT);

    /* Call function under test */
    FLT_MGR_Check_Irqstat();


    /* Verify test results */
    TEST_ASSERT_TRUE(FLT_MGR_Irqstat_Fault);
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() will set FLT_MGR_Irqstat_Fault to true when no short is indicated
 * in IRQSTAT but other faults are.
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_sets_FLT_MGR_Irqstat_Fault_true_when_no_short_in_IRQSTAT1_but_other_faults(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = false;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0xFFC0u); /* all bits are set in IRQSTAT1 and IRQSTAT2 except the short circuit bits */

    FLT_LOG_Record_Fault_Active_Expect(FLT_LOG_SHORT_CIRCUIT);

    /* Call function under test */
    FLT_MGR_Check_Irqstat();


    /* Verify test results */
    TEST_ASSERT_TRUE(FLT_MGR_Irqstat_Fault);
}

/**
 * Verifies that FLT_MGR_Check_Irqstat() will set FLT_MGR_Irqstat_Fault to false if no fault is detected in IRQSTAT1
 */
/* Reqs: SWREQ_1948 */
void test_FLT_MGR_Check_Irqstat_sets_FLT_MGR_Irqstat_Fault_false_when_no_fault_in_IRQSTAT1(void)
{
    /* Ensure known test state */
    FLT_MGR_Irqstat_Fault = true;

    /* Setup expected call chain */
    mc_get_irqstat_ExpectAndReturn(0u); /* no fault bits are set in IRQSTAT */
    FLT_LOG_Record_Fault_Inactive_Expect(FLT_LOG_SHORT_CIRCUIT);

    /* Call function under test */
    FLT_MGR_Check_Irqstat();


    /* Verify test results */
    TEST_ASSERT_FALSE(FLT_MGR_Irqstat_Fault);
}
