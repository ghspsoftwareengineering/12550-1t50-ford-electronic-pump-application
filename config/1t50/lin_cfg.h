#ifndef LIN_CFG_H
#   define LIN_CFG_H
/*===========================================================================*/
/**
 * @file lin_cfg.h
 *
 *   Configuration items for the LIN Physical Layer
 *
 *------------------------------------------------------------------------------
 *
 * Copyright 2014 GHSP, Inc., All Rights Reserved.
 * GHSP Confidential
 * 
 * NOTICE:  
 * All information contained herein is, and remains the property of GHSP, Inc. 
 * The intellectual and technical concepts contained herein are proprietary to 
 * GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in 
 * process, and are protected by trade secret or copyright law. Dissemination 
 * of this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from GHSP, Inc.
 *
 *------------------------------------------------------------------------------
 *
 * @section DESC DESCRIPTION:
 *
 * This file contains the configuration for the LIN Physical Layer. This includes
 * configuring the list of LIN Frames that are handled by this slave.
 *
 * @section ABBR ABBREVIATIONS:
 *   - None
 *
 * @section TRACE TRACEABILITY INFO:
 *   - Design Document(s):
 *     - None
 *
 *   - Requirements Document(s):
 *     - None
 *
 *   - Applicable Standards (in order of precedence: highest first):
 *     - <a href="http://sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx">
 *       "GHSP Coding Standard" [12-Mar-2006]</a>
 *
 * @section DFS DEVIATIONS FROM STANDARDS:
 *   - None
 *
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/

/*===========================================================================*
 * #define Constants
 *===========================================================================*/

/*===========================================================================*\
 * #define MACROS
\*===========================================================================*/
/**
 * Configure the Network address of this LIN Slave node
 */
#define LIN_NETWORK_ADDRESS (0x62u)

/**
 * Configure this item for the maximum number of data bytes that the LIN Transport
 * layer will support. Any messages that are longer than this maximum number of data bytes
 * will be discarded.
 * 
 * The maximum value of this configuration is 4096.
 */ 
#define LINT_MAX_MSG_SIZE (256u)
/*===========================================================================*\
 * Custom Type Declarations
\*===========================================================================*/

#endif /* LIN_CFG_H */
