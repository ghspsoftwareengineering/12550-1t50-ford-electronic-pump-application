/***************************************************************************//**
 * @file			mem_prot_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef MEM_PROT_INTERRUPTHANDLER_H_          
#define MEM_PROT_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * MEM_PROT IRQ vector numbers
 ******************************************************************************/
typedef enum {
  mem_prot_IRQ_EVT_UNDEFINED          = 0u,
  mem_prot_IRQ_EVT_DMISALIGNED        = 1u,

  mem_prot_INTERRUPT_VECTOR_CNT       = 2u  /**< Number of available interrupt vectors */
} mem_prot_eInterruptVectorNum_t;


/***************************************************************************//**
 * Pointer to MEM_PROT context data
 ******************************************************************************/
typedef void * mem_prot_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*mem_prot_InterruptCallback_t) (mem_prot_eInterruptVectorNum_t irqsrc, mem_prot_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * MEM_PROT environment data
 ******************************************************************************/
typedef struct mem_prot_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module */
    mem_prot_InterruptCallback_t InterrupVectorTable[mem_prot_INTERRUPT_VECTOR_CNT];
    
    /** MEM_PROT module context data */
    mem_prot_pInterruptContextData_t ContextData;
    
} mem_prot_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to MEM_PROT environment data
 ******************************************************************************/
typedef mem_prot_sInterruptEnvironmentData_t * mem_prot_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to MEM_PROT module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with mem_prot_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The MEM_PROT IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void mem_prot_InterruptEnable(mem_prot_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to MEM_PROT module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The MEM_PROT IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void mem_prot_InterruptDisable(mem_prot_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to MEM_PROT module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void mem_prot_InterruptRegisterCallback(mem_prot_eInterruptVectorNum_t irqvecnum, mem_prot_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to MEM_PROT module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void mem_prot_InterruptDeregisterCallback(mem_prot_eInterruptVectorNum_t irqvecnum);

/***************************************************************************//**
 * Handles the MEM_PROT related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void mem_prot_InterruptHandler(void);

/***************************************************************************//**
 * @brief Initialize MEM_PROT module
 *
 * @param environmentdata  Pointer to Environment data for MEM_PROT module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and MEM_PROT (mem_prot_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       MEM_PROT module is configured for use.
 *
 * @detaildesc
 * Initializes the MEM_PROT software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as MEM_PROTs or not.
 *
 ******************************************************************************/
void mem_prot_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, mem_prot_pInterruptEnvironmentData_t environmentdata, mem_prot_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

