/***************************************************************************//**
 * @file			adc_ctrl_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef ADC_CTRL_INTERRUPTHANDLER_H_          
#define ADC_CTRL_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * ADC_CTRL IRQ vector numbers
 ******************************************************************************/
typedef enum {
  
  adc_ctrl_IRQ_DMA_READ_ERR       = 0u,
  adc_ctrl_IRQ_DMA_WRITE_ERR      = 1u,
  adc_ctrl_IRQ_SUM_WRITTEN_EVT    = 2u,
  adc_ctrl_IRQ_LIST_DONE_EVT      = 3u,
  adc_ctrl_IRQ_SADR_DONE_NEMPTY   = 4u,
  adc_ctrl_IRQ_SADR_NEW_NFULL     = 5u,
  adc_ctrl_IRQ_OOR0               = 6u,
  adc_ctrl_IRQ_OOR1               = 7u,
  adc_ctrl_IRQ_OOR2               = 8u,
  adc_ctrl_IRQ_OOR3               = 9u,
  adc_ctrl_IRQ_OOR4               = 10u,
  adc_ctrl_IRQ_OOR5               = 11u,
  adc_ctrl_IRQ_OOR6               = 12u,
  adc_ctrl_IRQ_OOR7               = 13u,
  adc_ctrl_IRQ_OOR8               = 14u,
  adc_ctrl_IRQ_OOR9               = 15u,

  adc_ctrl_INTERRUPT_VECTOR_CNT   = 16u  /**< Number of available interrupt vectors */
} adc_ctrl_eInterruptVectorNum_t;


/***************************************************************************//**
 * Pointer to ADC_CTRL context data
 ******************************************************************************/
typedef void * adc_ctrl_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*adc_ctrl_InterruptCallback_t) (adc_ctrl_eInterruptVectorNum_t irqsrc, adc_ctrl_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * ADC_CTRL environment data
 ******************************************************************************/
typedef struct adc_ctrl_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module */
    adc_ctrl_InterruptCallback_t InterrupVectorTable[adc_ctrl_INTERRUPT_VECTOR_CNT];
    
    /** ADC_CTRL module context data */
    adc_ctrl_pInterruptContextData_t ContextData;
    
} adc_ctrl_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to ADC_CTRL environment data
 ******************************************************************************/
typedef adc_ctrl_sInterruptEnvironmentData_t * adc_ctrl_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to ADC_CTRL module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with adc_ctrl_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The ADC_CTRL IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void adc_ctrl_InterruptEnable(adc_ctrl_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to ADC_CTRL module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The ADC_CTRL IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void adc_ctrl_InterruptDisable(adc_ctrl_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to ADC_CTRL module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void adc_ctrl_InterruptRegisterCallback(adc_ctrl_eInterruptVectorNum_t irqvecnum, adc_ctrl_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to ADC_CTRL module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void adc_ctrl_InterruptDeregisterCallback(adc_ctrl_eInterruptVectorNum_t irqvecnum);

/***************************************************************************//**
 * Handles the ADC_CTRL related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void adc_ctrl_InterruptHandler(void);

/***************************************************************************//**
 * @brief Initialize ADC_CTRL module
 *
 * @param environmentdata  Pointer to Environment data for ADC_CTRL module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and ADC_CTRL (adc_ctrl_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       ADC_CTRL module is configured for use.
 *
 * @detaildesc
 * Initializes the ADC_CTRL software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as ADC_CTRLs or not.
 *
 ******************************************************************************/
void adc_ctrl_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, adc_ctrl_pInterruptEnvironmentData_t environmentdata, adc_ctrl_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

