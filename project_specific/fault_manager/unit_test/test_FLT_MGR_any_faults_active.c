/**
 *  @file test_FLT_MGR_any_faults_active.c
 *
 */
#include "unity.h"
#include "global.h"
#include "fault_manager.h"
#include "fault_manager_imp_defines.h"
#include "fault_manager_imp_types.h"
#include "speed_meas.h"
#include "motor_manager.h"

#include <string.h>


/* MOCKS */
#include "mock_fault_logger.h"
#include "mock_motor_ctrl.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_motor_manager.h"
#include "mock_nvm_mgr.h"


/* STATIC's */
bool_t Fault_Active[FLT_MGR_NUM_FAULTS];

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that FLT_MGR_Is_Any_Fault_Active() will return false if no fault is active
 * 
 * Fault_Active[] = all false
 */
/* Reqs: SWREQ_1950 */
void test_FLT_MGR_Is_Any_Fault_Active_When_no_faults_present(void)
{
    bool_t actual_response = true;


    /* Ensure known test state */
 
    /* Setup expected call chain */
 
    /* Call function under test */
    actual_response = FLT_MGR_Is_Any_Fault_Active();

    /* Verify test results */
    TEST_ASSERT_FALSE(actual_response);
}

/**
 * @test Verifies that FLT_MGR_Is_Any_Fault_Active() will return true if one or more faults are active
 * Fault_Active[1] = true
 */
/* Reqs: SWREQ_1950 */
void test_FLT_MGR_Is_Any_Fault_Active_When_faults_present(void)
{
    bool_t actual_response = false;

    /* Ensure known test state */
    Fault_Active[1] = true;

    /* Setup expected call chain */
 

    /* Call function under test */
    actual_response = FLT_MGR_Is_Any_Fault_Active();

    /* Verify test results */
    TEST_ASSERT_TRUE(actual_response);
}


/**
 * @test Verifies that FLT_MGR_Is_Any_Fault_Active() will return true if one or more faults are active
 * Fault_Active[0] = true
 * Fault_Active[FLT_MGR_NUM_FAULTS - 1] = true
 */
/* Reqs: SWREQ_1950 */
void test_FLT_MGR_Is_Any_Fault_Active_When_more_than_one_faults_present(void)
{
    bool_t actual_response = false;

    /* Ensure known test state */
    Fault_Active[0] = true;
    Fault_Active[FLT_MGR_NUM_FAULTS - 1] = true;
    /* Setup expected call chain */
 

    /* Call function under test */
    actual_response = FLT_MGR_Is_Any_Fault_Active();

    /* Verify test results */
    TEST_ASSERT_TRUE(actual_response);
}
