/**
 *  @file test_VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl.c
 *
 */
#include "unity.h"
#include "lin_vehicle_comm.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_fault_manager.h"
#include "mock_foc_stubs.h"
#include "mock_lin_transport.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"
#include "mock_time_utils.h"

/* STATIC's */
uint8_t vcomm_V_Pc_Actl;

void setUp(void)
{
    vcomm_V_Pc_Actl = 0;
}

void tearDown(void)
{

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl() sets the LIN signal value
 * to 255 when the actual RPM is 4200
 */
/* Reqs: SWREQ_2059, SWREQ_1388 */
void test_VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_sets_signal_to_255_When_speed_is_4200(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(4200);
    
    /* Verify test results */
    TEST_ASSERT_HEX8_WITHIN(1, 255, vcomm_V_Pc_Actl);
    
}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl() caps the LIN signal value
 * to 255 when the actual RPM is 4400
 */
/* Reqs: SWREQ_2059, SWREQ_1388, SWREQ_1924 */
void test_VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_sets_signal_to_255_When_speed_is_4400(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(4400);

    /* Verify test results */
    TEST_ASSERT_HEX8_WITHIN(1, 255, vcomm_V_Pc_Actl);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl() sets the LIN signal value
 * to 63 when the actual RPM is 1037
 */
/* Reqs: SWREQ_2059, SWREQ_1388 */
void test_VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_sets_signal_to_63_When_speed_is_1037(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(1037);

    /* Verify test results */
    TEST_ASSERT_HEX8_WITHIN(1, 63, vcomm_V_Pc_Actl);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl() sets the LIN signal value
 * to 1 when the actual RPM is 16
 */
/* Reqs: SWREQ_2059, SWREQ_1388 */
void test_VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_sets_signal_to_1_When_speed_is_16(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(16);

    /* Verify test results */
    TEST_ASSERT_HEX8_WITHIN(1, 1, vcomm_V_Pc_Actl);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl() sets the LIN signal value
 * to 0 when the actual RPM is 0
 */
/* Reqs: SWREQ_2059, SWREQ_1388 */
void test_VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_sets_signal_to_0_When_speed_is_0(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(0);

    /* Verify test results */
    TEST_ASSERT_HEX8_WITHIN(1, 0, vcomm_V_Pc_Actl);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl() sets the LIN signal value
 * to 15 when the actual RPM is 247
 */
/* Reqs: SWREQ_2059, SWREQ_1388 */
void test_VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_sets_signal_to_15_When_speed_is_247(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(247);

    /* Verify test results */
    TEST_ASSERT_HEX8_WITHIN(1, 15, vcomm_V_Pc_Actl);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl() sets the LIN signal value
 * to 127 when the actual RPM is 2091
 */
/* Reqs: SWREQ_2059, SWREQ_1388 */
void test_VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_sets_signal_to_127_When_speed_is_2091(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(2091);

    /* Verify test results */
    TEST_ASSERT_HEX8_WITHIN(1, 127, vcomm_V_Pc_Actl);

}

/**
 * @test Verifies that VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl() sets the LIN signal value
 * to 191 when the actual RPM is 3145
 */
/* Reqs: SWREQ_2059, SWREQ_1388 */
void test_VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl_sets_signal_to_191_When_speed_is_3145(void)
{
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    VCOMM_Signal_Set_TrnOilPumpV_Pc_Actl(3145);

    /* Verify test results */
    TEST_ASSERT_HEX8_WITHIN(1, 191, vcomm_V_Pc_Actl);

}
