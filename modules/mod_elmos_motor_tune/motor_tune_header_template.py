header_file_template = '''
#ifndef {guard}
#    define {guard}

/**
 *  @file {file}
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup elmos_motor_cfg_api Elmos Motor Tune Configuration Interface Documentation
 *  @{{
 *      @details Contains the static configuration for the Elmos Field Oriented Motor Control
 *               system.
 *
 *      @page ABBR Abbreviations
 *        - FOC - Field Oriented Control
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "foc.h"
#include "types.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
{defines}
/** @}} doxygen end group */

#endif /* {guard} */
'''