#ifndef FAULT_LOGGER_IMP_DEFINES_H
#define FAULT_LOGGER_IMP_DEFINES_H
/**
 *  @file fault_logger_imp_defines.h
 *
 *  @ref fault_logger_imp
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fault_logger_imp 1T50 Fault Logger Implementation
 *  @{
 *
 *      @page fault_logger_imp_defines_api 1T50 Fault Logger defines Interface
 *
 *      Private API used internal to the implementation of the 1T50 Fault Logger
 *      module.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
#define FLT_MAX_INCIDENTS_TO_LOG (16u)
#define FLT_LOG_NVM_BLOCK_COUNT (FLT_MAX_INCIDENTS_TO_LOG / 2u)

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/** @} doxygen end group */
#endif /* FAULT_LOGGER_IMP_DEFINES_H */
