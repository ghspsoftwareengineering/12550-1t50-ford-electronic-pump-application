include $(GHSP_MAKE_DIR)/pre_module.mak

####################################################
# BEGIN: User configurable portion of the makefile #
####################################################

# Module only supports the production build
#ifeq ($(_build_),1t50)
# Module only supports the IAR RH850 Toolchain
ifneq ($($(_build_)_TOOLCHAIN),unity)

#
# Define the output directories for this module
#
$(_build_)_ELMOS_MOTOR_TUNE_OUT_DIR := $($(_build_)_GENERATED_DIRECTORY)/elmos_motor_tune_api

#
# Create the output directories for this module.
#
$(shell $(MKDIR) -p $($(_build_)_ELMOS_MOTOR_TUNE_OUT_DIR))

#
# Add API to the list of generated APIs
#
ALL_GENERATED_PUBLIC_APIS += $($(_build_)_ELMOS_MOTOR_TUNE_OUT_DIR)

ELMOS_MOTOR_TUNE_GEN := $(_module_path_)/motor_tune_gen.py

.PHONY: $(_module_target_)
.PHONY: $(_module_name_)
$(_module_name_):$(_module_target_)
$(_module_target_):$($(_build_)_ELMOS_MOTOR_TUNE_OUT_DIR)/motor_cfg.h
$($(_build_)_ELMOS_MOTOR_TUNE_OUT_DIR)/motor_cfg.h:$($(_build_)_MOTOR_CONFIG_FILE)
	@$(ECHO) "Generating Elmos Motor Tuning configuration $@"
	@$(PYTHON) $(ELMOS_MOTOR_TUNE_GEN) $< --output $@

####################################################
# END: user configurable section of the makefile   #
####################################################
ALL_$(_build_)_MODULES += $(_module_name_)

endif #ifneq ($($(_build_)_TOOLCHAIN),unity)
#endif #ifeq ($(_build_),1t50)
