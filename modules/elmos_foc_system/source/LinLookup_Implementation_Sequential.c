/***************************************************************************//**
 * @file			LinLookup_Implementation_Fixed_Sequential.c
 *
 * @creator		sbai
 * @created		13.01.2015
 *
 * $Id: $
 *
 * $Revision: $
 *
 * @misra{M3CM Dir-2.7. - PRQA Msg 3206,
 * The Elmos LIN Driver defines interfaces for every module of it. This interfaces consist
 * of typedefinitions of function pointers combined in struct. This struct has to be
 * implemented by every implementation of every module. Because of this matter of fact\,
 * there can be different implementations for the same module and for the same reason one
 * implementation use and/or write to a parameter defined in the function pointer typedefinition
 * and another one not. So the parameter of the interface function are binding\, but must not
 * be used and/or not written to them.,
 * Unnecessary code.,
 * None}
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "el_helper.h"
#include "LinLookup_Implementation_Sequential.h"
#include "Lin_Basictypes.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#if LINDRVIMP_AQR_MOD_SZE == 1
#define LOOKUP_LAYER_SEQ_CODE _Pragma("location=\"LOOKUP_LAYER_SEQ_CODE\"")
#define LOOKUP_LAYER_SEQ_DATA _Pragma("location=\"LOOKUP_LAYER_SEQ_DATA\"")
#else
#define LOOKUP_LAYER_SEQ_CODE 
#define LOOKUP_LAYER_SEQ_DATA 
#endif

#define LOC_FIX_LOOKUP_TBL_SZE   LIN_MAX_FRAMEID_CNT

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: Concrete description.
 *
 ******************************************************************************/
struct loc_sSeqLookupEnvironmentData /**<< TODO: Concrete description. */
{
    LinBusIf_pFrameID_t             AssignmentTables[LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS];         /**<< TODO: Concrete description. */
    LinProtoIf_cpFrameDescription_t FrameDescriptionLists[LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS];    /**<< TODO: Concrete description. */
    LinProtoIf_pGenericCbCtxData_t  PerFrameDescLstCbCtxData[LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS]; /**<< TODO: Concrete description. */
    Lin_Bool_t                      FrmDescLstLdfRelevance[LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS];   /**<< TODO: Concrete description. */
    Lin_uint8_t                     FrmCnts[LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS];                  /**<< TODO: Concrete description. */
    Lin_uint8_t                     TotalFrameCnt;                                                            /**<< TODO: Concrete description. */
};

typedef struct loc_sSeqLookupEnvironmentData    loc_sSeqLookupEnvironmentData_t;  /**<< TODO: Concrete description. */
typedef        loc_sSeqLookupEnvironmentData_t* loc_pSeqLookupEnvironmentData_t;  /**<< TODO: Concrete description. */

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 *
 ******************************************************************************/
LOOKUP_LAYER_SEQ_CODE static Lin_Bool_t loc_ExecuteFrameIdAssignment(loc_pSeqLookupEnvironmentData_t   lookupEnvData, Lin_uint8_t                           frameDescLstIdx,
                                                                     Lin_uint8_t                    frameDescIdx,  LinLookupIf_cpAssignFrameIDRangeLst_t frameIdLst,
                                                                     LinLookupIf_FrameIdLstLength_t frameIdLstLen, Lin_Bool_t                            tryOnly);

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINLOOKUPIMP_SEQUENTIAL_EXT_IFFUN_STRCT_ACCESS == 1
LOOKUP_LAYER_SEQ_DATA const LinLookupIf_sInterfaceFunctions_t LinLookupImp_InterfaceFunctions_Sequential =
#else
LOOKUP_LAYER_SEQ_DATA static const LinLookupIf_sInterfaceFunctions_t LinLookupImp_InterfaceFunctions_Sequential =
#endif
{
  .InterfaceVersion           = LINLOOKUP_INTERFACE_MODULE_API_VERSION,

  .Initialization             = &LinLookupImp_Initialization_Sequential,
  .GetSubInterface            = &LinLookupImp_GetSubInterface_Sequential,
  .AssignFrameID              = &LinLookupImp_AssignFrameID_Sequential,
  .AssignFrameIDRange         = &LinLookupImp_AssignFrameIDRange_Sequential,
  .GetFrameDescription        = &LinLookupImp_GetFrameDescription_Sequential,
  .GetFrameIdAssignment       = &LinLookupImp_GetFrameIdAssignment_Sequential,
  .AddFrameDescriptionList    = &LinLookupImp_AddFrameDescLst_Sequential,
  .RemoveFrameDescriptionList = &LinLookupImp_RmvFrameDescLst_Sequential,
  .GetPerFrmDescLstCbCtxData  = &LinLookupImp_GetPerFrameDescLstCbCtxData_Sequential,
};

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
LOOKUP_LAYER_SEQ_CODE static Lin_Bool_t loc_ExecuteFrameIdAssignment(loc_pSeqLookupEnvironmentData_t   lookupEnvData, Lin_uint8_t                           frameDescLstIdx,
                                                                     Lin_uint8_t                    frameDescIdx,  LinLookupIf_cpAssignFrameIDRangeLst_t frameIdLst,
                                                                     LinLookupIf_FrameIdLstLength_t frameIdLstLen, Lin_Bool_t                            tryOnly)
{
  Lin_Bool_t retVal = LIN_FALSE;
  Lin_uint8_t i;

  if(frameDescLstIdx < LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS)
  {
    for(i = 0; i < frameIdLstLen; i++)
    {
      if(tryOnly == LIN_FALSE)
      {
        switch(frameIdLst[i])
        {
          case LinLookupIf_AssignFrameIDRangeCmd_UNASSIGN:
            (lookupEnvData->AssignmentTables[frameDescLstIdx])[frameDescIdx] = LIN_INVALID_FRAMEID;
            break;
          case LinLookupIf_AssignFrameIDRangeCmd_DONOTCARE:
            break;
          default:
            if(frameIdLst[i] <= LIN_MAX_FRAMEID)// No frame IDs above 63
            {
              /* Assign PID to frame */
              (lookupEnvData->AssignmentTables[frameDescLstIdx])[frameDescIdx] = frameIdLst[i];
            }
            break;
        }
      }

      frameDescIdx++;

      /* Increase index of Frame Description List if its end has been reached */
      if(frameDescIdx >= lookupEnvData->FrmCnts[frameDescLstIdx])
      {
        frameDescLstIdx++;
        frameDescIdx = 0;

        while(((frameDescLstIdx < LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS) &&
               (lookupEnvData->FrmDescLstLdfRelevance[frameDescLstIdx] == LIN_FALSE)))
        {
          frameDescLstIdx++;
        }

        if(frameDescLstIdx >= LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS)
        {
          break;
        }
      }
      else{}
    }

    retVal = LIN_TRUE;
  }
  else{}

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
LOOKUP_LAYER_SEQ_CODE Lin_Bool_t LinLookupImp_Initialization_Sequential(LinLookupIf_pGenericEnvData_t    genericLookupEnvData, LinLookupIf_EnvDataSze_t lookupEnvDataSze,
                                                                        LinLookupIf_pGenericImpCfgData_t genericLookupImpCfgData)
// PRQA S 3206 --
{
  Lin_Bool_t returnValue = FALSE;

  if((genericLookupEnvData != LIN_NULL) &&
		 (lookupEnvDataSze >= sizeof(loc_sSeqLookupEnvironmentData_t)))
  {
    /* Initialize lookup environment data */
    el_FillBytes(0x00, genericLookupEnvData, lookupEnvDataSze);

    returnValue = LIN_TRUE;
  }
  else{}

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
/* See justification at the file comment header */
// PRQA S 3206 ++
LOOKUP_LAYER_SEQ_CODE Lin_Bool_t LinLookupImp_GetSubInterface_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr)
// PRQA S 3206 --
{
  return(LIN_FALSE);
}

/***************************************************************************//**
 *
 ******************************************************************************/
LOOKUP_LAYER_SEQ_CODE Lin_Bool_t LinLookupImp_AssignFrameID_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinProtoIf_MsgID_t msgID, LinBusIf_FrameID_t frameID)
{
  Lin_Bool_t retVal = LIN_FALSE;

  if(genericLookupEnvData != LIN_NULL)
  {
    loc_pSeqLookupEnvironmentData_t lookupEnvData = (loc_pSeqLookupEnvironmentData_t) genericLookupEnvData;
    LinLookupIf_FrameIdx_t frameIdx = 0;
    Lin_uint8_t i;

    for(i = 0; i < LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS; i++)
    {
      Lin_uint8_t j;

      if(lookupEnvData->FrmDescLstLdfRelevance[i] == LIN_TRUE)
      {
        for(j = 0; j < lookupEnvData->FrmCnts[i]; j++)
        {
          if((lookupEnvData->FrameDescriptionLists[i])[j].MsgID == msgID)
          {
            retVal = LinLookupImp_AssignFrameIDRange_Sequential(genericLookupEnvData, frameIdx, &frameID, 1);
            break;
          }

          frameIdx++;
        }
      }
      else{}
    }
  }
  else{}

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
LOOKUP_LAYER_SEQ_CODE Lin_Bool_t LinLookupImp_AssignFrameIDRange_Sequential(LinLookupIf_pGenericEnvData_t         genericLookupEnvData, LinLookupIf_FrameIdx_t         frameIdx,
                                                                            LinLookupIf_cpAssignFrameIDRangeLst_t frameIdLst,           LinLookupIf_FrameIdLstLength_t frameIdLstLen)
{
  Lin_Bool_t returnValue = LIN_FALSE;
  
  if(genericLookupEnvData != LIN_NULL)
  {
    loc_pSeqLookupEnvironmentData_t lookupEnvData = (loc_pSeqLookupEnvironmentData_t) genericLookupEnvData;
    Lin_uint8_t un_assignments = 0;
    Lin_uint8_t i;

    /* Count un-/assignments */
    for(i = 0; i < frameIdLstLen; i++)
    {
      if(frameIdLst[i] != (LinBusIf_FrameID_t) LinLookupIf_AssignFrameIDRangeCmd_DONOTCARE)
      {
        un_assignments++;
      }
    }

    if(un_assignments <= lookupEnvData->TotalFrameCnt)
    {
      Lin_uint8_t frameDescLstIdx = LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS;
      Lin_uint8_t frameDescIdx = 0;

      /* Search for Frame Description List containing
       * the desired frame index to start from */
      for(i = 0; i < LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS; i++)
      {
        if((frameIdx < lookupEnvData->FrmCnts[i]) && (lookupEnvData->FrmDescLstLdfRelevance[i] == LIN_TRUE))
        {
          frameDescLstIdx = i;
          frameDescIdx = frameIdx;
          break;
        }

        if(lookupEnvData->FrmDescLstLdfRelevance[i] == LIN_TRUE)
        {
          frameIdx -= lookupEnvData->FrmCnts[i];
        }
      }

      if(loc_ExecuteFrameIdAssignment(lookupEnvData, frameDescLstIdx, frameDescIdx, frameIdLst, frameIdLstLen, LIN_TRUE) == LIN_TRUE)
      {
        returnValue = loc_ExecuteFrameIdAssignment(lookupEnvData, frameDescLstIdx, frameDescIdx, frameIdLst, frameIdLstLen, LIN_FALSE);
      }

    }
    else{}
  }

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
LOOKUP_LAYER_SEQ_CODE LinProtoIf_cpFrameDescription_t LinLookupImp_GetFrameDescription_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinBusIf_FrameID_t frameID)
{
  LinProtoIf_cpFrameDescription_t returnValue   = LIN_NULL;

  if(genericLookupEnvData != LIN_NULL)
  {
    loc_pSeqLookupEnvironmentData_t lookupEnvData = (loc_pSeqLookupEnvironmentData_t) genericLookupEnvData;

    if(frameID <= LIN_MAX_FRAMEID)
    {
      Lin_uint8_t tblidx = 0;      

      while ((tblidx < LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS) && (returnValue == LIN_NULL))
      {
        const Lin_uint8_t frameCnt = lookupEnvData->FrmCnts[tblidx];
        
        Lin_uint8_t idx = 0;
        
        while (idx < frameCnt )
        {
          if ( frameID == (lookupEnvData->AssignmentTables[tblidx])[idx] )
          {
            returnValue = &((lookupEnvData->FrameDescriptionLists[tblidx])[idx]);
            break;
          }
          else {}
          idx++;
        }
        tblidx++;
      }
    }
    else {}
  }
  else {}
  
  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
LOOKUP_LAYER_SEQ_CODE LinBusIf_cpFrameID_t LinLookupImp_GetFrameIdAssignment_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinLookupIf_cpGenericFrameDescLst_t genericFrmDscLst)
{
  LinBusIf_cpFrameID_t retVal = LIN_NULL;

  if(genericLookupEnvData != LIN_NULL)
  {
    loc_pSeqLookupEnvironmentData_t lookupEnvData = (loc_pSeqLookupEnvironmentData_t) genericLookupEnvData;

    Lin_uint8_t i;

    for(i = 0; i < LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS; i++)
    {
      if(lookupEnvData->FrameDescriptionLists[i] == genericFrmDscLst)
      {
        retVal = lookupEnvData->AssignmentTables[i];
        break;
      }
    }
  }

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
LOOKUP_LAYER_SEQ_CODE Lin_Bool_t LinLookupImp_AddFrameDescLst_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinLookupIf_cpGenericFrameDescLst_t      genericFrmDscLst,
                                                                         Lin_Bool_t                    ldfRelevance,         LinLookupIf_pGenericFrmDescLstEnvData_t genericFrmDescLstEnvData,
                                                                         LinLookupIf_EnvDataSze_t      frmDescLstEnvDataSze, LinLookupIf_pGenericCbCtxData_t         perFrmDescLstCbCtxData)
{
  Lin_Bool_t returnValue = LIN_FALSE;

  if((genericLookupEnvData != LIN_NULL) && (genericFrmDscLst != LIN_NULL))
  {
    loc_pSeqLookupEnvironmentData_t     lookupEnvData = (loc_pSeqLookupEnvironmentData_t) genericLookupEnvData;
    LinProtoIf_cpFrameDescription_t  frameDescLst  = (LinProtoIf_cpFrameDescription_t) genericFrmDscLst;

    Lin_uint8_t  i;

    /* Add pointer to Frame Description List if there is a free slot */
    for(i = 0; i < LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS; i++)
    {
      if(lookupEnvData->FrameDescriptionLists[i] == genericFrmDscLst)
      {
        break;
      }
      else if(lookupEnvData->FrameDescriptionLists[i] == LIN_NULL)
      {
        Lin_uint8_t frmCnt = 0;

        /* Count the number of frames and initialize lookup table*/
        while(frameDescLst[frmCnt].Kind != LinProtoIf_FrameKind_INVALID)
        {
          frmCnt++;
        }

        if(frmCnt <= frmDescLstEnvDataSze)
        {
          Lin_uint8_t j;
		  
          lookupEnvData->FrameDescriptionLists[i] = genericFrmDscLst;
          lookupEnvData->PerFrameDescLstCbCtxData[i] = perFrmDescLstCbCtxData;
          lookupEnvData->AssignmentTables[i] = genericFrmDescLstEnvData;
          lookupEnvData->FrmDescLstLdfRelevance[i] = ldfRelevance;

          for(j = 0; j < frmCnt; j++)
          {
            (lookupEnvData->AssignmentTables[i])[j] = frameDescLst[j].DefaultFrameID;
          }

          lookupEnvData->FrmCnts[i] = frmCnt;

          /* Add frame count to Total Frame Count */
          if(lookupEnvData->FrmDescLstLdfRelevance[i] == LIN_TRUE)
          {
            lookupEnvData->TotalFrameCnt += frmCnt;
          }
          returnValue = LIN_TRUE;
        }
        else{}

        break;
      }
      else{}
    }
  }
  else{}

  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
LOOKUP_LAYER_SEQ_CODE Lin_Bool_t LinLookupImp_RmvFrameDescLst_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinLookupIf_cpGenericFrameDescLst_t genericFrmDscLst)
{
  Lin_Bool_t                       returnValue = FALSE;
  
  if((genericLookupEnvData != LIN_NULL) && (genericFrmDscLst != LIN_NULL))
  {
    loc_pSeqLookupEnvironmentData_t     lookupEnvData = (loc_pSeqLookupEnvironmentData_t) genericLookupEnvData;

    Lin_uint8_t i;

    /* Search for pointer to Frame Description List and delete it */
    for(i = 0; i < LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS; i++)
    {
      if(lookupEnvData->FrameDescriptionLists[i] == genericFrmDscLst)
      {
        Lin_uint8_t j;

        /* Subtract frame count from Total Frame Count */
        if(lookupEnvData->FrmDescLstLdfRelevance[i] == LIN_TRUE)
        {
          lookupEnvData->TotalFrameCnt -= lookupEnvData->FrmCnts[i];
        }

        for(j = i; j < (LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS - 1u); j++)
        {
          lookupEnvData->FrameDescriptionLists[j] = lookupEnvData->FrameDescriptionLists[j + 1u];
          lookupEnvData->AssignmentTables[j] = lookupEnvData->AssignmentTables[j + 1u];
          lookupEnvData->FrmCnts[j] = lookupEnvData->FrmCnts[j + 1u];
          lookupEnvData->FrmDescLstLdfRelevance[j] = lookupEnvData->FrmDescLstLdfRelevance[j + 1u];
        }

        /* Delete pointer to Frame Description List and corresponding Frame Count */
        lookupEnvData->FrameDescriptionLists[(LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS - 1u)] = LIN_NULL;
        lookupEnvData->AssignmentTables[(LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS - 1u)] = LIN_NULL;
        lookupEnvData->FrmCnts[(LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS - 1u)] = 0u;
        lookupEnvData->FrmDescLstLdfRelevance[(LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS - 1u)] = LIN_FALSE;

        break;
      }
      else{}
    }
    returnValue = LIN_TRUE;
  }
  else{}
 
  return(returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
LOOKUP_LAYER_SEQ_CODE LinLookupIf_pGenericCbCtxData_t LinLookupImp_GetPerFrameDescLstCbCtxData_Sequential(LinLookupIf_pGenericEnvData_t genericLookupEnvData, LinLookupIf_cpGenericFrameDescLst_t frameDescLstAdr)
{
  LinLookupIf_pGenericCbCtxData_t retVal = LIN_NULL;
  Lin_uint8_t i;

  if(genericLookupEnvData != LIN_NULL)
  {
    loc_pSeqLookupEnvironmentData_t lookupEnvData = (loc_pSeqLookupEnvironmentData_t) genericLookupEnvData;

    for(i = 0; i < LINLOOKUP_INTERFACE_MAX_FRAME_DESCIPTION_LISTS; i++)
    {
      if(lookupEnvData->FrameDescriptionLists[i] == frameDescLstAdr)
      {
        retVal = lookupEnvData->PerFrameDescLstCbCtxData[i];
        break;
      }
    }
  }

  return(retVal);
}

