/**
 * @ingroup FOC
 *
 * @{
 */

/**********************************************************************/
/*! @file foc.c
 * @brief computation of the FOC algorithm
 *
 * @author       JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <foc.h>
#include <motor_ctrl.h>
#include <adc.h>
#include <adc_lists.h>
#include <math_utils.h>
#include <pwm_ctrl.h>
#include <defines.h>
#include <stdlib.h>
#include <string.h>
#include <speed_meas.h>
#include <systime.h>
#include <defines.h>

#if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
 #include <uart.h>
#endif

/* -------------------
   local defines
   ------------------- */

typedef struct
{
  int16_t * i1;
  int16_t * i2;
  int16_t * v_low;
  int16_t * v_mid;
  int16_t * v_max;
} foc_ibat_calc_data_lookup_t;

/** @var foc_ctrl_loop_data
 * @brief struct that holds all parameters, flags and process values of module FOC */
static foc_ctrl_loop_data_t foc_ctrl_loop_data;

/** @brief get a pointer towards the top-level structure of this module
 *
 * @details use this function with utmost care! generally, the element variables SHOULD NOT be written from outside this module
 */
INLINE const foc_ctrl_loop_data_t * foc_get_data( void )
{
  return &foc_ctrl_loop_data;
}

/** @brief set-function for the phase current i_u */
INLINE void foc_set_i_u( const int16_t i_u )
{
  foc_ctrl_loop_data.i_u = i_u;
}

/** @brief get-function for the phase current i_u */
INLINE int16_t foc_get_i_u( void )
{
  return foc_ctrl_loop_data.i_u;
}

/** @brief set-function for the phase current i_v */
INLINE void foc_set_i_v( const int16_t i_v )
{
  foc_ctrl_loop_data.i_v = i_v;
}

/** @brief get-function for the phase current i_v */
INLINE int16_t foc_get_i_v( void )
{
  return foc_ctrl_loop_data.i_v;
}

/** @brief set-function for the phase current i_w */
INLINE void foc_set_i_w( const int16_t i_w )
{
  foc_ctrl_loop_data.i_w = i_w;
}

/** @brief get-function for the phase current i_w */
INLINE int16_t foc_get_i_w( void )
{
  return foc_ctrl_loop_data.i_w;
}

/** @brief set-function for the phase voltage v_u */
INLINE void foc_set_v_u( const int16_t v_u )
{
  foc_ctrl_loop_data.v_u = v_u;
}

/** @brief get-function for the phase voltage v_u */
INLINE int16_t foc_get_v_u( void )
{
  return foc_ctrl_loop_data.v_u;
}

/** @brief set-function for the phase voltage v_v */
INLINE void foc_set_v_v( const int16_t v_v )
{
  foc_ctrl_loop_data.v_v = v_v;
}

/** @brief get-function for the phase voltage v_v */
INLINE int16_t foc_get_v_v( void )
{
  return foc_ctrl_loop_data.v_v;
}

/** @brief set-function for the phase voltage v_w */
INLINE void foc_set_v_w( const int16_t v_w )
{
  foc_ctrl_loop_data.v_w = v_w;
}

/** @brief get-function for the phase voltage v_w */
INLINE int16_t foc_get_v_w( void )
{
  return foc_ctrl_loop_data.v_w;
}

/** @brief set-function for the rotor angle during open loop commutation */
INLINE void foc_set_rotor_angle_startup ( uint16_t val )
{
  foc_ctrl_loop_data.rotor_angle_startup = val;
}

/** @brief set-function for target value of i_q */
INLINE void foc_set_i_q_ref ( int16_t val )
{
  foc_ctrl_loop_data.i_q_ref = val;
}

/** @brief set-function for target value of i_d */
INLINE void foc_set_i_d_ref ( int16_t val )
{
  foc_ctrl_loop_data.i_d_ref = val;
}

/** @brief get-function for target value of i_q */
INLINE int16_t foc_get_i_q_ref ( void )
{
  return foc_ctrl_loop_data.i_q_ref;
}

/** @brief get-function for target value of i_d */
int16_t foc_get_i_d_ref ( void )
{
  return foc_ctrl_loop_data.i_d_ref;
}

/** @brief set-function for value of VBAT (input is ADC value; stored value is [V/256]) */
INLINE void foc_set_supply_voltage_unsafe ( int16_t val )
{
  /* internal voltage scale: voltage[volts] * 256, so a shift left for 8 positions */
  /* the argument is the ADC value hence the computation is (val * voltage_conversion_factor) << 8; NOTE: since "voltage_conversion_factor" already contains a scaling by 16 shifts to the left (because the actual */
  /* value is <<< 1), the MUL result is shifted right by 8, yielding an overall shift left of 8 */
  math_mulS16_unsafe( val, foc_ctrl_loop_data.voltage_conversion_factor );
  math_mul_shiftright_result( 8u );
  foc_ctrl_loop_data.vbat = (int16_t) math_mul_get_result( False );
}

/** @brief get-function for value of VBAT */
INLINE int16_t foc_get_supply_voltage( void )
{
  return foc_ctrl_loop_data.vbat;
}

/** @brief set-function for mode of rotor angle computation (hall/sensorless) */
INLINE void foc_set_rotor_angle_meas_mode ( foc_sens_meas_mode_t meas_mode )
{
  foc_ctrl_loop_data.rotor_angle_meas_mode = meas_mode;
}

/** @brief get-function for mode of rotor angle computation (hall/sensorless) */
INLINE foc_sens_meas_mode_t foc_get_rotor_angle_meas_mode ( void )
{
  return foc_ctrl_loop_data.rotor_angle_meas_mode;
}

/** @brief get-function for value of i_d */
INLINE int16_t foc_get_current_i_d ( void )
{
  return foc_ctrl_loop_data.i_d;
}

/** @brief get-function for value of i_q */
INLINE int16_t foc_get_current_i_q ( void )
{
  return foc_ctrl_loop_data.i_q;
}

void foc_set_stall_detect_min_rotor_speed(uint16_t stall_detect_min_rotor_speed)
{
    foc_ctrl_loop_data.stall_det_min_rotor_speed = stall_detect_min_rotor_speed;
}

void foc_set_motor_constant(uint16_t param_motor_constant)
{
    foc_ctrl_loop_data.param_motor_constant = param_motor_constant;
}

/** @brief hold the two latest values of v_alpha and v_beta in memory
 *
 * details
 * The two latest values of v_alpha/beta are needed by the Luenberger observer, so they are only needed <br>
 * when operating in sensorless mode. Since 'foc_ctrl_loop_data.v_alpha' has the unit of PWM ticks and <br>
 * the observer needs them in [V/256], the necessary conversion is also done here.
 */
INLINE static void foc_update_space_vector_history ( foc_ctrl_loop_data_t * foc_data )
{
  /* computation v_alpha, v_beta: foc_value / pwm_cnt_max * supply_voltage (because v_u, v_v and v_w are directly used as thresholds, otherwise there would be a coefficient) */
  math_mulS16_unsafe( foc_data->v_alpha, foc_data->vbat );
  math_unblockedDivS32_unsafe((s32) math_mul_get_result( True ), (int16_t) ( PWM_PERIOD_DURATION_TICKS ));
  foc_data->v_alpha_history[ 1 ] = foc_data->v_alpha_history[ 0 ];
  foc_data->v_alpha_history[ 0 ] = (int16_t) math_unblockedDivReadResult_unsafe( False );

  math_mulS16_unsafe( foc_data->v_beta, foc_data->vbat );
  math_unblockedDivS32_unsafe((s32) math_mul_get_result( True ), (int16_t) ( PWM_PERIOD_DURATION_TICKS ));
  foc_data->v_beta_history[ 1 ] = foc_data->v_beta_history[ 0 ];
  foc_data->v_beta_history[ 0 ] = (int16_t) math_unblockedDivReadResult_unsafe( False );
}

/** @brief clear the process data of the low-pass filter (see typedef foc_low_pass_filter_t) */
INLINE static void foc_low_pass_filter_reset ( foc_low_pass_filter_t * lpf )
{
  lpf->K           = (int16_t) ( SQRT2 * 16384.0 );
  lpf->K_shift_val = 14;

  lpf->output = 0;
}

/** @brief iterate the low-pass filter (see typedef foc_low_pass_filter_t) */
INLINE static int16_t foc_low_pass_filter_calc_unsafe ( foc_low_pass_filter_t * lpf, const int16_t input, const int16_t T )
{
  /* output = T * ((K * input) - output) + output */
  /* whereas T = 1 / ((tau / dt) + 1), 'tau' being the time constant of the filter and 'dt' the time between two iterations of the filter */
  /* this can be simplified to T = dt/tau under the condition that tau >>> dt */
  math_mulS16_unsafe( lpf->K, input );
  math_mul_shiftright_result((uint16_t) lpf->K_shift_val );

  math_mulS16_unsafe( T, (int16_t) math_mul_get_result( False ) - (int16_t) HIWORD((u32) lpf->output ));

  lpf->output += math_mul_get_result( True );

  return (int16_t) HIWORD((u32) lpf->output );
}

/* vector transformation functionalities ************************************** */

/** @brief straight-forward implementation of the Clarke transformation */
INLINE static void foc_clarke ( foc_ctrl_loop_data_t * foc_data )
{
  foc_data->i_alpha = foc_data->i_u;

  math_mulS16_unsafe( foc_data->i_u + SHL_S16( foc_data->i_v, 1u ), (int16_t) (( 1.0 / SQRT3 ) * 32768.0 ));
  math_mul_shiftright_result( 15u );
  foc_data->i_beta = (int16_t) math_mul_get_result( False );
}

/** @brief straight-forward implementation of the Park transformation */
INLINE static void foc_park ( foc_ctrl_loop_data_t * foc_data )
{
  math_mulS16_unsafe( foc_data->i_alpha, foc_data->cosine_of_current_rotor_angle );
  math_mulS16_acc_unsafe( foc_data->i_beta, foc_data->sine_of_current_rotor_angle );
  math_mul_shiftright_result( 15u );
  foc_data->i_d = (int16_t) math_mul_get_result( False );

  math_mulS16_unsafe( foc_data->i_beta, foc_data->cosine_of_current_rotor_angle );
  math_nmulS16_acc_unsafe( foc_data->i_alpha, foc_data->sine_of_current_rotor_angle );
  math_mul_shiftright_result( 15u );
  foc_data->i_q = (int16_t) math_mul_get_result( False );
}

/** @brief straight-forward implementation of the inverse Park transformation */
INLINE static void foc_park_inv ( foc_ctrl_loop_data_t * foc_data )
{
  math_mulS16_unsafe( foc_data->v_d, foc_data->cosine_of_current_rotor_angle );
  math_nmulS16_acc_unsafe( foc_data->v_q, foc_data->sine_of_current_rotor_angle );
  math_mul_shiftright_result( 15u );
  foc_data->v_alpha = (int16_t) math_mul_get_result( False );

  math_mulS16_unsafe( foc_data->v_d, foc_data->sine_of_current_rotor_angle );
  math_mulS16_acc_unsafe( foc_data->v_q, foc_data->cosine_of_current_rotor_angle );
  math_mul_shiftright_result( 15u );
  foc_data->v_beta = (int16_t) math_mul_get_result( False );
}

#if ( FOC_USE_SVM == 1 )
/** @brief straight-forward implementation of the SVM (from alpha/beta space to v_u, v_v and v_w) */
  INLINE static void foc_svm ( foc_ctrl_loop_data_t * foc_data )
  {
    foc_data->svm_phi = math_get_angle_unsafe( foc_data->v_beta, foc_data->v_alpha );

    if( abs( foc_data->v_alpha ) > abs( foc_data->v_beta ))
    {
      /* length:= alpha / cos(phi) */
      foc_data->svm_length = (int16_t) math_divS32_unsafe(((s32) foc_data->v_alpha ) << 15, math_cosine_unsafe( foc_data->svm_phi ), False );
    }
    else
    {
      /* length:= beta / sin(phi) */
      foc_data->svm_length = (int16_t) math_divS32_unsafe(((s32) foc_data->v_beta ) << 15, math_sine_unsafe( foc_data->svm_phi ), False );
    }

    math_mulS16_unsafe( math_svm_waveform_unsafe( foc_data->svm_phi + (uint16_t) ( MATH_ANGLE_MAX / 4u )), foc_data->svm_length );
    math_mul_shiftright_result( 15u );
    foc_data->v_u = ( int16_t ) math_mul_get_result( False );

    math_mulS16_unsafe( math_svm_waveform_unsafe(( foc_data->svm_phi + (uint16_t) ( MATH_ANGLE_MAX / 4u )) - (uint16_t) ( MATH_ANGLE_MAX / 3u )), foc_data->svm_length );
    math_mul_shiftright_result( 15u );
    foc_data->v_v = ( int16_t ) math_mul_get_result( False );

    math_mulS16_unsafe( math_svm_waveform_unsafe(( foc_data->svm_phi + (uint16_t) ( MATH_ANGLE_MAX / 4u )) + (uint16_t) ( MATH_ANGLE_MAX / 3u )), foc_data->svm_length );
    math_mul_shiftright_result( 15u );
    foc_data->v_w = ( int16_t ) math_mul_get_result( False );
  }
#endif

#if ( FOC_USE_SVM != 1 )
/** @brief straight-forward implementation of the inverse Clarke transformation */
  INLINE static void foc_clarke_inv ( foc_ctrl_loop_data_t * foc_data )
  {
    foc_data->v_u = foc_data->v_alpha;

    math_mulS16_unsafe( foc_data->v_beta, (int16_t) ( SQRT3 * 16384.0 )); /* second operand is sqrt(3)/2 -> thus multiply by 2^14 and right-shift by 15 (i.e. divide by 2^15) */
    math_mul_shiftright_result( 15u );

    int16_t loc_buf = (int16_t) math_mul_get_result( False );                 /* debug: overflow possible @ |v_beta| > 32687/sqrt(3) */
    foc_data->v_v = ( int16_t ) - SHR_S16( foc_data->v_alpha, 1u ) + loc_buf;
    foc_data->v_w = ( int16_t ) - SHR_S16( foc_data->v_alpha, 1u ) - loc_buf;
  }
#endif

/** @brief todo
 *
 * @details
 * todo<br>
 */
#if ( FOC_EN_VBAT_COMPENSATION == 1 )
  INLINE static void foc_compensate_vbat ( foc_ctrl_loop_data_t * foc_data )
  {
    /* scaler = 1024 => 10 bits, i.e. the calculation is valid as long as the ratio of nominal to momentary VBAT is smaller than 64; otherwise overflow of loc_comp_coeff */
    int16_t loc_comp_coeff = (int16_t) math_divS32_unsafe((s32) ((s64) FOC_VBAT_NOMINAL_MV * INT64_C( 256 ) * INT64_C( 1024 ) / INT64_C( 1000 )), foc_data->vbat, False );

    math_mulS16_unsafe( loc_comp_coeff, foc_data->v_d );
    math_mul_shiftright_result( 10u );
    foc_data->v_d = (int16_t) math_mul_get_result( False );

    math_mulS16_unsafe( loc_comp_coeff, foc_data->v_q );
    math_mul_shiftright_result( 10u );
    foc_data->v_q = (int16_t) math_mul_get_result( False );
  }
#endif

/** @brief generation of the angle for the FOC calculation during open loop operation
 *
 * @details
 * When the rotor is accelerated from standstill to the point where sensorless operation <br>
 * is possible, the FOC is calculated with an "impressed" angle in order to produce the <br>
 * speed ramp. This function generates said angle.
 */
INLINE static void foc_generate_rotor_angle_open_loop ( foc_ctrl_loop_data_t * foc_data )
{
  if( MC_RAMP_SPEED == mc_get_motor_state())
  {
    if( mc_get_direction() == FORWARD )
    {
      foc_data->rotor_angle_startup += mc_get_startup_angle_incr();
    }
    else
    {
      foc_data->rotor_angle_startup -= mc_get_startup_angle_incr();
    }
  }
}

/** @brief implementation of the detection of a stalled rotor via FOC process data
 *
 * @details
 * The general idea is this: at rotor angle=0, the estimated BEMF value e_alpha_filtered should be at BEMF_amplitude/sqrt(2) (because it lags e_alpha by <br>
 * 45 degrees electrical), no matter what direction the rotor is rotating. This value is monitored with the help of two user parameters (motor constant and detection tolerance): <br>
 * from the rotor speed, the expected BEMF amplitude is calculated and compared with the estimated value. If the difference is greater than <br>
 * the user-defined tolerance, a stalled rotor is assumed.<br>
 * Note: the tolerance is given in % (deviation from the estimated BEMF value), whereas a value of 2^8 = 256 represents 100%. So, as an example, <br>
 * a value of 100 represents 39%(=100/256) of allowed deviation. Values greater than 256 are possible.
 */
INLINE static void foc_check_for_stall_condition( foc_ctrl_loop_data_t * foc_data )
{
  math_mulU16_unsafe( spdmeas_get_cur_speed(), foc_data->param_motor_constant );
  math_mul_shiftright_result( 8u );

  foc_data->stall_det_bemf_amplitude_ref = (int16_t) math_mul_get_result( False );

  math_mulS16_unsafe( foc_data->stall_det_bemf_amplitude_ref, foc_data->stall_det_thrshld );
  math_mul_shiftright_result( 8u );

  int16_t loc_tolerance = (int16_t) math_mul_get_result( False );

  math_mulS16_unsafe( foc_data->luenberger_data.e_alpha_model_filtered, (int16_t) ( SQRT2 * 256.0 ));
  math_mul_shiftright_result( 8u );
  foc_data->stall_det_bemf_amplitude_current = (int16_t) math_mul_get_result( False );

  int16_t loc_diff = foc_data->stall_det_bemf_amplitude_current - foc_data->stall_det_bemf_amplitude_ref;
  loc_diff = ( loc_diff < 0 ) ? -( loc_diff ) : loc_diff;

  if(( spdmeas_get_cur_speed() > foc_data->stall_det_min_rotor_speed ) &&
     ( foc_ctrl_loop_data.rotor_angle_open_to_closed_loop_error < foc_ctrl_loop_data.open_to_closed_loop_smoothing_coeff ) &&
     ( loc_diff > loc_tolerance ))
  {
    mc_stalled_rotor_condition_handler();
  }
}

/** @brief clear luenberger process data */
INLINE static void foc_luenberger_reset_data( void )
{
  foc_ctrl_loop_data.luenberger_data.i_alpha_model = 0;
  foc_ctrl_loop_data.luenberger_data.i_beta_model  = 0;

  foc_ctrl_loop_data.luenberger_data.e_alpha_model_raw = 0;
  foc_ctrl_loop_data.luenberger_data.e_beta_model_raw  = 0;

  foc_ctrl_loop_data.luenberger_data.diff_i_alpha_last = 0;
  foc_ctrl_loop_data.luenberger_data.diff_i_beta_last  = 0;

  foc_ctrl_loop_data.luenberger_data.e_alpha_model_filtered = 0;
  foc_ctrl_loop_data.luenberger_data.e_beta_model_filtered  = 0;

  foc_low_pass_filter_reset( &( foc_ctrl_loop_data.luenberger_data.lpf_e_alpha ));
  foc_low_pass_filter_reset( &( foc_ctrl_loop_data.luenberger_data.lpf_e_beta ));
}

/** @brief implementation of the BEMF estimator (i.e. Luenberger observer)
 *
 * @details
 * The BEMF estimator is the most fundamental part of the sensorless FOC. Basically, the first-order model <br>
 * of an electrical leg of the motor is constantly computed and compensated against a measured unit (i.e the current). <br>
 * This first-order model includes the voltage at the motor phase connection as well as its phase current, its winding resistance, <br>
 * phase inductance and the BEMF. The whole thing is calculated in alpha/beta space and basically solves Kirchhoff's Law... <br>
 * So, the equation is: phase_voltage = R*phase_current + L*d(phase_current)/dt + BEMF . <br>
 * Transformation into the digitial domain (time/value-discrete) and reordering to yield i_phase(n+1) results in: <br>
 * i_phase(n+1) = (1 - R / (L * PWM_FREQ)) * i_phase(n) + 1 / L * (phase_voltage(n) - BEMF(n)) <br>
 * This model is compensated against the measured currents (i_model - i_real) through a PD controller whose output is BEMF(n). <br>
 * Since BEMF(n) (i.e. e_alpha/beta_raw) is way too noisy to use it for the calculation of the rotor angle, they are filtered through a <br>
 * first-order low-pass filter. The corner frequency of the LPF is always kept at the speed of the rotor with an amplification of K = sqrt(2), yielding <br>
 * an output signal (e_alpha/beta_filtered) having a phase lag of 45 degrees electrical and the same amplitude compared to its input. <br>
 * From the estimated BEMF in alpha/beta space, the rotor angle can easily be computed by arctan(). <br>
 * Obviously, differences between L_d and L_q can not be cared for when using this approach.
 */
INLINE static void foc_luenberger_observer_calc ( foc_ctrl_loop_data_t * foc_data )
{
  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_LUENBERGER_CALC_DURATION )
    dbg_pin3_set();
  #endif

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_LUENBERGER_CALC_OVERFLOW )
    int16_t _result_h;
  #endif

  /* convert i_alpha, i_beta to ampere-scale, imperative: numerically, the scale for I and U must be the same, because they are used in one equation */
  /* then compute the current that is to be expected by the model (i_alpha_model_new, i_beta_model_new) */

  /* internal voltage scale: current[amperes] * 256, so a shift left for 8 positions */
  /* the argument is the ADC value hence the computation is  (val * current_conversion_factor) << 8; NOTE: since "current_conversion_factor" already contains a scaling by 16 shifts to the left (because the actual */
  /* value is <<< 1), the MUL result is shifted right by 8, yielding an overall shift left of 8 */
  math_mulS16_unsafe((int16_t) ( foc_data->current_conversion_factor ), foc_data->i_alpha );
  math_mul_shiftright_result( 8u );

  foc_data->luenberger_data.i_alpha_real = (int16_t) math_mul_get_result( False );

  /* motor_model_coeff_a and motor_model_coeff_b contain a factor (left shift by 16) because their actual values are between 0 and 1... */
  /* this factor is compensated at the end (by the one-cycle shift functionality of MULT module) */
  math_mulS16_unsafe ( foc_data->luenberger_data.motor_model_coeff_a, foc_data->luenberger_data.i_alpha_model );
  math_mulS16_acc_unsafe( foc_data->luenberger_data.motor_model_coeff_b, foc_data->v_alpha_history[ 1 ] );
  math_nmulS16_acc_unsafe( foc_data->luenberger_data.motor_model_coeff_b, foc_data->luenberger_data.e_alpha_model_raw );

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_LUENBERGER_CALC_OVERFLOW )

    math_mul_shiftright_result( 1u );

    _result_h = (int16_t) HIWORD((u32) math_mul_get_result( True ));

    /* ((( S16_MAX+1 ) * 256 ) >> 16 ) = 128 */
    if(( _result_h >= 128 ) || ( _result_h < -128 ))
    {
      dbg_pin3_set();
      __no_operation();
      dbg_pin3_clear();
    }

    math_mul_shiftright_result((uint16_t) ( foc_data->luenberger_data.motor_model_coeff_scaler - 1 ));
  #else
    math_mul_shiftright_result( foc_data->luenberger_data.motor_model_coeff_scaler );
  #endif

  int16_t i_alpha_model_new = (int16_t) math_mul_get_result( False );

  math_mulS16_unsafe((int16_t) ( foc_data->current_conversion_factor ), foc_data->i_beta );
  math_mul_shiftright_result( 8u );

  foc_data->luenberger_data.i_beta_real = (int16_t) math_mul_get_result( False );

  math_mulS16_unsafe( foc_data->luenberger_data.motor_model_coeff_a, foc_data->luenberger_data.i_beta_model );
  math_mulS16_acc_unsafe( foc_data->luenberger_data.motor_model_coeff_b, foc_data->v_beta_history[ 1 ] );
  math_nmulS16_acc_unsafe( foc_data->luenberger_data.motor_model_coeff_b, foc_data->luenberger_data.e_beta_model_raw );

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_LUENBERGER_CALC_OVERFLOW )

    math_mul_shiftright_result( 1u );

    _result_h = (int16_t) HIWORD( math_mul_get_result( True ));

    /* (( 32768 * 256 ) >> 16 ) = 128 */
    if(( _result_h >= 128 ) || ( _result_h < -128 ))
    {
      dbg_pin3_set();
      __no_operation();
      dbg_pin3_clear();
    }

    math_mul_shiftright_result((uint16_t) ( foc_data->luenberger_data.motor_model_coeff_scaler - 1 ));
  #else
    math_mul_shiftright_result( foc_data->luenberger_data.motor_model_coeff_scaler );
  #endif

  int16_t i_beta_model_new = (int16_t) math_mul_get_result( False );

  /* compensate the model by comparing the expected values to the measured ones */
  foc_data->luenberger_data.diff_i_alpha = foc_data->luenberger_data.i_alpha_real - i_alpha_model_new;
  foc_data->luenberger_data.diff_i_beta  = foc_data->luenberger_data.i_beta_real - i_beta_model_new;

  /* compensator for current model of the motor */
  /* alpha-component: P-controller */
  math_mulS16_unsafe( foc_data->luenberger_data.diff_i_alpha, foc_data->luenberger_data.current_comp_Kp );
  /* alpha-component: D-controller */
  math_mulS16_acc_unsafe( foc_data->luenberger_data.diff_i_alpha - foc_data->luenberger_data.diff_i_alpha_last, foc_data->luenberger_data.current_comp_Kd );

  math_mul_shiftright_result( foc_data->luenberger_data.current_comp_scaler );
  foc_data->luenberger_data.corr_alpha = (int16_t) math_mul_get_result( False );

  /* beta-component: P-controller */
  math_mulS16_unsafe( foc_data->luenberger_data.diff_i_beta, foc_data->luenberger_data.current_comp_Kp );
  /* beta-component: D-controller */
  math_mulS16_acc_unsafe( foc_data->luenberger_data.diff_i_beta - foc_data->luenberger_data.diff_i_beta_last, foc_data->luenberger_data.current_comp_Kd );

  math_mul_shiftright_result( foc_data->luenberger_data.current_comp_scaler );
  foc_data->luenberger_data.corr_beta = (int16_t) math_mul_get_result( False );

  /* set newly estimated BEMF values */
  foc_data->luenberger_data.e_alpha_model_raw -= foc_data->luenberger_data.corr_alpha;
  foc_data->luenberger_data.e_beta_model_raw  -= foc_data->luenberger_data.corr_beta;

  /* calculate amplification factor T for the LPF for e_alpha/e_beta in a way that the filter produces a phase lag of exactly 45 degrees (-> T = speed[eRPM] * 0.32) = (speed[eRPM] * 82) >> 8 */
  math_mulU16_unsafe( spdmeas_get_cur_speed(), 82u );
  math_mul_shiftright_result( 8u );

  foc_data->luenberger_data.lpf_gain_T = (int16_t) math_mul_get_result( False );

  /* low-pass filtering of e_alpha, e_beta */
  foc_data->luenberger_data.e_alpha_model_filtered = foc_low_pass_filter_calc_unsafe( &( foc_data->luenberger_data.lpf_e_alpha ), foc_data->luenberger_data.e_alpha_model_raw, foc_data->luenberger_data.lpf_gain_T );
  foc_data->luenberger_data.e_beta_model_filtered  = foc_low_pass_filter_calc_unsafe( &( foc_data->luenberger_data.lpf_e_beta ), foc_data->luenberger_data.e_beta_model_raw, foc_data->luenberger_data.lpf_gain_T );

  /* compute new rotor angle from estimated (and filtered) BEMF components */
  foc_data->luenberger_data.rotor_angle_estimated = math_get_angle_unsafe( foc_data->luenberger_data.e_beta_model_filtered, foc_data->luenberger_data.e_alpha_model_filtered );

  if( mc_get_direction() == FORWARD )
  {
    foc_data->luenberger_data.rotor_angle_estimated -= (uint16_t) ( MATH_ANGLE_MAX / 4u ) - (uint16_t) ( MATH_ANGLE_MAX / 8u );
  }
  else
  {
    foc_data->luenberger_data.rotor_angle_estimated += (uint16_t) ( MATH_ANGLE_MAX / 4u ) - (uint16_t) ( MATH_ANGLE_MAX / 8u );
  }

  /* remember i_alpha/_beta and compensator error for the next iteration of this model */
  foc_data->luenberger_data.i_alpha_model     = i_alpha_model_new;
  foc_data->luenberger_data.i_beta_model      = i_beta_model_new;
  foc_data->luenberger_data.diff_i_alpha_last = foc_data->luenberger_data.diff_i_alpha;
  foc_data->luenberger_data.diff_i_beta_last  = foc_data->luenberger_data.diff_i_beta;

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_ROTOR_ANGLE_TICK )
    /* a little tricky: the literal number is the amount of ticks we want to have per electrical revolution, the following if-clause sets a confidence interval via a threshold of the remainder (cause the exakt value will seldomly occur ...) */
    math_unblockedDivS32_unsafe((s32) foc_data->rotor_angle_estimated, (int16_t) ( MATH_ANGLE_MAX / 4 ));

    if( REG_DIVIDER_REMAINDER < (uint16_t) 1000 )
    {
      dbg_pin3_set();
    }
    else
    {
      dbg_pin3_clear();
    }
  #endif

  #if ( DBG_PIN3_FUNCTION == DBG_ESTIMATED_ROTOR_ANGLE_IS_ZERO )
    /* attention, this is not precise!!! */
    if( foc_data->luenberger_data.rotor_angle_estimated < (uint16_t) ( MATH_ANGLE_MAX / 36u ))
    {
      dbg_pin3_set();
    }
    else
    {
      dbg_pin3_clear();
    }
  #endif

  #if ( DBG_PIN3_FUNCTION == DBG_SHOW_LUENBERGER_CALC_DURATION )
    dbg_pin3_clear();
  #endif
}

/** @brief retrieve current rotor angle from hall sensor interface
 *
 * @details
 * For now, we just read the angle from the hall module. If needbe, one could also smoothen the signal here.
 */
INLINE static uint16_t foc_get_hall_sens_angle( void )
{
  #if ( FOC_EN_HALL_SENS == 1 )
    uint16_t loc_return_value = hall_get_angle_interpolated();

    #if ( FOC_HALL_SIGNAL_SMOOTHING == 1 )

      static u8 loc_last_sensor_state = 0u;
      static uint16_t loc_last_angle       = 0u;
      static uint16_t loc_last_angle_incr  = 0u;
      static int16_t loc_error_accu       = 0;

      static uint16_t loc_timestamp_last_sensor_edge = 0u;

      if( hall_interpolation_valid())
      {
        if( hall_get_sensor_state() != loc_last_sensor_state )
        {
          /* when a hall sensor edge occurred, store the error between the expected angle and the actual angle */
          if( hall_get_cur_direction() > 0 )
          {
            loc_error_accu = (int16_t) (uint16_t) (( loc_last_angle + loc_last_angle_incr ) - hall_get_angle_interpolated());
          }
          else
          {
            loc_error_accu = (int16_t) (uint16_t) (( loc_last_angle - loc_last_angle_incr ) - hall_get_angle_interpolated());
          }

          loc_timestamp_last_sensor_edge = systime_getU16();
        }
        else
        {
          /* otherwise, dissipate the error in due time (=> the faster the rotor speed, the fewer iterations between two hall sensor edges, the
           * faster the error has to be dissipated) */
          loc_error_accu -= (int16_t) math_divS32_unsafe((s32) loc_error_accu, (int16_t) (uint16_t) ( hall_get_last_sensor_state_duration() >> 1u ), False );
        }

        #if ( HALL_CCT_MODE == HALL_CCT_DISABLED )
          loc_return_value += (uint16_t) loc_error_accu;
        #elif (( HALL_CCT_MODE == HALL_CCT_ENABLED_STD ) || ( HALL_CCT_MODE == HALL_CCT_ENABLED_W_BLANKING_TIME ))
          /* disable this filter in case of hall highspeed mode */
          if( !( hall_highspeed_mode_active()))
          {
            loc_return_value += (uint16_t) loc_error_accu;
          }
        #else
        #error Unknown HALL_CCT_MODE
        #endif
      }

      /* if there's no sensor edge for a long time, the interpolated angle is faulty -> use the raw one
       * this has obviously to be tuned to the maximum rotor speed gradient that is possible within the application */
      if(( systime_getU16() - loc_timestamp_last_sensor_edge ) > (uint16_t) ( hall_get_last_sensor_state_duration() << 1u ))
      {
        loc_return_value = hall_get_angle_raw();
      }

      loc_last_sensor_state = hall_get_sensor_state();
      loc_last_angle        = hall_get_angle_interpolated();
      loc_last_angle_incr   = hall_get_angle_incr_per_period();
    #endif

    /* if the rotor's speed is too low, the interpolated angle is unreliable -> use the raw one */
    if( hall_get_cur_speed() < (uint16_t) hall_get_min_speed_interpolation())
    {
      loc_return_value = hall_get_angle_raw();
    }

    return loc_return_value;
  #else
    return 0u;
  #endif
}

/** @brief implementation of the actual, mathematical FOC algorithm
 *
 * @details
 * This implementation is actually quite straight-forward since the heavy lifting is done inside mc_isr_handler(), like retrieving and evaluating ADC data etc. <br>
 * This function computes the FOC algorithm in an abstract way, without knowing about single/dual shunt operation, for instance. Yet there is <br>
 * an exception to that rule: the voltages (like v_d and v_q) have the unit [PWM ticks], hence they are not independet from the PWMN module settings. <br>
 * So, this is what's happening here in chronological order: <br>
 * - update of the history of v_alpha/v_beta <br>
 * - transformation of phase currents into alpha/beta space <br>
 * - computing the current angle of hall interface/BEMF estimator (if enabled) <br>
 * - selecting the source of the angle that is used for calculating the FOC during this particular iteration (open/closed loop, for instance) <br>
 * - transformation of currents into d/q space <br>
 * - computing an iteration of the two PI controllers (output is v_d/v_q) <br>
 * - transformation of v_d and v_q back to stator-oriented, three-phase space (UVW) <br>
 * - at angle = 0, execute check for stalled rotor (and set trigger for UART data logger) <br>
 * <br>
 * Note: there's loads of inline documentation in the source code. Read it.
 */
INLINE void foc_full_foc_calc( void )
{
  static uint16_t loc_last_rotor_angle = 0u;

  foc_update_space_vector_history( &foc_ctrl_loop_data );

  /* the following lines implement the transformation of the phase currents to d-q space, including
   * estimating/measuring/interpolating the current rotor angle
   */
  foc_clarke( &foc_ctrl_loop_data );

  #if ( FOC_EN_HALL_SENS == 1 )
    hall_calc_rotor_angle_unsafe();
  #endif

  #if ( FOC_EN_OBSERVER == 1 )
    foc_luenberger_observer_calc( &foc_ctrl_loop_data );
  #endif

  if( FOC_SENSORLESS == foc_ctrl_loop_data.rotor_angle_meas_mode )
  {
    foc_ctrl_loop_data.current_rotor_angle = foc_ctrl_loop_data.luenberger_data.rotor_angle_estimated;

    if( MC_OPEN_LOOP == mc_get_comm_mode())
    {
      foc_generate_rotor_angle_open_loop( &foc_ctrl_loop_data );

      /* save angle_error -> angle computed by luenberger observer vs. impressed angle during startup */
      if( MC_RAMP_SPEED == mc_get_motor_state())
      {
        if( mc_get_direction() == FORWARD )
        {
          foc_ctrl_loop_data.rotor_angle_open_to_closed_loop_error = foc_ctrl_loop_data.current_rotor_angle - foc_ctrl_loop_data.rotor_angle_startup;
        }
        else
        {
          foc_ctrl_loop_data.rotor_angle_open_to_closed_loop_error = foc_ctrl_loop_data.rotor_angle_startup - foc_ctrl_loop_data.current_rotor_angle;
        }
      }

      foc_ctrl_loop_data.current_rotor_angle = foc_ctrl_loop_data.rotor_angle_startup;
    }
    /* dissipate angle_error from transition open loop to closed loop smoothly */
    else if( foc_ctrl_loop_data.rotor_angle_open_to_closed_loop_error >= foc_ctrl_loop_data.open_to_closed_loop_smoothing_coeff )
    {
      foc_ctrl_loop_data.rotor_angle_open_to_closed_loop_error -= foc_ctrl_loop_data.open_to_closed_loop_smoothing_coeff;

      if( mc_get_direction() == FORWARD )
      {
        foc_ctrl_loop_data.current_rotor_angle -= foc_ctrl_loop_data.rotor_angle_open_to_closed_loop_error;
      }
      else
      {
        foc_ctrl_loop_data.current_rotor_angle += foc_ctrl_loop_data.rotor_angle_open_to_closed_loop_error;
      }
    }
    else
    {
/*Message 2013*/
    }

  }
  else if( FOC_HALL_SENSOR == foc_ctrl_loop_data.rotor_angle_meas_mode )
  {
    foc_ctrl_loop_data.current_rotor_angle = foc_get_hall_sens_angle();
  }
  else
  {
/*Message 2013*/
  }

  /* immediately compute sine() and cosine() since they are used multiple times */
  foc_ctrl_loop_data.sine_of_current_rotor_angle   = math_sine_unsafe ( foc_ctrl_loop_data.current_rotor_angle );
  foc_ctrl_loop_data.cosine_of_current_rotor_angle = math_cosine_unsafe ( foc_ctrl_loop_data.current_rotor_angle );

  foc_park( &foc_ctrl_loop_data );

  /* now we are in d-q space and can control our system via two scalar units, i_d and i_q */
  pi_controller_exec_unsafe( &foc_ctrl_loop_data.controller_i_d, foc_ctrl_loop_data.i_d_ref, foc_ctrl_loop_data.i_d );
  pi_controller_exec_unsafe( &foc_ctrl_loop_data.controller_i_q, foc_ctrl_loop_data.i_q_ref, foc_ctrl_loop_data.i_q );

  foc_ctrl_loop_data.v_d = foc_ctrl_loop_data.controller_i_d.set_value;
  foc_ctrl_loop_data.v_q = foc_ctrl_loop_data.controller_i_q.set_value;

  #if ( FOC_EN_VBAT_COMPENSATION == 1 )
    foc_compensate_vbat( &foc_ctrl_loop_data );
  #endif

  /* now transform the voltages v_d and v_q back back to UVW space */
  foc_park_inv( &foc_ctrl_loop_data );

  #if ( FOC_USE_SVM == 1 )
    /* 7.46us */
    foc_svm( &foc_ctrl_loop_data );
  #else
    /* 0.7 us */
    foc_clarke_inv( &foc_ctrl_loop_data );
  #endif

  /* at rotor angle = 0 -> set trigger condition for data logger and check for stalled rotor */
  if( spdmeas_get_cur_direction() == FORWARD )
  {
    if(( loc_last_rotor_angle > (uint16_t) ((dbl) MATH_ANGLE_MAX * 0.75 )) && ( foc_ctrl_loop_data.current_rotor_angle < (uint16_t) ((dbl) MATH_ANGLE_MAX * 0.25 )))
    {
      #if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
        uart_set_data_logger_trigger();
      #endif

      if(( MC_RUNNING == mc_get_motor_state()) && ( FOC_SENSORLESS == foc_ctrl_loop_data.rotor_angle_meas_mode ))
      {
        foc_check_for_stall_condition( &foc_ctrl_loop_data );
      }
    }
  }
  else
  {
    if(( loc_last_rotor_angle < (uint16_t) ((dbl) MATH_ANGLE_MAX * 0.25 )) && ( foc_ctrl_loop_data.current_rotor_angle > (uint16_t) ((dbl) MATH_ANGLE_MAX * 0.75 )))
    {
      #if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
        uart_set_data_logger_trigger();
      #endif

      if(( MC_RUNNING == mc_get_motor_state()) && ( FOC_SENSORLESS == foc_ctrl_loop_data.rotor_angle_meas_mode ))
      {
        foc_check_for_stall_condition( &foc_ctrl_loop_data );
      }
    }
  }

  loc_last_rotor_angle = foc_ctrl_loop_data.current_rotor_angle;
}

/*! @brief initialize FOC module
 *
 * @details
 * Initialize storage structure and set some parameters to sensible start values.
 */
NO_INLINE void foc_init ( void )
{
  (void) memset((void *) &foc_ctrl_loop_data, 0, sizeof( foc_ctrl_loop_data ));                                                    /* PRQA S 0314 */ /* justification: come on, it's memset ... */

  foc_ctrl_loop_data.open_to_closed_loop_smoothing_coeff      = (uint16_t) FOC_OPEN_TO_CLOSED_LOOP_SMOOTHING_COEFF_INIT;
  foc_ctrl_loop_data.luenberger_data.motor_model_coeff_scaler = (uint16_t) FOC_I_MODEL_SCALE_BITS_INIT;

  #if ( FOC_EN_HALL_SENS == 1 )
    hall_init();
  #endif

  pi_controller_init( &foc_ctrl_loop_data.controller_i_d, (int16_t) FOC_CONTROLLLER_I_D_INIT_KP, (uint16_t) FOC_CONTROLLLER_I_D_INIT_KI );
  pi_controller_init( &foc_ctrl_loop_data.controller_i_q, (int16_t) FOC_CONTROLLLER_I_Q_INIT_KP, (uint16_t) FOC_CONTROLLLER_I_Q_INIT_KI );
}

void foc_generate_struct_signature ( void )
{
  foc_ctrl_loop_data.struct_signature = U16PTR_TO_U16( &foc_ctrl_loop_data.struct_signature );
}

/*! @brief Preload i_q controller to a given output value
 *
 * @details
 * This is only needed for synchronizing to a turning rotor, because right from the moment of the first <br>
 * outout PWM, the system is supposed to produce no current for a configurable amount of time (to let the <br>
 * control loop settle). 'v_q_out' must be given in the internal voltage scale [1/256] and 'direction' is actually of <br>
 * the type motor_direction_t .
 */
INLINE void foc_calc_i_q_ctrl_err_sum_preload_value( const int16_t v_q_out, const motor_direction_t direction )
{
  if( FORWARD == direction )
  {
    math_mulS16_unsafe((int16_t) v_q_out, (int16_t) ( PWM_PERIOD_DURATION_TICKS ));
  }
  else
  {
    math_nmulS16_unsafe((int16_t) v_q_out, (int16_t) ( PWM_PERIOD_DURATION_TICKS ));
  }

  s32 loc_buf = math_divS32_unsafe((s32) math_mul_get_result( True ), foc_ctrl_loop_data.vbat, True );
  loc_buf                                   = SHL_S32( loc_buf, PI_CONTROLLER_KI_KP_SCALE_BITS );
  foc_ctrl_loop_data.controller_i_q.err_sum = math_divS32_unsafe( loc_buf, (int16_t) foc_ctrl_loop_data.controller_i_q.Ki, True );
}

/*! @brief REinitialize FOC module
 *
 * @details
 * Clear all process data of the FOC module, so that the state before a start is consistent. <br>
 * This function needs to be called before every startup! <br>
 */
INLINE void foc_reset_data ( const bool_t reset_spdmeas_module )
{
  pi_controller_reset( &foc_ctrl_loop_data.controller_i_d );
  pi_controller_reset( &foc_ctrl_loop_data.controller_i_q );

  foc_ctrl_loop_data.rotor_angle_startup = 0u;

  #if ( FOC_EN_OBSERVER == 1 )
    foc_luenberger_reset_data();
  #endif

  #if ( FOC_EN_HALL_SENS == 1 )
    hall_reset_data();
  #endif

  foc_ctrl_loop_data.i_q_ref = 0;
  foc_ctrl_loop_data.i_d_ref = 0;

  if( reset_spdmeas_module )
  {
    spdmeas_reset_data();
  }
}

/*! @brief initialize FOC module for synchronization to a turning rotor
 *
 * @details
 * When synchronizing to a turning rotor, the motor control state machine monitors the BEMF with switched-off gate <br>
 * driver for a certain amount of time. At an exactly given point (speak: rotor angle), the control enables the calculation of <br>
 * the FOC and the gate driver. This point is at 90 degrees electrical for forward motion and 270 degrees electrical when rotating backwards. <br>
 * This function is called directly before this point in order to initialize all FOC process values according to the <br>
 * information that has been extracted from monitoring the BEMF (like the amplitude). In doing so, the synchronization <br>
 * will be smooth and will have way less transient oscillations compared to not initializing the process values.
 */
INLINE void foc_init_data_for_sync( const uint16_t bemf_ampl, const int16_t vsup_adc, const motor_direction_t direction )
{
  int16_t loc_bemf_ampl_scaled = 0;

  foc_reset_data( False );

  foc_set_supply_voltage_unsafe( vsup_adc );

  /* BEMF amplitude in PWM ticks = bemf_ampl_adc * pwm_max / vsup_adc */
  math_mulS16_unsafe((int16_t) bemf_ampl, (int16_t) ( PWM_PERIOD_DURATION_TICKS ));
  int16_t loc_bemf_ampl_pwm_ticks = (int16_t) math_divS32_unsafe((s32) math_mul_get_result( True ), (int16_t) vsup_adc, False );

  /* convert BEMF amplitude to internal voltage scale (vbat is in said scale..) */
  math_mulS16_unsafe( loc_bemf_ampl_pwm_ticks, foc_ctrl_loop_data.vbat );
  loc_bemf_ampl_scaled = (int16_t) math_divS32_unsafe((s32) math_mul_get_result( True ), (int16_t) ( PWM_PERIOD_DURATION_TICKS ), False );

  foc_ctrl_loop_data.v_alpha_history[ 0 ] = -(int16_t) loc_bemf_ampl_scaled;
  foc_ctrl_loop_data.v_alpha_history[ 1 ] = -(int16_t) loc_bemf_ampl_scaled;

  foc_ctrl_loop_data.v_beta_history[ 0 ] = 0;
  foc_ctrl_loop_data.v_beta_history[ 1 ] = 0;

  foc_ctrl_loop_data.luenberger_data.e_alpha_model_raw = -(int16_t) loc_bemf_ampl_scaled;                                              /* [V/256]; e_beta is set to 0 by foc_reset_data() */

  /* the filtered values lag the raw values by 45 degrees electrical thus: e_alpha = - bemf_amplitude / sqrt(2) */
  /* bemf_amplitude / sqrt(2) = bemf_amplitude * sqrt(2) / 2 */
  math_mulS16_unsafe( loc_bemf_ampl_scaled, (int16_t) ( SQRT2 * 256.0 ));
  math_mul_shiftright_result( 9u );

  if( FORWARD == direction )
  {
    foc_ctrl_loop_data.luenberger_data.e_alpha_model_filtered = -(int16_t) math_mul_get_result( False );
    foc_ctrl_loop_data.luenberger_data.e_beta_model_filtered  = (int16_t) math_mul_get_result( False );
  }
  else
  {
    foc_ctrl_loop_data.luenberger_data.e_alpha_model_filtered = -(int16_t) math_mul_get_result( False );
    foc_ctrl_loop_data.luenberger_data.e_beta_model_filtered  = -(int16_t) math_mul_get_result( False );
  }

  foc_ctrl_loop_data.luenberger_data.lpf_e_alpha.output = SHL_S32((s32) foc_ctrl_loop_data.luenberger_data.e_alpha_model_filtered, 16u );
  foc_ctrl_loop_data.luenberger_data.lpf_e_beta.output  = SHL_S32((s32) foc_ctrl_loop_data.luenberger_data.e_beta_model_filtered, 16u );

  foc_ctrl_loop_data.v_alpha = -(int16_t) loc_bemf_ampl_pwm_ticks;                                                                     /* [PWM ticks] */
  foc_ctrl_loop_data.v_beta  = 0;

  /* preload i_q controller so that the output voltage amplitude immediately matches the BEMF voltage amplitude (= v_alpha in voltage scale) */
  foc_calc_i_q_ctrl_err_sum_preload_value( loc_bemf_ampl_scaled, direction );
}

/*! @brief gather all necessary data for computing the DC link current from phase currents and voltages
 *
 * @details
 * Disclaimer: This one is rather tricky and additionally requires understanding of module 'spdmeas'. <br>
 * The general idea is that the DC link current is the mean value of the current flowing through the sum shunt. <br>
 * Consequently, only non-zero voltage vectors contribute to the DC link current value. Module 'spdmeas' holds a value (0...7) that <br>
 * directly encodes the order of output PWM thresholds (i.e. the phase voltages) which is cleverly used as index for the static <br>
 * array inside the function (this approach is quite fast). The array element returns the three phase voltages in their right order as <br>
 * well as the two phase currents which are needed for the calculation (see mc_supply_current_calc_unsafe(); the calculation is explained there). <br>
 */
INLINE foc_ibat_calc_data_t foc_get_ibat_calc_data_unsafe ( void )
{
  foc_ibat_calc_data_t loc_return_value = {0};

  /* the following table implements the mapping between the order of the output voltages (from module spdmeas_) and the respective
     process values needed for computing the supply curent IBAT */
  static foc_ibat_calc_data_lookup_t loc_ibat_calc_data_lookup [ 8 ] =                                                             /* PRQA S 3678 */
  {
    {( int16_t *) 0, ( int16_t *) 0, ( int16_t *) 0, ( int16_t *) 0, ( int16_t *) 0},                                                                  /* invalid */
    {&foc_ctrl_loop_data.i_u, &foc_ctrl_loop_data.i_v, &foc_ctrl_loop_data.v_u, &foc_ctrl_loop_data.v_w, &foc_ctrl_loop_data.v_v}, /* v_v > v_w > v_u */
    {&foc_ctrl_loop_data.i_v, &foc_ctrl_loop_data.i_w, &foc_ctrl_loop_data.v_v, &foc_ctrl_loop_data.v_u, &foc_ctrl_loop_data.v_w}, /* v_w > v_u > v_v */
    {&foc_ctrl_loop_data.i_u, &foc_ctrl_loop_data.i_w, &foc_ctrl_loop_data.v_u, &foc_ctrl_loop_data.v_v, &foc_ctrl_loop_data.v_w}, /* v_w > v_v > v_u */
    {&foc_ctrl_loop_data.i_w, &foc_ctrl_loop_data.i_u, &foc_ctrl_loop_data.v_w, &foc_ctrl_loop_data.v_v, &foc_ctrl_loop_data.v_u}, /* v_u > v_v > v_w */
    {&foc_ctrl_loop_data.i_w, &foc_ctrl_loop_data.i_v, &foc_ctrl_loop_data.v_w, &foc_ctrl_loop_data.v_u, &foc_ctrl_loop_data.v_v}, /* v_v > v_u > v_w */
    {&foc_ctrl_loop_data.i_v, &foc_ctrl_loop_data.i_u, &foc_ctrl_loop_data.v_v, &foc_ctrl_loop_data.v_w, &foc_ctrl_loop_data.v_u}, /* v_u > v_w > v_v */
    {( int16_t *) 0, ( int16_t *) 0, ( int16_t *) 0, ( int16_t *) 0, ( int16_t *) 0},                                                                  /* invalid */
  };

  /* since we only need the momentary voltage pattern, the most recent pattern is masked out by the AND operation ... */
  uint16_t loc_idx = spdmeas_get_volt_uvw_pattern() & (uint16_t) 0x07;

  int16_t loc_i1 = *( loc_ibat_calc_data_lookup[ loc_idx ].i1 );
  int16_t loc_i2 = *( loc_ibat_calc_data_lookup[ loc_idx ].i2 );

  /* function is called on interrupt level, therefore MUL/DIV may be used; convert currents from ADC values to A/256 */
  math_mulS16_unsafe((int16_t) ( foc_ctrl_loop_data.current_conversion_factor ), loc_i1 );
  math_mul_shiftright_result( 8u );

  loc_i1 = (int16_t) math_mul_get_result( False );

  math_mulS16_unsafe((int16_t) ( foc_ctrl_loop_data.current_conversion_factor ), loc_i2 );
  math_mul_shiftright_result( 8u );

  loc_i2 = (int16_t) math_mul_get_result( False );

  loc_return_value.i1        = loc_i1;
  loc_return_value.i2        = loc_i2;
  loc_return_value.v_val_low = *( loc_ibat_calc_data_lookup[ loc_idx ].v_low );
  loc_return_value.v_val_mid = *( loc_ibat_calc_data_lookup[ loc_idx ].v_mid );
  loc_return_value.v_val_max = *( loc_ibat_calc_data_lookup[ loc_idx ].v_max );

  return loc_return_value;
}

/* }@
 */
