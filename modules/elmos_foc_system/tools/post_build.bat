@echo off
rem HOWTO: enter the following line under project options/Build Actions/Post-build command line: $PROJ_DIR$\tools\post_build.bat "$PROJ_DIR$\tools\" $PROJ_FNAME$
rem change the list file output to "Text" under project options/Linker/List/File Format
rem you might change the debug output level unter Tools/Options.../Messages/Show Build Messages to "All"

cd /D %1

set HEADER_FILES=types.h motor_ctrl.h foc.h pid_controller.h hallsens.h uart.h adc.h speed_meas.h ..\app_user\app_template.h ..\app_user\app_speedpoti.h

set PERL_EXECUTABLE=perl 
if exist "strawberry_perl\perl\bin\perl.exe" set PERL_EXECUTABLE=strawberry_perl\perl\bin\perl.exe
rem echo %PERL_EXECUTABLE%

if "%2" == "e52305a" goto GENERATE_e52305a
if "%2" == "e52306a" goto GENERATE_e52306a
if "%2" == "e52352a" goto GENERATE_e52352a

rem @echo on
echo ERROR! Neither e52305a nor e52306a nor e52352a is specified!!
goto END

:GENERATE_e52305a
if exist "52305_foc_var_addresses.map" del /F 52305_foc_var_addresses.map
if exist "52305_foc_var.list" del /F 52305_foc_var.list

%PERL_EXECUTABLE% generate_variable_listing.pl ..\bin\52305\List\foc_e52305a.map %HEADER_FILES%
goto END

:GENERATE_e52306a
if exist "52306_foc_var_addresses.map" del /F 52306_foc_var_addresses.map
if exist "52306_foc_var.list" del /F 52306_foc_var.list

%PERL_EXECUTABLE% generate_variable_listing.pl ..\bin\52306\List\foc_e52306a.map %HEADER_FILES%
goto END

:GENERATE_e52352a
if exist "52352_foc_var_addresses.map" del /F 52352_foc_var_addresses.map
if exist "52352_foc_var.list" del /F 52352_foc_var.list

%PERL_EXECUTABLE% generate_variable_listing.pl ..\bin\52352\List\foc_e52352a.map %HEADER_FILES%
goto END

:END