/***************************************************************************//**
 * @file   LinBus_Implementation.h
 *
 * @creator      RPY
 * @created      2013/10/16
 * @sdfv         Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  LIN bus ("PHY") implementation (Code).
 *
 * @purpose
 *
 * Implements an abstraction layer which encapsulates all the low-level
 * transport effort. Basically an event based API is exposed, which
 * can be used by some higher-level protocol layer to implement a LIN stack.
 *
 * $Id: LinBus_Implementation.h 2459 2017-05-15 11:54:42Z rpy $
 *
 * $Revision: 2459 $
 *
 ******************************************************************************/

#ifndef LINBUS_IMPLEMENTATION_H_
#define LINBUS_IMPLEMENTATION_H_

/* ****************************************************************************/
/* ***************************** INCLUDES *************************************/
/* ****************************************************************************/

#include "LinBus_Interface.h"
#include "LinSNPD_Interface.h"

/* ****************************************************************************/
/* ************************ DEFINES AND MACROS ********************************/
/* ****************************************************************************/
#define LINBUSIMP_CONFIG_DATA_VERSION   0x0101  /* expected config data version */ 
#define LINBUSIMP_IGNORE_MSGTOUT_NEVER               0x00u       /**< Do not ignore message timeouts  */
#define LINBUSIMP_IGNORE_MSGTOUT_FOR_EVER            0xffu       /**< Ignore message timeouts for ever */

/* ****************************************************************************/
/* *************************** GLOBALE VARIABLES ******************************/
/* ****************************************************************************/
#ifdef LINBUSIMP_EXT_IFFUN_STRCT_ACCESS
extern const LinBusIf_sInterfaceFunctions_t  LinBusImp_InterfaceFunctions;
#endif /* LINBUSIMP_EXT_IFFUN_STRCT_ACCESS  */

/* ****************************************************************************/
/* ******************** EXTERNAL FUNCTIONS / INTERFACE ************************/
/* ****************************************************************************/

/** @addtogroup LinBusImpInterfaceFunctions */
/**@{*/

/***************************************************************************//**
 * @brief Implementation of LIN BUS layer 'Initialization' function.
 *
 * @copydetails LinBusIf_InitializationIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinBusImp_Initialization(LinBusIf_pGenericEnvData_t     genericBusEnvData, LinBusIf_EnvDataSze_t         busEnvDataSze,
                                    LinBusIf_cpCallbackFunctions_t busCbFuns,         LinBusIf_pGenericCbCtxData_t  genericBusCbCtxData,
                                    LinBusIf_Baudrate_t            init_baudrate,     LinBusIf_pGenericImpCfgData_t genericImpCfgData);

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
Lin_Bool_t LinBusImp_GetSubInterface(LinBusIf_pGenericEnvData_t genericBusEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * @brief Implementation of LIN BUS layer 'Task' function.
 *
 * @copydetails LinBusIf_TaskIfFun_t
 *
 ******************************************************************************/
 void LinBusImp_Task(LinBusIf_pGenericEnvData_t genericBusEnvData);

 /***************************************************************************//**
  * @brief TODO: Add description
  *
  * @copydetails LinBusIf_GetMillisecondsIfFun_t
  *
  ******************************************************************************/
 Lin_uint32_t LinBusImp_GetMilliseconds(LinBusIf_pGenericEnvData_t genericBusEnvData);

 /***************************************************************************//**
  * @copydetails LinBusIf_ShutdownIfFun_t
  *
  * Terminates genericBusEnvData processing, setup hardware to a safe mode and go to state
  * LBS_OFFLINE.
  *
  ******************************************************************************/
 void LinBusImp_ShutdownBus(LinBusIf_pGenericEnvData_t genericBusEnvData);

 /***************************************************************************//**
  * @copydetails LinBusIf_GoToSleepIfFun_t
  *
  ******************************************************************************/
 void LinBusImp_GoToSleep(LinBusIf_pGenericEnvData_t genericBusEnvData);

 /***************************************************************************//**
  * @copydetails LinBusIf_GetStateIfFun_t
  *
  ******************************************************************************/
 LinBusIf_eState_t LinBusImp_GetBusState(LinBusIf_pGenericEnvData_t genericBusEnvData);

 /***************************************************************************//**
  * @copydetails LinBusIf_ChangeBaudrateIfFun_t
  *
  * The 'new_baudrate' value will be used as current expected baud rate,
  * sync measurement baud rate and timeouts are updated accordingly.
  *
  * New baud rate will become effective the next time genericBusEnvData starts
  * break detection and sync measurement.
  *
  ******************************************************************************/
Lin_Bool_t LinBusImp_BusChangeBaudrate(LinBusIf_pGenericEnvData_t genericBusEnvData, LinBusIf_Baudrate_t new_baudrate,Lin_Bool_t try_only);

 /***************************************************************************//**
  * @brief Change genericBusEnvData idle timeout.
  *
  * @copydetails LinBusIf_ChangeIdleTimeoutIfFun_t
  *
  * New idle time will become effective after next message has been processed (or
  * after LinBusImp_RestartBus() )
  *
  ******************************************************************************/
 void LinBusImp_BusChangeIdleTimeout(LinBusIf_pGenericEnvData_t genericBusEnvData, LinBusIf_TimeOut_t timeout);

 /***************************************************************************//**
  * @copydetails LinBusIf_RestartIfFun_t
  *
  * Reinitialize genericBusEnvData.
  *
  * If some failure condition has been detected (genericBusEnvData internal or from some other
  * subsystem) the LIN genericBusEnvData can be reseted to a known state.
  *
  * The genericBusEnvData will be in LBS_SYNCING state.
  *
  ******************************************************************************/
 void LinBusImp_RestartBus(LinBusIf_pGenericEnvData_t genericBusEnvData);

 /***************************************************************************//**
  * @copydetails LinBusIf_WakeupClusterIfFun_t
  *
  * Afterwards the genericBusEnvData will be in LBS_SYNCING state.
  *
  * Currently this method is blocking. It will return after the wakeup has been
  * completed (see LINBUSIMP_DEFAULT_WAKEUP_DURATION (~2,5 ms))
  *
  ******************************************************************************/
 void LinBusImp_WakeupCluster(LinBusIf_pGenericEnvData_t genericBusEnvData);

 /***************************************************************************//**
  * @copydetails LinBusIf_SendHeaderIfFun_t
  *
  * Once the PID has been send out a LINBUSIMP_EventPIDReceivedCallback_t is
  * reported to continue the message body.
  *
  * There is no queuing, once header sending is initiated, other requests are
  * ignored up to PID reporting.
  *
  ******************************************************************************/
Lin_Bool_t LinBusImp_SendHeader(LinBusIf_pGenericEnvData_t genericBusEnvData, LinBusIf_FrameID_t frameID);

/**@} LinBusImpInterfaceFunctions */

/* ****************************************************************************/
/* ********************STRUCTS, ENUMS AND TYPEDEFS ****************************/
/* ****************************************************************************/

typedef Lin_uint32_t  LinBusImp_ClockFreqValue_t;

/***************************************************************************//**
 * @brief Struct for BUS layer implementation specific configuration flags used in configuration data.
 ******************************************************************************/
typedef struct 
{
  Lin_uint8_t    CallMeasDone              : 1;  /**< 1 = Allow application to validate baudrate measurement */
  Lin_uint8_t    CheckBaudrate             : 1;  /**< 1 = Verify if selected baudrate can be exactly represented as a SCI clock divider value */ 
  Lin_uint8_t    DetectPostPIDCollisions   : 1;  /**< 1 = Monitor the phase between PID reception and beginn of slave transmision for any disturbance or master activity. Report them as bus collisions */ 
} LinBusImp_sCfgDataFlags_t;

/***************************************************************************//**
 * @brief Struct for BUS layer implementation specific configuration data.
 ******************************************************************************/
struct LinBusImp_sCfgData
{
  Lin_Version_t                       Version;             /* Config data struct version */ 
  
  LinBusImp_ClockFreqValue_t          ClockFrequency;  
  
  LinBusImp_sCfgDataFlags_t           ConfigFlags;
  Lin_uint8_t                         DebouncerValue;    
  Lin_uint8_t                         SendHeaderBreakLen;    
  Lin_uint8_t                         IgnoreMsgTimeouts;   /* Ignore nessage timeouts for n initial messages (successfull PID reception is the counting point) (0x00 = never, 0xff = for ever)  */  
};

typedef struct LinBusImp_sCfgData    LinBusImp_sCfgData_t;
typedef        LinBusImp_sCfgData_t* LinBusImp_pCfgData_t;



#endif /* LINBUS_IMPLEMENTATION_H_ */
