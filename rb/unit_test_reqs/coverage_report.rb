
class UnitTestReqs

  # Class used to store and create html report based on the UnitTestReqs::SwRequirements object
  class CoverageReport

    # Create new CoverageReport Object
    #
    # @param sw_reqs [UnitTestReqs::SwRequirements]
    def initialize(sw_reqs)
      @sw_reqs = sw_reqs
    end

    # creates an html report from the software requirements object provided.
    # @return [String] HTML code that can be written to a file
    def to_html
      progress = @sw_reqs.coverage_percentage
      covered_requirement_rows = @sw_reqs.covered_requirements.map do |e|
        row = <<EOS
      <tr>
        <td class="text-left">#{e.key}</td>
        <td class="text-left">#{e.id}</td>
        <td class="text-left">#{e.category}</td>
        <td class="text-left">#{e.text.gsub("\n", "<br>")}</td>
        <td class="text-left">#{e.verification_method.join("<br>")}</td>
      </tr>
EOS
        row
      end

      uncovered_requirement_rows = @sw_reqs.uncovered_requirements.map do |e|
        row = <<EOS
      <tr>
        <td class="text-left">#{e.key}</td>
        <td class="text-left">#{e.id}</td>
        <td class="text-left">#{e.category}</td>
        <td class="text-left">#{e.text.gsub("\n", "<br>")}</td>
        <td class="text-left">#{e.verification_method.join("<br>")}</td>
      </tr>
EOS
        row
      end

      under_review_requirement_rows = @sw_reqs.under_review_requirements.map do |e|
        row = <<EOS
      <tr>
        <td class="text-left">#{e.key}</td>
        <td class="text-left">#{e.id}</td>
        <td class="text-left">#{e.category}</td>
        <td class="text-left">#{e.text.gsub("\n", "<br>")}</td>
        <td class="text-left">#{e.verification_method.join("<br>")}</td>
      </tr>
EOS
        row
      end

      html_text = <<EOS
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>SW Requirement Coverage Report</title>
    <style>
@import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100);
body {
  background-color: #474747;
  font-family: "Roboto", helvetica, arial, sans-serif;
  font-size: 16px;
  font-weight: 400;
  text-rendering: optimizeLegibility;
}

div.table-title {
  display: block;
  margin: auto;
  padding:5px;
  width: 100%;
}
.table-title h3 {
   color: #fafafa;
   font-size: 30px;
   font-weight: 400;
   font-style:normal;
   font-family: "Roboto", helvetica, arial, sans-serif;
   text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
   text-transform:uppercase;
}

div.page-links {
  color: #fafafa;
  font-size: 20px;
  font-weight: 400;
  font-style:normal;
  font-family: "Roboto", helvetica, arial, sans-serif;
  text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
  text-align: center;
}

.footer {
  color: #fafafa;
  font-size: 8px;
  font-weight: 400;
  font-style:normal;
  font-family: helvetica, arial, sans-serif;
  text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
  text-align: center;
}

/*** Link Sytles **/

a:link {
  background-color: #d0d0d0;
  color: green;
  text-decoration: none;
}

a:visited {
  text-decoration: none;
  color: white;
}

a:hover {
  text-decoration: underline;
}

a:active {
  text-decoration: underline;
}
/*** Table Styles **/

.table-fill {
  background: white;
  border-radius:3px;
  border-collapse: collapse;
  margin: auto;
  padding:5px;
  width: 100%;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
  animation: float 5s infinite;
}
 
th {
  color:#D5DDE5;;
  background:#1b1e24;
  border-bottom:4px solid #9ea7af;
  border-right: 1px solid #343a45;
  font-size:23px;
  font-weight: 100;
  padding:24px;
  text-align:left;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
  vertical-align:middle;
}

th:first-child {
  border-top-left-radius:3px;
}
 
th:last-child {
  border-top-right-radius:3px;
  border-right:none;
}
  
tr {
  border-top: 1px solid #C1C3D1;
  border-bottom-: 1px solid #C1C3D1;
  color:#666B85;
  font-size:16px;
  font-weight:normal;
  text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
}
 
tr:hover td {
  background:#4E5066;
  color:#FFFFFF;
  border-top: 1px solid #22262e;
}
 
tr:first-child {
  border-top:none;
}

tr:last-child {
  border-bottom:none;
}
 
tr:nth-child(odd) td {
  background:#EBEBEB;
}
 
tr:nth-child(odd):hover td {
  background:#4E5066;
}

tr:last-child td:first-child {
  border-bottom-left-radius:3px;
}
 
tr:last-child td:last-child {
  border-bottom-right-radius:3px;
}
 
td {
  background:#FFFFFF;
  padding:10px;
  text-align:left;
  vertical-align:middle;
  font-weight:300;
  font-size:18px;
  text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
  border-right: 1px solid #C1C3D1;
}

td:last-child {
  border-right: 0px;
}

th.text-left {
  text-align: left;
}

th.text-center {
  text-align: center;
}

th.text-right {
  text-align: right;
}

td.text-left {
  text-align: left;
}

td.text-center {
  text-align: center;
}

td.text-right {
  text-align: right;
}

    </style>
</head>

<body>
  <div class="page-links">
    <a id="top" href="#SW-Reqs">SW Requirements</a> | <a href="#Uncovered-Reqs">Uncovered Requirements</a> | <a href="#Under-Review-Reqs">Under Review Requirements</a>
  </div>
  <div class="table-title">
    <h3>Unit Test Coverage Summary</h3>
    <table class="table-fill">
      <thead>
        <tr>
          <th class="text-left">Total Requirements</th>
          <th class="text-left">Total Covered</th>
          <th class="text-left">Progress</th>
        </tr>
      </thead>
      <tbody class="table-hover">
        <tr>
          <td class="text-left">#{@sw_reqs.requirements.length}</td>
          <td class="text-left">#{@sw_reqs.covered_requirements_count}</td>
          <td class="text-left">#{progress}% <progress value="#{progress}" max="100"/></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="table-title">
    <h3 id="SW-Reqs">Software Requirements</h3>
  </div>
  <table class="table-fill">
    <thead>
      <tr>
        <th class="text-left">Key</th>
        <th class="text-left">ID</th>
        <th class="text-left">Type</th>
        <th class="text-left">Text</th>
        <th class="text-left">Verification Method</th>
      </tr>
    </thead>
    <tbody class="table-hover">
      #{covered_requirement_rows.join("\n")}
    </tbody>
  </table>
  <div class="table-title">
    <h3 id="Uncovered-Reqs">Uncovered Software Requirements</h3>
  </div>
  <table class="table-fill">
    <thead>
      <tr>
        <th class="text-left">Key</th>
        <th class="text-left">ID</th>
        <th class="text-left">Type</th>
        <th class="text-left">Text</th>
        <th class="text-left">Verification Method</th>
      </tr>
    </thead>
    <tbody class="table-hover">
      #{uncovered_requirement_rows.join("\n")}
    </tbody>
  </table>
  <div class="table-title">
    <h3 id="Under-Review-Reqs">Under Review Software Requirements</h3>
  </div>
  <table class="table-fill">
    <thead>
      <tr>
        <th class="text-left">Key</th>
        <th class="text-left">ID</th>
        <th class="text-left">Type</th>
        <th class="text-left">Text</th>
        <th class="text-left">Verification Method</th>
      </tr>
    </thead>
    <tbody class="table-hover">
      #{under_review_requirement_rows.join("\n")}
    </tbody>
  </table>
</body>
<footer class="footer">
    <p>Unit Test Requirements Coverage Report v#{UnitTestReqs::VERSION}</p>
    <a href="#top">Back To Top</a>
</footer>
</html> 

EOS
    end

  end
end
