/**
 * @ingroup FOC
 *
 * @{
 */

/**********************************************************************/
/*! @file speed_meas.c
 * @brief rotor speed measurement
 *
 * @details
 * Pretty lean implementation which measures the speed by monitoring the order of the <br>
 * phase voltages, repectively changes of them. This bears the further advantage that other <br>
 * modules can query the order of phase voltages encoded into one literal number at any given time and <br>
 * without computational effort. <br>
 * Prerequisite: function spdmeas_set_voltage_uvw() must be called with an absolutely <br>
 * constant schedule! As of now, it's called with every ISR (mc_isr_handler() ) and you should not <br>
 * change that unless you are really, really sure about that. <br>
 * <br>
 * module prefix: spdmeas_
 *
 * @author       RHA
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/
#include "debug.h"
#include "global.h"
#include "math_utils.h"
#include "speed_meas.h"
#include "systime.h"
#include <stdlib.h>

/** @var speed_meas_data
 * @brief struct that holds all parameters, flags and process values of module speed measurement */
static speed_meas_data_t speed_meas_data;

/** @brief get a pointer towards the top-level structure of this module
 */
const speed_meas_data_t * spdmeas_get_data ( void )
{
  return &speed_meas_data;
}

/*! @brief retrieve value of 'phase_voltage_pattern' (i.e. current and last phase voltage order) */
INLINE uint16_t spdmeas_get_volt_uvw_pattern( void )
{
  return speed_meas_data.phase_voltage_pattern;
}

/*! @brief retrieve value current rotational speed [eRPM] */
INLINE uint16_t spdmeas_get_cur_speed( void )
{
  return speed_meas_data.speed;
}

/*! @brief retrieve value current rotational direction (measured by module spdmeas) */
INLINE motor_direction_t spdmeas_get_cur_direction( void )
{
  return speed_meas_data.cur_direction;
}


/*! @brief "main" function of the speed measurement
 *
 * @details
 * this function is called with every PWM (from the ISR handling the FOC) and takes the three output voltages<br>
 * these voltages are then evaluated for ther order, variable "cur_phase_voltage_order" holds the result:<br>
 * 1 ->  v > w > u<br>
 * 2 ->  w > u > v<br>
 * 3 ->  w > v > u<br>
 * 4 ->  u > v > w<br>
 * 5 ->  v > u > w<br>
 * 6 ->  u > w > v<br>
 * <br>
 * variable "phase_voltage_pattern" holds the current pattern in the lowest 3 bits and the most recent pattern in the following<br>
 * 3 bits, thereby making it very easy to detect changes in the order easily via lookup table ("direction_lut")<br>
 */
INLINE void spdmeas_set_voltage_uvw( const int16_t u, const int16_t v, const int16_t w )
{
  static BOOL loc_recalc_speed = False;
  static uint16_t loc_duration      = 0xFFFFu;

  /** @var spdmeas_direction_lut
   * @brief holds the truth table for changes of the phase voltage order; just use variable 'phase_voltage_pattern' as index, and the return<br>
   * value is 0 (no change), 1 (change in forward rotation) or -1 (change in backward rotation) */
  const static int8_t spdmeas_direction_lut[ 8 * 8 ] =
  {
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 0, -1, 0, 0,
    0, 0, 0, -1, 0, 0, 1, 0,
    0, -1, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 1, -1, 0,
    0, 1, 0, 0, -1, 0, 0, 0,
    0, 0, -1, 0, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0
  };

  if( !loc_recalc_speed )
  {
    loc_duration = systime_getU16() - speed_meas_data.speed_recalc_timestamp;

    /* check for stalled rotor (i.e. an electrical revolution takes too long) */
    if( MC_RUNNING == mc_get_motor_state())
    {
      if( loc_duration >= SYSTIME_MS_TO_SYSTIME_TICKS( 2500u ))  /* by that, we implicitely impose a minimum rotational speed of 24 eRPM! */
      {
        mc_stalled_rotor_condition_handler();
      }
    }

    /* generate the literal number describing the phase voltage order */
    speed_meas_data.cur_phase_voltage_order = 0u;

    if( u < v ){speed_meas_data.cur_phase_voltage_order |= 1u; }
    if( v < w ){speed_meas_data.cur_phase_voltage_order |= 2u; }
    if( w < u ){speed_meas_data.cur_phase_voltage_order |= 4u; }

    speed_meas_data.phase_voltage_pattern    = ( speed_meas_data.last_phase_voltage_order << 3 ) | speed_meas_data.cur_phase_voltage_order;
    speed_meas_data.last_phase_voltage_order = speed_meas_data.cur_phase_voltage_order;

    /* from the newly generated "phase_voltage_pattern", check if a change of order had occurred */
    speed_meas_data.cur_phase_voltage_order_change = (int16_t) spdmeas_direction_lut[ speed_meas_data.phase_voltage_pattern ];

    /* if any change occurred ... */
    if( speed_meas_data.cur_phase_voltage_order_change != 0 )
    {
      /* .. check if the change is in the same direction as the last one (just a noise filter) ... */
      if( speed_meas_data.cur_phase_voltage_order_change == speed_meas_data.last_phase_voltage_order_change )
      {
        speed_meas_data.phase_voltage_order_change_evnt_cntr += speed_meas_data.cur_phase_voltage_order_change;

        /* .. and if we crossed enough voltage sectors ... (6 would mean a full electrical revolution) ... */
        if((uint16_t) abs( speed_meas_data.phase_voltage_order_change_evnt_cntr ) >= speed_meas_data.speed_recalc_cntr_thrshld )
        {
          /* .. and if enough time has passed since the last speed calculation, recalculate it */
          if( loc_duration >= speed_meas_data.min_meas_duration )
          {
            loc_recalc_speed = True;
          }
        }
      }

      speed_meas_data.last_phase_voltage_order_change = speed_meas_data.cur_phase_voltage_order_change;
    }
  }
  else
  {
    /* the else-branch implements the recalculation of the rotor speed */
    if( speed_meas_data.phase_voltage_order_change_evnt_cntr > 0 )
    {
      speed_meas_data.cur_direction = FORWARD;
      math_mulU16_unsafe((uint16_t) ((((u32) PWM_OUT_FREQ * 60uL ) / 6uL ) / ( 1uL << 2 )), (uint16_t) speed_meas_data.phase_voltage_order_change_evnt_cntr );
    }
    else
    {
      speed_meas_data.cur_direction = BACKWARD;
      math_mulU16_unsafe((uint16_t) ((((u32) PWM_OUT_FREQ * 60uL ) / 6uL ) / ( 1uL << 2 )), (uint16_t) ( int16_t ) - speed_meas_data.phase_voltage_order_change_evnt_cntr );
    }

    math_mul_shiftleft_result( 2u );
    math_unblockedDivU32_unsafe((u32) math_mul_get_result( True ), loc_duration );

    speed_meas_data.phase_voltage_order_change_evnt_cntr = 0;
    speed_meas_data.speed_recalc_timestamp               = systime_getU16();

    loc_recalc_speed = False;

    speed_meas_data.speed = (uint16_t) math_unblockedDivReadResult_unsafe( False );
  }
}

/*! @brief initialize speed measurement module */
NO_INLINE void spdmeas_init( void )
{
  speed_meas_data.speed_recalc_cntr_thrshld = (uint16_t) 6u;
  speed_meas_data.min_meas_duration         = (uint16_t) ( SYSTIME_TICKS_PER_SEC ) / 100u;
}

/*! @brief reset process values of module spdmeas */
INLINE void spdmeas_reset_data( void )
{
  speed_meas_data.speed                    = 0u;
  speed_meas_data.last_phase_voltage_order = 0u;

  speed_meas_data.last_phase_voltage_order_change = 0;

  speed_meas_data.phase_voltage_order_change_evnt_cntr = 0;
  speed_meas_data.speed_recalc_timestamp               = systime_getU16();
}

void spdmeas_generate_struct_signature ( void )
{
  speed_meas_data.struct_signature = U16PTR_TO_U16( &speed_meas_data.struct_signature );
}

/* }@
 */
