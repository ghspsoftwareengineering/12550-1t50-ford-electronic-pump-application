/***************************************************************************//**
 * @file   LinBus_Implemantation.c
 *
 * @creator      RPY
 * @created      2013/10/16
 *
 * @brief  LIN genericBusEnvData ("PHY") implementation (Code).
 *
 * Implements an abstraction layer which encapsulates all the low-level
 * transport effort. Basically an event based API is exposed, which
 * can be used by some higher-level protocol layer to implement a LIN stack.
 *
 * $Id: LinBus_Implementation.c 2477 2017-05-15 19:46:47Z rpy $
 *
 * $Revision: 2477 $
 *
 * @misra{M3CM Dir-2.7. - PRQA Msg 3206,
 * The Elmos LIN Driver defines interfaces for every module of it. This interfaces consist
 * of typedefinitions of function pointers combined in struct. This struct has to be
 * implemented by every implementation of every module. Because of this matter of fact\,
 * there can be different implementations for the same module and for the same reason one
 * implementation use and/or write to a parameter defined in the function pointer typedefinition
 * and another one not. So the parameter of the interface function are binding\, but must not
 * be used and/or not written to them.,
 * Unnecessary code.,
 * None}
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ***************************** INCLUDES *************************************/
/* ****************************************************************************/
#include "vic_InterruptHandler.h"

#include "el_helper.h"
#include "LinBus_Implementation.h"

#include "io_e52398a.h"


#include "linsci_InterruptHandler.h"

/* ****************************************************************************/
/* ************************ DEFINES AND MACROS ********************************/
/* ****************************************************************************/
#if LINDRVIMP_AQR_MOD_SZE == 1
#define BUS_LAYER_CODE _Pragma("location=\"BUS_LAYER_CODE\"")
#define BUS_LAYER_DATA _Pragma("location=\"BUS_LAYER_DATA\"")
#else
#define BUS_LAYER_CODE 
#define BUS_LAYER_DATA 
#endif

/* ****************************************************************************/
/* ******************** STRUCTS, ENUMS AND TYPEDEFS ***************************/
/* ****************************************************************************/

// helpful defines related to io_e52398a.h (check once this file is updated)

#define V_LINSCI_TIMER_PREPARE(x)                             ((((uint16_t)(x))&0x03u)<<2u)      /* [3:2]   */

#define V_LINSCI_UART_CONFIG_PARITY(x)                        ((((uint16_t)(x))&0x03u) << 2u)    /* [3:2]   */
#define V_LINSCI_UART_CONFIG_DEBOUNCE(x)                      ((((uint16_t)(x))&0x07u) << 12u)   /* [14:12] */



/***************************************************************************//**
 * LIN bus state enumeration
 ******************************************************************************/
typedef LinBusIf_eState_t LinBusImp_eState_t; /**< for this implementation bus states match exactly the interface state representation */

#define LinBusImp_STATE_OFFLINE              LinBusIf_STATE_OFFLINE
#define LinBusImp_STATE_SLEEP                LinBusIf_STATE_SLEEP
#define LinBusImp_STATE_SYNCING              LinBusIf_STATE_IDLE
#define LinBusImp_STATE_RECEIVING_DATA       LinBusIf_STATE_RECEIVING_DATA
#define LinBusImp_STATE_TRANSMITING_DATA     LinBusIf_STATE_TRANSMITING_DATA

typedef Lin_uint16_t                         LinBusImp_IrqNumber_t;                    /**< IRQ number as passed into irq handlers*/

typedef linsci_sInterruptEnvironmentData_t   LinBusImp_SciIRQHandlingEnvData_t;        /**< Env data pointer for SCI IRQs */
typedef linsci_InterruptCallback_t           LinBusImp_SCICallback_t;                  /**< SCI interrupt function callback type */

typedef Lin_uint16_t                         LinBusImp_DividerValue_t;                 /**< Baudrate divider value representation */

typedef struct 
{
  Lin_uint8_t                              CallMeasDone            : 1;  /**< if set true, additional IRQ and callback will be issued to verify how to proceed with updated baudrates. */            
  Lin_uint8_t                              CheckBaudrate           : 1;  /**< if set true, only baudrate changes which match exactly divider values will be accepted. */            
  Lin_uint8_t                              DetectPostPIDCollisions : 1;  /**< if set true, monitor the phase between PID reception and beginn of slave transmision for any disturbance or master activity. Report them as bus collisions */  
} loc_sBusFlags_t;

/***************************************************************************//**
 * LIN driver genericBusEnvData runtime state
 ******************************************************************************/
struct loc_sBusEnvironmentData
{
/* private: */
  
  LinBusImp_SciIRQHandlingEnvData_t       SciEnvironmentData;     /**< environment data for SCI interrupt handling */
  LinBusIf_cpCallbackFunctions_t          Callbacks;              /**< The set of callbacks associated with this genericBusEnvData instance */
  LinBusIf_pGenericCbCtxData_t            CallbackCtxData;        /**< The callback context data, to pass back information back to the using implementation */

  LinBusImp_eState_t                      BusState;               /**< Current genericBusEnvData state */

  LinBusIf_FrameID_t                      CurrentFrameID;         /**< Frame ID of last header received */
  LinBusIf_pData_t                        CurrentBuffer;          /**< Rx/Tx buffer */
  LinBusIf_BufLength_t                    CurrentBufferLen;       /**< Number of bytes in  buffer */
  
  LinBusImp_ClockFreqValue_t              ClockFreq;              /**< System operating frequency in Hz */
  
  LinBusImp_DividerValue_t                SyncMeasDivider;        /**< The initial divider value for sync measurement. */
  LinBusImp_DividerValue_t                CurrentDivider;         /**< Current nominal divider value */
  LinBusIf_BreakLen_t                     CurrentBreakLength;     /**< Current nominal divider value */

  loc_sBusFlags_t                         Flags;               /**< Set of runtime configuration flags */   
  Lin_uint8_t                             SendHeaderBreakLen;  /**< break signal duration */  
  Lin_uint16_t                            DebouncerValue;      /**< config regiter value for debouncer configuration */ 
  
  LinBusIf_Tick_t                         TickCount;              /**< Cuurent tick count (ms) */    
  LinBusIf_Tick_t                         SleepTimeout;           /**< Idle time out to go to sleep mode */
  LinBusIf_Tick_t                         SleepCount;             /**< Millisecond to go to sleep. */    
  Lin_uint8_t                             IgnoreTimeoutsCount;    /**< The number of upcommong (successfull) messages for which header und message timeouts will not be checked. (0xff = for ever). */    
};

/***************************************************************************//**
 * Forward declaration of the main genericBusEnvData runtime data
 * structure see LinBusImp_Struct_t
 ******************************************************************************/
typedef struct loc_sBusEnvironmentData    loc_sBusEnvironmentData_t;
typedef        loc_sBusEnvironmentData_t* loc_pBusEnvironmentData_t;

/* ****************************************************************************/
/* ****************** FORWARD DECLARATIONS / PROTOTYPES ***********************/
/* ****************************************************************************/

/***************************************************************************//**
 * Call wrapper for error reporting.
 *
 * \copydetails LinBusIf_ErrorCbFun_t
 *
 * @see LinBusIf_ErrorCbFun_t
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_CallBusErrorCallback(const loc_pBusEnvironmentData_t busEnvData, const LinBusIf_Error_t error);

/***************************************************************************//**
 * Call wrapper for restart events.
 *
 * \copydetails LinBusIf_RestartCbFun_t
 *
 * @see LinBusIf_RestartCbFun_t
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_CallRestartCallback(loc_pBusEnvironmentData_t busEnvData);

#if LINBUSIMP_ENABLE_MEAS_DONE_CALLBACK == 1
/***************************************************************************//**
 * Call wrapper for baud measurement notification.
 *
 * \copydetails LinBusIf_MeasDoneCbFun_t
 *
 * @see LinBusIf_MeasDoneCbFun_t
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eMeasAction_t loc_CallMeasDoneCallback(loc_pBusEnvironmentData_t busEnvData, LinBusImp_DividerValue_t cur_div);

#endif

/***************************************************************************//**
 * Call wrapper for PID based processing dispatch.
 *
 * \copydetails LinBusIf_MeasDoneCbFun_t
 *
 * @see LinBusIf_MeasDoneCbFun_t
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eFrameIdAction_t loc_CallFrameIdReceivedCallback(loc_pBusEnvironmentData_t busEnvData, LinBusIf_FrameID_t frameId,
                                                                                       LinBusIf_ppData_t            rxtx_buf,   LinBusIf_pBufLength_t buf_len,
                                                                                       LinProtoIf_pCrcType_t        crcType);

/***************************************************************************//**
 * Call wrapper for reporting of successfully received message.
 *
 * \copydetails LinBusIf_FrameIdReceivedCbFun_t
 *
 * @see LinBusIf_FrameIdReceivedCbFun_t
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eMsgAction_t loc_CallReceiveDoneCallback(loc_pBusEnvironmentData_t busEnvData, LinBusIf_FrameID_t   frameId,
                                                                               LinBusIf_pData_t          rx_buf,     LinBusIf_BufLength_t buf_len,
																			   LinBusIf_Error_t          error);

/***************************************************************************//**
 * Call wrapper for reporting of successfully transmitted message.
 *
 * \copydetails LinBusIf_TransmitDoneCbFun_t
 *
 * @see LinBusIf_TransmitDoneCbFun_t
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eMsgAction_t loc_CallTransmitDoneCallback(loc_pBusEnvironmentData_t busEnvData, LinBusIf_FrameID_t   frameId,
                                                                                LinBusIf_pData_t          tx_buf,     LinBusIf_BufLength_t buf_len,
																				LinBusIf_Error_t          error);

/***************************************************************************//**
 * Call wrapper for reporting of genericBusEnvData idle timeout expiration.
 *
 * \copydetails LinBusIf_IdleCbFun_t
 *
 * @see LinBusIf_IdleCbFun_t
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eIdleAction_t loc_CallIdleCallback(loc_pBusEnvironmentData_t busEnvData);



/***************************************************************************//**
 * Release LIN buffers
 *
 * @param[in]  genericBusEnvData: Pointer to reserved RAM space for BUS environment data
 *
 * @return
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eMsgAction_t loc_ReleaseBuffer(loc_pBusEnvironmentData_t busEnvData, LinBusIf_Error_t error);


/* *****************************************************************************
 *
 * Helper methods
 *
 ******************************************************************************/

#if LINBUSIMP_ENABLE_SEND_HEADER == 1

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_PID_t loc_GetPidForFrameId (LinBusIf_FrameID_t frameId);

#endif

/***************************************************************************//**
 * Calculate SCI divider value
 *
 * @param[in] baudrate
 *
 * @return    Divider value
 *
 * Calculate divider value for desired baudrate.
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusImp_DividerValue_t loc_CalcDivider(LinBusImp_ClockFreqValue_t clk, LinBusIf_Baudrate_t baudrate);

/***************************************************************************//**
 * Calculate baudrate out of divider value
 *
 * @param[in] div
 *
 * @return    baudrate
 *
 * Calculate baudrate out of divider value.
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_Baudrate_t loc_CalcBaudrate(LinBusImp_ClockFreqValue_t clk, LinBusImp_DividerValue_t divider);


/***************************************************************************//**
 * This method recalculates all baudrate related timeouts
 *
 * @param[in] genericBusEnvData
 * @param[in] new_div
 * @param[in] freq
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline Lin_Bool_t loc_UpdateBaudrates(loc_pBusEnvironmentData_t busEnvData, LinBusIf_Baudrate_t new_baudrate, LinBusImp_ClockFreqValue_t freq,Lin_Bool_t try_only);

/* *****************************************************************************
 *
 * Top-level state management methods
 *
 ******************************************************************************/

/***************************************************************************//**
 * Change LIN genericBusEnvData state to "offline"
 *
 * @param[in] genericBusEnvData
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_ChangeState_Offline(loc_pBusEnvironmentData_t busEnvData);

/***************************************************************************//**
 * Change LIN genericBusEnvData state to "sleep"
 *
 * @param[in] genericBusEnvData
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_ChangeState_Sleep(loc_pBusEnvironmentData_t busEnvData, Lin_Bool_t state_check);


/***************************************************************************//**
 * Change LIN genericBusEnvData state to "synchronizing"
 *
 * @param[in] genericBusEnvData
 *
 ******************************************************************************/
BUS_LAYER_CODE static void loc_ChangeState_Syncing(loc_pBusEnvironmentData_t busEnvData, LinBusIf_Error_t error);

/***************************************************************************//**
 * Change LIN genericBusEnvData state to "receiving data"
 *
 * @param[in] genericBusEnvData
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_ChangeState_ReceivingData(loc_pBusEnvironmentData_t busEnvData, LinBusIf_FrameID_t pid, uint16_t header_time, LinProtoIf_eCrcType_t crcType);

/***************************************************************************//**
 * Change LIN genericBusEnvData state to "transmitting data"
 *
 * @param[in] genericBusEnvData
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_ChangeState_TransmitingData(loc_pBusEnvironmentData_t busEnvData, LinBusIf_FrameID_t pid,LinProtoIf_eCrcType_t crcType);

/* *****************************************************************************
 *
 * Interrupt handlers
 *
 ******************************************************************************/

/***************************************************************************//**
 * LIN interrupt handler for 1ms tick
 *
 * @param[in] genericBusEnvData
 * @param[in] sno
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_Tick(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData);

/***************************************************************************//**
 * LIN interrupt handler for time compare match
 *
 * @param[in] genericBusEnvData
 * @param[in] sno
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_TimerCmp(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData);


/***************************************************************************//**
 * LIN interrupt handler for finish transmit of data with DMA
 *
 * @param[in] genericBusEnvData
 * @param[in] sno
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_TxFinished(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData);

/***************************************************************************//**
 * LIN interrupt handler for finish transmit of data with DMA
 *
 * @param[in] genericBusEnvData
 * @param[in] sno
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_RxDMAFinished(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData);

/***************************************************************************//**
 * LIN interrupt handler for genericBusEnvData collision detection
 *
 * @param[in] genericBusEnvData
 * @param[in] sno
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_BusError(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData);

/* ****************************************************************************/
/* *************************** GLOBALE VARIABLES ******************************/
/* ****************************************************************************/
#if LINBUSIMP_EXT_IFFUN_STRCT_ACCESS == 1
BUS_LAYER_DATA const LinBusIf_sInterfaceFunctions_t LinBusImp_InterfaceFunctions =
#else
BUS_LAYER_DATA static const LinBusIf_sInterfaceFunctions_t LinBusImp_InterfaceFunctions =
#endif
{
  .InterfaceVersion  = LINBUS_INTERFACE_MODULE_API_VERSION,
  .Initialization    = &LinBusImp_Initialization,
  .GetSubInterface   = &LinBusImp_GetSubInterface,
  .Task              = &LinBusImp_Task,
  .GetMilliseconds   = &LinBusImp_GetMilliseconds,
  .Shutdown          = &LinBusImp_ShutdownBus,
  .GoToSleep         = &LinBusImp_GoToSleep,
  .GetState          = &LinBusImp_GetBusState,
  .ChangeBaudrate    = &LinBusImp_BusChangeBaudrate,
  .ChangeIdleTimeout = &LinBusImp_BusChangeIdleTimeout,
  .Restart           = &LinBusImp_RestartBus,
  .WakeupCluster     = &LinBusImp_WakeupCluster,
  .SendHeader        = &LinBusImp_SendHeader,
};

/* ****************************************************************************/
/* *********************** MODULE GLOBALE VARIABLES ***************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************* FUNCTION DEFINITIONS *****************************/
/* ****************************************************************************/

/* *****************************************************************************
 *
 * Call wrappers
 *
 ******************************************************************************/
/***************************************************************************//**
 * defgroup call_wrappers call wrappers
 * Wrapper methods for callback functions @{
 ******************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_CallBusErrorCallback(const loc_pBusEnvironmentData_t busEnvData, const LinBusIf_Error_t error)
{
  const LinBusIf_ErrorCbFun_t fun = busEnvData->Callbacks->Error;

  if (fun != LIN_NULL)
  {
    fun(busEnvData, &LinBusImp_InterfaceFunctions, error, busEnvData->CurrentFrameID, busEnvData->CallbackCtxData);
  }
  else{}
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_CallRestartCallback(loc_pBusEnvironmentData_t busEnvData)
{
  const LinBusIf_RestartCbFun_t fun = busEnvData->Callbacks->Restart;

  if (fun != LIN_NULL)
  {
    fun(busEnvData, &LinBusImp_InterfaceFunctions, busEnvData->CallbackCtxData);
  }
  else{}
}

#if LINBUSIMP_ENABLE_MEAS_DONE_CALLBACK == 1
/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eMeasAction_t loc_CallMeasDoneCallback(loc_pBusEnvironmentData_t busEnvData, LinBusImp_DividerValue_t cur_div)
{
  LinBusIf_eMeasAction_t rv = LinBusIf_MEASACT_UPDATE;
  const LinBusIf_MeasDoneCbFun_t fun = busEnvData->Callbacks->MeasDone;
  
  if (fun != LIN_NULL)
  {	
		// recalculate break length into measured baudrate scale. 
		LinBusIf_BreakLen_t meas_break_len = (LinBusIf_BreakLen_t)(((((uint32_t)(busEnvData->SyncMeasDivider))+1) * ((uint32_t)(busEnvData->CurrentBreakLength))) / (((uint32_t)(cur_div))+1));
				
    rv = fun(busEnvData, &LinBusImp_InterfaceFunctions,
			       loc_CalcBaudrate(busEnvData->ClockFreq, busEnvData->CurrentDivider),
             loc_CalcBaudrate(busEnvData->ClockFreq, busEnvData->SyncMeasDivider),
             loc_CalcBaudrate(busEnvData->ClockFreq, cur_div),
		         meas_break_len, busEnvData->CallbackCtxData);
  }
  else{}

  return (rv);
}
#endif

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eFrameIdAction_t loc_CallFrameIdReceivedCallback(loc_pBusEnvironmentData_t busEnvData, LinBusIf_FrameID_t frameId,
                                                                                       LinBusIf_ppData_t         rxtx_buf,   LinBusIf_pBufLength_t buf_len,
                                                                                       LinProtoIf_pCrcType_t     crcType)
{
  LinBusIf_eFrameIdAction_t rv = LinBusIf_PIDACT_IGNORE;
  const LinBusIf_FrameIdReceivedCbFun_t fun =  busEnvData->Callbacks->FrameIdReceived;
  
  if (fun != LIN_NULL)
  {
    rv = fun(busEnvData, &LinBusImp_InterfaceFunctions, frameId, rxtx_buf, buf_len, crcType, busEnvData->CallbackCtxData);
  }
  else{}

  return (rv);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eMsgAction_t loc_CallReceiveDoneCallback(loc_pBusEnvironmentData_t busEnvData, LinBusIf_FrameID_t frameId,
                                                                               LinBusIf_pData_t          rx_buf,     LinBusIf_BufLength_t  buf_len,
																																					     LinBusIf_Error_t          error)
{
  LinBusIf_eMsgAction_t ma = LinBusIf_MSGACT_CONTINUNE;
  const LinBusIf_ReceiveDoneCbFun_t fun = busEnvData->Callbacks->ReceiveDone;

  if (fun != LIN_NULL)
  {
    ma = fun(busEnvData, &LinBusImp_InterfaceFunctions, frameId, rx_buf, buf_len, error,busEnvData->CallbackCtxData);
  }
  else{}

  return (ma);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eMsgAction_t loc_CallTransmitDoneCallback(loc_pBusEnvironmentData_t busEnvData, LinBusIf_FrameID_t frameId,
                                                                                LinBusIf_pData_t          tx_buf,     LinBusIf_BufLength_t  buf_len,
																																								LinBusIf_Error_t          error)
{
  LinBusIf_eMsgAction_t ma = LinBusIf_MSGACT_CONTINUNE;
  const LinBusIf_TransmitDoneCbFun_t fun = busEnvData->Callbacks->TransmitDone;
  
  if (fun != LIN_NULL)
  {
    ma = fun(busEnvData, &LinBusImp_InterfaceFunctions, frameId, tx_buf, buf_len, error, busEnvData->CallbackCtxData);
  }
  else{}

  return (ma);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eIdleAction_t loc_CallIdleCallback(loc_pBusEnvironmentData_t busEnvData)
{
  LinBusIf_eIdleAction_t rv = LinBusIf_IDLEACT_CONTINUNE;
  const LinBusIf_IdleCbFun_t fun = busEnvData->Callbacks->Idle;
  
  if (fun != LIN_NULL)
  {
    rv = fun(busEnvData, &LinBusImp_InterfaceFunctions, busEnvData->CallbackCtxData);
  }
  else{}

  return (rv);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_CallWakeupCallback(loc_pBusEnvironmentData_t busEnvData)
{
  const LinBusIf_WakeupCbFun_t fun = busEnvData->Callbacks->Wakeup;
  
  if (fun != LIN_NULL)
  {
    fun(busEnvData, &LinBusImp_InterfaceFunctions, busEnvData->CallbackCtxData);
  }
  else{}
}


/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_eMsgAction_t loc_ReleaseBuffer(loc_pBusEnvironmentData_t busEnvData, LinBusIf_Error_t error)
{
  LinBusIf_eMsgAction_t ma = LinBusIf_MSGACT_CONTINUNE;
  
  /* give previous buffer back to upper level code */
  if (busEnvData->CurrentBuffer != LIN_NULL)
  {
    LinBusIf_FrameID_t tmpFrmID = busEnvData->CurrentFrameID;
      
    if (busEnvData->BusState == LinBusImp_STATE_RECEIVING_DATA)
    {      
      ma = loc_CallReceiveDoneCallback(busEnvData, tmpFrmID, busEnvData->CurrentBuffer, busEnvData->CurrentBufferLen, error);
    }
    else if (busEnvData->BusState == LinBusImp_STATE_TRANSMITING_DATA)
    {      
      ma = loc_CallTransmitDoneCallback(busEnvData, tmpFrmID, busEnvData->CurrentBuffer, busEnvData->CurrentBufferLen, error);
    }
    else{}

    busEnvData->CurrentBuffer = LIN_NULL;
    busEnvData->CurrentBufferLen = 0;
  }

  return (ma);
}

#if LINBUSIMP_ENABLE_SEND_HEADER == 1
/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_PID_t loc_GetPidForFrameId (LinBusIf_FrameID_t frameId)
{
  static const LinBusIf_PID_t Pids[LIN_MAX_FRAMEID_CNT] =
  {
    0x80u, 0xC1u, 0x42u, 0x03u, 0xC4u, 0x85u, 0x06u, 0x47u,
    0x08u, 0x49u, 0xCAu, 0x8Bu, 0x4Cu, 0x0Du, 0x8Eu, 0xCFu,
    0x50u, 0x11u, 0x92u, 0xD3u, 0x14u, 0x55u, 0xD6u, 0x97u,
    0xD8u, 0x99u, 0x1Au, 0x5Bu, 0x9Cu, 0xDDu, 0x5Eu, 0x1Fu,
    0x20u, 0x61u, 0xE2u, 0xA3u, 0x64u, 0x25u, 0xA6u, 0xE7u,
    0xA8u, 0xE9u, 0x6Au, 0x2Bu, 0xECu, 0xADu, 0x2Eu, 0x6Fu,
    0xF0u, 0xB1u, 0x32u, 0x73u, 0xB4u, 0xF5u, 0x76u, 0x37u,
    0x78u, 0x39u, 0xBAu, 0xFBu, 0x3Cu, 0x7Du, 0xFEu, 0xBFu
  };

  return(Pids[frameId & LIN_MAX_FRAMEID]);  
}

#endif

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusImp_DividerValue_t loc_CalcDivider(LinBusImp_ClockFreqValue_t clk, LinBusIf_Baudrate_t baudrate)
{
  return((LinBusImp_DividerValue_t) (((clk / baudrate) << 1u) - 1u));
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline LinBusIf_Baudrate_t loc_CalcBaudrate(LinBusImp_ClockFreqValue_t clk, LinBusImp_DividerValue_t divider)
{
  return((clk / ((LinBusImp_ClockFreqValue_t) divider + 1u)) << 1u);
}

/***************************************************************************//**
 * Not every divider value register can be reverse calculated to the real
 * baudrate.
 *
 * TODO: Find a better calculation or check method.
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline Lin_Bool_t loc_UpdateBaudrates(loc_pBusEnvironmentData_t busEnvData, LinBusIf_Baudrate_t new_baudrate, LinBusImp_ClockFreqValue_t freq,Lin_Bool_t try_only)
{
  Lin_Bool_t retVal = LIN_FALSE;
  
  if (new_baudrate != 0u)
  {
    LinBusImp_DividerValue_t new_div = loc_CalcDivider(freq, new_baudrate);

    if ( (busEnvData->Flags.CheckBaudrate == 0) || (new_baudrate == loc_CalcBaudrate(freq, new_div)) )
    {
      if (try_only == LIN_FALSE)
      {
       busEnvData->CurrentDivider  = new_div;
       /* start with the nominal baudrate ...
         * this will be updated once new measurements arrive. */
        busEnvData->SyncMeasDivider = new_div;
    
        LINSCI_BAUD_RATE = busEnvData->SyncMeasDivider;
      }
      retVal = LIN_TRUE;
    }
    else {}
  }
  else {}

  return(retVal);
}

/** @} */ // end of group helper_methods

/* *****************************************************************************
 *
 * Top-level state management methods
 *
 ******************************************************************************/
/***************************************************************************//**
 * @defgroup tl_state_management_methods Top level state management methods
 * These methods ensure correct SCI configuration, eventually invocation of
 * event handlers,triggering of processing and corresponding interrupt
 * configuration for a given genericBusEnvData state. @{
 ******************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_ChangeState_Offline(loc_pBusEnvironmentData_t busEnvData)
{
  busEnvData->BusState = LinBusImp_STATE_OFFLINE;
  busEnvData->CurrentFrameID = LIN_INVALID_FRAMEID;
  busEnvData->CurrentBreakLength = 0;
  
  LINSCI_LIN_CONTROL = (uint16_t) E_LINSCI_LIN_CONTROL_ABORT_RX | (uint16_t) E_LINSCI_LIN_CONTROL_ABORT_TX;
  
  (void) loc_ReleaseBuffer(busEnvData, Lin_BusIf_ERR_UNEXPECTED_SHUTDOWN);
  

  /* Bits meant to be 0 are commented out but
   * present in register definition */
  LINSCI_UART_CONFIG = 0u 
//                       | E_LINSCI_UART_CONFIG_RE    
//                       | E_LINSCI_UART_CONFIG_TE    
                       | (uint16_t) V_LINSCI_UART_CONFIG_PARITY(0u)
//                       | E_LINSCI_UART_CONFIG_STOP           
                       | (uint16_t) E_LINSCI_UART_CONFIG_MASK_BRK_ERR    
//                       | E_LINSCI_UART_CONFIG_CLK_SRC 
                       | (uint16_t) E_LINSCI_UART_CONFIG_TXD_MASK
                       | (uint16_t) E_LINSCI_UART_CONFIG_TXD_VAL
                       | (uint16_t) E_LINSCI_UART_CONFIG_RXD_MASK
                       | (uint16_t) E_LINSCI_UART_CONFIG_RXD_VAL
                       | (uint16_t) V_LINSCI_UART_CONFIG_DEBOUNCE(LINBUSIMP_DEFAULT_DEBOUNCER_VALUE);  
  
  LINSCI_LIN_CONFIG = 0u;

  /* Bits meant to be 0 are commented out but
   * present in register definition */
  LINSCI_TIMER = 0u;
//                 | E_LINSCI_TIMER_ENABLE
//                 | E_LINSCI_TIMER_CLK_SRC
//                 | V_LINSCI_TIMER_PREPARE(0)
//                 | E_LINSCI_TIMER_BREAK_RESTART
//                 | E_LINSCI_TIMER_TXD_TIMEOUT_E;  

  LINSCI_ERROR = 0x03FFu; // clear all error flags

  busEnvData->SleepCount = 0u;
    
  LINSCI_IRQ_MASK = 0x0000u;
  LINSCI_IRQ_STATUS = 0xFFFFu;  

}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_ChangeState_Sleep(loc_pBusEnvironmentData_t busEnvData, Lin_Bool_t state_check)
{ 
  Lin_Bool_t gotosleep = LIN_TRUE;  
  
  istate_t istate = __get_interrupt_state();
  __disable_interrupt();
  
  LINSCI_IRQ_STATUS = (uint16_t) E_LINSCI_IRQ_STATUS_RXD_FALLING; // clear falling edge IRQ flag
  
  if ( state_check == LIN_TRUE )
  {
    if ( busEnvData->BusState != LinBusImp_STATE_SYNCING ) { gotosleep = LIN_FALSE; }
    else {}
  }
  else {}
          
  if (LINSCI_STATUS_bit.rx_header_state != 0 || LINSCI_IRQ_STATUS_bit.rxd_falling != 0) { gotosleep = LIN_FALSE; } // go into sleep only if no pending header processing  
  else {}
    
      
  if (gotosleep == LIN_TRUE) 
  { 
    LINSCI_IRQ_MASK = 0x0000u;
    LINSCI_IRQ_STATUS = 0xFFFFu;

    LINSCI_LIN_CONTROL = (uint16_t) E_LINSCI_LIN_CONTROL_ABORT_RX | (uint16_t) E_LINSCI_LIN_CONTROL_ABORT_TX;
       
    busEnvData->BusState = LinBusImp_STATE_SLEEP;
    busEnvData->CurrentFrameID = LIN_INVALID_FRAMEID;
    busEnvData->CurrentBreakLength = 0;

    __set_interrupt_state(istate);
    
    /* Bits meant to be 0 are commented out but
    * present in register definition */
    LINSCI_UART_CONFIG = 0u 
//                       | E_LINSCI_UART_CONFIG_RE    
//                       | E_LINSCI_UART_CONFIG_TE    
                       | (uint16_t) V_LINSCI_UART_CONFIG_PARITY(0u)
//                       | E_LINSCI_UART_CONFIG_STOP           
                       | (uint16_t) E_LINSCI_UART_CONFIG_MASK_BRK_ERR    
//                       | E_LINSCI_UART_CONFIG_CLK_SRC 
//                       | E_LINSCI_UART_CONFIG_TXD_MASK
                       | (uint16_t) E_LINSCI_UART_CONFIG_TXD_VAL
//                       | E_LINSCI_UART_CONFIG_RXD_MASK
                       | (uint16_t) E_LINSCI_UART_CONFIG_RXD_VAL
                       | (uint16_t) busEnvData->DebouncerValue;  
  
    LINSCI_LIN_CONFIG = 0;

    /* Bits meant to be 0 are commented out but
    * present in register definition */
    LINSCI_TIMER = 0u
//               | E_LINSCI_TIMER_ENABLE
//               | E_LINSCI_TIMER_CLK_SRC
               | (uint16_t) V_LINSCI_TIMER_PREPARE(0u)
               | (uint16_t) E_LINSCI_TIMER_BREAK_RESTART
               | (uint16_t) E_LINSCI_TIMER_TXD_TIMEOUT_E;

    LINSCI_ERROR = 0x03FFu; // clear all error flags

    busEnvData->SleepCount = 0u; 
  
    LINSCI_IRQ_MASK = (uint16_t) E_LINSCI_IRQ_STATUS_RXD_FALLING | (uint16_t) E_LINSCI_IRQ_STATUS_TICK_1MS;

  }
  else // skip go to sleep.
  {
    __set_interrupt_state(istate);
  }  
}

/***************************************************************************//**
 * TODO: Explain magic number and replace with define.
 ******************************************************************************/
BUS_LAYER_CODE static void loc_ChangeState_Syncing(loc_pBusEnvironmentData_t busEnvData, LinBusIf_Error_t error)
{
  LINSCI_LIN_CONTROL = (uint16_t) E_LINSCI_LIN_CONTROL_ABORT_RX | (uint16_t) E_LINSCI_LIN_CONTROL_ABORT_TX;
  
  if (loc_ReleaseBuffer(busEnvData, error) == LinBusIf_MSGACT_SLEEP)
  {
    loc_ChangeState_Sleep(busEnvData,LIN_FALSE);
  }
  else
  {
    busEnvData->BusState = LinBusImp_STATE_SYNCING;
    busEnvData->CurrentFrameID = LIN_INVALID_FRAMEID;
	busEnvData->CurrentBreakLength = 0;

    /* Disable SCI IRQs and clear all pending IRQs. */
    LINSCI_IRQ_MASK = 0x0000u;
    LINSCI_IRQ_STATUS = 0xFFFFu;
 
    LINSCI_BAUD_RATE = busEnvData->SyncMeasDivider;
                 
      /* Bits meant to be 0 are commented out but
       * present in register definition */
    LINSCI_UART_CONFIG = 0u
                       | (uint16_t) E_LINSCI_UART_CONFIG_RE    
                       | (uint16_t) E_LINSCI_UART_CONFIG_TE    
                       | (uint16_t) V_LINSCI_UART_CONFIG_PARITY(0u)
  //                     | E_LINSCI_UART_CONFIG_STOP
                       | (uint16_t) E_LINSCI_UART_CONFIG_MASK_BRK_ERR    
  //                     | E_LINSCI_UART_CONFIG_CLK_SRC
  //                     | E_LINSCI_UART_CONFIG_TXD_MASK
                       | (uint16_t) E_LINSCI_UART_CONFIG_TXD_VAL
  //                     | E_LINSCI_UART_CONFIG_RXD_MASK
                       | (uint16_t) E_LINSCI_UART_CONFIG_RXD_VAL
                       | (uint16_t) busEnvData->DebouncerValue;  

    LINSCI_LIN_CONFIG = 0u 
                      | (uint16_t) E_LINSCI_LIN_CONFIG_AUTOBAUD
                      | (uint16_t) E_LINSCI_LIN_CONFIG_COLLISION
                      | (uint16_t) E_LINSCI_LIN_CONFIG_BREAK_THD
                      | (uint16_t) E_LINSCI_LIN_CONFIG_HEADER_PROCESSING
                      | (uint16_t) E_LINSCI_LIN_CONFIG_FILTER_PID
                      | (uint16_t) E_LINSCI_LIN_CONFIG_CHKSUM_ENABLE
  //                    | E_LINSCI_LIN_CONFIG_CHKSUM_TYPE
                      | (uint16_t) E_LINSCI_LIN_CONFIG_CHKSUM_INSERT
                      | (uint16_t) E_LINSCI_LIN_CONFIG_SUPPRESS_TX_FB
                      | (uint16_t) E_LINSCI_LIN_CONFIG_SYNC_VALIDATION
                      | (uint16_t) E_LINSCI_LIN_CONFIG_DMA_RX_SKIP_LAST;  
  
    /* TODO: Explain magic number and replace with define */
    LINSCI_TIMER = 0u
  //               | E_LINSCI_TIMER_ENABLE   // will be enabled on break restart.
  //               | E_LINSCI_TIMER_CLK_SRC
								 | (uint16_t) V_LINSCI_TIMER_PREPARE(2u)
								 | (uint16_t) E_LINSCI_TIMER_BREAK_RESTART
                 | (uint16_t) E_LINSCI_TIMER_TXD_TIMEOUT_E
							   | (uint16_t) E_LINSCI_TIMER_CAPTURE_E
								 | (uint16_t) E_LINSCI_TIMER_PID_CMP_VAL_E;

    LINSCI_TIMER_COMPARE = 0u;
	
	  LINSCI_LIN_CONTROL = E_LINSCI_LIN_CONTROL_RX_SLEEP | E_LINSCI_LIN_CONTROL_ABORT_RX;

    LINSCI_ERROR = 0x03FFu; // clear all error flags

    busEnvData->SleepCount = busEnvData->SleepTimeout;


    /* Enable SCI IRQs */
    LINSCI_IRQ_MASK = (uint16_t) ( ( busEnvData->IgnoreTimeoutsCount == LINBUSIMP_IGNORE_MSGTOUT_NEVER ) ? E_LINSCI_IRQ_STATUS_SCI_TIMER_CMP : 0 ) |
  #if LINBUSIMP_ENABLE_MEAS_DONE_CALLBACK == 1
                      ( (busEnvData->Flags.CallMeasDone == 1) ? (uint16_t) E_LINSCI_IRQ_STATUS_SYNC_EVT  : 0u ) |
  #endif
                      (uint16_t) E_LINSCI_IRQ_STATUS_BUS_ERR           |
                      (uint16_t) E_LINSCI_IRQ_STATUS_RECEIVER_ERR      |
                      (uint16_t) E_LINSCI_IRQ_STATUS_HEADER_ERR        |
                      (uint16_t) E_LINSCI_IRQ_STATUS_PID_EVT           |
                      (uint16_t) E_LINSCI_IRQ_STATUS_RX_DMA_FINISHED   |
                      (uint16_t) E_LINSCI_IRQ_STATUS_TX_FINISH_EVT     |
                      (uint16_t) E_LINSCI_IRQ_STATUS_TICK_1MS          |
                      (uint16_t) 0u ;
  }  
}

/***************************************************************************//**
 * TODO: Explain magic number and replace with define.
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_ChangeState_ReceivingData(loc_pBusEnvironmentData_t busEnvData, LinBusIf_PID_t pid,uint16_t header_time, LinProtoIf_eCrcType_t crcType)
{
  busEnvData->BusState = LinBusImp_STATE_RECEIVING_DATA;

  LINSCI_TIMER_COMPARE = ( ( ( (uint16_t) busEnvData->CurrentBufferLen ) + 1u ) * 224u ) + header_time + LINBUSIMP_DEFAULT_TIMER_MESSAGE_TIMEOUT_EXT; // ( (number_of_bytes + crc) * 1.4 * 10 bit * 16 times baudrate ) + header_time + x bit times "master jitter";
  
  if ( crcType == LinBusIf_CRC_Extended ) // in general hardware is setup for classic checksum. Add PID to checksum register for extended checksum frames
  {
    LINSCI_LIN_CHECKSUM = 0x0100u | pid;
  }
  
  LINSCI_IRQ_STATUS = (uint16_t) E_LINSCI_IRQ_STATUS_RX_DMA_FINISHED   |  // clear DMA IRQ status (if any)
                      (uint16_t) E_LINSCI_IRQ_STATUS_TX_FINISH_EVT     ;    

  LINSCI_DMA_RX_ADDRESS      = (uint16_t)  (((Lin_uintptr_t) busEnvData->CurrentBuffer) & 0x0000FFFFu);
  //LINSCI_DMA_RX_ADDRESS_HIGH = (uint16_t) ((((Lin_uintptr_t) busEnvData->CurrentBuffer) & 0xFFFF0000u) >> 16u);
  LINSCI_DMA_RX_LENGTH = ((uint16_t) busEnvData->CurrentBufferLen) + 1;  
}

/***************************************************************************//**
 * TODO: Explain magic number and replace with define.
 ******************************************************************************/
BUS_LAYER_CODE static inline void loc_ChangeState_TransmitingData(loc_pBusEnvironmentData_t busEnvData, LinBusIf_FrameID_t pid,LinProtoIf_eCrcType_t crcType)
{    
  busEnvData->BusState = LinBusImp_STATE_TRANSMITING_DATA;

  LINSCI_TIMER = 0u
//                 | E_LINSCI_TIMER_ENABLE
//                 | E_LINSCI_TIMER_CLK_SRC
                 | (uint16_t) V_LINSCI_TIMER_PREPARE(2u)
                 | (uint16_t) E_LINSCI_TIMER_BREAK_RESTART
                 | (uint16_t) E_LINSCI_TIMER_TXD_TIMEOUT_E 
							   | (uint16_t) E_LINSCI_TIMER_CAPTURE_E
								 | (uint16_t) E_LINSCI_TIMER_PID_CMP_VAL_E;
  
  if ( crcType == LinBusIf_CRC_Extended ) // in general hardware is setup for classic checksum. Add PID to checksum register for extended checksum frames
  {
    LINSCI_LIN_CHECKSUM = 0x0100u | pid;
  }
  
  LINSCI_LIN_CONTROL = E_LINSCI_LIN_CONTROL_TX_CHKSUM;

  LINSCI_IRQ_STATUS = (uint16_t) E_LINSCI_IRQ_STATUS_RX_DMA_FINISHED   |  // clear DMA IRQ status 
                      (uint16_t) E_LINSCI_IRQ_STATUS_TX_DMA_FINISHED   |                        
                      (uint16_t) E_LINSCI_IRQ_STATUS_TX_FINISH_EVT     ;    

  LINSCI_DMA_TX_ADDRESS      = (uint16_t)  (((Lin_uintptr_t) busEnvData->CurrentBuffer) & 0x0000FFFFu);
  //LINSCI_DMA_TX_ADDRESS_HIGH = (uint16_t) ((((Lin_uintptr_t) busEnvData->CurrentBuffer) & 0xFFFF0000u) >> 16u);
	
  istate_t istate = __get_interrupt_state();
  __disable_interrupt();
	
  if ( ( busEnvData->Flags.DetectPostPIDCollisions == 1 ) && ( LINSCI_STATUS_bit.rx_post_pid_edge == 1 ) )
  {
    __set_interrupt_state(istate);
    loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_BUS_COLLISION);
    loc_ChangeState_Syncing(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_BUS_COLLISION);
  }	
  else
  {
    LINSCI_DMA_TX_LENGTH = (uint16_t) busEnvData->CurrentBufferLen;
    __set_interrupt_state(istate);  
  }
}


/* *****************************************************************************
 *
 * Interrupt handlers
 *
 ******************************************************************************/
/***************************************************************************//**
 * @defgroup interrupt_handler Interrupt handler
 * Interrupt handler @{
 ******************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_Tick(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;  
  
  ++(busEnvData->TickCount);
  
  if (busEnvData->SleepCount > 0u)
  {
    --(busEnvData->SleepCount);
    if (busEnvData->SleepCount == 0u)  
    {
      LinBusIf_eIdleAction_t rv = loc_CallIdleCallback(busEnvData);
      switch (rv)
      {        
        case LinBusIf_IDLEACT_SLEEP:
        {
          loc_ChangeState_Sleep(busEnvData,LIN_TRUE); // goto sleep... 
          break;
        }
        
        case LinBusIf_IDLEACT_CONTINUNE:
        default:                             
        { break; }
      }      
    }
    else {}
  }
  else {}
}

/***************************************************************************//**
 * TODO: Explain magic number and replace with define.
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_TimerCmp(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;
  LinBusIf_Error_t error = LinBusIf_ERROR_NO_ERROR;
  
  Lin_uint16_t remain_len = LINSCI_DMA_RX_LENGTH; // remember rx length before aborting SCI communication.
  LINSCI_LIN_CONTROL  = (uint16_t) E_LINSCI_LIN_CONTROL_ABORT_RX | (uint16_t) E_LINSCI_LIN_CONTROL_ABORT_TX; // stop processing immediately  ...
  
  if (busEnvData->BusState == LinBusImp_STATE_SYNCING)
  {
    error = LinBusIf_ERR_HEADER_TIMEOUT;
    loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_HEADER_TIMEOUT);
    loc_ChangeState_Syncing(busEnvData, error);
  }

  if (busEnvData->BusState == LinBusImp_STATE_RECEIVING_DATA)
  {      
    if (remain_len == (((uint16_t) busEnvData->CurrentBufferLen) + 1u ) ) // use distinct error code, if no data has been received at all. 
    {
      error = LinBusIf_ERR_MESSAGE_TIMEOUT_NO_DATA;
      loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_MESSAGE_TIMEOUT_NO_DATA);
    }
    else
    {
      error = LinBusIf_ERR_MESSAGE_TIMEOUT;
      loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_MESSAGE_TIMEOUT);
    }
    
    loc_ChangeState_Syncing(busEnvData, error);
  }        
}


/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_PIDReceived(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;
   
  uint16_t header_time  = LINSCI_TIMER_COUNTER;  
  LINSCI_TIMER_COMPARE  = 0u;

  LinBusIf_FrameID_t frame_pid = (LinBusIf_FrameID_t) LINSCI_LIN_PID; // hw will rise this event only if PID is valid, so no further checks are required
  LinBusIf_FrameID_t frame_id  = frame_pid & 0x3fu; 
  
#if LINBUSIMP_ENABLE_AUTO_UPDATE_BAUDRATE == 1
    
#if LINBUSIMP_ENABLE_MEAS_DONE_CALLBACK == 1
    if (busEnvData->Flags.CallMeasDone == 0)
    {
#endif    
      busEnvData->SyncMeasDivider = LINSCI_TBIT2_LENGTH; // store current (auto-updated) baudrate
#if LINBUSIMP_ENABLE_MEAS_DONE_CALLBACK == 1
    }
    else {}
#endif    
    
#endif

  if ( busEnvData->IgnoreTimeoutsCount != LINBUSIMP_IGNORE_MSGTOUT_NEVER && 
       busEnvData->IgnoreTimeoutsCount != LINBUSIMP_IGNORE_MSGTOUT_FOR_EVER )
  {
    --busEnvData->IgnoreTimeoutsCount;
  }
  else {}

  LinBusIf_Data_t *rxtx_buf_ptr         = LIN_NULL;
  LinBusIf_BufLength_t rxtx_buf_len     = 0;
  LinProtoIf_eCrcType_t rxtx_crc_type   = LinBusIf_CRC_Extended;
  LinBusIf_eFrameIdAction_t rxtx_rv;
                             
  rxtx_rv = loc_CallFrameIdReceivedCallback(busEnvData, frame_id, &rxtx_buf_ptr, &rxtx_buf_len, &rxtx_crc_type);
    
  switch (rxtx_rv)
  {
    case LinBusIf_PIDACT_RECEIVE:
    case LinBusIf_PIDACT_TRANSMIT:
    {
      if ((rxtx_buf_ptr != LIN_NULL) && (rxtx_buf_len != 0u) && (rxtx_buf_len <= LINBUSIMP_DEFAULT_MAX_DATA_LEN) )
      {
        busEnvData->CurrentFrameID = frame_id;
        busEnvData->CurrentBuffer = rxtx_buf_ptr;
        busEnvData->CurrentBufferLen = rxtx_buf_len;
        
        if(rxtx_rv == LinBusIf_PIDACT_RECEIVE)
        {
          loc_ChangeState_ReceivingData(busEnvData,frame_pid,header_time,rxtx_crc_type);
        }
        else /* "rxtx_rv == LinBusIf_PIDACT_TRANSMIT" implied by switch-case. */
        {
          loc_ChangeState_TransmitingData(busEnvData,frame_pid,rxtx_crc_type);
        }
      }
      else
      {
        loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_BUFFER_ERROR);
        loc_ChangeState_Syncing(busEnvData, LinBusIf_ERROR_NO_ERROR);
      }
      break;
    }

    case LinBusIf_PIDACT_IGNORE:
    default:
    {
      loc_ChangeState_Syncing(busEnvData, LinBusIf_ERROR_NO_ERROR);
      break;
    }
  };             
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_TxFinished(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;
 
  if (busEnvData->BusState == LinBusImp_STATE_TRANSMITING_DATA)
  {         
    if (loc_ReleaseBuffer(busEnvData, LinBusIf_ERROR_NO_ERROR) == LinBusIf_MSGACT_SLEEP)
    {
      loc_ChangeState_Sleep(busEnvData,LIN_FALSE);
    }
    else
    {          
      loc_ChangeState_Syncing(busEnvData, LinBusIf_ERROR_NO_ERROR);
    }
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_RxDMAFinished(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;
  
  if (busEnvData->BusState == LinBusImp_STATE_RECEIVING_DATA)
  {      
    if(LINSCI_STATUS_bit.rx_chksum_valid == 0u)
    {
      loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t)  LinBusIf_ERR_CRC);
      
      loc_ChangeState_Syncing(busEnvData, LinBusIf_ERR_CRC);
    }
    else
    {
      if (loc_ReleaseBuffer(busEnvData, LinBusIf_ERROR_NO_ERROR) == LinBusIf_MSGACT_SLEEP)
      {
        loc_ChangeState_Sleep(busEnvData,LIN_FALSE);
      }
      else
      {      
        loc_ChangeState_Syncing(busEnvData, LinBusIf_ERROR_NO_ERROR);
      }
    }    
  }
}

/***************************************************************************//**
 * 'E_LINSCI_ERROR_CONCURRENT_BRK' detected by hardware is not announced to
 * higher levels or user application.
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_BusError(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;
  LinBusIf_Error_t error = LinBusIf_ERROR_NO_ERROR;
    
  if (LINSCI_ERROR_bit.concurrent_brk == 1u) 
  {
    LINSCI_ERROR = E_LINSCI_ERROR_CONCURRENT_BRK;  
    // in this LIN driver it is actually not an error case ... just release buffers and switch to "syncing" state
  }

  if (LINSCI_ERROR_bit.txd_timeout == 1u) 
  {
    LINSCI_ERROR = E_LINSCI_ERROR_TXD_TIMEOUT;  
    error = LinBusIf_ERR_HW_FAIL;
    loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_HW_FAIL);    
  }

  if (LINSCI_ERROR_bit.bus_collision == 1u) 
  {
    LINSCI_ERROR = E_LINSCI_ERROR_BUS_COLLISION;  
    error = LinBusIf_ERR_BUS_COLLISION;
    loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_BUS_COLLISION);    
  }
  
  loc_ChangeState_Syncing(busEnvData, error);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_HeaderError(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;
  LinBusIf_Error_t error = LinBusIf_ERROR_NO_ERROR;
    
  if (LINSCI_ERROR_bit.pid_parity_err == 1u) 
  {
    LINSCI_ERROR = E_LINSCI_ERROR_PID_PARITY_ERR;  
    error = LinBusIf_ERR_PID_PARITY;
    loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_PID_PARITY);    
  }
  
  if ((LINSCI_ERROR_bit.sync_invalid == 1u) || (LINSCI_ERROR_bit.sync_ov == 1u) || (LINSCI_ERROR_bit.sync_err == 1u)) 
  {
    LINSCI_ERROR = (uint16_t) E_LINSCI_ERROR_SYNC_INVALID | (uint16_t) E_LINSCI_ERROR_SYNC_OV | (uint16_t) E_LINSCI_ERROR_SYNC_ERR;  
    error = LinBusIf_ERR_SYNC_FAIL;
    loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_SYNC_FAIL);    
  }
  
  loc_ChangeState_Syncing(busEnvData, error);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_ReceiverError(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;
  LinBusIf_Error_t error = LinBusIf_ERROR_NO_ERROR;
  
  if (LINSCI_ERROR_bit.frame_err == 1u) 
  {
    LINSCI_ERROR = E_LINSCI_ERROR_FRAME_ERR;  
    
    if ((busEnvData->BusState == LinBusImp_STATE_RECEIVING_DATA) || (busEnvData->BusState == LinBusImp_STATE_TRANSMITING_DATA))
    {
      error = LinBusIf_ERR_FRAMING_ERROR;
      loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_FRAMING_ERROR);    
    }
    else {}
  }

  if (LINSCI_ERROR_bit.parity_err == 1u) 
  {
    LINSCI_ERROR = E_LINSCI_ERROR_PARITY_ERR;  
    error = LinBusIf_ERR_HW_FAIL;
    loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_HW_FAIL); // since LIN communication does not define byte-parity this error should never occure. 
  }

  if (LINSCI_ERROR_bit.rx_overflow == 1u) 
  {
    LINSCI_ERROR = E_LINSCI_ERROR_RX_OVERFLOW;  
    error = LinBusIf_ERR_RX_OVERRUN;
    loc_CallBusErrorCallback(busEnvData, (LinBusIf_Error_t) LinBusIf_ERR_RX_OVERRUN); // typically will not occur within message body, since data storage is DMA handles. May occure only if PID processing takes very long (> 2 byte times ) and master sends continous byte stream 
  }
  
  loc_ChangeState_Syncing(busEnvData, error);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_RxDFalling(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;
  
  if (busEnvData->BusState == LinBusImp_STATE_SLEEP)
  {
    loc_CallWakeupCallback(busEnvData);
    
    loc_ChangeState_Syncing(busEnvData, LinBusIf_ERROR_NO_ERROR);
  }     
}

#if LINBUSIMP_ENABLE_MEAS_DONE_CALLBACK == 1
/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_InteruptHandler_SyncEvent(LinBusImp_IrqNumber_t sno, LinBusIf_pGenericEnvData_t envData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t *) envData;
	
  /* get captured timer value */
  busEnvData->CurrentBreakLength = LINSCI_TIMER_CAPTURE;
	
  LinBusImp_DividerValue_t cur_div = LINSCI_TBIT2_LENGTH;
     
  LinBusIf_eMeasAction_t rv = loc_CallMeasDoneCallback(busEnvData, cur_div);
  
  switch (rv)
  {
    /* use measurement as the initial baudrate for subsequent messages */
    case LinBusIf_MEASACT_UPDATE:
    {
      busEnvData->SyncMeasDivider = cur_div;
      break;
    }
    
    /* use measurement as the new nominal baudrate (recalc timings) */
    case LinBusIf_MEASACT_UPDATE_NOMINAL:
    {
      (void) loc_UpdateBaudrates(busEnvData, loc_CalcBaudrate(busEnvData->ClockFreq, cur_div), busEnvData->ClockFreq,LIN_FALSE);
      break;
    }
    
    /* reset baudrate to default */
    case LinBusIf_MEASACT_RESET:
    {
       busEnvData->SyncMeasDivider = busEnvData->CurrentDivider;
       LINSCI_BAUD_RATE = busEnvData->CurrentDivider;       
       break;
    }
    
    /* ignore this measurement, use previous measured or set value */
    case LinBusIf_MEASACT_DISMISS:
    default:
    {
      LINSCI_BAUD_RATE = busEnvData->SyncMeasDivider;      
      break;
    }
  } 
}

#endif

/** @} */ // end of group interrupt_handler

/* *****************************************************************************
 *
 * Main API methods
 *
 ******************************************************************************/

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE Lin_Bool_t LinBusImp_Initialization(LinBusIf_pGenericEnvData_t     genericBusEnvData, LinBusIf_EnvDataSze_t         busEnvDataSze,
                                                   LinBusIf_cpCallbackFunctions_t busCbFuns,         LinBusIf_pGenericCbCtxData_t  genericBusCbCtxData,
                                                   LinBusIf_Baudrate_t            baudrate,          LinBusIf_pGenericImpCfgData_t genericImpCfgData)
{
  Lin_Bool_t rv = LIN_FALSE;

  loc_pBusEnvironmentData_t busEnvData  = (loc_sBusEnvironmentData_t*) genericBusEnvData;
  LinBusImp_pCfgData_t busImpCfgData    = (LinBusImp_pCfgData_t) genericImpCfgData;
  	
  if ((genericBusEnvData != LIN_NULL) &&
      (busEnvDataSze >= sizeof(loc_sBusEnvironmentData_t)) &&
      (genericImpCfgData != LIN_NULL) &&
      (busImpCfgData->Version == LINBUSIMP_CONFIG_DATA_VERSION) && 
      (busCbFuns != LIN_NULL) &&	
      (busCbFuns->CallbackVersion == LINBUS_INTERFACE_MODULE_API_VERSION))
  {
	LinBusIf_cpCallbackFunctions_t cb = busCbFuns;
	LinBusImp_ClockFreqValue_t freq   = busImpCfgData->ClockFrequency;
		
    /* setup environment data */
      
    // busEnvData->SciEnvironmentData will be initialized later in interrupt initialization      
      
    busEnvData->Callbacks = cb;
    busEnvData->CallbackCtxData = genericBusCbCtxData;
      
    busEnvData->BusState = LinBusImp_STATE_OFFLINE;
      
    busEnvData->CurrentFrameID = LIN_INVALID_FRAMEID;
    busEnvData->CurrentBuffer = LIN_NULL;
    busEnvData->CurrentBufferLen = 0U;

    busEnvData->ClockFreq = freq;

    // busEnvData->SyncMeasDivider and busEnvData->CurrentDivider will be set in loc_UpdateBaudrates()
      
    busEnvData->CurrentBreakLength = 0;
		
    busEnvData->Flags.CallMeasDone = busImpCfgData->ConfigFlags.CallMeasDone;
    busEnvData->Flags.CheckBaudrate = busImpCfgData->ConfigFlags.CheckBaudrate;
    busEnvData->Flags.DetectPostPIDCollisions = busImpCfgData->ConfigFlags.DetectPostPIDCollisions;        
    busEnvData->SendHeaderBreakLen = busImpCfgData->SendHeaderBreakLen;
    busEnvData->DebouncerValue = V_LINSCI_UART_CONFIG_DEBOUNCE(busImpCfgData->DebouncerValue);
    busEnvData->IgnoreTimeoutsCount = busImpCfgData->IgnoreMsgTimeouts;

    busEnvData->TickCount = 0U;              
    busEnvData->SleepTimeout = ((LinBusIf_TimeOut_t) (LINBUSIMP_DEFAULT_IDLE_TIMEOUT));
    busEnvData->SleepCount = 0U;
          
                
    if(loc_UpdateBaudrates(genericBusEnvData, baudrate, freq,LIN_FALSE) == LIN_TRUE)  
    {      
                            
      /* Register callbacks */
      
      linsci_InterruptInitialisation(&vic_InterfaceFunctions,&busEnvData->SciEnvironmentData, busEnvData);
      
      linsci_InterruptRegisterCallback(linsci_IRQ_SCI_TIMER_CMP,          (LinBusImp_SCICallback_t)LinBusImp_InteruptHandler_TimerCmp);
      
      linsci_InterruptRegisterCallback(linsci_IRQ_BUS_ERR,                (LinBusImp_SCICallback_t)LinBusImp_InteruptHandler_BusError);
      linsci_InterruptRegisterCallback(linsci_IRQ_RECEIVER_ERR,           (LinBusImp_SCICallback_t)LinBusImp_InteruptHandler_ReceiverError);
      linsci_InterruptRegisterCallback(linsci_IRQ_HEADER_ERR,             (LinBusImp_SCICallback_t)LinBusImp_InteruptHandler_HeaderError);
      
      linsci_InterruptRegisterCallback(linsci_IRQ_PID_EVT,                (LinBusImp_SCICallback_t)LinBusImp_InteruptHandler_PIDReceived);
      linsci_InterruptRegisterCallback(linsci_IRQ_RX_DMA_FINISHED,        (LinBusImp_SCICallback_t)LinBusImp_InteruptHandler_RxDMAFinished);
      linsci_InterruptRegisterCallback(linsci_IRQ_TX_FINISH_EVT,          (LinBusImp_SCICallback_t)LinBusImp_InteruptHandler_TxFinished);
      
      linsci_InterruptRegisterCallback(linsci_IRQ_RXD_FALLING,            (LinBusImp_SCICallback_t) LinBusImp_InteruptHandler_RxDFalling);
      linsci_InterruptRegisterCallback(linsci_IRQ_TICK_1MS,               (LinBusImp_SCICallback_t) LinBusImp_InteruptHandler_Tick);
      
#if LINBUSIMP_ENABLE_MEAS_DONE_CALLBACK == 1
      linsci_InterruptRegisterCallback(linsci_IRQ_SYNC_EVT,               (LinBusImp_SCICallback_t) LinBusImp_InteruptHandler_SyncEvent);
#endif 
      LinBusImp_RestartBus(genericBusEnvData);
      
      rv = LIN_TRUE;
    }
    else
    {
      loc_CallBusErrorCallback((loc_pBusEnvironmentData_t) genericBusEnvData, (LinBusIf_Error_t) LinBusIf_ERR_INIT);
    }
  }
  else{}

  return (rv);
}

/***************************************************************************//**
 * TODO: Document exactly the meaning of return value LIN_TRUE and LIN_FALSE.
 ******************************************************************************/
BUS_LAYER_CODE Lin_Bool_t LinBusImp_GetSubInterface(LinBusIf_pGenericEnvData_t genericBusEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr)
{  
  Lin_Bool_t retVal = LIN_FALSE;
  
  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE LinBusIf_Tick_t LinBusImp_GetMilliseconds(LinBusIf_pGenericEnvData_t genericBusEnvData)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t*) genericBusEnvData;
  Lin_uint32_t returnValue = 0xFFFFFFFFu;

  if (genericBusEnvData != LIN_NULL)
  {
    returnValue = busEnvData->TickCount;
  }
  else {}

  return (returnValue);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_RestartBus(LinBusIf_pGenericEnvData_t genericBusEnvData)
{
  if (genericBusEnvData != LIN_NULL)
  {
    loc_CallRestartCallback((loc_pBusEnvironmentData_t) genericBusEnvData);
    loc_ChangeState_Syncing((loc_pBusEnvironmentData_t) genericBusEnvData, LinBusIf_ERROR_NO_ERROR);
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_ShutdownBus(LinBusIf_pGenericEnvData_t genericBusEnvData)
{
  if (genericBusEnvData != LIN_NULL)
  {
    loc_ChangeState_Offline((loc_pBusEnvironmentData_t) genericBusEnvData);
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_GoToSleep(LinBusIf_pGenericEnvData_t genericBusEnvData)
{
  if (genericBusEnvData != LIN_NULL)
  {
    loc_ChangeState_Sleep((loc_pBusEnvironmentData_t) genericBusEnvData,LIN_TRUE);
  }
}

/***************************************************************************//**
 * 
 ******************************************************************************/
BUS_LAYER_CODE LinBusIf_eState_t LinBusImp_GetBusState(LinBusIf_pGenericEnvData_t genericBusEnvData)
{  
  LinBusIf_eState_t rv = LinBusImp_STATE_OFFLINE;
  
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t*) genericBusEnvData;
  if (busEnvData != LIN_NULL)
  {
    rv = busEnvData->BusState; // reuses interface based state representation
  }
  else {}
  
  return (rv);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE Lin_Bool_t LinBusImp_BusChangeBaudrate(LinBusIf_pGenericEnvData_t genericBusEnvData, LinBusIf_Baudrate_t new_baudrate,Lin_Bool_t try_only)
{
  Lin_Bool_t retVal = LIN_FALSE;
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t*) genericBusEnvData;

  if (genericBusEnvData != LIN_NULL)
  {
    retVal = loc_UpdateBaudrates(genericBusEnvData, new_baudrate, busEnvData->ClockFreq, try_only);
  }

  return(retVal);
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_BusChangeIdleTimeout(LinBusIf_pGenericEnvData_t genericBusEnvData, LinBusIf_TimeOut_t timeout)
{
  loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t*) genericBusEnvData;

  if (genericBusEnvData != LIN_NULL)
  {
    busEnvData->SleepTimeout = timeout;  
    
    if (busEnvData->SleepCount > timeout)
    {
      busEnvData->SleepCount = timeout;
    }
    else {}
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_WakeupCluster(LinBusIf_pGenericEnvData_t genericBusEnvData)
{
  if (genericBusEnvData != LIN_NULL)
  {
    loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t*) genericBusEnvData;      
    loc_ChangeState_Offline(busEnvData);
             
    LINSCI_UART_CONFIG = 0u 
//                       | E_LINSCI_UART_CONFIG_RE    
//                       | E_LINSCI_UART_CONFIG_TE    
                       | (uint16_t) V_LINSCI_UART_CONFIG_PARITY(0u)
//                       | E_LINSCI_UART_CONFIG_STOP           
                       | (uint16_t) E_LINSCI_UART_CONFIG_MASK_BRK_ERR    
//                       | E_LINSCI_UART_CONFIG_CLK_SRC 
                       | (uint16_t) E_LINSCI_UART_CONFIG_TXD_MASK
//                       | E_LINSCI_UART_CONFIG_TXD_VAL
                       | (uint16_t) E_LINSCI_UART_CONFIG_RXD_MASK
//                       | E_LINSCI_UART_CONFIG_RXD_VAL
                       | busEnvData->DebouncerValue
                       ;
    
    LINSCI_TIMER         = 0u;  // reset timer 
    LINSCI_TIMER_COMPARE = 0u;
   
    LINSCI_TIMER         = 0u
                           | (uint16_t) E_LINSCI_TIMER_ENABLE
                           | (uint16_t) E_LINSCI_TIMER_CLK_SRC
                           | (uint16_t) V_LINSCI_TIMER_PREPARE(0u)
  //                         | E_LINSCI_TIMER_BREAK_RESTART
  //                         | E_LINSCI_TIMER_TXD_TIMEOUT_E
                           ;  
   
    while (LINSCI_TIMER_COUNTER < LINBUSIMP_DEFAULT_WAKEUP_DURATION) {}

    LINSCI_TIMER         = 0;  // stop timer
    
    loc_ChangeState_Syncing(busEnvData, LinBusIf_ERROR_NO_ERROR);
  }
}

/***************************************************************************//**
 *
 ******************************************************************************/
BUS_LAYER_CODE void LinBusImp_Task(LinBusIf_pGenericEnvData_t genericBusEnvData)
{

}

/***************************************************************************//**
 *  TODO: Has to be tested.
 ******************************************************************************/
BUS_LAYER_CODE Lin_Bool_t LinBusImp_SendHeader(LinBusIf_pGenericEnvData_t genericBusEnvData, LinBusIf_FrameID_t frameID)
{
  Lin_Bool_t rv = LIN_FALSE;
  
#if LINBUSIMP_ENABLE_SEND_HEADER == 1
  if (genericBusEnvData != LIN_NULL)
  {
    loc_pBusEnvironmentData_t busEnvData = (loc_sBusEnvironmentData_t*) genericBusEnvData;

    if (busEnvData->BusState == LinBusImp_STATE_SYNCING)
    {
            
      LINSCI_DATA = 0x100u | ( (uint16_t) busEnvData->SendHeaderBreakLen - 1u );
      while(LINSCI_STATUS_bit.tx_fifo_full == 1u) {}

      LINSCI_DATA = 0x55;
      while(LINSCI_STATUS_bit.tx_fifo_full == 1u) {}

      LINSCI_DATA = loc_GetPidForFrameId(frameID);
      while(LINSCI_STATUS_bit.tx_fifo_full == 1u) {}
                 
      // ... feedback loop should trigger processing of pid callback next... 
      rv = LIN_TRUE;
    }
    else {}
  }
  else {}
#endif  
  
  return(rv);
}

