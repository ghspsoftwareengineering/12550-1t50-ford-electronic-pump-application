/***************************************************************************//**
 * @file			LinDrvImp_Config.h
 *
 * @creator		sbai
 * @created		02.06.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef LINDRVIMP_CONFIG_H_
#define LINDRVIMP_CONFIG_H_

#pragma system_include

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** INTERFACE OPTIONS *************************** */
/* ****************************************************************************/

/* ****************************************************************************/
/*                                  Bus Layer                                 */
/* ****************************************************************************/

/* ****************************************************************************/
/*                                Lookup Layer                                */
/* ****************************************************************************/

/* ****************************************************************************/
/*                               Protocol Layer                               */
/* ****************************************************************************/

/* ****************************************************************************/
/*                              Transport Layer                               */
/* ****************************************************************************/

/* ****************************************************************************/
/*                             Data Storage Layer                             */
/* ****************************************************************************/

/* ****************************************************************************/
/*                               Diagnose Layer                               */
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinBus_Implementation.h"
#include "vic_InterruptHandler.h"
#include "LinLookup_Implementation_Sequential.h"
#include "LinProto_Implementation.h"
#include "LinDrvImp_FrameAndSIDConfig.h"
#include "LinTrans_Implementation.h"
#include "LinDrvImp_FrameAndSIDConfig.h"
#include "LinDataStg_Implementation.h"
#include "LinDiag_Implementation.h"
#include "bootloader_UserConfig.h"
#include "bootloader_interface.h"
#include "Lin_EnvDataSizes.h"
#include "LinDrvImp_Callbacks.h"
#include "rom_API.h"

/* ****************************************************************************/
/* ***************************** USER INCLUDES ********************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************* BASIC LIN CONFIGURATION ************************ */
/* ****************************************************************************/

typedef void (* LinDrvImp_Hook_t)(void);


#define LINDRVIMP_CONFIG_COOKIE_VALID   0xA596u
#define LINDRVIMP_CONFIG_LOCATION       0x8000u

struct LinDrvImp_sFLASHCfgData
{
  Lin_uint16_t                          ConfigCookie;
  
  LinDiagIf_sProductIdentification_t    ProductIdentification;
  LinDiagIf_SerialNumber_t              SerialNumber;
  Lin_uint8_t                           InitialNAD;
    
  Lin_uint16_t                          ADCSampleExt;
  Lin_uint16_t                          ADCSampleExtAA;
  Lin_uint16_t                          ADCSampleExtVT;
    
  Lin_Bool_t                            EnableHighSpeed;  
  Lin_uint8_t                           DebouncerValue;    

  LinDrvImp_Hook_t                      PreInitHook;
  LinDrvImp_Hook_t                      PostInitHook;
  LinDrvImp_Hook_t                      TaskHook;

  Lin_uint32_t                          InitialBaudrate;
  
};

typedef struct LinDrvImp_sFLASHCfgData   LinDrvImp_FLASHCfgData_t;
typedef        LinDrvImp_FLASHCfgData_t* LinDrvImp_pFLASHCfgData_t;
typedef const  LinDrvImp_FLASHCfgData_t* LinDrvImp_cpFLASHCfgData_t;

extern LinDrvImp_cpFLASHCfgData_t        LinDrvImp_ConfigData;

/* ****************************************************************************/
/*                                  Bus Layer                                 */
/* ****************************************************************************/
  #define LINBUSIMP_CLK_FREQUENZY           48000000
  #define LINBUSIMP_BAUDRATE                10420
  #define LINBUSIMP_DEBOUNCER_VALUE         2  
  #define LINBUSIMP_SEND_HEADER_BREAK_LEN   13
  #define LINBUSIMP_DEFAULT_IGNORE_MSGTOUT   0x00u /**< Default ignore message timeout value. */

  #define VIC_IF_FUNS                   &vic_Implementation_InterfaceFunctions

/* ****************************************************************************/
/*                                Lookup Layer                                */
/* ****************************************************************************/

/* ****************************************************************************/
/*                               Protocol Layer                               */
/* ****************************************************************************/
#define LINPROTOIMP_MSG_BUF_LEN           8u

/* ****************************************************************************/
/*                              Transport Layer                               */
/* ****************************************************************************/
#define LINTRANSIMP_MSG_BUF_LEN         105u

/* ****************************************************************************/
/*                             Data Storage Layer                             */
/* ****************************************************************************/

/* ****************************************************************************/
/*                               Diagnose Layer                               */
/* ****************************************************************************/
  #define LINDIAGIMP_INITIAL_NAD          1u
  #define LINDIAGIMP_REQST_CH_FRM_ID      60u
  #define LINDIAGIMP_RESP_CH_FRM_ID       61u
  #define LINDIAGIMP_REQST_CH_MSG_SZE     8u
  #define LINDIAGIMP_RESP_CH_MSG_SZE      8u
  #define LINDIAGIMP_REQST_CH_CRC_TYPE    LinProtoIf_CRC_Classic
  #define LINDIAGIMP_RESP_CH_CRC_TYPE     LinProtoIf_CRC_Classic
  #define LINDIAGIMP_NAS_TIMEOUT          1000u
  #define LINDIAGIMP_NCR_TIMEOUT          1000u

  #define LINDIAGIMP_SUPPLIERID           0x0023u
  #define LINDIAGIMP_FUNCTIONID           0x0523u
  #define LINDIAGIMP_VARIANTID            0x06u
  #define LINDIAGIMP_SERIALNUMBER         0x01020304u

/* ****************************************************************************/
/* ************************* CALLBACK CONFIGURATION ************************* */
/* ****************************************************************************/

/* ****************************************************************************/
/*                                  Bus Layer                                 */
/* ****************************************************************************/

/* ****************************************************************************/
/*                                Lookup Layer                                */
/* ****************************************************************************/

/* ****************************************************************************/
/*                               Protocol Layer                               */
/* ****************************************************************************/
  #define LINDRVIMP_PROTO_USED_IF_VERSION    LINPROTO_INTERFACE_MODULE_API_VERSION

  /* The callback functions have to match to the typedefs of the interface */
  /* Dont't forget to include the module where your callbacks are defined! */
  #define LINDRVIMP_PROTO_ERROR_CALLBACK     &LinDrvImp_ProtoErrorCbFun
  #define LINDRVIMP_PROTO_RESTART_CALLBACK   &LinDrvImp_ProtoRestartCbFun
  #define LINDRVIMP_PROTO_MEASDONE_CALLBACK  &LinDrvImp_ProtoMeasDoneCbFun
  #define LINDRVIMP_PROTO_IDLE_CALLBACK      &LinDrvImp_ProtoIdleCbFun
  #define LINDRVIMP_PROTO_WAKEUP_CALLBACK    &LinDrvImp_ProtoWakupCbFun

  #define LINDRVIMP_PROTO_CALLBACK_CTX_DATA  LIN_NULL //&((LinBusIf_pGenericCbCtxData_t) 0)

/* ****************************************************************************/
/*                              Transport Layer                               */
/* ****************************************************************************/

/* ****************************************************************************/
/*                             Data Storage Layer                             */
/* ****************************************************************************/
  #define LINDRVIMP_DATASTG_USED_IF_VERSION    LINDATASTG_INTERFACE_MODULE_API_VERSION

  #define LINDRVIMP_DATASTG_ERROR_CALLBACK     &LinDrvImp_DataStgErrorCbFun

  #define LINDRVIMP_DATASTG_CALLBACK_CTX_DATA  LIN_NULL

/* ****************************************************************************/
/*                               Diagnose Layer                               */
/* ****************************************************************************/
  #define LINDRVIMP_DIAG_USED_IF_VERSION     LINDIAG_INTERFACE_MODULE_API_VERSION

  #define LINDRVIMP_DIAG_GOTOSLEEP_CALLBACK                   &LinDrvImp_DiagGoToSleepCbFun
  #define LINDRVIMP_DIAG_ERROR_CALLBACK                       &LinDrvImp_DiagErrorCbFun

  #define LINDRVIMP_DIAG_CALLBACK_CTX_DATA  LIN_NULL

/* ****************************************************************************/
/* ********************** ADVANCED DRIVER CONFIGURATION ********************* */
/* ****************************************************************************/

/* ****************************************************************************/
/*                                  Bus Layer                                 */
/* ****************************************************************************/

/* ****************************************************************************/
/*                                Lookup Layer                                */
/* ****************************************************************************/

/* ****************************************************************************/
/*                               Protocol Layer                               */
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
extern LinBusIf_cpInterfaceFunctions_t LinDrvImp_BusIfFuns;
extern LinDataStgIf_cpInterfaceFunctions_t LinDrvImp_DataStgIfFuns;
extern LinLookupIf_cpInterfaceFunctions_t LinDrvImp_LookupIfFuns;
extern LinProtoIf_cpInterfaceFunctions_t LinDrvImp_ProtoIfFuns;
extern LinTransIf_cpInterfaceFunctions_t LinDrvImp_TransIfFuns;
extern LinDiagIf_cpInterfaceFunctions_t LinDrvImp_DiagIfFuns;

#pragma data_alignment=2
extern Lin_uint8_t LinDrvImp_BusEnvData[LINBUSIMP_ENVIRONMENT_DATA_SZE];

#pragma data_alignment=2
extern Lin_uint8_t LinDrvImp_DataStgEnvData[LINDATASTGIMP_ENVIRONMENT_DATA_SZE];

#pragma data_alignment=2
extern Lin_uint8_t LinDrvImp_LookupEnvData[LINDRV_LOOKUP_ENVIRONMENT_DATA_SZE];

#pragma data_alignment=2
extern Lin_uint8_t LinDrvImp_ProtoEnvData[LINPROTOIMP_ENVIRONMENT_DATA_SZE(LINPROTOIMP_MSG_BUF_LEN)];

#pragma data_alignment=2
extern Lin_uint8_t LinDrvImp_TransEnvData[LINTRANSIMP_ENVIRONMENT_DATA_SZE(LINTRANSIMP_MSG_BUF_LEN)];

#pragma data_alignment=2
extern Lin_uint8_t LinDrvImp_DiagEnvData[LINDIAGIMP_ENVIRONMENT_DATA_SZE];

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre				  (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
Lin_Bool_t LinDrvImp_Init(vic_cpInterfaceFunctions_t vicIfFuns);


/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre				  (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void LinDrvImp_Task(void);

#endif /* LINDRVIMP_CONFIG_H_ */
