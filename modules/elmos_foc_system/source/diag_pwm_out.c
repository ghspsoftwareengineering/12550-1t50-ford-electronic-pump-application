/**
 * @ingroup HW
 *
 * @{
 */

/**********************************************************************/
/*! @file diag_pwm_out.c
 * @brief generation of a diagnostic PWM output signal
 *
 * @details
 * configuration and control functions for generating a PWM output signal <br>
 * at a PORTC GPIO pin via the CCTIMER3 module <br>
 *
 * module prefix: diago_
 *
 * @author       RHA
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <diag_pwm_out.h>
#include <defines.h>

#include <debug.h>

#if ( USE_PWM_DIAG_OUTPUT == 1 )

  static diago_data_t diago_data;

/*! @brief initialize diagnostic PWM output
 *
 * @details
 * Initialize CCTIMER module (prescaler!) and set the IOMUX of the input pin <br>
 * NOTE: diago_enable(True) must be called before the generation actually starts!
 */
  void diago_init( const u16 prescaler )
  {
    SYS_STATE_MODULE_ENABLE_bit.cctimer_3 = 1u;

    CCTIMER_3_PRESCALER = prescaler - 1u;

    CCTIMER_3_CONFIG_A_bit.mode        = 2u; /* compare mode, loop */
    CCTIMER_3_CONFIG_A_bit.capture_sel = 0u; /* other */
    CCTIMER_3_CONFIG_A_bit.restart_sel = 0u;

    CCTIMER_3_CONFIG_B_bit.mode        = 0u; /* compare mode */
    CCTIMER_3_CONFIG_B_bit.capture_sel = 0u; /* other */
    CCTIMER_3_CONFIG_B_bit.restart_sel = 1u; /* restart on compare event */

    #if ( PWM_DIAG_OUTPUT_PORT != GPIO_C )
    #error Diagnostic PWM output is only supported at PORT C!
    #endif

    #if ( PWM_DIAG_OUTPUT_PIN == GPIO_IO_0 )
      IOMUX_CTRL_PC0123_IO_SEL_bit.sel_0 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_3_PWM;
    #elif ( PWM_DIAG_OUTPUT_PIN == GPIO_IO_1 )
      IOMUX_CTRL_PC0123_IO_SEL_bit.sel_1 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_3_PWM;
    #elif ( PWM_DIAG_OUTPUT_PIN == GPIO_IO_2 )
      IOMUX_CTRL_PC0123_IO_SEL_bit.sel_2 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_3_PWM;
    #elif ( PWM_DIAG_OUTPUT_PIN == GPIO_IO_3 )
      IOMUX_CTRL_PC0123_IO_SEL_bit.sel_3 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_3_PWM;
    #elif ( PWM_DIAG_OUTPUT_PIN == GPIO_IO_4 )
      IOMUX_CTRL_PC4567_IO_SEL_bit.sel_4 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_3_PWM;
    #elif ( PWM_DIAG_OUTPUT_PIN == GPIO_IO_5 )
      IOMUX_CTRL_PC4567_IO_SEL_bit.sel_5 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_3_PWM;
    #elif ( PWM_DIAG_OUTPUT_PIN == GPIO_IO_6 )
      IOMUX_CTRL_PC4567_IO_SEL_bit.sel_6 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_3_PWM;
    #elif ( PWM_DIAG_OUTPUT_PIN == GPIO_IO_7 )
      IOMUX_CTRL_PC4567_IO_SEL_bit.sel_7 = (u16) IOMUX_CTRL_FUNC_C_CCTIMER_3_PWM;
    #else
    #error PWM_DIAG_INPUT_PIN has an invalid value!
    #endif

    #warning Diagnostic PWM output enabled. PWM_DIAG_OUTPUT_PIN functionality overridden!  CCTIMER_3 in use!  /*  PRQA S 1008, 3115 */
  }

/*! @brief main function of this module
 *
 * @details
 * Since there's no guaranteed timing when new values are set, diago_set_dutycycle() and <br>
 * diago_set_period() only set "shadow registers". At the beginning of each output PWM cycle, those new <br>
 * values are stored to the CCTIMER module. In case the counter value is already beyond new compare value, <br>
 * the next posssible compare value is set and the correct compare value is set afterwards.
 */
  void diago_main( void )
  {
    static BOOL loc_rewrite_cmp_val = False;

    /* */
    if( CCTIMER_3_IRQ_STATUS_bit.restart_a != 0u )
    {
      /* set new compare value */
      if( CCTIMER_3_CAPCMP_B != diago_data.cmp_val )
      {
        if( CCTIMER_3_COUNTER_B >= diago_data.cmp_val )
        {
          __disable_interrupt();
          CCTIMER_3_CAPCMP_B = CCTIMER_3_COUNTER_B + 2u;
          __enable_interrupt();
          loc_rewrite_cmp_val = True;
        }
        else
        {
          CCTIMER_3_CAPCMP_B = diago_data.cmp_val;
        }
      }

      /* set new PWM period */
      if( CCTIMER_3_CAPCMP_A != diago_data.period_val )
      {
        CCTIMER_3_CAPCMP_A = diago_data.period_val;
      }

      CCTIMER_3_IRQ_STATUS_bit.restart_a = 1u;
    }
    else if( loc_rewrite_cmp_val )
    {
      u16 loc_buf = CCTIMER_3_CAPCMP_B;

      if( CCTIMER_3_COUNTER_B > loc_buf )
      {
        CCTIMER_3_CAPCMP_B  = diago_data.cmp_val;
        loc_rewrite_cmp_val = False;
      }
    }
    else
    {
      /*Message 2013*/ /* justification: intentionally empty */
    }
  }

/*! @brief enable (and restart) CCTIMER or disable it */
  INLINE void diago_enable( const BOOL enable )
  {
    if( enable )
    {
      CCTIMER_3_CAPCMP_A = diago_data.period_val;
      CCTIMER_3_CAPCMP_B = diago_data.cmp_val;
      CCTIMER_3_CONTROL  = (u16) E_CCTIMER_CONTROL_ENABLE_A | (u16) E_CCTIMER_CONTROL_ENABLE_B | (u16) E_CCTIMER_CONTROL_RESTART_A | (u16) E_CCTIMER_CONTROL_RESTART_B | (u16) E_CCTIMER_CONTROL_RESTART_P;
    }
    else
    {
      CCTIMER_3_CONTROL = 0u;
    }
  }

/*! @brief set new duty cycle (only takes effect with the following output PWM period) */
  INLINE void diago_set_dutycycle( const u16 cmp_val )
  {
    diago_data.cmp_val = cmp_val;
  }

/*! @brief set new period (only takes effect with the following output PWM period) */
  INLINE void diago_set_period( const u16 period_val )
  {
    diago_data.period_val = period_val;
  }

#endif /* _#if ( USE_PWM_DIAG_OUTPUT == 1 ) */

/* }@
 */
