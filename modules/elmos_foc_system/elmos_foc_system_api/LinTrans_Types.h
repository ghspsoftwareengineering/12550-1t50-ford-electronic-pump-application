/***************************************************************************//**
 * @file			LinTrans_Types.h
 *
 * @creator		sbai
 * @created		25.03.2015
 *
 * @brief  		Definitions of basic data types for the 'LIN TRANS Layer'.
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINTRANS_TYPES_H_
#define LINTRANS_TYPES_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "Lin_Basictypes.h"
#include "LinBus_Types.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINTRANSIF_GOTOSLEEP_NAD  0x00u /**< NAD of a 'Go to sleep' request. (See also: LIN 2.2a Specification - Chapter 4.2.3.2 NAD) */
#define LINTRANSIF_FUNCTIONAL_NAD 0x7Eu /**< Functional NAD. (See also: LIN 2.2a Specification - Chapter 4.2.3.2 NAD) */
#define LINTRANSIF_BROADCAST_NAD  0x7Fu /**< Broadcast NAD. (See also: LIN 2.2a Specification - Chapter 4.2.3.2 NAD) */

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/
typedef Lin_Error_t  LinTransIf_Error_t;  

typedef Lin_uint8_t  LinTransIf_NAD_t;          /**< Data type for the 'Node Address' (NAD)
                                                     (See also: LIN 2.2a Specification - Chapter 4.2.3.2 NAD) */
typedef Lin_uint8_t  LinTransIf_SID_t;          /**< Data type for SID (Service Identifier)
                                                     (See also: LIN 2.2a Specification - Chapter 3.2.1.5 SID) */

typedef Lin_pvoid_t      LinTransIf_pGenericEnvData_t;    /**< Generic pointer to environment data of the TRANS layer module. */
typedef Lin_EnvDataSze_t LinTransIf_EnvDataSze_t;         /**< LIN Transport layer data type for the environment data length. */
typedef Lin_pvoid_t      LinTransIf_pGenericImpCfgData_t; /**< Generic pointer to configuration parameter of the specific TRANS layer implementation */
typedef Lin_pvoid_t      LinTransIf_pGenericCbCtxData_t;  /**< Pointer to TRANS callback context data. */

typedef Lin_uint8_t              LinTransIf_Data_t;       /**< Basic TRANS data type */
typedef LinTransIf_Data_t*       LinTransIf_pData_t;      /**< Pointer to LinTransIf_Data_t */
typedef const LinTransIf_Data_t* LinTransIf_cpData_t; 
typedef LinTransIf_Data_t**      LinTransIf_ppData_t;     /**< Pointer-Pointer to LinTransIf_Data_t */

typedef Lin_BufLength_t          LinTransIf_BufLength_t;  /**< TRANS layer data type of length values */
typedef LinTransIf_BufLength_t*  LinTransIf_pBufLength_t; /**< Pointer to LinTransIf_Length_t */

typedef Lin_uint16_t             LinTransIf_PDUMsgLen_t;  /**< */
typedef LinTransIf_PDUMsgLen_t*  LinTransIf_pPDUMsgLen_t; /**< */

typedef LinBusIf_Tick_t LinTransIf_Tick_t;                /**< Redefinition of the BUS Layer 'LinBusIf_Tick_t'. */

typedef Lin_uint32_t         LinTransIf_Timeout_t;        /**< Type for basic timeout values. */
typedef LinTransIf_Timeout_t LinTransIf_NasTimeout_t;     /**< Type for N_As timeout values. */
typedef LinTransIf_Timeout_t LinTransIf_NcrTimeout_t;     /**< Type for N_Cr timeout values. */

typedef Lin_Bool_t LinTransIf_ErrorFlag_t;                /**< Type for signaling an error during PDU transmission. */

/***************************************************************************//**
 * @brief LIN TRANS layer error enumerator
 *
 * @misra{M3CM Dir-10.1. - PRQA Msg 4521,
 * The Elmos LIN Driver defines areas of error codes for every module and sub-areas between
 * general defined interface error codes and implementation specific error codes. To link
 * between this areas some arithmetic offset calculation has to be done between different
 * enum types.,
 * Conflicts in  signedness.,
 * Always make sure unsigned values ate defined and used.}
 *
 ******************************************************************************/
// PRQA S 4521 ++
enum LinTransIf_eErrorCodes
{
  LinTransIf_ERR_NO_ERROR                    = Lin_NO_ERROR,               /**< No error at all. */
  LinTransIf_ERR_INIT                        = Lin_ERROR_AREA_TRANS + 0u,  /**< Error at TRANS layer module initialization. */
  LinTransIf_ERR_MULTI_PDU_NOT_SUPPORTED     = Lin_ERROR_AREA_TRANS + 1u,  /**< Multi PDU request received, but not supported. */
  LinTransIf_ERR_UNKNOWN_PCI_TYPE            = Lin_ERROR_AREA_TRANS + 2u,  /**< Unknown PCI type. */
  LinTransIf_ERR_UNEXPECTED_CF               = Lin_ERROR_AREA_TRANS + 3u,  /**< Unexpected 'Continues Frame'. */
  LinTransIf_ERR_FRAME_CNT                   = Lin_ERROR_AREA_TRANS + 4u,  /**< Wrong frame count in Multi-PDU. */
  LinTransIf_ERR_BUFFER_TOO_SMALL            = Lin_ERROR_AREA_TRANS + 5u,  /**< Buffer to small to receive message. */
  LinTransIf_ERR_NAS_TIMEOUT                 = Lin_ERROR_AREA_TRANS + 6u,  /**< N_As timeout occurred. */
  LinTransIf_ERR_NCR_TIMEOUT                 = Lin_ERROR_AREA_TRANS + 7u,  /**< N_Cr timeout occurred. */
  LinTransIf_ERR_NOT_ENOUGH_DATA             = Lin_ERROR_AREA_TRANS + 8u,  /**< Less data to send, then in 'Init Response' passed. */
  LinTransIf_ERR_TOO_MUCH_DATA               = Lin_ERROR_AREA_TRANS + 9u,  /**< More data to send, then in 'Init Response' passed. */
  LinTransIf_ERR_INVALID_SF_PDU              = Lin_ERROR_AREA_TRANS + 10u, /**< Invalid single frame PDU. */
  LinTransIf_ERR_INVALID_FF_PDU              = Lin_ERROR_AREA_TRANS + 11u, /**< Invalid first frame PDU. */
  LinTransIf_ERR_INVALID_SID_CB_RET_VAL      = Lin_ERROR_AREA_TRANS + 12u, /**< Invalid SID callback return value. */
  LinTransIf_ERR_PROCESSING_TOO_SLOW         = Lin_ERROR_AREA_TRANS + 13u, /**< Next request received while processing callback is still active. */
  LinTransIf_ERR_IMPL_ERROR_AREA             = Lin_ERROR_AREA_TRANS + (LIN_ERROR_AREA_SIZE/2u) /**< Any additional implementation specific error codes start here. */
};
// PRQA S 4521 --

typedef enum LinTransIf_eErrorCodes LinTransIf_eErrorCodes_t; /**< Typedef of LinTransIf_eErrorCodes. */

/***************************************************************************//**
 * @brief Enumerator to tell the TRANS layer module how to proceed with the SID.
 ******************************************************************************/
enum LinTransIf_eSIDReaction
{
  LinTransIf_SIDRA_IGNORED                 = 0x00,  /**< Ignore SID. */
  LinTransIf_SIDRA_SUCCESSFULLY_PROC       = 0x01,  /**< Successfully processed. */
  LinTransIf_SIDRA_WAIT_FOR_RESPONSE_FRAME = 0x02,  /**< Wait for an response frame. */
  LinTransIf_SIDRA_NO_CALLBACK_FNC_FOUND   = 0xFF   /**< No callback found. */
};

typedef enum LinTransIf_eSIDReaction LinTransIf_eSIDReaction_t; /**< Typedef of LinTransIf_eSIDReaction. */

/***************************************************************************//**
 * @brief Enumerator about who to proceed with a received NAD.
 ******************************************************************************/
enum LinTransIf_eCheckNADResult
{
  LinTransIf_ChkNADRes_Unknown = 0, /**< Unknown/Default return value. */
  LinTransIf_ChkNADRes_Accept  = 1, /**< Accept NAD. */
  LinTransIf_ChkNADRes_Decline = 2  /**< Decline NAD. */
};

typedef enum LinTransIf_eCheckNADResult LinTransIf_eCheckNADResult_t; /**< Typedef of LinTransIf_eCheckNADResult. */

/***************************************************************************//**
 * @brief TRANS layer communication status enumerator.
 ******************************************************************************/
enum LinTransIf_eComStatus
{
  LinTransIf_ComStatus_IDLE                       = 0, /**< Idle. */
  LinTransIf_ComStatus_RECEIVING_REQEUST          = 1, /**< Receiving a request. */
  LinTransIf_ComStatus_WAITING_FOR_RESPONSE_FRAME = 2, /**< Waiting for a response frame. */
  LinTransIf_ComStatus_TRANSMITTING_RESPONSE      = 3, /**< Transmitting a response. */
  LinTransIf_ComStatus_INVALID                    = 4  /**< Invalid/Unknown status. */
};

typedef enum LinTransIf_eComStatus LinTransIf_eComStatus_t; /**< Typedef of LinTransIf_eComStatus. */

/***************************************************************************//**
 * @brief TRANS layer timeout type enumerator.
 ******************************************************************************/
enum LinTransIf_eTimeoutType
{
  LinTransIf_Timeout_NAS = 0, /**< N_As timeout. */
  LinTransIf_Timeout_NCR = 1, /**< N_Cr timeout. */
};

typedef enum LinTransIf_eTimeoutType LinTransIf_eTimeoutType_t; /**< Typedef of LinTransIf_eTimeoutType. */

struct         LinTransIf_sInterfaceFunctions;                                      /**< Forward declaration of LIN TRANS layer interface functions. */
typedef struct LinTransIf_sInterfaceFunctions    LinTransIf_sInterfaceFunctions_t;  /**< Typedef of LinTransIf_sInterfaceFunctions. */
typedef        LinTransIf_sInterfaceFunctions_t* LinTransIf_pInterfaceFunctions_t;  /**< Typedef of pointer to LinTransIf_sInterfaceFunctions. */
typedef const  LinTransIf_sInterfaceFunctions_t* LinTransIf_cpInterfaceFunctions_t;  /**< Typedef of constant pointer to LinTransIf_sInterfaceFunctions. */

struct         LinTransIf_sCallbackFunctions;                                     /**< Forward declaration of LIN TRANS layer callback functions. */
typedef struct LinTransIf_sCallbackFunctions    LinTransIf_sCallbackFunctions_t;  /**< Typedef of LinTransIf_sCallbackFunctions. */
typedef        LinTransIf_sCallbackFunctions_t* LinTransIf_pCallbackFunctions_t;  /**< Typedef of pointer to LinTransIf_sCallbackFunctions. */
typedef const  LinTransIf_sCallbackFunctions_t* LinTransIf_cpCallbackFunctions_t; /**< Typedef of constant pointer to LinTransIf_sCallbackFunctions. */

struct         LinTransIf_sThis;                       /**< Forward declaration of LIN TRANS layer This-Pointer. */
typedef struct LinTransIf_sThis    LinTransIf_sThis_t; /**< Typedef for LinTransIf_sThis. */
typedef        LinTransIf_sThis_t* LinTransIf_pThis_t; /**< Typedef of pointer to LinTransIf_sThis. */

struct         LinTransIf_sInitParam;                            /**< Forward declaration of LIN TRANS layer initialization parameter struct. */
typedef struct LinTransIf_sInitParam    LinTransIf_sInitParam_t; /**< Typedef of LinTransIf_sInitParam */
typedef        LinTransIf_sInitParam_t* LinTransIf_pInitParam_t; /**< Typedef of pointer to LinTransIf_sInitParam */

typedef void * LinTransIf_SIDCbCtx_t; /**< Pointer to SID callback context data. */

struct         LinTransIf_sSIDDescription;                                  /**< Forward declaration of LIN TRANS layer 'SID Description' struct. */
typedef struct LinTransIf_sSIDDescription    LinTransIf_sSIDDescription_t;  /**< Typedef of LinTransIf_sSIDDescription. */
typedef        LinTransIf_sSIDDescription_t* LinTransIf_pSIDDescription_t;  /**< Typedef of pointer to LinTransIf_sSIDDescription */
typedef const  LinTransIf_sSIDDescription_t* LinTransIf_cpSIDDescription_t; /**< Typedef of constant pointer to LinTransIf_sSIDDescription. */

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* LINTRANS_TYPES_H_ */
