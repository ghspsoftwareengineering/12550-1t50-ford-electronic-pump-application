#ifndef FDL_CFG_H
#   define FDL_CFG_H
/*===========================================================================*/
/**
 * @file fdl_cfg.h
 *
 *   Template file for Flash Driver Library for S12Z projects.
 *
 *------------------------------------------------------------------------------
 *
 * Copyright 2014 GHSP, Inc., All Rights Reserved.
 * GHSP Confidential
 * 
 * NOTICE:  
 * All information contained herein is, and remains the property of GHSP, Inc. 
 * The intellectual and technical concepts contained herein are proprietary to 
 * GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in 
 * process, and are protected by trade secret or copyright law. Dissemination 
 * of this information or reproduction of this material is strictly forbidden 
 * unless prior written permission is obtained from GHSP, Inc.
 *
 *------------------------------------------------------------------------------
 *
 * @section DESC DESCRIPTION:
 *
 * Template file for configuring the implementation of the FDL module.
 *
 * @section ABBR ABBREVIATIONS:
 *   - None
 *
 * @section TRACE TRACEABILITY INFO:
 *   - Design Document(s):
 *     - None
 *
 *   - Requirements Document(s):
 *     - None
 *
 *   - Applicable Standards (in order of precedence: highest first):
 *     - <a href="http://sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx">
 *       "GHSP Coding Standard" [12-Mar-2006]</a>
 *
 * @section DFS DEVIATIONS FROM STANDARDS:
 *   - None
 *
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/

/*===========================================================================*
 * #define Constants
 *===========================================================================*/

/* size in bytes of the NVM area on the chip. This will vary between CAN EVB platform and others */
#define FDL_CFG_EEPROM_SIZE             (1024u)

/* NVM start address based on device */
#define FDL_CFG_EEPROM_START_ADDR       (0u)

/* NVM end address */
#define FDL_CFG_EEPROM_END_ADDR         (FDL_CFG_EEPROM_START_ADDR + FDL_CFG_EEPROM_SIZE)

/*
 * We're going to divide the entire 1K flash array into sub-blocks of 32 bytes. Each valid
 * block will have it's own 8-bit CRC.
 */
#define FDL_CFG_EEPROM_BLOCK_SIZE       (32u)

/* max number of bytes to read at one time (4...32 by incr of 4) */
#define FDL_CFG_MAX_READ_BYTES_ALLOWED  (32u)

/* the total number of blocks available in the nvm */
#define FDL_CFG_EEPROM_NUM_BLOCKS      (FDL_CFG_EEPROM_SIZE / FDL_CFG_EEPROM_BLOCK_SIZE)

/*===========================================================================*\
 * #define MACROS
\*===========================================================================*/

/*===========================================================================*\
 * Custom Type Declarations
\*===========================================================================*/


#endif                          /* TEMPLATE_CFG_H */
