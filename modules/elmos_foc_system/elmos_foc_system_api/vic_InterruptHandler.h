/***************************************************************************//**
 * @file			vic_Implementation.h
 *
 * @creator		sbai
 * @created		16.06.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef VIC_INTERRUPTHANDLER_H_
#define VIC_INTERRUPTHANDLER_H_

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "vic_Interface.h"
#include "el_helper.h"
/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
extern const vic_sInterfaceFunctions_t vic_InterfaceFunctions;

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
vic_pInterruptModuleEnvironmentData_t vic_GetPointerToEnvironmentData(vic_eInterruptVectorNum_t module);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void vic_RegisterModule(vic_eInterruptVectorNum_t    module, vic_InterruptCallback_t interrupthandler,
                                                     vic_pInterruptModuleEnvironmentData_t moduleenvironmentdata);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void vic_DeregisterModule(vic_eInterruptVectorNum_t    module);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void vic_EnableModule(vic_eInterruptVectorNum_t moduleIRQNum);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void vic_DisableModule(vic_eInterruptVectorNum_t moduleIRQNum);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void vic_EnableMain(void);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         (optional) Which are the conditions to call this function? i.e. none
 *
 * @post        (optional) How has the status changed, after the function ran?
 *
 * TODO: A more detailed description.
 *
 * @see        (optional) Crossreference
 *
 ******************************************************************************/
void vic_DisableMain(void);

/***************************************************************************//**
 * @brief Initialize VIC module
 *
 * @param environmentdata  Pointer to Environment data for VIC module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and SYSS (syss_SystemStateModule)
 *             have to present and initialized.
 *
 * @post       VIC module is configured for use.
 *
 * @detaildesc
 *
 ******************************************************************************/
void vic_IRQInitialisation(vic_pInterruptEnvironmentData_t penvironmentdata);


#endif /* VIC_INTERRUPTHANDLER_H_ */
