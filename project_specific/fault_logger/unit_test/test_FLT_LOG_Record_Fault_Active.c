/**
 *  @file test_FLT_LOG_Record_Fault_Active.c
 *
 */
#include "unity.h"
#include "52307_test_regs.h"
#include "fault_logger.h"
#include "fault_logger_imp_defines.h"
#include "fault_logger_imp_types.h"
#include "lin_vehicle_comm.h"
#include <string.h>
#include "time_utils.h"

/* MOCKS */
#include "mock_fault_environmental_logger.h"
#include "mock_lin_vehicle_comm.h"
#include "mock_motor_ctrl.h"
#include "mock_nvm_mgr.h"
#include "mock_serial_data_buffer.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"

/* STATIC's */
Time_MS_T flt_log_Fault_Start_Times[FLT_LOG_NUM_FAULTS];
bool_t flt_log_Fault_Is_Active[FLT_LOG_NUM_FAULTS];
flt_log_Occurence_Block_T flt_log_Occurence_Blocks[FLT_LOG_NUM_DATA_BLOCKS];
bool_t flt_log_Enabled;

uint16_t Test_Regs[TEST_REG_END] = {0};

void setUp(void)
{
    uint8_t Block_ID = 0;

    memset(flt_log_Fault_Start_Times, 0u, sizeof(flt_log_Fault_Start_Times));
    memset(flt_log_Fault_Is_Active, 0u, sizeof(flt_log_Fault_Is_Active));

    for(Block_ID = 0; Block_ID < FLT_LOG_NUM_DATA_BLOCKS; Block_ID++)
    {
        uint8_t Fault_Index = 0;

        flt_log_Occurence_Blocks[Block_ID].Dirty = false;

        for (Fault_Index = 0; Fault_Index < 7; Fault_Index++)
        {
            flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Fault_Index].IDLE_State_Count = 0;
            flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Fault_Index].RUN_State_Count = 0;
            flt_log_Occurence_Blocks[Block_ID].Block_Data.Fault_Occurrence_Data[Fault_Index].Total_Duration_Sec = 0;
        }
    }

    flt_log_Enabled = false;
}

void tearDown(void)
{

}

/*----------------------------*\
 * FLT_LOG_OVER_VOLTAGE Tests *
\*----------------------------*/
/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the IDLE_State_Count for the fault record
 * associated with FLT_LOG_OVER_VOLTAGE when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Idle_Count_for_FLT_LOG_OVER_VOLTAGE_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_IDLE);
    fltel_Log_Fault_Expect(FLT_LOG_OVER_VOLTAGE);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_VOLTAGE);
    
    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_OVER_VOLTAGE]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_OVER_VOLTAGE]);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[0].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the RUN_State_Count for the fault record
 * associated with FLT_LOG_OVER_VOLTAGE when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Run_Count_for_FLT_LOG_OVER_VOLTAGE_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_RUN);
    fltel_Log_Fault_Expect(FLT_LOG_OVER_VOLTAGE);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_VOLTAGE);

    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_OVER_VOLTAGE]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_OVER_VOLTAGE]);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[0].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_OVER_VOLTAGE when the fault is already active.
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counters_for_FLT_LOG_OVER_VOLTAGE_when_fault_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;
    flt_log_Fault_Is_Active[FLT_LOG_OVER_VOLTAGE] = true;
    flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_VOLTAGE);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[0].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_OVER_VOLTAGE when the Fault Logger is disabled .
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counter_for_FLT_LOG_OVER_VOLTAGE_when_Fault_Logger_Disabled(void)
{
    /* Ensure known test state */
    flt_log_Enabled = false;
    flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_VOLTAGE);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[0].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[0].Dirty);
}

/*--------------------------------*\
 * FLT_LOG_OVER_TEMPERATURE Tests *
\*--------------------------------*/
/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the IDLE_State_Count for the fault record
 * associated with FLT_LOG_OVER_TEMPERATURE when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Idle_Count_for_FLT_LOG_OVER_TEMPERATURE_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_IDLE);
    fltel_Log_Fault_Expect(FLT_LOG_OVER_TEMPERATURE);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_TEMPERATURE);

    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_OVER_TEMPERATURE]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_OVER_TEMPERATURE]);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[0].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the RUN_State_Count for the fault record
 * associated with FLT_LOG_OVER_TEMPERATURE when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Run_Count_for_FLT_LOG_OVER_TEMPERATURE_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_RUN);
    fltel_Log_Fault_Expect(FLT_LOG_OVER_TEMPERATURE);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_TEMPERATURE);

    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_OVER_TEMPERATURE]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_OVER_TEMPERATURE]);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[0].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_OVER_TEMPERATURE when the fault is already active.
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counters_for_FLT_LOG_OVER_TEMPERATURE_when_fault_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;
    flt_log_Fault_Is_Active[FLT_LOG_OVER_TEMPERATURE] = true;
    flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_TEMPERATURE);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[0].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_OVER_TEMPERATURE when the Fault Logger is disabled .
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counter_for_FLT_LOG_OVER_TEMPERATURE_when_Fault_Logger_Disabled(void)
{
    /* Ensure known test state */
    flt_log_Enabled = false;
    flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_OVER_TEMPERATURE);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[0].Block_Data.Fault_Occurrence_Data[5].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[0].Dirty);
}

/*--------------------------------*\
 * FLT_LOG_LIN_DATA_ERROR Tests *
\*--------------------------------*/
/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the IDLE_State_Count for the fault record
 * associated with FLT_LOG_LIN_DATA_ERROR when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Idle_Count_for_FLT_LOG_LIN_DATA_ERROR_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_IDLE);
    fltel_Log_Fault_Expect(FLT_LOG_LIN_DATA_ERROR);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_LIN_DATA_ERROR);

    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_LIN_DATA_ERROR]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_LIN_DATA_ERROR]);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[1].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the RUN_State_Count for the fault record
 * associated with FLT_LOG_LIN_DATA_ERROR when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Run_Count_for_FLT_LOG_LIN_DATA_ERROR_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_RUN);
    fltel_Log_Fault_Expect(FLT_LOG_LIN_DATA_ERROR);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_LIN_DATA_ERROR);

    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_LIN_DATA_ERROR]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_LIN_DATA_ERROR]);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[1].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_LIN_DATA_ERROR when the fault is already active.
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counters_for_FLT_LOG_LIN_DATA_ERROR_when_fault_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;
    flt_log_Fault_Is_Active[FLT_LOG_LIN_DATA_ERROR] = true;
    flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_LIN_DATA_ERROR);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[0].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_LIN_DATA_ERROR when the Fault Logger is disabled .
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counter_for_FLT_LOG_LIN_DATA_ERROR_when_Fault_Logger_Disabled(void)
{
    /* Ensure known test state */
    flt_log_Enabled = false;
    flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_LIN_DATA_ERROR);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[1].Block_Data.Fault_Occurrence_Data[1].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[1].Dirty);
}

/*--------------------------------*\
 * FLT_LOG_BLOCKED_GEROTOR Tests *
\*--------------------------------*/
/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the IDLE_State_Count for the fault record
 * associated with FLT_LOG_BLOCKED_GEROTOR when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Idle_Count_for_FLT_LOG_BLOCKED_GEROTOR_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_IDLE);
    fltel_Log_Fault_Expect(FLT_LOG_BLOCKED_GEROTOR);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_BLOCKED_GEROTOR);

    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_BLOCKED_GEROTOR]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_BLOCKED_GEROTOR]);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[2].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the RUN_State_Count for the fault record
 * associated with FLT_LOG_BLOCKED_GEROTOR when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Run_Count_for_FLT_LOG_BLOCKED_GEROTOR_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_RUN);
    fltel_Log_Fault_Expect(FLT_LOG_BLOCKED_GEROTOR);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_BLOCKED_GEROTOR);

    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_BLOCKED_GEROTOR]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_BLOCKED_GEROTOR]);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[2].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_BLOCKED_GEROTOR when the fault is already active.
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counters_for_FLT_LOG_BLOCKED_GEROTOR_when_fault_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;
    flt_log_Fault_Is_Active[FLT_LOG_BLOCKED_GEROTOR] = true;
    flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_BLOCKED_GEROTOR);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[2].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_BLOCKED_GEROTOR when the Fault Logger is disabled .
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counter_for_FLT_LOG_BLOCKED_GEROTOR_when_Fault_Logger_Disabled(void)
{
    /* Ensure known test state */
    flt_log_Enabled = false;
    flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_BLOCKED_GEROTOR);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[0].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[1].Dirty);
}

/*--------------------------------*\
    fltel_Log_Fault_Expect(FLT_LOG_SELF_PROTECT_STALL);
    fltel_Log_Fault_Expect(FLT_LOG_SELF_PROTECT_STALL);
 * FLT_LOG_SHORT_CIRCUIT Tests *
\*--------------------------------*/
/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the IDLE_State_Count for the fault record
 * associated with FLT_LOG_SHORT_CIRCUIT when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Idle_Count_for_FLT_LOG_SHORT_CIRCUIT_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_IDLE);
    fltel_Log_Fault_Expect(FLT_LOG_SHORT_CIRCUIT);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_SHORT_CIRCUIT);

    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_SHORT_CIRCUIT]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_SHORT_CIRCUIT]);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[2].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will increment the RUN_State_Count for the fault record
 * associated with FLT_LOG_SHORT_CIRCUIT when the fault is not already active.
 */
void test_FLT_LOG_Record_Fault_Active_increments_Run_Count_for_FLT_LOG_SHORT_CIRCUIT_when_fault_not_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */
    TMUT_Get_Base_Time_MS_ExpectAndReturn(5000); /* Arbitrary timestamp to signify the start of the active period */
    VCOMM_Signal_Get_TrnOilPumpMde_D_Stat_ExpectAndReturn(VCOMM_MODE_RUN);
    fltel_Log_Fault_Expect(FLT_LOG_SHORT_CIRCUIT);

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_SHORT_CIRCUIT);

    /* Verify test results */
    TEST_ASSERT_TRUE(flt_log_Fault_Is_Active[FLT_LOG_SHORT_CIRCUIT]);
    TEST_ASSERT_EQUAL_INT(5000, flt_log_Fault_Start_Times[FLT_LOG_SHORT_CIRCUIT]);
    TEST_ASSERT_EQUAL_UINT8(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(1, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].Total_Duration_Sec);
    TEST_ASSERT_TRUE(flt_log_Occurence_Blocks[2].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_SHORT_CIRCUIT when the fault is already active.
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counters_for_FLT_LOG_SHORT_CIRCUIT_when_fault_previously_active(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;
    flt_log_Fault_Is_Active[FLT_LOG_SHORT_CIRCUIT] = true;
    flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_SHORT_CIRCUIT);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[2].Dirty);
}

/**
 * Verifies that the function FLT_LOG_Record_Fault_Active will NOT increment the IDLE_State_Count or the RUN_State_Count
 * for the fault record associated with FLT_LOG_SHORT_CIRCUIT when the Fault Logger is disabled .
 */
void test_FLT_LOG_Record_Fault_Active_doesnt_increment_counter_for_FLT_LOG_SHORT_CIRCUIT_when_Fault_Logger_Disabled(void)
{
    /* Ensure known test state */
    flt_log_Enabled = false;
    flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count = 13;
    flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count = 10;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_SHORT_CIRCUIT);

    /* Verify test results */
    TEST_ASSERT_EQUAL_UINT8(13, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].IDLE_State_Count);
    TEST_ASSERT_EQUAL_UINT8(10, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].RUN_State_Count);
    TEST_ASSERT_EQUAL_UINT16(0, flt_log_Occurence_Blocks[2].Block_Data.Fault_Occurrence_Data[1].Total_Duration_Sec);
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[1].Dirty);
}

/*------------------------*\
 * FLT_LOG_NO_ERROR Tests *
\*------------------------*/
/**
 * Verifies that the function FLT_LOG_Record_Fault_Active do nothing when passed FLT_LOG_NO_ERROR.
 */
void test_FLT_LOG_Record_Fault_Active_does_nothing_for_FLT_LOG_NO_ERROR(void)
{
    /* Ensure known test state */
    flt_log_Enabled = true;

    /* Setup expected call chain */

    /* Call function under test */
    FLT_LOG_Record_Fault_Active(FLT_LOG_NO_ERROR);

    /* Verify test results */
}
