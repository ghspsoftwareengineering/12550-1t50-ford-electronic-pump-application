/**
 *  @file uds_callouts.c
 *
 *  @ref uds_callouts_api
 *
 *  Functions required by UDS that must be implemented by the system in which
 *  UDS is used. UDS defines what these functions do but not how they do them
 *  since this functionality may vary from system to system.
 *  
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup uds_callouts_imp uds_callouts Implementation
 *  @{
 *
 *      System specific implementation of the @ref uds_callouts_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fdl.h"
#include "global.h"
#include <string.h>
#include "uds_cot.h"
#include "uds_types.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/
#define UDS_EEPROM_BASE_ADDRESS ((uint32_t)0x10000u)
#define UDS_EEPROM_SIZE ((size_t)FDL_CFG_EEPROM_SIZE)
#define UDS_EEPROM_END_ADDRESS ((uint32_t)(UDS_EEPROM_BASE_ADDRESS + UDS_EEPROM_SIZE))

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
/**
    Callout that allows the system to indicate that there are suitable conditions
    to enter the requested Diagnostic Session.
   
    @param [in] Session_Type - Session Type being entered
   
    @return true - Conditions are suitable
            false - Conditions are not suitable
*/
bool_t UDS_Conditions_Correct_For_Diag_Session(UDS_Diag_Session_T Session_Type)
{
    /* once we have conditions to enter Diag session it can be implemented here */
    
    return true;
}

/**
 */
void UDS_Transfer_To_Bootloader(bool_t const Was_Successful)
{

}

UDS_Response_Code_T UDS_Read_Memory(uint32_t Read_Address, size_t Length, uint8_t * Buffer)
{
    UDS_Response_Code_T Status = UDS_RESP_POSITIVE;
    uint32_t End_Address = Read_Address + Length;

    if (    (Read_Address >= UDS_EEPROM_BASE_ADDRESS)
         && (Read_Address <= UDS_EEPROM_END_ADDRESS)
         && (End_Address <= UDS_EEPROM_END_ADDRESS))
    {
        FDL_Read(Buffer, Read_Address - UDS_EEPROM_BASE_ADDRESS, (uint8_t)Length);
    }
    else if (    (Read_Address < UDS_EEPROM_BASE_ADDRESS)
              && (End_Address < UDS_EEPROM_BASE_ADDRESS))
    {
        memcpy(Buffer, (void const *)(uint16_t)Read_Address, Length);
    }
    else
    {
        Status = UDS_RESP_REQ_OUT_OF_RANGE;
    }
    return Status;
}
/** @} doxygen end group */
