/***************************************************************************//**
 * @file			vic_Implementation.c
 *
 * @creator		sbai
 * @created		16.06.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "io_e52398a.h"

#include "vic_InterruptHandler.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/***************************************************************************//**
 * Handles the VIC related interrupt requests.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 * @pre         TODO: Add description of precondition
 *
 * TODO: A more detailed description.
 *
 ******************************************************************************/
__interrupt static void loc_DummyInterruptHandler(void);


const vic_sInterfaceFunctions_t vic_InterfaceFunctions =
{
  .InterfaceVersion            = VIC_INTERFACE_VERSION,

  .IRQInitialisation           = &vic_IRQInitialisation,

  .GetPointerToEnvironmentData = &vic_GetPointerToEnvironmentData,

  .RegisterModule              = &vic_RegisterModule,
  .DeregisterModule            = &vic_DeregisterModule,
  .EnableModule                = &vic_EnableModule,
  .DisableModule               = &vic_DisableModule,

  .EnableMain                  = &vic_EnableMain,
  .DisableMain                 = &vic_DisableMain
};

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
vic_pInterruptModuleEnvironmentData_t vic_GetPointerToEnvironmentData(vic_eInterruptVectorNum_t module)
{
  vic_pInterruptEnvironmentData_t penvironmentdata = (vic_pInterruptEnvironmentData_t) VIC_TABLE_BASE; // PRQA S 0306
  vic_pInterruptModuleEnvironmentData_t rv = NULL;

  if (module < vic_INTERRUPT_VECTOR_CNT)
  { 
    rv = penvironmentdata->ModuleEnvironmentData[module];
  }
  else {}
  
  return rv;
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void vic_RegisterModule(vic_eInterruptVectorNum_t    module, vic_InterruptCallback_t interrupthandler,
                                              vic_pInterruptModuleEnvironmentData_t moduleenvironmentdata)
{
  vic_pInterruptEnvironmentData_t penvironmentdata = (vic_pInterruptEnvironmentData_t) VIC_TABLE_BASE; // PRQA S 0306

  if (module < vic_INTERRUPT_VECTOR_CNT)
  {
    penvironmentdata->InterrupVectorTable[module] = interrupthandler;
    penvironmentdata->ModuleEnvironmentData[module] = moduleenvironmentdata;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void vic_DeregisterModule(vic_eInterruptVectorNum_t    module)
{
  vic_pInterruptEnvironmentData_t penvironmentdata = (vic_pInterruptEnvironmentData_t) VIC_TABLE_BASE; // PRQA S 0306

  if (module < vic_INTERRUPT_VECTOR_CNT)
  {
    vic_DisableModule(module);
    penvironmentdata->InterrupVectorTable[module] = &loc_DummyInterruptHandler;
    penvironmentdata->ModuleEnvironmentData[module] = NULL;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void vic_EnableModule(vic_eInterruptVectorNum_t moduleIRQNum)
{
  VIC_IRQ_VENABLE = (uint16_t) moduleIRQNum;
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void vic_DisableModule(vic_eInterruptVectorNum_t moduleIRQNum)
{
  VIC_IRQ_VDISABLE = (uint16_t) moduleIRQNum;
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void vic_EnableMain(void)
{
  VIC_MAIN_ENABLE = (uint16_t) 1u;
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void vic_DisableMain(void)
{
  VIC_MAIN_ENABLE = (uint16_t) 0u;
}

/***************************************************************************//**
 * @brief Initialize VIC module
 *
 * @param environmentdata  Pointer to Environment data for VIC module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and SYSS (syss_SystemStateModule)
 *             have to present and initialized.
 *
 * @post       VIC module is configured for use.
 *
 * @detaildesc
 *
 ******************************************************************************/
void vic_IRQInitialisation(vic_pInterruptEnvironmentData_t penvironmentdata)
{
  uint8_t i;

  VIC_TABLE_BASE = (uint16_t) penvironmentdata; // PRQA S 0306
  VIC_TABLE_TYPE = (uint16_t) vic_TT_COMBINE_WITH_VECTOR_NUM;

  /* Initialize module IRQ table with dummy functions */
  for(i = 0u; i < (uint8_t) vic_INTERRUPT_VECTOR_CNT; i++ )
  {
    penvironmentdata->InterrupVectorTable[i] = &loc_DummyInterruptHandler;
    penvironmentdata->ModuleEnvironmentData[i] = NULL;
  }
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
__interrupt static void loc_DummyInterruptHandler(void)
{
  vic_eInterruptVectorNum_t irqvecnum = (vic_eInterruptVectorNum_t) VIC_IRQ_VNO;

  /* Clear IRQ */
  VIC_IRQ_VNO = (uint16_t) irqvecnum;

  /* Disable interrupt */
  VIC_IRQ_VDISABLE = (uint16_t) irqvecnum;
}
