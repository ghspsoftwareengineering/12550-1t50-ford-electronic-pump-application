/**
 * @ingroup UART
 *
 * @{
 */

/**********************************************************************/
/*! @file uart.c
 * @brief UART module
 *
 * @details
 * todo
 *
 * module prefix: uart_
 *
 * @author       RHA
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <uart.h>
#include <adc.h>
#include <debug.h>

#include <string.h>
#include <stdlib.h>

#include <systime.h>
#include <defines.h>

#if ( SCI_USAGE_TYPE == SCI_DATALOGGER )

/* -------------
   local defines
   ------------- */

  static uart_system_info_t uart_system_info = { /* PRQA S 3218  */ /* justification: table would decrease readability if they were included in functions */
    IC_MAJOR_ID, IC_MINOR_ID,
    IC_MAJOR_VERSION, IC_MINOR_VERSION,
    PCB_MAJOR_ID, PCB_MINOR_ID,
    PCB_MAJOR_VERSION, PCB_MINOR_VERSION,
    FIRMWARE_MAJOR_ID, FIRMWARE_MINOR_ID,
    FIRMWARE_MAJOR_VERSION, FIRMWARE_MINOR_VERSION,

    ((((u16) USR_SHUNT_MEASUREMENT_TYPE ) & USR_SHUNT_MEASUREMENT_TYPE_MSK ) << USR_SHUNT_MEASUREMENT_TYPE_SHFT )
    | ((((u16) USR_EN_SINGLE_SHUNT_SILENT_MODE ) & USR_EN_SINGLE_SHUNT_SILENT_MODE_MSK ) << USR_EN_SINGLE_SHUNT_SILENT_MODE_SHFT )
    | ((((u16) FOC_EN_HALL_SENS ) & FOC_EN_HALL_SENS_MSK ) << FOC_EN_HALL_SENS_SHFT )
    #if ( FOC_EN_HALL_SENS != 0 )
      | ((((u16) USR_USE_HALL_SPEED_CALC ) & USR_USE_HALL_SPEED_CALC_MSK ) << USR_USE_HALL_SPEED_CALC_SHFT )
    #endif
    | ((((u16) HALL_CCT_MODE ) & HALL_CCT_MODE_MSK ) << HALL_CCT_MODE_SHFT )
    | ((((u16) USR_EN_INITIAL_POSITION_DETECTION ) & USR_EN_INITIAL_POSITION_DETECTION_MSK ) << USR_EN_INITIAL_POSITION_DETECTION_SHFT )
    | ((((u16) FOC_EN_VBAT_COMPENSATION ) & FOC_EN_VBAT_COMPENSATION_MSK ) << FOC_EN_VBAT_COMPENSATION_SHFT )
    | ((((u16) FOC_EN_OBSERVER ) & FOC_EN_OBSERVER_MSK ) << FOC_EN_OBSERVER_SHFT )
    | ((((u16) FOC_USE_SVM ) & FOC_USE_SVM_MSK ) << FOC_USE_SVM_SHFT )
    | ((((u16) USE_PWM_DIAG_INPUT ) & USE_PWM_DIAG_INPUT_MSK ) << USE_PWM_DIAG_INPUT_SHFT )
    | ((((u16) USE_PWM_DIAG_OUTPUT ) & USE_PWM_DIAG_OUTPUT_MSK ) << USE_PWM_DIAG_OUTPUT_SHFT ),

    UART_PROTOCOL_MAJOR_VERSION, UART_PROTOCOL_MINOR_VERSION,
    __DATE__, __TIME__,                          /* "Mmm dd yyyy" */ /* "hh:mm:ss" */
  };

  typedef enum
  {
    UART_CMD_CODE_READ  = 0,
    UART_CMD_CODE_WRITE = 1
  } uart_cmd_code_t;

  typedef enum
  {
    UART_ADDR_EXT_CPU      = 0,
    UART_ADDR_EXT_SYS_INFO = 1,
    UART_ADDR_EXT_DBG      = 2,
    UART_ADDR_EXT_DLOG     = 3,
    UART_ADDR_EXT_SPI      = 4,
    UART_ADDR_EXT_MC       = 5
  } uart_addr_ext_t;

  #define UART_DATALOGGER_U16_CNT 4
  #define UART_DATALOGGER_U8_CNT  0
  #define UART_TX_TIMEOUT_TICKS   (( 5.0 / 1000.0 ) * (dbl) ( SYSTIME_TICKS_PER_SEC ))

  typedef struct
  {
    u16 addr;
    u16 addr_ext;
    u16 cmd_code;
    u16 master_frame_length;
    u16 slave_frame_length;
    u16 master_data[ 8 ];
  } uart_master_frame_t;

/* allow use of union */
  _Pragma( "diag_suppress = Pm093" )
  typedef struct
  {
    u16 err_code;
    u16 length;
    union
    {
      u8 data[ 16 ];
      u16 data16[ 8 ];
      u32 data32[ 4 ];
    };
  }
  uart_tx_frame_t;

/* disallow use of union */
  _Pragma( "diag_default = Pm093" )
  typedef struct
  {
    uart_master_frame_t std_master_frame;
    uart_tx_frame_t std_tx_frame;
    u16 rx_start_timestamp;
    u16 datalogger_cycle_cnt_init;
  }
  uart_data_t;

  _Pragma( " pack(1)" )
  typedef struct
  {
    u8 chunkStartID;
    u16 data_u16[ UART_DATALOGGER_U16_CNT ];
    #if ( UART_DATALOGGER_U8_CNT > 0 )
      u8 data_u8[ UART_DATALOGGER_U8_CNT ];
    #endif
    u8 chunkStopID;
  } uart_datalogger_buffer_t;
  _Pragma( "pack()" )

  typedef struct
  {
    uart_datalogger_buffer_t buf_1;
    uart_datalogger_buffer_t buf_2;
    u16 * data_ptr_u16[ UART_DATALOGGER_U16_CNT ];
    #if ( UART_DATALOGGER_U8_CNT > 0 )
      u8 * data_ptr_u8[ UART_DATALOGGER_U8_CNT ];
    #endif
    BOOL cur_buf;
    BOOL trigger;
    u16 cycle_cnt;
    u16 trigger_start_timestamp;
  } uart_datalogger_t;

/* ------------------
   local declarations
   ------------------ */

  static uart_datalogger_t uart_datalogger_data;
  static uart_data_t uart_data;


  INLINE static BOOL uart_test_rx_complete ( void )
  {
    BOOL result = False;

    if((u8) LINSCI_DMA_RX_LENGTH == 0u )
    {
      result = True;
    }
    else
    {
        while(( LINSCI_ERROR_bit.rx_overflow ) || ( LINSCI_STATUS_bit.rx_fifo_full ))
        {
          /* todo: while() ohne Abbruchbedingung?! */
          LINSCI_ERROR_bit.rx_overflow = 1u;
          LINSCI_DATA;
        }
    }

    return result;
  }

  static void uart_start_rx ( void )
  {
    LINSCI_DMA_RX_LENGTH = 0u;

    while(( LINSCI_ERROR_bit.rx_overflow ) || ( LINSCI_STATUS_bit.rx_fifo_full ))
    {
      /* todo: while() ohne Abbruchbedingung?! */
      LINSCI_ERROR_bit.rx_overflow = 1u;
      LINSCI_DATA;
    }

    LINSCI_IRQ_STATUS = 0xFFFFu;

    LINSCI_DMA_RX_ADDRESS        = ( u16 ) & uart_data.std_master_frame;
    LINSCI_DMA_RX_LENGTH         = sizeof( uart_master_frame_t );
    uart_data.rx_start_timestamp = systime_getU16();
  }

  INLINE static BOOL uart_test_rx_timeout ( u16 ticks )
  {
    BOOL result = False;

    if( LINSCI_DMA_RX_LENGTH == sizeof( uart_master_frame_t ))
    {
      /* wait for first char -> reset timeout */
      uart_data.rx_start_timestamp = systime_getU16();
    }
    else
    {
      if(( systime_getU16() - uart_data.rx_start_timestamp ) >= ticks )
      {
        result = True;
      }
    }

    return result;
  }

  INLINE static void uart_start_tx ( u16 addr, u8 length )
  {
    LINSCI_DMA_RX_LENGTH  = 0u;
    LINSCI_DMA_RX_ADDRESS = 0u;

    LINSCI_DMA_TX_ADDRESS = addr;
    LINSCI_DMA_TX_LENGTH  = (u16) length;
  }

  INLINE static BOOL uart_test_tx_complete ( void )
  {
    BOOL result = False;

    if( LINSCI_DMA_TX_LENGTH == 0u )
    {
      result = True;
    }

    return result;
  }

  INLINE static BOOL uart_is_rx ( void )
  {
    BOOL result = False;

    if( LINSCI_DMA_RX_ADDRESS != 0u )
    {
      result = True;
    }

    return result;
  }

/* helper */
  INLINE static BOOL uart_tx_frame_builder_err( u16 err_code, u16 * slave_buf, u16 * slave_frame_raw_length )
  {
    uart_data.std_tx_frame.err_code = err_code;
    uart_data.std_tx_frame.length   = 0u;
    *slave_buf                      = ( u16 ) & uart_data.std_tx_frame;
    *slave_frame_raw_length         = sizeof( uart_data.std_tx_frame.err_code ) + sizeof( uart_data.std_tx_frame.length ) + 0u;
    return True;
  }

  INLINE static BOOL uart_tx_frame_builder_ack( u16 * slave_buf, u16 * slave_frame_raw_length )
  {
    return uart_tx_frame_builder_err( 0u, slave_buf, slave_frame_raw_length );
  }

  INLINE static BOOL uart_tx_frame_builder_xLength( u16 length, u16 * slave_buf, u16 * slave_frame_raw_length )
  {
    uart_data.std_tx_frame.err_code = 0u;
    uart_data.std_tx_frame.length   = length;
    *slave_buf                      = ( u16 ) & uart_data.std_tx_frame;
    *slave_frame_raw_length         = sizeof( uart_data.std_tx_frame.err_code ) + sizeof( uart_data.std_tx_frame.length ) + length;
    return True;
  }

  INLINE static BOOL uart_tx_frame_builder_u32( u32 data, u16 * slave_buf, u16 * slave_frame_raw_length )
  {
    uart_data.std_tx_frame.err_code    = 0u;
    uart_data.std_tx_frame.length      = sizeof( u32 );
    uart_data.std_tx_frame.data32[ 0 ] = data;
    *slave_buf                         = ( u16 ) & uart_data.std_tx_frame;
    *slave_frame_raw_length            = sizeof( uart_data.std_tx_frame.err_code ) + sizeof( uart_data.std_tx_frame.length ) + sizeof( u32 );
    return True;
  }

  INLINE static BOOL uart_rx_user_handler_read ( const uart_master_frame_t * master_frame, u16 * slave_buf, u16 * slave_frame_raw_length )
  {
    BOOL result = False;

    switch((uart_addr_ext_t) master_frame->addr_ext )
    {
      case UART_ADDR_EXT_CPU:
      {
        if( master_frame->slave_frame_length <= sizeof( uart_data.std_tx_frame.data ))
        {
          result = uart_tx_frame_builder_xLength( master_frame->slave_frame_length, slave_buf, slave_frame_raw_length );
          memcpy((void *) uart_data.std_tx_frame.data, (void *) master_frame->addr, master_frame->slave_frame_length );
        }
        else
        {
          result = uart_tx_frame_builder_err( 1u, slave_buf, slave_frame_raw_length );
        }

        break;
      }

      case UART_ADDR_EXT_SYS_INFO:
      {
        if( master_frame->slave_frame_length <= sizeof( uart_data.std_tx_frame.data ))
        {
          result = uart_tx_frame_builder_xLength( master_frame->slave_frame_length, slave_buf, slave_frame_raw_length );
          (void) memcpy((void *) uart_data.std_tx_frame.data, (void *) (u16) (( u16 ) & uart_system_info + (u16) master_frame->addr ), master_frame->slave_frame_length );
        }
        else
        {
          result = uart_tx_frame_builder_err( 1u, slave_buf, slave_frame_raw_length );
        }

        break;
      }

      case UART_ADDR_EXT_DBG:
      {
        if( master_frame->slave_frame_length <= sizeof( uart_data.std_tx_frame.data ))
        {
          switch( master_frame->addr )
          {
            case 0u: {result = uart_tx_frame_builder_u32( systime_getU32(), slave_buf, slave_frame_raw_length ); break; }
            case 1u: {result = uart_tx_frame_builder_u32( 0u, slave_buf, slave_frame_raw_length ); break; }
            default: {result = uart_tx_frame_builder_err( 1u, slave_buf, slave_frame_raw_length ); break; }
          }
        }
        else
        {
          result = uart_tx_frame_builder_err( 1u, slave_buf, slave_frame_raw_length );
        }

        break;
      }

      case UART_ADDR_EXT_DLOG:
      {
        /* length field -> pwm-cycles */

        /* prepare the datalogger */
        #if ( UART_DATALOGGER_U16_CNT > 0 )
          uart_datalogger_data.data_ptr_u16[ 0 ] = (u16 *) master_frame->master_data[ 0 ];
        #endif

        #if ( UART_DATALOGGER_U16_CNT > 1 )
          uart_datalogger_data.data_ptr_u16[ 1 ] = (u16 *) master_frame->master_data[ 1 ];
        #endif

        #if ( UART_DATALOGGER_U16_CNT > 2 )
          uart_datalogger_data.data_ptr_u16[ 2 ] = (u16 *) master_frame->master_data[ 2 ];
        #endif

        #if ( UART_DATALOGGER_U16_CNT > 3 )
          uart_datalogger_data.data_ptr_u16[ 3 ] = (u16 *) master_frame->master_data[ 3 ];
        #endif

        #if ( UART_DATALOGGER_U16_CNT > 4 )
          uart_datalogger_data.data_ptr_u16[ 4 ] = (u16 *) master_frame->master_data[ 4 ];
        #endif

        #if ( UART_DATALOGGER_U8_CNT > 0 )
          uart_datalogger_data.data_ptr_u8[ 0 ] = (u8 *) master_frame->master_data[ UART_DATALOGGER_U16_CNT + 0 ];
        #endif

        #if ( UART_DATALOGGER_U8_CNT > 1 )
          uart_datalogger_data.data_ptr_u8[ 1 ] = (u8 *) master_frame->master_data[ UART_DATALOGGER_U16_CNT + 1 ];
        #endif

        #if ( UART_DATALOGGER_U8_CNT > 2 )
          uart_datalogger_data.data_ptr_u8[ 2 ] = (u8 *) master_frame->master_data[ UART_DATALOGGER_U16_CNT + 2 ];
        #endif

        uart_data.datalogger_cycle_cnt_init = master_frame->slave_frame_length;

        uart_data.std_tx_frame.err_code = 0u;
        uart_data.std_tx_frame.length   = master_frame->slave_frame_length;
        *slave_buf                      = U16PTR_TO_U16((u16 *) &uart_data.std_tx_frame );
        *slave_frame_raw_length         = sizeof( uart_data.std_tx_frame.err_code ) + sizeof( uart_data.std_tx_frame.length ) + 0u;
        result                          = True;

        break;
      }
      default:
      {
/* Message 2016 */
        break;
      }
    }

    return result;
  }

  INLINE static BOOL uart_rx_user_handler_write ( uart_master_frame_t * master_frame, u16 * slave_buf, u16 * slave_frame_raw_length )
  {
    BOOL result = False;

    switch((uart_addr_ext_t) master_frame->addr_ext )
    {
      /* WRITE to CPU address space */
      case UART_ADDR_EXT_CPU:
      {
        if(( master_frame->master_frame_length == 2u ) && (( master_frame->addr & 1u ) == 0u ))
        {
          WRITE_REG_U16((void *) master_frame->addr, master_frame->master_data[ 0 ] );
          result = uart_tx_frame_builder_ack( slave_buf, slave_frame_raw_length );
        }
        else if( master_frame->slave_frame_length <= sizeof( uart_data.std_tx_frame.data ))
        {
          memcpy((void *) master_frame->addr, (void *) master_frame->master_data, master_frame->master_frame_length );
          result = uart_tx_frame_builder_ack( slave_buf, slave_frame_raw_length );
        }
        else
        {
          result = uart_tx_frame_builder_err( 1u, slave_buf, slave_frame_raw_length );
        }

        break;
      }

      /* WRITE to MC module */
      case UART_ADDR_EXT_MC:
      {
        result = uart_tx_frame_builder_err( 1u, slave_buf, slave_frame_raw_length );

        #if 0
          if( master_frame->slave_frame_length <= sizeof( uart_data.std_tx_frame.data ))
          {
            switch( master_frame->addr )
            {
              default:
              {
                result = uart_tx_frame_builder_err( 1u, slave_buf, slave_frame_raw_length );
                break;
              }
            }
          }
          else
          {
            result = uart_tx_frame_builder_err( 1u, slave_buf, slave_frame_raw_length );
          }
        #endif

        break;
      }


      /* WRITE not handled */
      default:
      {
/* Message 2016 */
        break;
      }
    }

    return result;
  }

  INLINE static BOOL uart_rx_user_handler ( uart_master_frame_t * master_frame, u16 * slave_buf, u16 * slave_frame_raw_length )
  {
    BOOL result = False;

    switch((uart_cmd_code_t) master_frame->cmd_code )
    {
      case UART_CMD_CODE_READ:
      {
        result = uart_rx_user_handler_read( master_frame, slave_buf, slave_frame_raw_length );
        break;
      }

      case UART_CMD_CODE_WRITE:
      {
        result = uart_rx_user_handler_write( master_frame, slave_buf, slave_frame_raw_length );
        break;
      }

      default:
      {
/* Message 2016 */
        break;
      }
    }

    return result;
  }

  INLINE void uart_datalogger_exec ( void )
  {
    if( uart_datalogger_data.trigger )
    {
      if( uart_datalogger_data.cycle_cnt != 0u )
      {
        /* todo: remove */
        if( LINSCI_DMA_TX_LENGTH != 0u )
        {
          /* error handling */
        }
        else
        {
          uart_datalogger_data.cycle_cnt--;

          if( uart_datalogger_data.cur_buf )
          {
            #if ( UART_DATALOGGER_U16_CNT > 0 )
              uart_datalogger_data.buf_1.data_u16[ 0 ] = *uart_datalogger_data.data_ptr_u16[ 0 ];
            #endif

            #if ( UART_DATALOGGER_U16_CNT > 1 )
              uart_datalogger_data.buf_1.data_u16[ 1 ] = *uart_datalogger_data.data_ptr_u16[ 1 ];
            #endif

            #if ( UART_DATALOGGER_U16_CNT > 2 )
              uart_datalogger_data.buf_1.data_u16[ 2 ] = *uart_datalogger_data.data_ptr_u16[ 2 ];
            #endif

            #if ( UART_DATALOGGER_U16_CNT > 3 )
              uart_datalogger_data.buf_1.data_u16[ 3 ] = *uart_datalogger_data.data_ptr_u16[ 3 ];
            #endif

            #if ( UART_DATALOGGER_U16_CNT > 4 )
              uart_datalogger_data.buf_1.data_u16[ 4 ] = *uart_datalogger_data.data_ptr_u16[ 4 ];
            #endif

            #if ( UART_DATALOGGER_U8_CNT > 0 )
              uart_datalogger_data.buf_1.data_u8[ 0 ] = *uart_datalogger_data.data_ptr_u8[ 0 ];
            #endif

            #if ( UART_DATALOGGER_U8_CNT > 1 )
              uart_datalogger_data.buf_1.data_u8[ 1 ] = *uart_datalogger_data.data_ptr_u8[ 1 ];
            #endif

            #if ( UART_DATALOGGER_U8_CNT > 2 )
              uart_datalogger_data.buf_1.data_u8[ 2 ] = *uart_datalogger_data.data_ptr_u8[ 2 ];
            #endif

            uart_start_tx(( u16 ) & uart_datalogger_data.buf_1, sizeof( uart_datalogger_data.buf_1 ));

            uart_datalogger_data.cur_buf = False;
          }
          else
          {
            #if ( UART_DATALOGGER_U16_CNT > 0 )
              uart_datalogger_data.buf_2.data_u16[ 0 ] = *uart_datalogger_data.data_ptr_u16[ 0 ];
            #endif

            #if ( UART_DATALOGGER_U16_CNT > 1 )
              uart_datalogger_data.buf_2.data_u16[ 1 ] = *uart_datalogger_data.data_ptr_u16[ 1 ];
            #endif

            #if ( UART_DATALOGGER_U16_CNT > 2 )
              uart_datalogger_data.buf_2.data_u16[ 2 ] = *uart_datalogger_data.data_ptr_u16[ 2 ];
            #endif

            #if ( UART_DATALOGGER_U16_CNT > 3 )
              uart_datalogger_data.buf_2.data_u16[ 3 ] = *uart_datalogger_data.data_ptr_u16[ 3 ];
            #endif

            #if ( UART_DATALOGGER_U16_CNT > 4 )
              uart_datalogger_data.buf_2.data_u16[ 4 ] = *uart_datalogger_data.data_ptr_u16[ 4 ];
            #endif

            #if ( UART_DATALOGGER_U8_CNT > 0 )
              uart_datalogger_data.buf_2.data_u8[ 0 ] = *uart_datalogger_data.data_ptr_u8[ 0 ];
            #endif

            #if ( UART_DATALOGGER_U8_CNT > 1 )
              uart_datalogger_data.buf_2.data_u8[ 1 ] = *uart_datalogger_data.data_ptr_u8[ 1 ];
            #endif

            #if ( UART_DATALOGGER_U8_CNT > 2 )
              uart_datalogger_data.buf_2.data_u8[ 2 ] = *uart_datalogger_data.data_ptr_u8[ 2 ];
            #endif

            uart_start_tx(( u16 ) & uart_datalogger_data.buf_2, sizeof( uart_datalogger_data.buf_2 ));

            uart_datalogger_data.cur_buf = False;
          }
        }
      }
    }
  }

  INLINE void uart_set_data_logger_trigger( void )
  {
    uart_datalogger_data.trigger = True;
  }

/*! @brief init/configure UART */
  void uart_init( void )
  {
    LINSCI_BAUD_RATE = (u16) (((u32) ( F_CPU ) * 2uL ) / (u32) ( UART_BAUD_RATE ));

    LINSCI_UART_CONFIG_bit.debounce = 0u;

    LINSCI_UART_CONFIG_bit.re = 1u;
    LINSCI_UART_CONFIG_bit.te = 1u;

    memset((void *) &uart_datalogger_data, 0, sizeof( uart_datalogger_data ));
    memset((void *) &uart_data, 0, sizeof( uart_data ));

    uart_datalogger_data.buf_1.chunkStartID = 0x55u;
    uart_datalogger_data.buf_1.chunkStopID  = 0xAAu;
    uart_datalogger_data.buf_2.chunkStartID = 0x55u;
    uart_datalogger_data.buf_2.chunkStopID  = 0xAAu;

    uart_datalogger_data.trigger = False;

    uart_start_rx();
  }

/*! @brief handle UART communication
 *
 * @details
 * must be called as often as possible, at least every
 * [duration of a transmission of a byte], otherwise bytes may get lost...
 * all functionalities of module UART are handled (RX, TX, command interpreter)
 */
  void uart_main ( void )
  {
    while( LINSCI_IRQ_STATUS_bit.rx_dma_finished == 1u )
    {
      /* clear interrupt flag */
      LINSCI_IRQ_STATUS_bit.rx_dma_finished = 1u;
    }

    if( uart_is_rx())
    {
      /* rx mode */
      if( uart_test_rx_complete())
      {
        u16 tx_buf;
        u16 tx_length;

        if( uart_rx_user_handler( &uart_data.std_master_frame, &tx_buf, &tx_length ))
        {
          uart_start_tx( tx_buf, (u8) tx_length );
        }
        else
        {
          uart_start_rx();
        }
      }
      else if( uart_test_rx_timeout((u16) (UART_TX_TIMEOUT_TICKS) +1u ))
      {
        uart_start_rx();
      }
      else
      {
/*Message 2013*/
      }
    }
    else
    {
      /* tx mode */

      if( uart_test_tx_complete())
      {
        if( uart_datalogger_data.cycle_cnt == 0u )
        {
          if( uart_data.datalogger_cycle_cnt_init == 0u )
          {
            uart_start_rx();
          }
          else
          {
            uart_datalogger_data.trigger_start_timestamp = systime_getU16();
            uart_datalogger_data.trigger                 = False;
            uart_datalogger_data.cycle_cnt               = uart_data.datalogger_cycle_cnt_init;
            uart_data.datalogger_cycle_cnt_init          = 0u;
          }
        }
        else
        {
          if( uart_datalogger_data.trigger == False )
          {
            if(( systime_getU16() - uart_datalogger_data.trigger_start_timestamp ) >= (u16) ((dbl) SYSTIME_TICKS_PER_SEC * 1.0 ))
            {
              /* timeout */
              uart_start_rx();
            }
          }
        }
      }
    }
  }

#endif

/* }@
 */
