/***************************************************************************//**
 * @file			LinDrvImp_Callbacks.c
 *
 * @creator		sbai
 * @created		16.07.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief               TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinDrvImp_Callbacks.h"

/* ****************************************************************************/
/* ***************************** USER INCLUDES ********************************/
/* ****************************************************************************/
#include <string.h>
#if 0
/* todo: changed! */
 #include "mod_cctimer.h"
 #include "mod_gpio.h"
#endif
#include <defines.h>
#include "fault_manager.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define  WAITTIME_WAIT_GOTOSLEEP    4000
#define  WAITTIME_RESETVALUE_FIRST  1500
#define  WAITTIME_RESETVALUE_SECOND 175
#define  WAITTIME_RESETVALUE_THIRD  175
#define  WAITTIME_RESETVALUE_FOURTH 1550

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/
uint16_t loc_waitTime = WAITTIME_WAIT_GOTOSLEEP;
Lin_Bool_t LinDrvImp_ResponseErrorSignal = LIN_FALSE;


/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/*                               BUS CALLBACKS                                */
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/

/* ****************************************************************************/
/*                              PROTO CALLBACKS                               */
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void LinDrvImp_ProtoErrorCbFun( LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                                LinProtoIf_Error_t error, LinBusIf_FrameID_t frameID,
                                LinProtoIf_cpFrameDescription_t frameDesc, LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData )
{
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void LinDrvImp_ProtoRestartCbFun( LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                                  LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData )
{

}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
LinProtoIf_eMeasDoneRetVal_t LinDrvImp_ProtoMeasDoneCbFun( LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                                                           LinProtoIf_Baudrate_t exp_baudrate, LinProtoIf_Baudrate_t prev_baudrate,
                                                           LinProtoIf_Baudrate_t meas_baudrate, LinProtoIf_BreakLen_t meas_break_len,
                                                           LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData )
{
  LinProtoIf_eMeasDoneRetVal_t retVal = LinProtoIf_MeasDoneRetVal_UPDATE;

  return ( retVal );
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
LinProtoIf_eIdleAction_t LinDrvImp_ProtoIdleCbFun( LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                                                   LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData )
{
  return ( LinBusIf_IDLEACT_SLEEP );
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void LinDrvImp_ProtoWakupCbFun( LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t protoIfFuns,
                                LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData )
{

}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
LinProtoIf_eMsgAction_t LinDrvImp_ProtoFrameIdProcCbFun( LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t ifFunctions,
                                                         LinBusIf_FrameID_t frameID, LinProtoIf_cpFrameDescription_t frameDesc,
                                                         LinProtoIf_Error_t error, LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData )
{
  return ( LinBusIf_MSGACT_CONTINUNE );
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
LinProtoIf_eMsgAction_t LinDrvImp_ResponseErrorFrameProcCbFun( LinProtoIf_pGenericEnvData_t genericProtoEnvData, LinProtoIf_cpInterfaceFunctions_t ifFunctions,
                                                               LinBusIf_FrameID_t frameID, LinProtoIf_cpFrameDescription_t frameDesc,
                                                               LinProtoIf_Error_t error, LinProtoIf_pGenericCbCtxData_t genericProtoCbCtxData )
{
  LinDrvImp_ResponseErrorSignal = 0;

  return ( LinBusIf_MSGACT_CONTINUNE );
}

/* ****************************************************************************/
/*                              TRANS CALLBACKS                               */
/* ****************************************************************************/

/* ****************************************************************************/
/*                           DATA STORAGE CALLBACKS                           */
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void LinDrvImp_DataStgErrorCbFun( LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, LinDataStgIf_cpInterfaceFunctions_t protoIfFuns,
                                  LinDataStgIf_Error_t error, LinDataStgIf_pGenericCbCtxData_t genericDataStgCbCtxData )
{

}

/* ****************************************************************************/
/*                               DIAG CALLBACKS                               */
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void LinDrvImp_DiagErrorCbFun( LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                               LinDiagIf_Error_t error, LinTransIf_SID_t SID,
                               LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData )
{
  if(( error != LinBusIf_ERR_HEADER_TIMEOUT ) &&
     ( error != LinBusIf_ERR_MESSAGE_TIMEOUT_NO_DATA ) &&
     ( error != LinBusIf_ERR_PID_PARITY ) &&
     ( error != LinBusIf_ERR_SYNC_FAIL ))
  {
    LinDrvImp_ResponseErrorSignal = 1;
  }
  FLT_MGR_Set_Lin_Fault_Callback((uint16_t) error);
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void LinDrvImp_DiagGoToSleepCbFun( LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                                   LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData )
{
}


