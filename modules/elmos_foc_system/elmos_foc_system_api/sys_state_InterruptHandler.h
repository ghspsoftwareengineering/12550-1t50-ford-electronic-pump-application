/***************************************************************************//**
 * @file			sys_state_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef SYS_STATE_INTERRUPTHANDLER_H_          
#define SYS_STATE_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * SYS_STATE IRQ vector numbers
 ******************************************************************************/
typedef enum {    
  sys_state_IRQ_WATCHDOG           = 0u,
  sys_state_IRQ_SW_RESET           = 1u,
  sys_state_IRQ_CPU_PARITY         = 2u,
  sys_state_IRQ_SRAM_PARITY        = 3u,
  sys_state_IRQ_FLASH_2BIT_ERR     = 4u,
  sys_state_IRQ_FLASH_1BIT_ERR     = 5u,
  sys_state_IRQ_SRAM_WR_PROT       = 6u,
  sys_state_IRQ_STACK_PROT         = 7u,
  sys_state_IRQ_EXEC_PROT          = 8u,
  sys_state_IRQ_SW_BL_RESET_0      = 9u,
  sys_state_IRQ_SW_BL_RESET_1      = 10u,
  
  sys_state_INTERRUPT_VECTOR_CNT   = 11u  /**< Number of available interrupt vectors */
} sys_state_eInterruptVectorNum_t;

/***************************************************************************//**
 * Pointer to SYS_STATE context data
 ******************************************************************************/
typedef void * sys_state_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*sys_state_InterruptCallback_t) (sys_state_eInterruptVectorNum_t irqsrc, sys_state_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * SYS_STATE environment data
 ******************************************************************************/
typedef struct sys_state_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module */
    sys_state_InterruptCallback_t InterrupVectorTable[sys_state_INTERRUPT_VECTOR_CNT];
    
    /** SYS_STATE module context data */
    sys_state_pInterruptContextData_t ContextData;
    
} sys_state_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to SYS_STATE environment data
 ******************************************************************************/
typedef sys_state_sInterruptEnvironmentData_t * sys_state_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to SYS_STATE module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with sys_state_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The SYS_STATE IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void sys_state_InterruptEnable(sys_state_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to SYS_STATE module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The SYS_STATE IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void sys_state_InterruptDisable(sys_state_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to SYS_STATE module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void sys_state_InterruptRegisterCallback(sys_state_eInterruptVectorNum_t irqvecnum, sys_state_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to SYS_STATE module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void sys_state_InterruptDeregisterCallback(sys_state_eInterruptVectorNum_t irqvecnum);


/***************************************************************************//**
 * Handles the SYS_STATE related interrupt requests.
 *
 ******************************************************************************/
 __interrupt void sys_state_InterruptHandler(void);


/***************************************************************************//**
 * @brief Initialize SYS_STATE module
 *
 * @param environmentdata  Pointer to Environment data for SYS_STATE module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and SYS_STATE (sys_state_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       SYS_STATE module is configured for use.
 *
 * @detaildesc
 * Initializes the SYS_STATE software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as SYS_STATEs or not.
 *
 ******************************************************************************/
void sys_state_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, sys_state_pInterruptEnvironmentData_t environmentdata, sys_state_pInterruptContextData_t contextdata);


#endif /* SYS_STATE_INTERRUPTHANDLER_H_ */

