#
# Directory that contains the reusable make rules
#
GHSP_MAKE_DIR := ghsp_make

# DO NOT DELETE
ifeq ($(GHSP_MAKE_DIR),)
   $(error "GHSP_MAKE_DIR not set. This must be set to the directory in the project that contains the reusable make rules!!!")
endif

# Include the reusable make rules to do the rest of the work
include $(GHSP_MAKE_DIR)/pre_project.mak

#########################################
#         Build Definitions             #
#########################################
BUILD_LIST := 1t50
#
# Define the toolchain for each build. This describes the compiler and platform
# that will be used in order to compile source files, create libraries and link 
# executables as necessary.
#
# Every build must have a defined <build>_TOOLCHAIN

#
# Ford 1T50 Electronic Pump
#
1t50_TOOLCHAIN          := IAR_MSP430
1t50_HEX_FILE_EXTENSION := .s28
1t50_LINKER_FILE_DIR    := project_specific/linker_files
1t50_MOTOR_CONFIG_FILE  := config/1t50/motor.cfg
1t50_IAR_PROJECT_FILE   := extras/IAR/e52306a.ewp
#$(call GHSP_MAKE_CREATE_BUILD, 1t50, IAR_MSP430, .s28, linker_files/1t50, vector-hexview)

#
# Unit Test build settings
#
$(call GHSP_MAKE_CREATE_DEFAULT_UNIT_TEST_BUILD, unit_test)

########################################
# Project File Structure Configuration #
########################################

#
# Directory within the project file structure that contains the project's
# configuration files. 
#
# If the project contains no configuration items, PROJECT_CONFIG_DIRECTORY should remain
# unset.

PROJECT_CONFIG_DIRECTORY := config

# Add the software requirements rule
sw-reqs-coverage:
	$(RUBY) rb/unit_test_reqs.rb "docs/1T50 Snapshot for Unit Test.csv" project_specific $(PROJECT_OUT_DIRECTORY)/unit_test_reqs

.PHONY: clean
clean:
	@$(RM) -rf $(PROJECT_OUT_DIRECTORY)/unit_test_reqs

#
# Optional Project output directory. 
# If specified, this directory will contain all of the generated output files for the build.
# If NOT specified, the default of out/ will be used

### PROJ_OUT_DIR := <directory>

# DO NOT DELETE
# Include the reusable make rules to do the rest of the work
include $(GHSP_MAKE_DIR)/project.mak
