/**
 *  @file fdl.c
 *
 *  @ref fdl_api
 *
 *  @copyright 2020 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup fdl_imp Flash Driver Library Implementation
 *  @{
 *
 *      System specific implementation of the @ref fdl_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fdl.h"
#include "global.h"
#include "nv25080_driver.h"

#include <string.h>

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
static uint32_t write_address = 0u;
static uint32_t read_address = 0u;
static uint32_t erase_address = 0u;
static uint8_t Length = 0u;
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
bool_t FDL_Init(void)
{
    NV25080_Init();
    return true;
}

bool_t FDL_Is_Initialized(void)
{
    return true;
}

bool_t FDL_Is_Busy(void)
{
    return NV25080_Is_Busy(); 
}

FDL_Status_T FDL_Poll(void)
{
    FDL_Status_T rv = FDL_STATUS_SUCCESS;

    if(NV25080_Is_Busy())
    {
        rv = FDL_STATUS_BUSY;
    }
    return (rv);
}


FDL_Status_T FDL_Write(uint8_t const *p_src_buffer, uint32_t dest_addr, uint8_t num_bytes)
{

    write_address = dest_addr;
    Length = num_bytes;
    NV25080_Write_Multiple_Bytes((uint16_t)write_address, p_src_buffer, Length);  
    return FDL_STATUS_BUSY;
}


FDL_Status_T FDL_Erase(uint32_t address, uint8_t num_bytes)
{
    erase_address = address;
    Length = num_bytes;
    NV25080_Erase((uint16_t)erase_address, Length); 
    return FDL_STATUS_BUSY;
}


FDL_Status_T FDL_Read(uint8_t * const p_dest_buffer, uint32_t src_addr, uint8_t num_bytes)
{
    read_address = src_addr;
    Length = num_bytes;
    NV25080_Read_Multiple_Bytes((uint16_t) read_address, p_dest_buffer, Length);  
    return FDL_STATUS_SUCCESS;
}

/** @} doxygen end group */
