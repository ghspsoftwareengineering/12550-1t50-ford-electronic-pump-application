/**
 * @ingroup HW
 *
 * @{
 */

#ifndef DIAG_PWM_OUT_H
#define DIAG_PWM_OUT_H

#include <defines.h>

typedef struct
{
  u16 cmp_val;
  u16 period_val;

  u16 struct_signature;

} diago_data_t;

/* -------------------
   global declarations
   ------------------- */

#if ( USE_PWM_DIAG_OUTPUT == 1 )
  void diago_init( const u16 prescaler );
  void diago_main( void );
  void diago_enable( const BOOL enable );
  void diago_set_dutycycle( const u16 cmp_val );
  void diago_set_period( const u16 period_val );
#endif

#endif /* _#ifndef DIAG_PWM_OUT_H */

/* }@
 */
