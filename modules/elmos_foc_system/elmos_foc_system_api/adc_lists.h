/**
 * @ingroup HW
 *
 * @{
 */

#ifndef ADC_LISTS_
#define ADC_LISTS_

#include "adc.h"
#include "global.h"

#define ADC_LISTCMD_IMMEDIATE_SAMPLE    ( 0xFFC0u ) /**< pretty much what the name suggests */
#define ADC_LISTCMD_WRITE_BUS           ( 0xFFF0u ) /**< "WRITE_BUS"       Store target_adr to 0x02[14:0] with AdrBit[15]=0 */
#define ADC_LISTCMD_CFG_WRITE           ( 0xFFF1u ) /**< "CFG_WRITE"       Store target_adr to CFG register */
#define ADC_LISTCMD_SYNC_OUT            ( 0xFFF2u ) /**< "SYNC_OUT"        trigger sync out event (see SYNC_OUT_CFG) */
#define ADC_LISTCMD_WAIT                ( 0xFFF3u ) /**< "WADC_LISTCMDT"   wADC_LISTCMDt for target_adr+2 clk cycles */
#define ADC_LISTCMD_ST_BP               ( 0xFFF4u ) /**< "ST_BP"           store base_phase in place of sum */
#define ADC_LISTCMD_ST_TADR             ( 0xFFF5u ) /**< "ST_TADR"         store TADR in place of sum */
#define ADC_LISTCMD_LOOP_UNTIL_BP       ( 0xFFF6u ) /**< "LOOP_UNTIL"      (0x02[14:0] <= base_phase <= target_adr[15:0]) */
#define ADC_LISTCMD_LOOP_UNTIL_NOT_BP   ( 0xFFF7u ) /**< "LOOP_UNTIL NOT"  (0x02[14:0] <= base_phase <= target_adr[15:0]) */
#define ADC_LISTCMD_LOOP_UNTIL_TADR     ( 0xFFF8u ) /**< "LOOP_UNTIL"      (0x02[14:0]<= TADR <= target_adr[15:0]) */
#define ADC_LISTCMD_LOOP_UNTIL_NOT_TADR ( 0xFFF9u ) /**< "LOOP_UNTIL NOT"  (0x02[14:0]<=TADR<= target_adr[15:0]) */
#define ADC_LISTCMD_GOTO_SUM            ( 0xFFFAu ) /**< "GOTO_SUM"        target_adr[15:0] WHEN last_SUM <= 0x02[14:0] */
#define ADC_LISTCMD_GOTO_NOT_SUM        ( 0xFFFBu ) /**< "GOTO_NOT_SUM"    target_adr[15:0] WHEN NOT last_SUM <= 0x02[14:0] */
#define ADC_LISTCMD_GOTO_BP             ( 0xFFFCu ) /**< "GOTO_BP"         target_adr[15:0] WHEN BP <= 0x02[14:0] */
#define ADC_LISTCMD_GOTO_NOT_BP         ( 0xFFFDu ) /**< "GOTO_NOT_BP"     target_adr[15:0] WHEN NOT BP <= 0x02[14:0] */
#define ADC_LISTCMD_GOTO_TADR           ( 0xFFFEu ) /**< "GOTO_TADR"       target_adr[15:0] WHEN TADR[14:0] = 0x02[14:0] if 0x02[14:0] = 0 ? GOTO always */
#define ADC_LISTCMD_GOTO_ALWAYS_1400    ( 0xFFFEu ) /**< "GOTO_TADR"       0x02[14:0] = 0 -> GOTO always if 14 downto 00 */
#define ADC_LISTCMD_END                 ( 0xFFFFu ) /**< "END"             list completed (last list item) */

/* values for field "trigger" in ADC list (refer to spec of 523.99) */

/* dead time events I/U _ u,v,w phase _ rise/fall */

/** Triggers after DEAD_TIME_WAIT0==I cycles if phase U was Rising.*/
#define I_u_r ADC_TRIG_U_RIS_DT0

/** Triggers after DEAD_TIME_WAIT0==I cycles if phase V was Rising.*/
#define I_v_r ADC_TRIG_V_RIS_DT0

/** Triggers after DEAD_TIME_WAIT0==I cycles if phase W was Rising.*/
#define I_w_r ADC_TRIG_W_RIS_DT0

#define I_A_r ( 0x042Au )

/** Triggers after DEAD_TIME_WAIT0==I cycles if phase U was Falling.*/
#define I_u_f ADC_TRIG_U_FALL_DT0

/** Triggers after DEAD_TIME_WAIT0==I cycles if phase V was Falling.*/
#define I_v_f ADC_TRIG_V_FALL_DT0

/** Triggers after DEAD_TIME_WAIT0==I cycles if phase W was Falling.*/
#define I_w_f ADC_TRIG_W_FALL_DT0

#define I_A_f ( 0x0415u )

/** Triggers after DEAD_TIME_WAIT1==U_X_r cycles if phase U was Rising.*/
#define U_u_r ADC_TRIG_U_RIS_DT1

/** Triggers after DEAD_TIME_WAIT1==U_X_r cycles if phase V was Rising.*/
#define U_v_r ADC_TRIG_V_RIS_DT1

/** Triggers after DEAD_TIME_WAIT1==U_X_r cycles if phase W was Rising.*/
#define U_w_r ADC_TRIG_W_RIS_DT1

/** Triggers after DEAD_TIME_WAIT2==U_X_f cycles if phase U was Falling.*/
#define U_u_f ADC_TRIG_U_FALL_DT2

/** Triggers after DEAD_TIME_WAIT2==U_X_f cycles if phase V was Falling.*/
#define U_v_f ADC_TRIG_V_FALL_DT2

/** Triggers after DEAD_TIME_WAIT2==U_X_f cycles if phase W was Falling.*/
#define U_w_f ADC_TRIG_W_FALL_DT2

/* -------------------
   global declarations
   ------------------- */

saradc_ctrl_list_entry_t * adc_lists_get_commutation_list ( void );
saradc_ctrl_list_entry_t * adc_lists_get_meas_bemf_list ( void );

void adc_lists_set_user_channel( const uint8_t idx, const uint8_t adc_channel );

#if ( USR_EN_INITIAL_POSITION_DETECTION == 1 )
  saradc_ctrl_list_entry_t * adc_lists_get_pos_detect_list ( void );

  void adc_lists_set_pos_detect_channel( const uint8_t adc_channel, const uint8_t vec_no, const uint16_t trig_time );
  void adc_lists_set_pos_detect_meas_dly( const uint16_t meas_delay );
#endif

#endif /* _#ifndef ADC_LISTS_H_ */

/* }@
 */
