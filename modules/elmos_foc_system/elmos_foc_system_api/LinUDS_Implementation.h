/***************************************************************************//**
 * @file			LinDiag_Implementation.h
 *
 * @creator		sbai
 * @created		12.02.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: LinUDS_Implementation.h 2415 2017-05-11 06:27:19Z sbai $
 *
 * $Revision: 2415 $
 *
 ******************************************************************************/

#ifndef LINUDS_IMPLEMENTATION_H_
#define LINUDS_MPLEMENTATION_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinDrvImp_CompilationConfig.h"
#include "LinUDS_Interface.h"
#include "LinDataStg_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINUDSIMP_VERSION             0x0110u /**< LIN DIAG implementation version */

#define LINUDSIMP_CONFIG_DATA_VERSION 0x0100  /* Expected config data version */

#define LINUDS_SUP_CLASS_1            1
#define LINUDS_SUP_CLASS_2            0
#define LINUDS_SUP_CLASS_3            0

#define ELMOS_BLTEST                  0

#define LINUDS_SNPD_REQ_MSG_LEN       5

#define LINUDS_SNPD_NAD_POS           4

#define LINUDS_SNPD_CMD_MEASURE       1
#define LINUDS_SNPD_CMD_KEEP_NAD      2
#define LINUDS_SNPD_CMD_STORE_NAD     3
#define LINUDS_SNPD_CMD_DISABLE       4

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/
/***************************************************************************//**
 * TODO: Concrete description.
 *
 ******************************************************************************/
struct LinUDSImp_sCfgData
{
    Lin_Version_t           Version;             /* Config data struct version */

    LinTransIf_sInitParam_t LinTransIfInitParam;
    LinBusIf_sThis_t        LinBusIfThisPointer;
    LinLookupIf_sThis_t     LinLookupIfThisPointer;
#if LINUDS_SUP_DATASTG == 1
    LinDataStgIf_sThis_t              LinDataStgIfThisPointer;
#else
    LinUDSIf_sProductIdentification_t InitialProdIdent;
    LinUDSIf_NAD_t                    InitialNad;
    LinUDSIf_SerialNumber_t           InitialSerialNumber;
#endif
#if LINUDS_SUP_SNPD == 1    
    LinSNPDIf_sThis_t       LinSNPDIfThisPointer;
    Lin_Bool_t              ImmediateNADUpdate;
#endif    
};

typedef struct LinUDSImp_sCfgData    LinUDSImp_sCfgData_t;
typedef        LinUDSImp_sCfgData_t* LinUDSImp_pCfgData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINUDSIMP_EXT_IFFUN_STRCT_ACCESS == 1
extern const LinUDSIf_sInterfaceFunctions_t LinUDSImp_InterfaceFunctions;
#endif /* LINUDSIMP_EXT_IFFUN_STRCT_ACCESS == 1 */

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
 Lin_Bool_t LinUDSImp_Initialization(LinUDSIf_pGenericEnvData_t     genericDiagEnvData,      LinUDSIf_EnvDataSze_t         diagEnvDataSze,
                                     LinUDSIf_cpCallbackFunctions_t diagCbFuns,              LinUDSIf_pGenericCbCtxData_t  genericDiagCbCtxData,
                                     Lin_Bool_t                     invalidReadByIDAnswered, LinUDSIf_pGenericImpCfgData_t genericDiagImpCfgData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
void LinUDSImp_Task(LinUDSIf_pGenericEnvData_t genericDiagEnvData);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinUDSImp_GetSubInterface(LinUDSIf_pGenericEnvData_t genericDiagEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
LinTransIf_NAD_t LinUDSImp_GetNad(LinUDSIf_pGenericEnvData_t genericDiagEnvData, LinUDSIf_eNADType_t nadtype);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
void LinUDSImp_SetNad(LinUDSIf_pGenericEnvData_t genericDiagEnvData, LinTransIf_NAD_t nad);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinUDSImp_AddRbiTable(LinUDSIf_pGenericEnvData_t genericDiagEnvData, LinUDSIf_pRbiLookupEntry_t rbiTbl);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinUDSImp_RmvRbiTable(LinUDSIf_pGenericEnvData_t genericDiagEnvData, LinUDSIf_pRbiLookupEntry_t rbiTbl);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinUDSImp_AddDidTable(LinUDSIf_pGenericEnvData_t genericDiagEnvData, LinUDSIf_cpDidLookupEntry_t didTbl);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinUDSImp_RmvDidTable(LinUDSIf_pGenericEnvData_t genericDiagEnvData, LinUDSIf_cpDidLookupEntry_t didTbl);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinUDSImp_AddRoutineIdTable(LinUDSIf_pGenericEnvData_t genericDiagEnvData, LinUDSIf_cpRoutineIdLookupEntry_t routineIdTbl);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinUDSImp_RmvRoutineIdTable(LinUDSIf_pGenericEnvData_t genericDiagEnvData, LinUDSIf_cpRoutineIdLookupEntry_t routineIdTbl);

/***************************************************************************//**
 * TODO: A short description.
 *
 * @param param TODO: Parameter description
 *
 * @return      TODO: return description
 *
 ******************************************************************************/
Lin_Bool_t LinUDSImp_SetUpTimer(LinUDSIf_pGenericEnvData_t genericDiagEnvData, Lin_uint8_t                  timerNum,
                                LinUDSIf_Timeout_t         timeout,            LinUDSIf_pGenericCbCtxData_t genericDiagCbCtxData);

#endif /* LINUDS_IMPLEMENTATION_H_ */

