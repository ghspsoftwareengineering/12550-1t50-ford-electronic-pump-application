/**
 *  @file mfg_data_dids.c
 *
 *  @ref mfg_data_dids_api
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup mfg_data_dids_imp mfg_data_dids Implementation
 *  @{
 *
 *      System specific implementation of the @ref mfg_data_dids_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "fault_logger.h"
#include "global.h"
#include "mfg_data_dids.h"
#include "mfg_data_defines.h"
#include "uds_types.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/
/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/
/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/
/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/
/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/
/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/
/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/

UDS_Response_Code_T MFDIDS_Get_Sw_Part_Number(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    size_t const num_bytes_serialized = SDB_Serialize_Bytes(p_Resp_SDB,
                                                            (void const *)MFD_SW_APPLICATION_PART_NUMBER,
                                                            MFD_SW_PART_NUM_LEN);

    return (num_bytes_serialized == (size_t)MFD_SW_PART_NUM_LEN) ? UDS_RESP_POSITIVE : UDS_RESP_RESPONSE_TOO_LONG;
}

UDS_Response_Code_T MFDIDS_Get_SW_Version(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    size_t const num_bytes_serialized = SDB_Serialize_Bytes(p_Resp_SDB,
                                                            (void const *)MFD_SW_VERSION_STRING,
                                                            MFD_SW_VERSION_LEN);

    return (num_bytes_serialized == (size_t)MFD_SW_VERSION_LEN) ? UDS_RESP_POSITIVE : UDS_RESP_RESPONSE_TOO_LONG;
}

UDS_Response_Code_T MFDIDS_Get_HW_Part_Number(Serial_Data_Buffer_T * const p_Resp_SDB)
{
    size_t const num_bytes_serialized = SDB_Serialize_Bytes(p_Resp_SDB,
                                                            (void const *)MFD_HW_PART_NUMBER,
                                                            MFD_HW_PART_NUM_LEN);

    return (num_bytes_serialized == (size_t)MFD_HW_PART_NUM_LEN) ? UDS_RESP_POSITIVE : UDS_RESP_RESPONSE_TOO_LONG;
}

UDS_Response_Code_T MFDIDS_Reset_EEPROM(Serial_Data_Buffer_T * const p_Req_SDB)
{
    /*uint8_t Clear_Serial_Number_Value[16] = {0u};*/

    bool_t Do_Erase = (bool_t)SDB_Deserialize_U8(p_Req_SDB);

    if (Do_Erase)
    {
        FLT_LOG_Clear_Fault_Records();
        FLTEL_Clear_Environmental_Log();
        /*FLTEL_Clear_Lifetime_Device_Data();*/
        /*mfdids_Set_Serial_Number(Clear_Serial_Number_Value);*/
    }

    return UDS_RESP_POSITIVE;
}
/** @} doxygen end group */
