/**
 * @ingroup CFG
 *
 * @{
 */

#ifndef TYPES_H__
#define TYPES_H__

#include <intrinsics.h>
#include <el_types.h>
#include <stdbool.h>

typedef double dbl;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

#define S8_MIN INT8_MIN
#define S8_MAX INT8_MAX

#define S8_MAX INT8_MAX
#define U8_MAX UINT8_MAX

#define S16_MIN INT16_MIN
#define S16_MAX INT16_MAX

#define S16_MAX INT16_MAX
#define U16_MAX UINT16_MAX

#define S32_MIN INT32_MIN
#define S32_MAX INT32_MAX

#define S32_MAX INT32_MAX
#define U32_MAX UINT32_MAX

#define S64_MIN INT64_MIN
#define S64_MAX INT64_MAX

#define S64_MAX INT64_MAX
#define U64_MAX UINT64_MAX

typedef _Bool BOOL;                        /* PRQA S 1056 */ /* justification: allowed because of using C99 compiler */

#define True  ((BOOL) 1 )
#define False ((BOOL) 0 )

#ifndef NO_INLINE
#define NO_INLINE _Pragma( "inline=never" )
#endif
#ifndef INLINE
#define INLINE _Pragma( "inline" )
#endif

u8 GET_BIT_POS( u16 val );

volatile u16 * U16_TO_U16PTR ( const volatile u16 addr );
u16 U16PTR_TO_U16 ( const volatile u16 * ptr );

s16 SHR_S16 ( const s16 value, const u16 places );
s16 SHL_S16 ( const s16 value, const u16 places );

s32 SHR_S32 ( const s32 value, const u16 places );
s32 SHL_S32 ( const s32 value, const u16 places );

void WRITE_REG_U16 ( volatile u16 * const addr, const u16 data );
void WRITE_REG_S16 ( volatile u16 * const addr, const s16 data );

u16 READ_REG_U16 ( volatile u16 const * addr );
s16 READ_REG_S16 ( volatile u16 const * addr );

void WRITE_REG_U32( volatile u16 * const addr, const u32 data );
void WRITE_REG_S32( volatile u16 * const addr, const s32 data );

u32 READ_REG_U32( volatile u16 const * addr );
s32 READ_REG_S32( volatile u16 const * addr );

BOOL U8_TO_BOOL( const u8 val );
BOOL S16_TO_BOOL( const s16 val );
BOOL U16_TO_BOOL( const u16 val );

u16 LOWORD( const u32 val );
u16 HIWORD( const u32 val );

void GPIO_C_SET( const u16 pin );
void GPIO_C_CLEAR( const u16 pin );

void GPIO_B_SET( const u16 pin );
void GPIO_B_CLEAR( const u16 pin );

#endif /* TYPES_H__ */

/* }@
 */
