/***************************************************************************//**
 * @file			gpio_InterruptHandler.h
 *
 * @creator		rpy
 * @created		04.09.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#ifndef GPIO_INTERRUPTHANDLER_H_          
#define GPIO_INTERRUPTHANDLER_H_

#pragma once

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "el_types.h"
#include "vic_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * GPIO IRQ vector numbers
 ******************************************************************************/
typedef enum {
  gpio_IRQ_EVT_POS_0 = 0u,
  gpio_IRQ_EVT_NEG_0 = 1u,
  gpio_IRQ_EVT_POS_1 = 2u,
  gpio_IRQ_EVT_NEG_1 = 3u,
  gpio_IRQ_EVT_POS_2 = 4u,
  gpio_IRQ_EVT_NEG_2 = 5u,
  gpio_IRQ_EVT_POS_3 = 6u,
  gpio_IRQ_EVT_NEG_3 = 7u,
  gpio_IRQ_EVT_POS_4 = 8u,
  gpio_IRQ_EVT_NEG_4 = 9u,
  gpio_IRQ_EVT_POS_5 = 10u,
  gpio_IRQ_EVT_NEG_5 = 11u,
  gpio_IRQ_EVT_POS_6 = 12u,
  gpio_IRQ_EVT_NEG_6 = 13u,
  gpio_IRQ_EVT_POS_7 = 14u,
  gpio_IRQ_EVT_NEG_7 = 15u,
	
  gpio_INTERRUPT_VECTOR_CNT       =           16u  /**< Number of available interrupt vectors */
} gpio_eInterruptVectorNum_t;


/***************************************************************************//**
 * Pointer to GPIO context data
 ******************************************************************************/
typedef void * gpio_pInterruptContextData_t;

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef void (*gpio_InterruptCallback_t) (gpio_eInterruptVectorNum_t irqsrc, gpio_pInterruptContextData_t contextdata);

/***************************************************************************//**
 * Callback function pointer type
 ******************************************************************************/
typedef enum
{
  gpio_MOD_A         = 0u,
  gpio_MOD_B         = 1u,
  gpio_MOD_C         = 2u,

  gpio_INSTANCE_CNT  = 3u  /**< Number of gpio modules implemented */

} gpio_InstanceNum_t;


/***************************************************************************//**
 * GPIO environment data
 ******************************************************************************/
typedef struct gpio_sInterruptEnvironmentData
{
    /** Interrupt vector table of this module */
    gpio_InterruptCallback_t InterrupVectorTable[gpio_INTERRUPT_VECTOR_CNT];
    
    /** GPIO module context data */
    gpio_pInterruptContextData_t ContextData;
        
} gpio_sInterruptEnvironmentData_t;

/***************************************************************************//**
 * Pointer to GPIO environment data
 ******************************************************************************/
typedef gpio_sInterruptEnvironmentData_t * gpio_pInterruptEnvironmentData_t;

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief     Enables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to GPIO module base address
 * @param[in] irqsrc            IRQ to be enabled
 *
 * @pre       A call back function to the related interrupt should have been
 *            registered with gpio_RegisterInterruptCallback().
 *
 * @post      The related call back function will be called if the desired
 *            interrupt occurs.
 *
 * @detaildesc
 * The GPIO IRQ_VENABLE register will be set the related IRQ number and therefore
 * the interrupt will be activated.
 *
 ******************************************************************************/
void gpio_InterruptEnable(gpio_InstanceNum_t instanceNum, gpio_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Disables an IRQ.
 *
 * @param[in] modulBaseAddress  Pointer to GPIO module base address
 * @param[in] irqsrc            IRQ to be disable
 *
 * @post      The interrupt will be disabled and the related callback function
 *            will no longer be called from the interrupt handler.
 *
 * @detaildesc
 * The GPIO IRQ_VDISABLE register will be set the related IRQ number and therefore
 * the interrupt will be deactivated.
 *
 ******************************************************************************/
void gpio_InterruptDisable(gpio_InstanceNum_t instanceNum, gpio_eInterruptVectorNum_t irqsrc);

/***************************************************************************//**
 * @brief Registers/Adds callback function to the module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to GPIO module base address
 * @param irqsrc            IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre     (optional) Which are the conditions to call this function? i.e. none
 *
 * @post    If the interrupt will be activated the registered callback function
 *          will be called if the IRQ occurs.
 *
 * @detaildesc
 * Registers the callback function at interrupt vector handling. It sets the
 * entry in the interrupt vector table to passed function pointer.
 *
 ******************************************************************************/
void gpio_InterruptRegisterCallback(gpio_InstanceNum_t instanceNum, gpio_eInterruptVectorNum_t irqvecnum, gpio_InterruptCallback_t cbfnc);

/***************************************************************************//**
 * @brief Deregisters/deletes callback function from module interrupt vector table.
 *
 * @param modulBaseAddress  Pointer to GPIO module base address
 * @param irqvecnum         IRQ number
 * @param cbfnc             Pointer to desired callback function
 *
 * @pre   The related IRQ should be disabled.
 *
 * @post  The entry in the module interrupt vector table will point to NULL and
 *        the related IRQ will be disabled.
 *
 * @detaildesc
 * Deregisters the callback function from interrupt vector handling. It sets the
 * entry in the interrupt vector table to NULL and disables the related interrupt.
 *
 ******************************************************************************/
void gpio_InterruptDeregisterCallback(gpio_InstanceNum_t instanceNum, gpio_eInterruptVectorNum_t irqvecnum);


/***************************************************************************//**
 * Handles the GPIO A related interrupt requests.
 *
 ******************************************************************************/
 __interrupt static void gpio_InterruptHandlerA(void);
 
 /***************************************************************************//**
 * Handles the GPIO B related interrupt requests.
 *
 ******************************************************************************/
 __interrupt static void gpio_InterruptHandlerB(void);
 
 /***************************************************************************//**
 * Handles the GPIO C related interrupt requests.
 *
 ******************************************************************************/
 __interrupt static void gpio_InterruptHandlerC(void);

/***************************************************************************//**
 * @brief Initialize GPIO module
 *
 * @param environmentdata  Pointer to Environment data for GPIO module in
 *                         user RAM
 *
 * @pre        VIC (vic_VectorInterruptControl) and GPIO (gpio_SystemStateModule)
 *             have to presented and initialized.
 *
 * @post       GPIO module is configured for use.
 *
 * @detaildesc
 * Initializes the GPIO software and hardware module, including the module
 * interrupt vector table. Configures if IRQ nesting is active and if IO2 and
 * IO3 are used as GPIOs or not.
 *
 ******************************************************************************/
void gpio_InterruptInitialisation(gpio_InstanceNum_t instanceNum, vic_cpInterfaceFunctions_t vicIf, gpio_pInterruptEnvironmentData_t environmentdata, gpio_pInterruptContextData_t contextdata);


#endif /* LIN_CTRL_INTERRUPTHANDLER_H_ */

