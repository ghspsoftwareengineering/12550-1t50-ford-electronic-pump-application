#ifndef _FLASH_API_H
#define _FLASH_API_H

#include "global.h"

/**
* @addtogroup FLASHAPI Flash API 
* @ingroup OTHER
* @{ */

typedef enum {
    flash_ok = 0,               /** < operation was successful */
    flash_failed = 1,           /** < operation failed (assumed to be a flash cell failure) */
    flash_operation_pending = 2 /** < the flash is still busy with some other operation (NOT this operation) and cannot be used right now */
} flash_status_t;


flash_status_t flash_init ( void );
flash_status_t flash_write ( uint16_t addr, const uint16_t *word_buf, uint16_t word_count );
flash_status_t flash_page_erase ( uint16_t addr );
flash_status_t flash_read ( uint16_t addr, const uint16_t **ref, uint16_t word_count );

/**  @} */

#endif
