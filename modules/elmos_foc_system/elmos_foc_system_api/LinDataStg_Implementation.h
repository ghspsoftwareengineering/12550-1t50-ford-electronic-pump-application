/***************************************************************************//**
 * @file			LinDataStg_Implementation.h
 *
 * @creator		sbai
 * @created		27.11.2015
 *
 * @brief  		TODO: Short description of this module
 *
 * $Id: LinDataStg_Implementation.h 1794 2017-01-30 10:47:17Z sbai $
 *
 * $Revision: 1794 $
 *
 ******************************************************************************/

#ifndef LINDATASTG_IMPLEMENTATION_H_
#define LINDATASTG_IMPLEMENTATION_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinDrvImp_CompilationConfig.h"
#include "LinDataStg_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/
#if LINDATASTGIMP_EXT_IFFUN_STRCT_ACCESS == 1
extern const LinDataStgIf_sInterfaceFunctions_t LinDataStgImp_InterfaceFunctions;
#endif /* LINDATASTGIMP_EXT_IFFUN_STRCT_ACCESS == 1 */

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief Implementation of LIN Data Storage layer 'Initialization' function.
 *
 * @copydetails LinBusIf_InitializationIfFun_t
 *
 ******************************************************************************/
Lin_Bool_t LinDataStgImp_Initialization(LinDataStgIf_pGenericEnvData_t     genericDataStgEnvData, LinDataStgIf_EnvDataSze_t        dataStgEnvDataSze,
                                        LinDataStgIf_cpCallbackFunctions_t dataStgCbFuns,         LinDataStgIf_pGenericCbCtxData_t genericdataStgCbCtxData,
                                        LinDataStgIf_pGenericImpCfgData_t  genericImpCfgData);

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
Lin_Bool_t LinDataStgImp_GetSubInterface(LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, Lin_eInterfaceIds_t interfaceId,  Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
LinDataStgIf_eErrorCodes_t LinDataStgImp_GetDataValue(LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                      LinDataStgIf_pData_t           buffer,                LinDataStgIf_Length_t      bufferLen,
                                                      LinDataStgIf_pLength_t         pDataValLen);

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
LinDataStgIf_eErrorCodes_t LinDataStgImp_SetDataValue(LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                      LinDataStgIf_pData_t           buffer,                LinDataStgIf_Length_t      bufferLen);

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief Struct for BUS layer implementation specific configuration data.
 ******************************************************************************/
struct LinDataStgImp_sCfgData
{
  LinDataStgIf_sThis_t FallbackDataStorage;
};

typedef struct LinDataStgImp_sCfgData    LinDataStgImp_sCfgData_t;
typedef        LinDataStgImp_sCfgData_t* LinDataStgImp_pCfgData_t;

/***************************************************************************//**
 * @brief LIN Data Storage layer implementation Data Value ID enumerator.
 *
 * @misra{M3CM Dir-10.1. - PRQA Msg 4521,
 * TODO
 * Conflicts in  signedness.,
 * Always make sure unsigned values ate defined and used.}
 ******************************************************************************/
// PRQA S 4521 ++
enum LinDataStgImp_eDataValueID
{
  LinDataStgImp_DVID_ConfigCookie            = LinDataStg_DVID_AREA_IMPLEMENTATION + 0u,      /* TODO */
  LinDataStgImp_DVID_ADCSampleExt            = LinDataStg_DVID_AREA_IMPLEMENTATION + 1u,      /* TODO */
  LinDataStgImp_DVID_ADCSampleExtAA          = LinDataStg_DVID_AREA_IMPLEMENTATION + 2u,      /* TODO */
  LinDataStgImp_DVID_ADCSampleExtVT          = LinDataStg_DVID_AREA_IMPLEMENTATION + 3u,      /* TODO */
  LinDataStgImp_DVID_EnableHighSpeed         = LinDataStg_DVID_AREA_IMPLEMENTATION + 4u,      /* TODO */
  LinDataStgImp_DVID_DebouncerValue          = LinDataStg_DVID_AREA_IMPLEMENTATION + 5u,      /* TODO */
  LinDataStgImp_DVID_SNPDImmediateNADUpdate  = LinDataStg_DVID_AREA_IMPLEMENTATION + 6u,      /* TODO */
  LinDataStgImp_DVID_SNPDPullUpCurrent       = LinDataStg_DVID_AREA_IMPLEMENTATION + 7u,      /* TODO */
  LinDataStgImp_DVID_SNPDStep5WaitMode       = LinDataStg_DVID_AREA_IMPLEMENTATION + 8u,      /* TODO */
  LinDataStgImp_DVID_SNPDEndOfBreakMode      = LinDataStg_DVID_AREA_IMPLEMENTATION + 9u,      /* TODO */
  LinDataStgImp_DVID_SNPDOversamplingMode    = LinDataStg_DVID_AREA_IMPLEMENTATION + 10u,     /* TODO */
  LinDataStgImp_DVID_SNPDPreselThres         = LinDataStg_DVID_AREA_IMPLEMENTATION + 11u,     /* TODO */
  LinDataStgImp_DVID_SNPDSelThres            = LinDataStg_DVID_AREA_IMPLEMENTATION + 12u,     /* TODO */
  LinDataStgImp_DVID_SNPD52136ACurrentOffset = LinDataStg_DVID_AREA_IMPLEMENTATION + 13u,     /* TODO */
  LinDataStgImp_DVID_GetSNPDThresholdsCbFun  = LinDataStg_DVID_AREA_IMPLEMENTATION + 14u,     /* TODO */
  LinDataStgImp_DVID_PreInitHook             = LinDataStg_DVID_AREA_IMPLEMENTATION + 15u,     /* TODO */
  LinDataStgImp_DVID_PostInitHook            = LinDataStg_DVID_AREA_IMPLEMENTATION + 16u,     /* TODO */
  LinDataStgImp_DVID_TaskHook                = LinDataStg_DVID_AREA_IMPLEMENTATION + 17u,     /* TODO */
  LinDataStgImp_DVID_LINPhySetModes          = LinDataStg_DVID_AREA_IMPLEMENTATION + 18u,     /* TODO */
  LinDataStgImp_DVID_LINPhyModes0            = LinDataStg_DVID_AREA_IMPLEMENTATION + 19u,     /* TODO */
  LinDataStgImp_DVID_LINPhyModes1            = LinDataStg_DVID_AREA_IMPLEMENTATION + 20u,     /* TODO */
  LinDataStgImp_DVID_InitialBaudrate         = LinDataStg_DVID_AREA_IMPLEMENTATION + 21u,     /* TODO */
  LinDataStgImp_DVID_EECfgData               = LinDataStg_DVID_AREA_IMPLEMENTATION + 22u,     /* TODO */
  LinDataStgImp_DVID_CNT                     = LinDataStg_DVID_AREA_IMPLEMENTATION + 23u,     /* Count of Implementation Data Value IDs */
  LinDataStgImp_DVID_MAX                     = LinDataStg_DVID_AREA_IMPLEMENTATION + 0x0FFFu, /* Maximal value of Implementation Data Value IDs */
  LinDataStgImp_DVID_INVALID                 = LinDataStg_DVID_INVALID,                  /* 0xFFFF */
};

typedef enum LinDataStgImp_eDataValueID LinDataStgImp_eDataValueID_t; /**< Typedef of LinDataStgImp_eDataValueID. */
// PRQA S 4521 --

/***************************************************************************//**
 * Data storage implementation specific error type
 *
 * @misra{M3CM Dir-10.1. - PRQA Msg 4521,
 * The Elmos LIN Driver defines areas of error codes for every module and sub-areas between
 * general defined interface error codes and implementation specific error codes. To link
 * between this areas some arithmetic offset calculation has to be done between different
 * enum types.,
 * Conflicts in  signedness.,
 * Always make sure unsigned values are defined and used.}
 *
 ******************************************************************************/
// PRQA S 4521 ++
enum LinDataStgImp_eErrorCodes
{
  LinDataStgImp_ERR_STATE_ERROR   = LinDataStgIf_ERR_IMPL_ERROR_AREA + 0
};
// PRQA S 4521 --

typedef enum LinDataStgImp_eErrorCodes LinDataStgImp_eErrorCodes_t;

#endif /* LINDATASTG_IMPLEMENTATION_H_ */
