/***************************************************************************//**
 * @file			LinDataStg_Interface.h
 *
 * @creator		sbai
 * @created		25.11.2015
 *
 * @brief     Definition of the interface for the 'LIN Data Storage Layer'.
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINDATASTG_INTERFACE_H_
#define LINDATASTG_INTERFACE_H_

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "Lin_Basictypes.h"
#include "LinDatastg_Types.h"
#include "LinDiag_Interface.h"
#include "LinTrans_Interface.h"
#include "LinUDS_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINDATASTG_INTERFACE_MODULE_API_VERSION 0x0100u /**<  @brief DATASTG Layer interface version */

/** @addtogroup LinDataStgIfIfFunDefs */
/**@{*/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ******************************************************************************
 * DATASTG Layer interface function types
 *******************************************************************************/

/***************************************************************************//**
 * @brief Typedef of DATASTG Layer 'Initialization' interface function.
 *
 * @param genericDataStgEnvData[in]     Pointer to reserved DATASTG Layer
 *                                      environment data.
 * @param dataStgEnvDataSze[in]         Size of the reserved RAM for DATASTG Layer
 *                                      environment data.
 * @param dataStgCbFuns[in]             Pointer to DATASTG Layer callback function
 *                                      struct. Implemented in the higher layer or
 *                                      user application.
 *                                      (LinDataStgIf_sCallbackFunctions)
 * @param genericDataStgCbCtxData[in]   Pointer to DATASTG Layer callback context
 *                                      data.
 * @param genericDataStgImpCfgData[in]  Pointer to implementation dependent
 *                                      configuration data for the DATASTG Layer.
 *                                      (LinDataStgImp_sCfgData)
 *
 * @return LIN_TRUE if the initialization was successfully LIN_FALSE if not.
 *
 * Initializes the LIN LIN Data Storage layer.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinDataStgIf_InitializationIfFun_t) (LinDataStgIf_pGenericEnvData_t     genericDataStgEnvData, LinDataStgIf_EnvDataSze_t        dataStgEnvDataSze,
                                                          LinDataStgIf_cpCallbackFunctions_t dataStgCbFuns,         LinDataStgIf_pGenericCbCtxData_t genericDataStgCbCtxData,
                                                          LinDataStgIf_pGenericImpCfgData_t  genericDataStgImpCfgData);

/***************************************************************************//**
 * @brief Typedef of DATASTG Layer 'Get Sub-Interface' interface function.
 *
 * @param genericDataStgEnvData[in] Pointer to reserved DATASTG Layer
 *                                  environment data.
 * @param interfaceId[in]           Sub-Interface ID.
 * @param ifThisPtr[out]            Variable (pointer) which will be set to the
 *                                  address of the This-Pointer of the desired
 *                                  sub-interface.
 *
 * @return  LIN_TRUE if the desired interface is available and the parameter
 *          @p ifThisPtr could be set.
 *
 * Acquires the desired sub-interface (@p interfaceId) and writes the address
 * of its This-Pointer to @p ifThisPtr.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinDataStgIf_GetSubInterfaceIfFun_t) (LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * @brief Typedef of LIN Data Storage layer 'Get Data Value'
 *        interface function.
 *
 * @param genericDataStgEnvData[in] Pointer to reserved DATASTG Layer
 *                                  environment data.
 * @param dataValueId[in]           Data Value ID. (LinDataStgIf_DataValueID_t)
 * @param buffer[out]               Pointer to buffer to copy the data to.
 * @param bufferLen[in]             Length of the provided buffer.
 * @param pDataValLen[in]           Pointer to variable to write length of the
 *                                  copied data to.
 *
 * @return Error code if something went wrong. LinDataStgIf_ERR_NO_ERROR if the
 *         everything was successful.
 *
 * Get the data defined by the Data Value ID (@p dataValueId).
 *
 * If parameter 'buffer' is LIN_NULL. Only the length of the 'Data Value' will be
 * copied to 'pDataValLen'.
 *
 ******************************************************************************/
typedef LinDataStgIf_eErrorCodes_t (*LinDataStgIf_GetDataValueIfFun_t) (LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                                        LinDataStgIf_pData_t           buffer,                LinDataStgIf_Length_t      bufferLen,
                                                                        LinDataStgIf_pLength_t         pDataValLen);

/***************************************************************************//**
 * @brief Typedef of LIN Data Storage layer 'Set Data Value'
 *        interface function.
 *
 * @param genericDataStgEnvData[in] Pointer to reserved DATASTG Layer
 *                                  environment data.
 * @param dataValueId[in]           Data Value ID. (LinDataStgIf_DataValueID_t)
 * @param buffer[in]                Pointer to buffer to copy the data from.
 * @param bufferLen[in]             Length of the provided buffer.
 *
 * @return Error code if something went wrong. LinDataStgIf_ERR_NO_ERROR if the
 *         everything was successful.
 *
 * Set the data defined by the Data Value ID (@p dataValueId).
 *
 ******************************************************************************/
typedef LinDataStgIf_eErrorCodes_t (*LinDataStgIf_SetDataValueIfFun_t) (LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, LinDataStgIf_DataValueID_t dataValueId,
                                                                        LinDataStgIf_pData_t           buffer,                LinDataStgIf_Length_t      bufferLen);

/***************************************************************************//**
 * @brief DATASTG Layer interface functions struct.
 *
 * Collective struct for the LIN Data Storage layer interface functions.
 ******************************************************************************/
struct LinDataStgIf_sInterfaceFunctions
{
    Lin_Version_t                       InterfaceVersion; /**< Interface version */

    LinDataStgIf_InitializationIfFun_t  Initialization;   /**< @brief Pointer to DATASTG Layer 'Initialization' interface function. (LinDataStgIf_InitializationIfFun_t) */
    LinDataStgIf_GetSubInterfaceIfFun_t GetSubInterface;  /**< @brief Pointer to DATASTG Layer 'Get Sub-Interface' interface function. (LinDataStgIf_GetSubInterfaceIfFun_t) */
    LinDataStgIf_GetDataValueIfFun_t    GetDataValue;     /**< @brief Pointer to DATASTG Layer 'Get Data Value' interface function. (LinDataStgIf_GetDataValueIfFun_t) */
    LinDataStgIf_SetDataValueIfFun_t    SetDataValue;     /**< @brief Pointer to DATASTG Layer 'Set Data Value' interface function. (LinDataStgIf_SetDataValueIfFun_t) */
};

/**@} LinDataStgIfIfFunDefs */

/* *****************************************************************************
 * LIN DataStgcol Layer Module callback function types
 ******************************************************************************/
/** @addtogroup LinDataStgIfCbFunDefs */
/**@{*/

/***************************************************************************//**
 * @brief Typedef of LIN Data Storage layer 'Error' callback function.
 *
 * @param genericDataStgEnvData[out]    Pointer to reserved DATASTG Layer
 *                                      environment data.
 * @param dataStgIfFuns[out]            Pointer to the DATASTG Layer interface
 *                                      function struct.
 * @param error[out]                    Occurred error.
 * @param genericDataStgCbCtxData[out]  Pointer to DATASTG Layer callback context
 *                                      data.
 *
 * Indicates an error in the DATASTG Layer and underlying layers of the LIN
 * Driver.
 *
 * @par "Call Description:"
 * @mscfile msc_datastg_errorcbcalled.dox
 * @n
 *
 ******************************************************************************/
typedef void (*LinDataStgIf_ErrorCbFun_t) (LinDataStgIf_pGenericEnvData_t genericDataStgEnvData, LinDataStgIf_cpInterfaceFunctions_t dataStgIfFuns,
                                           LinDataStgIf_Error_t           error,                 LinDataStgIf_pGenericCbCtxData_t    genericDataStgCbCtxData);

/***************************************************************************//**
 * @brief DATASTG Layer callback functions struct.
 *
 * The set of callback functions which are invoked in the higher layers.
 *
 ******************************************************************************/
struct LinDataStgIf_sCallbackFunctions
{
    Lin_Version_t              CallbackVersion; /**< @brief Callback Version */

    LinDataStgIf_ErrorCbFun_t  Error;           /**< @brief Pointer to DATASTG Layer 'Error' callback function. (LinDataStgIf_ErrorCbFun_t) */
};

/**@} LinDataStgIfCbFunDefs */

/***************************************************************************//**
 * @brief  Object-like This-pointer to connect the DATASTG Layer to higher layers.
 ******************************************************************************/
struct LinDataStgIf_sThis
{
    LinDataStgIf_cpInterfaceFunctions_t IfFunsTbl; /**< @brief Pointer to the DATASTG Layer interface function struct. (LinDataStgIf_sInterfaceFunctions) */
    LinDataStgIf_pGenericEnvData_t      EnvData;   /**< @brief Pointer to reserved DATASTG Layer environment data. */
};

/***************************************************************************//**
 * @brief DATASTG Layer interface configuration parameter
 *
 * Data needed for initialization of the DATASTG Layer.
 *
 * @see LinDataStgIf_InitializationIfFun_t
 *
 ******************************************************************************/
struct LinDataStgIf_sInitParam
{
    LinDataStgIf_cpInterfaceFunctions_t IfFunsTbl;  /**< @brief Pointer to the constant DATASTG Layer interface function struct.  */
    LinDataStgIf_pGenericEnvData_t      EnvData;    /**< @brief Pointer to reserved DATASTG Layer environment data. */
    LinDataStgIf_Length_t               EnvDataLen; /**< @brief Size of the reserved RAM for DATASTG Layer environment data. */
    LinDataStgIf_pGenericImpCfgData_t   ImpCfgData; /**< @brief Pointer to implementation dependent configuration data for the DATASTG Layer. (LinDataStgImp_sCfgData) */
};

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* LINDATASTG_INTERFACE_H_ */

