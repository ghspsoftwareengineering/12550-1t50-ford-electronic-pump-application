/**
 *  @file test_SYSIG_Get_Voltage.c
 *
 */
#include "unity.h"
#include "global.h"
#include "system_signals.h"
#include "uds.h"

/* MOCKS */
#include "mock_adc.h"
#include "mock_adc_lists.h"
#include "mock_motor_ctrl.h"
#include "mock_serial_data_buffer.h"
#include "mock_speed_meas.h"

/* STATIC's */
STATIC bool_t sysig_Use_Current_Override;
STATIC int16_t sysig_Override_Current;

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that SYSIG_Diag_Get_Current() will serialize the currently reported
 * Supply Current  and return UDS_RESP_POSITIVE.
 *
 */
/* Reqs: SWREQ_1928, SWREQ_2079, SWREQ_2080 */
void test_SYSIG_Diag_Get_Current_Serializes_reported_Supply_Current(void)
{
    UDS_Response_Code_T result = UDS_RESP_GENERAL_REJECT;
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    sysig_Use_Current_Override = false;

    /* Setup expected call chain */
    mc_get_supply_current_ExpectAndReturn(2560); /* 10.0 Amps */
    SDB_Serialize_I16_ExpectAndReturn(&Test_SDB, 100, SDB_EOK);

    /* Call function under test */
    result = SYSIG_Diag_Get_Current(&Test_SDB);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
}

/**
 * @test Verifies that SYSIG_Diag_Get_Current() will serialize the override
 * CPU temperature when there is an override enabled and return UDS_RESP_POSITIVE.
 *
 */
/* Reqs: SWREQ_1928, SWREQ_2079, SWREQ_2080 */
void test_SYSIG_Diag_Get_Current_Serializes_override_Supply_Current(void)
{
    UDS_Response_Code_T result = UDS_RESP_GENERAL_REJECT;
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    sysig_Use_Current_Override = true;
    sysig_Override_Current = 151; /* Override the current in 10th of an amp to 15.1A */

    /* Setup expected call chain */
    /* Will not read the ADC for CPU temperature when the override is active */
    SDB_Serialize_I16_ExpectAndReturn(&Test_SDB, 151, SDB_EOK);

    /* Call function under test */
    result = SYSIG_Diag_Get_Current(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
}

/**
 * @test Verifies that the function SYSIG_Diag_Override_Current() will enable
 * the override and set the override supply current to the value supplied
 * in the Serial Data Buffer
 *
 */
/* Reqs: SWREQ_1928, SWREQ_2079, SWREQ_2080 */
void test_SYSIG_Diag_Override_Current_Sets_and_Enables_Override(void)
{
    UDS_Response_Code_T result = UDS_RESP_GENERAL_REJECT;
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    sysig_Override_Current = 0xFFFF;
    sysig_Use_Current_Override = false;

    /* Setup expected call chain */
    SDB_Deserialize_I16_ExpectAndReturn(&Test_SDB, 104); /* Deserialize 10.4A */

    /* Call function under test */
    result = SYSIG_Diag_Override_Current(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
    TEST_ASSERT_EQUAL_INT16(104, sysig_Override_Current);
    TEST_ASSERT_TRUE(sysig_Use_Current_Override);
}

/**
 * @test Verifies that the function SYSIG_Diag_Override_Current() will disable
 * the override when the serialized value is INT16_MIN.
 *
 */
/* Reqs: SWREQ_1928, SWREQ_2079, SWREQ_2080 */
void test_SYSIG_Diag_Override_Current_Clears_and_Disables_Override(void)
{
    UDS_Response_Code_T result = UDS_RESP_GENERAL_REJECT;
    Serial_Data_Buffer_T Test_SDB;

    /* Ensure known test state */
    sysig_Override_Current = 0;
    sysig_Use_Current_Override = true;

    /* Setup expected call chain */
    SDB_Deserialize_I16_ExpectAndReturn(&Test_SDB, INT16_MIN);

    /* Call function under test */
    result = SYSIG_Diag_Override_Current(&Test_SDB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_INT(UDS_RESP_POSITIVE, result);
    TEST_ASSERT_FALSE(sysig_Use_Current_Override);
}
