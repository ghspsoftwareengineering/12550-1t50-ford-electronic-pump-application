# GHSP Homebrew LIN Driver

Basic LIN Driver that can be used for prototype software on the S12Z/Carcasonne microprocessor

## Build

Include in GHSP Make based project and add hb_lin to your list of libraries in the 
binary that will include HB LIN.
