/**
 * @ingroup HELP
 *
 * @{
 */

#ifndef SYSTIME_H
#define SYSTIME_H

#include "global.h"


void systime_init( void );
uint32_t systime_getU32( void );
uint16_t systime_getU16( void );
void systime_incr( void );

void systime_delay_blocking( uint16_t delay_us );

INLINE uint32_t SYSTIME_MS_TO_CPU_CLKS( uint16_t ms );
INLINE uint32_t SYSTIME_US_TO_CPU_CLKS( uint16_t us );
INLINE uint32_t SYSTIME_NS_TO_CPU_CLKS( uint16_t ns );
INLINE uint32_t SYSTIME_MS_TO_SYSTIME_TICKS( uint16_t ms );
INLINE uint32_t SYSTIME_US_TO_SYSTIME_TICKS( uint16_t us );

INLINE uint32_t SYSTIME_TICKS_TO_SYSTIME_MS(uint32_t ticks);

#endif /* _#ifndef SYSTIME_H_ */

/* }@
 */
