/***************************************************************************//**
 * @file			pwmn_InterruptHandler.c
 *
 * @creator		rpy
 * @created		26.01.2015
 * @sdfv      TODO: Automotive Spice or Elmos Flow or Demo Flow
 *
 * @brief  		TODO: Short description of this module
 *
 * @purpose
 *
 * TODO: A detailed description of this module
 *
 * $Id: $
 *
 * $Revision: $
 *
 ******************************************************************************/

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "io_e52398a.h"
#include <intrinsics.h>

#include "vic_InterruptHandler.h"
#include "pwmn_InterruptHandler.h"


/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************** FORWARD DECLARATIONS / PROTOTYPES *********************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************ MODULE GLOBALE VARIABLES **************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ************************** FUNCTION DEFINITIONS ****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ******************************* INTERRUPTS *********************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VENABLE register, but with the difference
 * to pwmn_SetIRQVEnable, that a special enum (pwmn_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void pwmn_InterruptEnable(pwmn_eInterruptVectorNum_t irqsrc)
{
  PWMN_IRQ_VENABLE = (uint16_t) irqsrc;
}

/***************************************************************************//**
 * @implementation
 * Simple word write access to IRQ_VDISABLE register, but with the difference
 * to pwmn_SetIRQVDisable, that a special enum (pwmn_eInterruptVectorNum_t)
 * is passed as parameter, for security reasons and a more concrete interface
 * definition.
 *
 ******************************************************************************/
void pwmn_InterruptDisable(pwmn_eInterruptVectorNum_t irqsrc)
{
  PWMN_IRQ_VDISABLE = (uint16_t) irqsrc;
}

/***************************************************************************//**
 * @implementation
 * At first the module number is looked up in a table, because module number
 * and module instance base address have a logical connection. Then the
 * interrupt callback function pointer is saved in module interrupt vector
 * table.
 *
 ******************************************************************************/
void pwmn_InterruptRegisterCallback(pwmn_eInterruptVectorNum_t irqvecnum, pwmn_InterruptCallback_t cbfnc)
{
  pwmn_sInterruptEnvironmentData_t* evironmentData = (pwmn_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_PWMN);

  if ((irqvecnum < pwmn_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
  {
    evironmentData->InterrupVectorTable[irqvecnum] = cbfnc;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Reverse function of "pwmn_RegisterInterruptCallback". Sets the corresponding
 * entry in the module interrupt vector table back to NULL. Very imported,for
 * security reasons the corresponding interrupt is disabled. Otherwise the
 * program could possibly call NULL.
 *
 ******************************************************************************/
void pwmn_InterruptDeregisterCallback(pwmn_eInterruptVectorNum_t irqvecnum)
{
  pwmn_sInterruptEnvironmentData_t* evironmentData = (pwmn_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_PWMN);

  if ((irqvecnum < pwmn_INTERRUPT_VECTOR_CNT) && (evironmentData != NULL))
  {
    pwmn_InterruptDisable(irqvecnum);
    evironmentData->InterrupVectorTable[irqvecnum] = NULL;
  }
  else {}
}

/***************************************************************************//**
 * @implementation
 * Every hardware PWMN module has it's own interrupt handler, also because each
 * module has it's own interrupt vector. So the module base address is fixed.
 * The interrupt vector table is provided by the user-application from RAM. The
 * module specific vector tables lie behind the VIC interrupt vector table, which
 * address is saved in the "TABLE_BASE". The module interrupt handler
 * gets the address from the function "vic_GetPointerToEviornmentData".
 * The interrupt vector number is saved in a local variable, because the value
 * of IRQ_VECTOR could possible change during processing of
 * pwmn_PWMN0_InterruptHandler. Next the registered interrupt callback function
 * (pwmn_RegisterInterruptCallback) is copied into a local function pointer,
 * checked if it's not NULL and at least called. At the end the interrupt
 * request flag is cleared, by writing back the interrupt vector number to
 * IRQ_VNO register.
 *
 ******************************************************************************/
 __interrupt void pwmn_InterruptHandler(void)
{
  pwmn_eInterruptVectorNum_t irqvecnum = (pwmn_eInterruptVectorNum_t) PWMN_IRQ_VNO;
  
  if (irqvecnum < pwmn_INTERRUPT_VECTOR_CNT) /* ensure the IRQ trigger is not already gone away until processing starts */
  {  
    pwmn_sInterruptEnvironmentData_t* evironmentData = (pwmn_sInterruptEnvironmentData_t*) vic_GetPointerToEnvironmentData(vic_IRQ_PWMN);

    pwmn_InterruptCallback_t fptr;
  
    uint16_t vicVmaxBackup = 0;
    uint16_t pwmnVmaxBackup = 0;
  
    /* IRQ nesting on VIC level */
    vicVmaxBackup = VIC_IRQ_VMAX;
    VIC_IRQ_VMAX = (uint16_t) vic_IRQ_PWMN;

    /* IRQ nesting on module level */
    pwmnVmaxBackup = PWMN_IRQ_VMAX;
    PWMN_IRQ_VMAX = 0;
  
    fptr = evironmentData->InterrupVectorTable[irqvecnum];
    
    if(fptr != NULL)
    {
      __enable_interrupt();
    
      /* Call interrupt callback function */
      fptr(irqvecnum,  (void*) evironmentData->ContextData);
                  
      __disable_interrupt();   
    }
    else 
    {
      /* if there is no handler function for a particular IRQ, disable this IRQ  */ 
      PWMN_IRQ_VDISABLE = (uint16_t) irqvecnum;
    }
  
    /* Clear interrupt request flag */
    PWMN_IRQ_VNO = (uint16_t) irqvecnum;
  
    /* IRQ nesting on mocule level */
    PWMN_IRQ_VMAX = pwmnVmaxBackup;
  
    /* IRQ nesting on VIC level */
    VIC_IRQ_VMAX = vicVmaxBackup;
  }
  else {}
    
}

/***************************************************************************//**
 * @implementation
 * TODO: Concrete implementation description.
 *
 ******************************************************************************/
void pwmn_InterruptInitialisation(vic_cpInterfaceFunctions_t vicIf, pwmn_pInterruptEnvironmentData_t environmentdata, pwmn_pInterruptContextData_t contextdata)
{
  if ((vicIf != NULL) && (vicIf->InterfaceVersion == VIC_INTERFACE_VERSION) && (environmentdata != NULL))
  {
    environmentdata->ContextData = contextdata;

    /* Register module at interrupt handler */
    vicIf->RegisterModule(vic_IRQ_PWMN, &pwmn_InterruptHandler, environmentdata);
    vicIf->EnableModule(vic_IRQ_PWMN);
  }
  else {}
}
