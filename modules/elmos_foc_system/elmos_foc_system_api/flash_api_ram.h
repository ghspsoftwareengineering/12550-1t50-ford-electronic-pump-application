#ifndef _FLASH_API_RAM_H
#define _FLASH_API_RAM_H

#include "intrinsics.h"

typedef enum {
  MB_WORD_READ  = 0x01,
  IB_WORD_READ  = 0x02,
  MB_ROW_PROG   = 0x04,
  IB_ROW_PROG   = 0x08,
  MB_PAGE_ER    = 0x10,
  IB_PAGE_ER    = 0x20,
  MB_MASS_ER    = 0x40,
  MB_IB_MASS_ER = 0x80,
} flash_modes_t;

typedef enum {
  FLASH_MAIN = 1,
  FLASH_INFO = 2,
} flash_block_t;

flash_status_t ram_write_words_to_flash (flash_block_t  block, uint16_t flash_addr , const uint16_t word_buf[], uint16_t word_count);
flash_status_t ram_flash_page_erase (flash_block_t  block, uint16_t flash_addr );
flash_status_t ram_flash_ib_read(uint16_t addr, uint16_t data[], uint16_t anzahl);
void init_ram_code(void);

#endif
