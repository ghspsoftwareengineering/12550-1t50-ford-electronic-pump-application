/***************************************************************************//**
 * @file			LinDataStg_Types.h
 *
 * @creator		sbai
 * @created		25.11.2015
 *
 * @brief     Definitions of basic data types for the 'LIN Data Storage Layer'.
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINDATASTG_TYPES_H_
#define LINDATASTG_TYPES_H_

#pragma system_include

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "Lin_Basictypes.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINDATASTG_DVID_AREA_SIZE 0x1000u

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/* ****************************************************************************/
/*                            Simple data types                               */
/* ****************************************************************************/
typedef Lin_Error_t LinDataStgIf_Error_t;

typedef Lin_pvoid_t      LinDataStgIf_pGenericEnvData_t;    /**< Generic pointer to environment data of the LIN Data Storage layer module. */
typedef Lin_EnvDataSze_t LinDataStgIf_EnvDataSze_t;         /**< LIN Data Storage layer data type for the environment data length. */
typedef Lin_pvoid_t      LinDataStgIf_pGenericImpCfgData_t; /**< Generic pointer to configuration parameter of the specific LIN Data Storage layer implementation. */
typedef Lin_pvoid_t      LinDataStgIf_pGenericCbCtxData_t;  /**< Pointer to LIN Data Storage callback context data. */

typedef Lin_uint8_t           LinDataStgIf_Data_t;          /**< Basic LIN Data Storage data type */
typedef LinDataStgIf_Data_t*  LinDataStgIf_pData_t;         /**< Pointer to LinDataStgIf_Data_t */
typedef LinDataStgIf_Data_t** LinDataStgIf_ppData_t;        /**< Pointer-Pointer to LinDataStgIf_Data_t */

typedef Lin_BufLength_t           LinDataStgIf_Length_t;       /**< LIN Data Storage layer data type of length values */
typedef LinDataStgIf_Length_t* LinDataStgIf_pLength_t;      /**< Pointer to LinDataStgIf_Length_t */

typedef Lin_uint16_t  LinDataStgImp_DataValueID_t;          /**< Type for Data Value ID (DVID). */

/***************************************************************************//**
 * @misra{M3CM Dir-10.1. - PRQA Msg 1840,
 * TODO
 * Conflicts in signedness.,
 * Always make sure unsigned values ate defined and used.}
 ******************************************************************************/
// PRQA S 1840 ++
enum LinDataStg_eDVIDAreas
{
  LinDataStg_DVID_AREA_INTERFACE      = 0,                                                                /**< TODO */
  LinDataStg_DVID_AREA_IMPLEMENTATION = LINDATASTG_DVID_AREA_SIZE,                                        /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_0     = LinDataStg_DVID_AREA_IMPLEMENTATION + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_1     = LinDataStg_DVID_AREA_RESERVED_0     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_2     = LinDataStg_DVID_AREA_RESERVED_1     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_3     = LinDataStg_DVID_AREA_RESERVED_2     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_4     = LinDataStg_DVID_AREA_RESERVED_3     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_5     = LinDataStg_DVID_AREA_RESERVED_4     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_6     = LinDataStg_DVID_AREA_RESERVED_5     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_7     = LinDataStg_DVID_AREA_RESERVED_6     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_8     = LinDataStg_DVID_AREA_RESERVED_7     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_9     = LinDataStg_DVID_AREA_RESERVED_8     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_10    = LinDataStg_DVID_AREA_RESERVED_9     + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_11    = LinDataStg_DVID_AREA_RESERVED_10    + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_RESERVED_12    = LinDataStg_DVID_AREA_RESERVED_11    + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_AREA_USER           = LinDataStg_DVID_AREA_RESERVED_12    + LINDATASTG_DVID_AREA_SIZE,  /**< TODO */
  LinDataStg_DVID_INVALID             = 0xFFFFu                                                           /**< TODO */
};
// PRQA S 1840 --

/***************************************************************************//**
 * @brief LIN Data Storage layer error enumerator
 *
 * @misra{M3CM Dir-10.1. - PRQA Msg 4521,
 * The Elmos LIN Driver defines areas of error codes for every module and sub-areas between
 * general defined interface error codes and implementation specific error codes. To link 
 * between this areas some arithmetic offset calculation has to be done between different
 * enum types.,
 * Conflicts in  signedness.,
 * Always make sure unsigned values ate defined and used.}
 *
 ******************************************************************************/
// PRQA S 4521 ++
enum LinDataStgIf_eErrorCodes
{
  LinDataStgIf_ERR_NO_ERROR                 = Lin_NO_ERROR,               /**< No error at all. */
  LinDataStgIf_ERR_READ_ONLY                = Lin_ERROR_AREA_DATASTG   + 1, /**< 'Data Value' is read-only. */
  LinDataStgIf_ERR_UNKOWN_DATA_VALUE_ID     = Lin_ERROR_AREA_DATASTG   + 2, /**< Unknown 'Data Value ID'*/
  LinDataStgIf_ERR_DATA_VALUE_ACCESS_FAILED = Lin_ERROR_AREA_DATASTG   + 3, /**< Set/Get of 'Data Value' failed. */
  LinDataStgIf_ERR_DATA_VALUE_LEN_MISMATCH  = Lin_ERROR_AREA_DATASTG   + 4, /**< 'Data Value' length mismatches. */
  LinDataStgIf_ERR_DATA_VAL_DEF_MISS        = Lin_ERROR_AREA_DATASTG   + 5, /**< Missing Data Value ID definition. */
  LinDataStgIf_ERR_IMPL_ERROR_AREA          = Lin_ERROR_AREA_DATASTG + (LIN_ERROR_AREA_SIZE/2) /**< Any additional implementation specific error codes start here. */
};
// PRQA S 4521 --

typedef enum LinDataStgIf_eErrorCodes LinDataStgIf_eErrorCodes_t; /**< Typedef of LinDataStgIf_eErrorCodes. */

typedef Lin_uint16_t LinDataStgIf_DataValueID_t; /**< Type for 'Data Value ID'. Placeholder for implementation depended enum 'LinDataStgImp_eDataValueID_t'. */

struct         LinDataStgIf_sInterfaceFunctions;                                        /**< Forward declaration of LIN Data Storage layer interface functions. */
typedef struct LinDataStgIf_sInterfaceFunctions    LinDataStgIf_sInterfaceFunctions_t;  /**< Typedef for LinDataStgIf_sInterfaceFunctions. */
typedef        LinDataStgIf_sInterfaceFunctions_t* LinDataStgIf_pInterfaceFunctions_t;  /**< Typedef of pointer to LinDataStgIf_sInterfaceFunctions. */
typedef const  LinDataStgIf_sInterfaceFunctions_t* LinDataStgIf_cpInterfaceFunctions_t; /**< Typedef of constant pointer to LinDataStgIf_sInterfaceFunctions. */

struct         LinDataStgIf_sCallbackFunctions;                                         /**< Forward declaration of LIN Data Storage layer callback functions. */
typedef struct LinDataStgIf_sCallbackFunctions     LinDataStgIf_sCallbackFunctions_t;   /**< Typedef for LinDataStgIf_sCallbackFunctions. */
typedef        LinDataStgIf_sCallbackFunctions_t*  LinDataStgIf_pCallbackFunctions_t;   /**< Typedef of pointer to LinDataStgIf_sCallbackFunctions. */
typedef const  LinDataStgIf_sCallbackFunctions_t*  LinDataStgIf_cpCallbackFunctions_t;  /**< Typedef of constant pointer to LinDataStgIf_sCallbackFunctions. */

struct         LinDataStgIf_sThis;                         /**< Forward declaration of LIN Data Storage layer This-Pointer. */
typedef struct LinDataStgIf_sThis    LinDataStgIf_sThis_t; /**< Typedef for LinDataStgIf_sThis. */
typedef        LinDataStgIf_sThis_t* LinDataStgIf_pThis_t; /**< Typedef of pointer to LinDataStgIf_sThis. */

struct         LinDataStgIf_sInitParam;                              /**< Forward declaration of LIN Data Storage layer initialization parameter struct. */
typedef struct LinDataStgIf_sInitParam    LinDataStgIf_sInitParam_t; /**< Typedef for LinDataStgIf_sInitParam */
typedef        LinDataStgIf_sInitParam_t* LinDataStgIf_pInitParam_t; /**< Typedef of pointer to LinDataStgIf_sInitParam */

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* LINDATASTG_TYPES_H_ */
