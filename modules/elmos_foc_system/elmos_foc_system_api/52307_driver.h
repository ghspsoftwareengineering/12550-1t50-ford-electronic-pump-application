/**
 * @defgroup HW Hardware driver
 * @ingroup HW
 *
 * @{
 */

#ifndef DRIVER_52307_H
#define DRIVER_52307_H

#include "global.h"

/* -------------------------
   523.07 register addresses
   ------------------------- */

#define CHIPCTRL   ( 0x02u )         /* Wake-up and sleep configuration */
#define SLEEPCTRL  ( 0x03u )         /* Sleep mode control 1 */
#define SAFECTRL   ( 0x05u )         /* Safety function configuration */
#define IOCFG      ( 0x06u )         /* Interface configuration */
#define VREGCTRL   ( 0x07u )         /* VCC and VG supply control */
#define SCPCTRL    ( 0x0Bu )         /* Short circuit protection control */
#define RSTSRC     ( 0x0Eu )         /* Reset sources */
#define WUSRC      ( 0x0Fu )         /* Wake-up sources */
#define WDCTRL     ( 0x10u )         /* Watchdog control */
#define WDTRIG     ( 0x11u )         /* Watchdog trigger */
#define IRQMSK1    ( 0x12u )         /* Interrupt mask register 1 */
#define IRQSTAT1   ( 0x13u )         /* Interrupt status register 1 */
#define IRQMSK2    ( 0x14u )         /* Interrupt mask register 2 */
#define IRQSTAT2   ( 0x15u )         /* Interrupt status register 2 */
#define IOCOMPTHR  ( 0x16u )         /* Motor over current threshold */
#define AMUX       ( 0x17u )         /* Analog signal measurement selection */
#define DMUX       ( 0x19u )         /* Digital value output SO selection */
#define DMON1      ( 0x1Au )         /* Digital value monitoring register 1 */
#define DMON2      ( 0x1Bu )         /* Digital value monitoring register 2 */
#define IOCOMPDEB  ( 0x1Du )         /* Motor over current debounce time */
#define SCMT       ( 0x26u )         /* Short circuit masking time selection */
#define SCTH_HS    ( 0x28u )         /* High side short circuit threshold selection */
#define SCTH_LS    ( 0x29u )         /* Low side short circuit threshold selection */
#define SCEN       ( 0x2Cu )         /* Short circuit protection enable */
#define SCDEB      ( 0x2Du )         /* Short circuit debounce time */
#define OFFSET_CFG ( 0x30u )         /* Offset Configuration */
#define OFFSET_VAL ( 0x31u )         /* Offset Value */

#define CHIPCTRL_INITVAL   ( 0x07u ) /* Wake-up and sleep configuration */
#define SLEEPCTRL_INITVAL  ( 0x00u ) /* Sleep mode control 1 */
#define SAFECTRL_INITVAL   ( 0x1Eu ) /* Safety function configuration */
#define IOCFG_INITVAL      ( 0x00u ) /* Interface configuration */
#define VREGCTRL_INITVAL   ( 0x03u ) /* VCC and VG supply control */
#define SCPCTRL_INITVAL    ( 0xD1u ) /* Short circuit protection control */
#define WDCTRL_INITVAL     ( 0x32u ) /* Watchdog control */
#define WDTRIG_INITVAL     ( 0x00u ) /* Watchdog trigger */
#define IRQMSK1_INITVAL    ( 0xFFu ) /* Interrupt mask register 1 */
#define IRQSTAT1_INITVAL   ( 0x00u ) /* Interrupt status register 1 */
#define IRQMSK2_INITVAL    ( 0xFFu ) /* Interrupt mask register 2 */
#define IRQSTAT2_INITVAL   ( 0x00u ) /* Interrupt status register 2 */
#define IOCOMPTHR_INITVAL  ( 0x3Fu ) /* Motor over current threshold */
#define AMUX_INITVAL       ( 0x84u ) /* Analog signal measurement selection */
#define DMUX_INITVAL       ( 0x00u ) /* Digital value output SO selection */
#define IOCOMPDEB_INITVAL  ( 0x11u ) /* Motor over current debounce time */
#define SCMT_INITVAL       ( 0x01u ) /* Short circuit masking time selection */
#define SCTH_HS_INITVAL    ( 0x1Fu ) /* High side short circuit threshold selection */
#define SCTH_LS_INITVAL    ( 0x1Fu ) /* Low side short circuit threshold selection */
#define SCEN_INITVAL       ( 0x3Fu ) /* Short circuit protection enable */
#define SCDEB_INITVAL      ( 0x01u ) /* Short circuit debounce time */
#define OFFSET_CFG_INITVAL ( 0x00u ) /* Offset Configuration */
#define OFFSET_VAL_INITVAL ( 0xA4u ) /* Offset Value */

#define MSK_523_07_ADR ( 0x7Fu )
#define SFT_523_07_ADR ( 0x08u )
#define MSK_523_07_RW  ( 0x01u )
#define SFT_523_07_RW  ( 0x0Fu )

/* --------------------------------
   bit positions of some registers
   -------------------------------- */

/* IRQSTAT1 */
#define BITPOS_OT     ( 1u << 7 )    /* Over temperature interrupt flag (cleared by writing 1) */
#define BITPOS_OC     ( 1u << 6 )    /* Over current interrupt flag / IO comparator interrupt flag (cleared by writing 1) */
#define BITPOS_SC_HS3 ( 1u << 5 )    /* HS 3 short circuit interrupt flag (cleared by writing 1) */
#define BITPOS_SC_HS2 ( 1u << 4 )    /* HS 2 short circuit interrupt flag (cleared by writing 1) */
#define BITPOS_SC_HS1 ( 1u << 3 )    /* HS 1 short circuit interrupt flag (cleared by writing 1) */
#define BITPOS_SC_LS3 ( 1u << 2 )    /* LS 3 short circuit interrupt flag (cleared by writing 1) */
#define BITPOS_SC_LS2 ( 1u << 1 )    /* LS 2 short circuit interrupt flag (cleared by writing 1) */
#define BITPOS_SC_LS1 ( 1u << 0 )    /* LS 1 short circuit interrupt flag (cleared by writing 1) */

/* IRQSTAT2 */
#define BITPOS_OC_DIAG ( 1u << 7 )   /* Over current diagnosis interrupt flag (cleared by writing 1) */
#define BITPOS_TXDTO   ( 1u << 6 )   /* TXD timeout failure interrupt flag (cleared by writing 1) */
#define BITPOS_SPI_CS  ( 1u << 5 )   /* SPI check sum failure interrupt flag (cleared by writing 1) */
/* "#define BITPOS_VBAT_OV  (( 1u ) << 3 )" this bit poistion exists in DMON2 under the same name... */
#define BITPOS_VGATE_OV ( 1u << 2 )  /* VG over voltage interrupt flag (cleared by writing 1) */
#define BITPOS_VGATE_UV ( 1u << 1 )  /* VG under voltage interrupt flag (cleared by writing 1) */
#define BITPOS_VCC_UV   ( 1u << 0 )  /* VCC under voltage interrupt flag (cleared by writing 1) */

/* DMON2 */
#define BITPOS_S       ( 1u << 7 )   /* status of pin S */
#define BITPOS_VBAT_OV ( 1u << 3 )   /* VBAT over voltage comparator */
#define BITPOS_VG_OV   ( 1u << 2 )   /* VGATE over voltage comparator */
#define BITPOS_VG_UV   ( 1u << 1 )   /* VGATE under voltage comparator */

typedef enum
{
  D52307_BEMF_CHANNEL_VSUP = 4,
  D52307_BEMF_CHANNEL_M1   = 8,
  D52307_BEMF_CHANNEL_M2   = 9,
  D52307_BEMF_CHANNEL_M3   = 10
} d52307_bemf_channel_t;

/* -------------------
   global declarations
   ------------------- */
/* blocking write IO register of 523.07 via SPI */
void d52307_write_reg( uint8_t adr, uint8_t data );

/* blocking read IO register of 523.07 via SPI */
void d52307_read_reg( uint8_t adr, uint8_t * const data );

/* initialize SPI interface and IO registers of 523.07 */
void d52307_init( void );

/* trigger watchdog of 523.07 */
void d52307_kick_the_dog( void );

/* read/clear gate driver errors */
void d52307_driver_error_get( uint8_t * const irqstat1_val, uint8_t * const irqstat2_val );
void d52307_driver_error_clear( void );

bool_t d52307_pin_S_status_get( void );
void d52307_go_to_sleep( void );

void d52307_set_bemf_channel( d52307_bemf_channel_t channel );

#endif /* "#ifndef DRIVER_52307_H" */

/* }@
 */
