/**
 *  @file test_FLT_LOG_Record_Fault_Active.c
 *
 */
#include "unity.h"
#include "52307_test_regs.h"
#include "fault_logger.h"
#include "fault_logger_imp_defines.h"
#include "fault_logger_imp_types.h"
#include "lin_vehicle_comm.h"
#include <string.h>
#include "time_utils.h"

/* MOCKS */
#include "mock_lin_vehicle_comm.h"
#include "mock_motor_ctrl.h"
#include "mock_nvm_mgr.h"
#include "mock_serial_data_buffer.h"
#include "mock_system_signals.h"
#include "mock_time_utils.h"

/* STATIC's */
flt_log_Occurence_Block_T flt_log_Occurence_Blocks[FLT_LOG_NUM_DATA_BLOCKS];

uint16_t Test_Regs[TEST_REG_END] = {0};

void setUp(void)
{
    uint8_t Block_ID = 0;

    for(Block_ID = 0; Block_ID < FLT_LOG_NUM_DATA_BLOCKS; Block_ID++)
    {
        uint8_t Fault_Index = 0;

        flt_log_Occurence_Blocks[Block_ID].Dirty = false;

    }
}

void tearDown(void)
{

}

/*----------------------------*\
 * FLT_LOG_OVER_VOLTAGE Tests *
\*----------------------------*/
/**
 * Verifies that the function FLT_LOG_Scan will trigger a write for block 0 when that block is marked dirty
 */
void test_FLT_LOG_Scan_triggers_NVM_Write_of_block_0_when_dirty(void)
{
    /* Ensure known test state */
    flt_log_Occurence_Blocks[0].Dirty = true;

    /* Setup expected call chain */
    NVMMGR_Write_NVM_Block_ExpectAndReturn(0, NVM_STATUS_SUCCESS);

    /* Call function under test */
    FLT_LOG_Scan();
    
    /* Verify test results */
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[0].Dirty);

}

/**
 * Verifies that the function FLT_LOG_Scan will trigger a write for block 1 when that block is marked dirty
 */
void test_FLT_LOG_Scan_triggers_NVM_Write_of_block_1_when_dirty(void)
{
    /* Ensure known test state */
    flt_log_Occurence_Blocks[1].Dirty = true;

    /* Setup expected call chain */
    NVMMGR_Write_NVM_Block_ExpectAndReturn(1, NVM_STATUS_SUCCESS);

    /* Call function under test */
    FLT_LOG_Scan();

    /* Verify test results */
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[1].Dirty);

}

/**
 * Verifies that the function FLT_LOG_Scan will trigger a write for block 2 when that block is marked dirty
 */
void test_FLT_LOG_Scan_triggers_NVM_Write_of_block_2_when_dirty(void)
{
    /* Ensure known test state */
    flt_log_Occurence_Blocks[2].Dirty = true;

    /* Setup expected call chain */
    NVMMGR_Write_NVM_Block_ExpectAndReturn(2, NVM_STATUS_SUCCESS);

    /* Call function under test */
    FLT_LOG_Scan();

    /* Verify test results */
    TEST_ASSERT_FALSE(flt_log_Occurence_Blocks[2].Dirty);

}
