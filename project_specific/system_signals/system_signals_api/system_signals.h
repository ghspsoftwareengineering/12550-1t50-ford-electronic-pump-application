#ifndef SYSTEM_SIGNALS_H
#define SYSTEM_SIGNALS_H

/**
 *  @file system_signals.h
 *
 *  @copyright 2019 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup system_signals_api System related signals and values Interface Documentation
 *  @{
 *      @details System Signals module used for converting any Voltage or Temperature
 *               signals from raw ADC counts.
 *
 *      @page ABBR Abbreviations
 *        - NONE
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "uds.h"


/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
/* A -> D volts per count * 100000. Actual is 2.5V/ 4096 or .000610352 */
#define A2D_V_PER_COUNT         (61)

#define SYSIG_DIAG_CPU_TEMP_SIZE (sizeof(int16_t))
#define SYSIG_DIAG_CURRENT_SIZE  (sizeof(int16_t))
#define SYSIG_DIAG_CPU_IDLE_SIZE (sizeof(uint8_t))

#define SYSIG_NUM_POLE_PAIRS (2u)

/** Period to execute the CPU Load measuring task. This number must match the IDLE_UNLOADED_MAX_COUNTS as they are related */
#define SYSIG_CPU_LOAD_TASK_INTERVAL_MS (25u)

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/
/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/
/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/**
 * Initializes the System Signals module.
 *
 * @note This must be executed before any Signals provided by the module are valid
 *
 * @return Value indicating success or failure of the initialization
 * @retval true - successful initialization
 * @retval false - failure to initialize
 */
bool_t SYSIG_Init(void);

/**
 * Using the ADC voltage value of the CPU temperature sense channel, retrieves
 * the CPU temperature
 *
 * @return  int16_t Temperature in Degrees C
 */
int16_t SYSIG_Get_CPU_Temp(void);

/**
 * Getter for the system supply current.
 *
 * @return Supply current in 1/10 A units
 */
int16_t SYSIG_Get_Current(void);

/**
 * Getter for system voltage.
 *
 * @return System voltage in 1/10 volt units 
 */
uint16_t SYSIG_Get_Voltage(void);

/**
 * Getter for actual RPM.
 *
 * @return Actual RPM in RPM units 
 */
uint16_t SYSIG_Get_Actual_RPM(void);

/**
 * Function to be called every time the system is idle. This is used to
 * keep track of the number of times during a specific interval of time that the system
 * is idle. This can then be used with a precomputed factor to determine the CPU Load.
 */
void SYSIG_Idle_Count_Handler(void);

/**
 * Periodic task for calculating the CPU load.
 */
void SYSIG_CPU_Load_Task(void);

/**
 * Provides a serialization of the current CPU Temperature
 *
 * @param p_Resp_SDB
 * @return
 */
UDS_Response_Code_T SYSIG_Diag_Get_CPU_Temp(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Overrides the reported CPU temperature to the supplied value. To disable the override
 * and restore the actual CPU temperature, write the value 0x8000 (INT16_MIN).
 *
 * @param p_Req_SDB
 * @return
 */
UDS_Response_Code_T SYSIG_Diag_Override_CPU_Temp(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Provides a serialization of the Supply Current
 *
 * @param p_Resp_SDB
 * @return
 */
UDS_Response_Code_T SYSIG_Diag_Get_Current(Serial_Data_Buffer_T * const p_Resp_SDB);

/**
 * Overrides the reported Supply Current to the supplied value. To disable the override
 * and restore the actual Supply Current, write the value 0x8000 (INT16_MIN).
 *
 * @param p_Req_SDB
 * @return
 */
UDS_Response_Code_T SYSIG_Diag_Override_Current(Serial_Data_Buffer_T * const p_Req_SDB);

/**
 * Provides the calculated percentage of time that the CPU is idle. Value is a percentage scaled
 * to a full uint8_t. 255 = 100%
 *
 * @param p_Resp_SDB - Serial Data Buffer into which the CPU Idle percentage is to be placed
 *
 * @return UDS_RESP_POSITIVE
 */
UDS_Response_Code_T SYSIG_Diag_Get_CPU_Idle(Serial_Data_Buffer_T * const p_Resp_SDB);

/** @} doxygen end group */

#endif /* SYSTEM_SIGNALS_H */
