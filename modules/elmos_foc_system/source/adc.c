/**
 * @ingroup HW
 *
 * @{
 */

/**********************************************************************/
/*! @file adc.c
 * @brief ADC module
 *
 * @details
 * contains all functions to initialize and configure the
 * ADC of the motCU as well as the IRQ callback functions and the
 * the access functions for the results<br>
 * module prefix: adc_
 *
 * @author       JBER
 */
/**********************************************************************/
/* Demo Code Usage Restrictions:
 * Elmos Semiconductor AG provides this source code file simply and solely for IC evaluation purposes in laboratory and
 * this file must not be used for other purposes or within non laboratory environments. Especially, the use or the integration in
 * production systems, appliances or other installations is prohibited.
 *
 * Disclaimer:
 * Elmos Semiconductor AG shall not be liable for any damages arising out of defects resulting from (1) delivered hardware or software,
 * (2) non observance of instructions contained in this document, or (3) misuse, abuse, use under abnormal conditions or alteration by anyone
 * other than Elmos Semiconductor AG. To the extend permitted by law Elmos Semiconductor AG hereby expressively disclaims and user expressively
 * waives any and all warranties of merchantability and of fitness for a particular purpose, statutory warranty of non-infringement and any other
 * warranty or product liability that may arise by reason of usage of trade, custom or course of dealing.
 */
/**********************************************************************/

#include <adc.h>

#include <debug.h>

#include <adc_lists.h>
#include <motor_ctrl.h>
#include <systime.h>
#include "vic_InterruptHandler.h"

#if ( SCI_USAGE_TYPE == SCI_DATALOGGER )
 #include <uart.h>
#endif

/* -------------
   local defines
   ------------- */
adc_data_t adc_data = {0u};

/* ------------------
   local declarations
   ------------------ */

/*! @brief ISR for "ADC list ready"-interrupt
 *
 * @details
 * callback is registered in adc_init(); when ADC has finished all
 * measurements that are contained in a list, this ISR is executed
 *
 * @param     irq (IRQ number)
 */
__interrupt static void adc_irq_handler_list_done ( void )
{
  mc_isr_handler();
  SARADC_CTRL_IRQ_VNO = GET_BIT_POS((uint16_t) E_SARADC_CTRL_IRQ_STATUS_LIST_DONE_EVT );
}

/*! @brief get pointer to structure which is holding the ADC result data */
INLINE const adc_data_t * adc_get_data ( void )
{
  return &adc_data;
}

/*! @brief clear structure which is holding the ADC result data */
INLINE void adc_clear_data( void )
{
  adc_data.phase_curr1        = (uint16_t) UINT16_MAX;
  adc_data.phase_curr1_offset = (uint16_t) UINT16_MAX;
  adc_data.phase_curr2        = (uint16_t) UINT16_MAX;
  adc_data.phase_curr2_offset = (uint16_t) UINT16_MAX;
  adc_data.dummy              = (uint16_t) UINT16_MAX;
  adc_data.vsup               = (uint16_t) UINT16_MAX;
  adc_data.phase_voltage_u    = (uint16_t) UINT16_MAX;
  adc_data.phase_voltage_v    = (uint16_t) UINT16_MAX;
  adc_data.phase_voltage_w    = (uint16_t) UINT16_MAX;
}

/*! @brief register new ISR for ADC interrupt */
void static adc_set_interrupt_handler( vic_InterruptCallback_t interrupthandler )
{
  istate_t istate = __get_interrupt_state();
  __disable_interrupt();

  vic_InterfaceFunctions.RegisterModule( vic_IRQ_ADC_CTRL, interrupthandler, NULL );

  __set_interrupt_state ( istate );
}

/*! @brief init/configure ADC module
 *
 * @details
 * configure the ADC, register the callback function for when
 * the ADC has finished a conversion list (the measurements are
 * triggered and configured via lists in RAM, see saradc_ctrl.h ->
 * struct saradc_ctrl_list_entry)
 */
NO_INLINE void adc_init ( void )
{
  /* register CFG */
  SARADC_CTRL_CFG_bit = (saradc_ctrl_cfg_bf_t)
  {
    .adc_on       = 1u,
    .adc_reset    = 1u,
    .mux_sel      = 0u,                                          /* DRIVER_WITH_MUXER */
    .ext_sel      = 0u,                                          /* DRIVER_MUXER_CHANNEL_NUMBER */
    .missed_dt_e  = 0u,
    .store_dt_evt = 0u,
    .adcloop_up   = 0u,
  };

/* register SARADC TIMING */
  SARADC_CTRL_CFG_SAR_TIMING_bit = (saradc_ctrl_cfg_sar_timing_bf_t)
  {
    .adc_clk_div        = 2u,                                    /* input clock of module is not divided */
    .mux_phase          = 5u,                                    /* clocks when MUXer is changed (?) */
    .sampling_extension = (uint16_t) ADC_SAMPLE_EXTENSION_CLK_CYCLES, /* clock cycles that the std sample time is extended */
  };

  /* register WADR_MIN & WADR_MAX */
  SARADC_CTRL_WADR_MIN = 0u;
  SARADC_CTRL_WADR_MAX = UINT16_MAX;

  /* register SYNC_OUT_CFG and SYNC_OUT_TRIG */
  SARADC_CTRL_SYNC_OUT_CFG_bit = (saradc_ctrl_sync_out_cfg_bf_t )
  {
    .length = 1u,                                                /* length of sync_out pulse in clock cycles */
    .pol    = 1u,                                                /* polarity of pulse */
    .src    = 2u,                                                /* source of pulse */
  };

  SARADC_CTRL_SYNC_OUT_TRIG = 0u;

  /* configure delays for measurements related to deadtime events */
  adc_deadtime_config( 0u, 0u, 0u );

  /* register callback function for ADC ISR*/
  adc_set_interrupt_handler( &adc_irq_handler_list_done );
  vic_InterfaceFunctions.EnableModule( vic_IRQ_ADC_CTRL );
  SARADC_CTRL_IRQ_VENABLE = GET_BIT_POS((uint16_t) E_SARADC_CTRL_IRQ_STATUS_LIST_DONE_EVT );

  adc_data.enable_pb7_dbg_signal = False;

  /* check whether measurement defines make sense */
  static_assert(( ADC_SECOND_CURRENT_SAMPLE - ADC_FIRST_CURRENT_SAMPLE ) >= ADC_CONVERSION_CLK_CYCLES, "measurement timings invalid because the two phase current measurements are too close to one another" );
}

void adc_generate_struct_signature ( void )
{
  adc_data.struct_signature = U16PTR_TO_U16( &adc_data.struct_signature );
}

/*! @brief not used */
INLINE void adc_main( void )
{
}

/*! @brief enable IRQ 'measurement list finished' */
INLINE void adc_list_ready_irq_enable ( void )
{
  VIC_IRQ_VENABLE         = (uint16_t) GET_BIT_POS((uint16_t) E_VIC_IRQ_STATUS0_ADC_CTRL );
  SARADC_CTRL_IRQ_VENABLE = GET_BIT_POS((uint16_t) E_SARADC_CTRL_IRQ_STATUS_LIST_DONE_EVT );
}

/*! @brief disable IRQ 'measurement list finished' */
INLINE void adc_list_ready_irq_disable ( void )
{
  SARADC_CTRL_IRQ_VDISABLE = GET_BIT_POS((uint16_t) E_SARADC_CTRL_IRQ_STATUS_LIST_DONE_EVT );

  if( SARADC_CTRL_IRQ_MASK == 0u )
  {
    VIC_IRQ_VDISABLE = (uint16_t) GET_BIT_POS((uint16_t) E_VIC_IRQ_STATUS0_ADC_CTRL );
  }
}

/*! @brief set the wait time before ADC sampling after beginning of deadtime event
 *
 * @param     deadtime_0 _1 _2
 */
INLINE void adc_deadtime_config ( uint8_t deadtime_wait_0, uint8_t deadtime_wait_1, uint8_t deadtime_wait_2 )
{
  SARADC_CTRL_DEAD_TIME_WAIT0 = (uint16_t) deadtime_wait_0;
  SARADC_CTRL_DEAD_TIME_WAIT1 = (uint16_t) deadtime_wait_1;
  SARADC_CTRL_DEAD_TIME_WAIT2 = (uint16_t) deadtime_wait_2;
}

/*! @brief set a new measurement command list for ADC and skip current list
 *
 * @details
 * explanation: the ADC of the motCU operates based on lists (i.e. arrays of
 * structs; -> saradc_ctrl.h and adc_lists.h) that are stored inside the RAM;
 * a list of 'saradc_table_list_entry's configures the measurements that are
 * to be done by the ADC; this function acquaints the ADC with a new list and thereby
 * starts the execution of the measurement tasks contained in this list
 *
 * @param     pointer to list
 */
INLINE void adc_measurement_list_set_and_skip ( saradc_ctrl_list_entry_t const * list_new )
{
  #if ( DBG_PIN3_FUNCTION == DBG_ADC_SHOW_SET_NEW_LIST )
    dbg_pin3_set();
  #endif

  _Pragma( "diag_suppress=Pm140" )                               /* justification: access to SFR */
  SARADC_CTRL_SADR_NEW = (uint16_t) list_new | 1u;                    /* PRQA S 0306 */ /* justification: access to SFR */
  _Pragma( "diag_default=Pm140" )

  #if ( DBG_PIN3_FUNCTION == DBG_ADC_SHOW_SET_NEW_LIST )
    dbg_pin3_clear();
  #endif
}

/*! @brief set a new measurement command list for ADC
 *
 * @details
 * explanation: the ADC of the motCU operates based on lists (i.e. arrays of
 * structs; -> saradc_ctrl.h and adc_lists.h) that are stored inside the RAM;
 * a list of 'saradc_table_list_entry's configures the measurements that are
 * to be done by the ADC; this function acquaints the ADC with a new list and thereby
 * starts the execution of the measurement tasks contained in this list
 *
 * @param     pointer to list, skip_list
 * @return
 */
INLINE void adc_measurement_list_set ( saradc_ctrl_list_entry_t const * list_new, bool_t skip_list )
{
  #if ( DBG_PIN3_FUNCTION == DBG_ADC_SHOW_SET_NEW_LIST )
    dbg_pin3_set();
  #endif

  if( skip_list )
  {
    _Pragma( "diag_suppress=Pm140" )                             /* justification: access to SFR */
    SARADC_CTRL_SADR_NEW = (uint16_t) list_new | 1u;                  /* PRQA S 0306 */ /* justification: access to SFR */
    _Pragma( "diag_default=Pm140" )
  }
  else
  {
    _Pragma( "diag_suppress=Pm140" )                             /* justification: access to SFR */
    SARADC_CTRL_SADR_NEW = (uint16_t) list_new & ( ~(uint16_t) 1 );        /* PRQA S 0306 */ /* justification: access to SFR */
    _Pragma( "diag_default=Pm140" )
  }

  #if ( DBG_PIN3_FUNCTION == DBG_ADC_SHOW_SET_NEW_LIST )
    dbg_pin3_clear();
  #endif
}

/*! @brief get-function for user measurements
 *
 * @details
 * there are 4 measurements per PWM period which can be freely configured by the application (for sampling
 * an external potentiometer, for instance)<br>
 * calling this function returns the last measured value of the signal indicated by the argument
 */
uint16_t adc_get_user_data( uint8_t idx )
{
  uint16_t loc_result = (uint16_t) UINT16_MAX;

  if( idx < (uint16_t) ADC_USER_CHANNEL_CNT )
  {
    loc_result = adc_data.usr_result[ idx ];
  }

  return loc_result;
}

uint16_t adc_get_supply_voltage(void)
{
    return adc_data.vsup;
}
/* }@
 */
