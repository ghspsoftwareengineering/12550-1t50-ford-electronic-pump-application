/***************************************************************************//**
 * @file		LinDiag_Interface.h
 *
 * @creator		sbai
 * @created		25.03.2015
 *
 * @brief  		Definition of the interface for the 'LIN DIAG Layer'.
 *
 * $Id$
 *
 * $Revision$
 *
 ******************************************************************************/

#ifndef LINDIAG_INTERFACE_H_
#define LINDIAG_INTERFACE_H_

/* ****************************************************************************/
/* ******************************** INCLUDES **********************************/
/* ****************************************************************************/
#include "LinDiag_Types.h"
#include "LinLookup_Interface.h"
#include "LinSNPD_Interface.h"
#include "LinTrans_Interface.h"

/* ****************************************************************************/
/* *************************** DEFINES AND MACROS *****************************/
/* ****************************************************************************/
#define LINDIAG_INTERFACE_MODULE_API_VERSION      0x0102u

/* ****************************************************************************/
/* *********************** STRUCTS, ENUMS AND TYPEDEFS ************************/
/* ****************************************************************************/

/***************************************************************************//**
 * @brief Typedef of DIAG layer 'Initialization' interface function.
 *
 * @param genericBusEnvData       @copydoc LinDiagIf_pGenericEnvData_t
 * @param diagEnvDataSze          Size of the reserved RAM space for DIAG environment
 *                                data.
 * @param diagCbFuns              Pointer to implementations of DIAG callbacks.
 * @param genericDiagCbCtxData    Pointer to DIAG callback context data.
 * @param invalidReadByIDAnswered Answer to an invalid "ReadById" request or not.
 * @param genericDiagImpCfgData   DIAG layer configuration data depending of the
 *                                implementation.
 *
 * @return                        LIN_TRUE if the initialization was successfully
 *                                LIN_FALSE if not.
 *
 * Initializes the LIN DIAG layer.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinDiagIf_InitializationIfFun_t) (LinDiagIf_pGenericEnvData_t     genericDiagEnvData,      LinDiagIf_EnvDataSze_t         diagEnvDataSze,
                                                       LinDiagIf_cpCallbackFunctions_t diagCbFuns,              LinDiagIf_pGenericCbCtxData_t  genericDiagCbCtxData,
                                                       Lin_Bool_t                      invalidReadByIDAnswered, LinDiagIf_pGenericImpCfgData_t genericDiagImpCfgData);

/***************************************************************************//**
 * @brief Typedef of DIAG Layer 'Task' interface function.
 *
 * @param genericDiagEnvData[in]   Pointer to reserved DIAG Layer environment data.
 *
 * The Task function has to be called periodically to process scheduled task of
 * the DIAG Layer.
 *
 ******************************************************************************/
typedef void (*LinDiagIf_TaskIfFun_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData);

/***************************************************************************//**
 * @brief Typedef of LIN DIAG layer 'Get Sub-Interface' interface function.
 *
 * @param genericDiagEnvData  @copydoc LinDiagIf_pGenericEnvData_t
 * @param interfaceId         @copydoc Lin_eInterfaceIds
 * @param ifThisPtr           @copydoc Lin_pThis
 *
 * @return  LIN_TRUE if the desired interface is available.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinDiagIf_GetSubInterfaceIfFun_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData, Lin_eInterfaceIds_t interfaceId, Lin_pThis_t ifThisPtr);

/***************************************************************************//**
 * @brief Typedef of DIAG layer 'Get NAD' interface function.
 *
 * @param genericBusEnvData     @copydoc LinDiagIf_pGenericEnvData_t
 * @param nadtype               Type of NAd to acquire.
 *
 * @return Desired NAD.
 *
 * Determine the actual NAD configured in the DIAG layer.
 *
 ******************************************************************************/
typedef LinDiagIf_NAD_t (*LinDiagIf_GetNADIfFun_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_eNADType_t nadtype);

/***************************************************************************//**
 * @brief Typedef of DIAG layer 'Set NAD' interface function.
 *
 * @param genericDiagEnvData    @copydoc LinDiagIf_pGenericEnvData_t
 * @param nad                   NAD to set.
 *
 * Configure the actually used NAD in the DIAG layer.
 *
 ******************************************************************************/
typedef void (*LinDiagIf_SetNADIfFun_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_NAD_t nad);

/***************************************************************************//**
 * @brief Typedef of DIAG layer 'Add RBI Table' interface function.
 *
 * @param genericDiagEnvData[in]  @copydoc LinDiagIf_pGenericEnvData_t
 * @param rbiTbl[in]              Pointer to 'RBI Lookup Table'.
 *
 * @return  LIN_TRUE if the 'RBI Lookup Table' is correctly defined and 
 *          has been succesfully added to the DIAG Layer.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinDiagIf_AddRbiTableIfFun_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_pRbiLookupEntry_t rbiTbl);

/***************************************************************************//**
 * @brief Typedef of DIAG layer 'Remove RBI Table'' interface function.
 *
 * @param genericDiagEnvData[in]  @copydoc LinDiagIf_pGenericEnvData_t
 * @param rbiTbl[in]              Pointer to 'RBI Lookup Table'.
 *
 * @return  LIN_TRUE if the 'RBI Lookup Table' has been succesfully removed 
 *          from the DIAG Layer.
 *
 ******************************************************************************/
typedef Lin_Bool_t (*LinDiagIf_RmvRbiTableIfFun_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_pRbiLookupEntry_t rbiTbl);

/***************************************************************************//**
 * @brief LIN DIAG layer interface functions struct.
 *
 * Collective struct for the DIAG layer interface functions.
 ******************************************************************************/
struct LinDiagIf_sInterfaceFunctions
{
    Lin_Version_t                      InterfaceVersion;  /**< @brief DIAG Layer interface version.*/

    LinDiagIf_InitializationIfFun_t    Initialization;    /**< TODO: Add description */
    LinDiagIf_TaskIfFun_t              Task;              /**< TODO: Add description */
    LinDiagIf_GetSubInterfaceIfFun_t   GetSubInterface;   /**< TODO: Add description */
    LinDiagIf_GetNADIfFun_t            GetNAD;            /**< TODO: Add description */
    LinDiagIf_SetNADIfFun_t            SetNAD;            /**< TODO: Add description */
    LinDiagIf_AddRbiTableIfFun_t       AddRbiTable;       /**< TODO: Add description */
    LinDiagIf_RmvRbiTableIfFun_t       RmvRbiTable;       /**< TODO: Add description */
};

/***************************************************************************//**
 * @brief Typedef of DIAG Layer 'Error' callback function.
 *
 * @param genericUdsEnvData[in]     Pointer to reserved DIAG Layer environment data.
 * @param diagIfFuns[in]            Pointer to the DIAG Layer interface function struct.
 * @param error[in]                 Occurred error.
 * @param sid[in]             	    Related SID if known.
 * @param genericDiagCbCtxData[in]  Pointer to DIAG Layer callback context data.
 *
 * Indicates an error in the DIAG Layer and underlying layers of the LIN
 * Driver.
 *
 * @par "Call Description:"
 * @mscfile msc_diag_errorcbcalled.dox
 * @n
 *
 ******************************************************************************/
typedef void (*LinDiagIf_ErrorCbFun_t) (LinDiagIf_pGenericEnvData_t   genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t diagIfFuns,
                                        LinDiagIf_Error_t             error,              LinDiagIf_SID_t                  sid,
                                        LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData);

/***************************************************************************//**
 * @brief Typedef of DIAG Layer 'Go To Sleep' callback function.
 *
 * @param genericUdsEnvData[in]     Pointer to reserved DIAG Layer environment data.
 * @param diagIfFuns[in]            Pointer to the DIAG Layer interface function struct.
 * @param genericDiagCbCtxData[in]  Pointer to DIAG Layer callback context data.
 *
 * This callback is called if the DIAG Layer received a Go-To-Sleep frame.
 *
 * @par "Call Description:"
 * @mscfile msc_diag_gotosleepcbcalled.dox
 * @n
 *
 ******************************************************************************/
typedef void (*LinDiagIf_GoToSleepCbFun_t) (LinDiagIf_pGenericEnvData_t   genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t ifFunctions,
                                            LinDiagIf_pGenericCbCtxData_t genericDiagCbCtxData);

/***************************************************************************//**
 * @brief Typedef of DIAG Layer 'Timeout' callback function.
 *
 * @param genericDiagEnvData[in]  Pointer to reserved DIAG Layer environment data.
 * @param diagIfFuns[in]          Pointer to the DIAG Layer interface function struct.
 * @param timerNum[in]            Timer index.
 * @param genericUdsCbCtxData[in] Pointer to DIAG Layer callback context data.
 *
 * This callback is called after the configured timeout has timed out.
 *
 * @par "Call Description:"
 * @mscfile msc_diag_timeoutcbcalled.dox
 * @n
 *
 ******************************************************************************/
typedef void (*LinDiagIf_TimeoutCbFun_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t diagIfFuns,
                                          Lin_uint8_t                 timerNum,           LinDiagIf_pGenericCbCtxData_t    genericDiagCbCtxData);

/***************************************************************************//**
 * @brief Typedef of DIAG Layer 'Store NAD' callback function.
 *
 * @param genericDiagEnvData[in]   Pointer to reserved DIAG Layer environment data.
 * @param diagIfFuns[in]           Pointer to the DIAG Layer interface function struct.
 * @param nad[in]                  NAD to store.
 * @param genericDiagCbCtxData[in] Pointer to DIAG Layer callback context data.
 *
 * This callback is called if the 'Store NAD' command has been send during
 * auto adressing.
 *
 * @par "Call Description:"
 * @mscfile msc_diag_storenadcbcalled.dox
 * @n
 *
 ******************************************************************************/
typedef void (*LinDiagIf_StoreNADCbFun_t) (LinDiagIf_pGenericEnvData_t genericDiagEnvData, LinDiagIf_cpInterfaceFunctions_t diagIfFuns,
                                           LinDiagIf_NAD_t             nad,                LinDiagIf_pGenericCbCtxData_t    genericDiagCbCtxData);

/***************************************************************************//**
 * @brief LIN UDS Layer callback functions struct.
 *
 * The set of callbacks which should be invoked in the user application.
 ******************************************************************************/
struct LinDiagIf_sCallbackFunctions
{
  Lin_Version_t              CallbackVersion; /**< @brief Callback Version */

  LinDiagIf_GoToSleepCbFun_t GoToSleep;       /**< @brief Pointer to DIAG Layer 'Go To Sleep' callback function. (@ref LinDiagIf_GoToSleepCbFun_t) @copydetails LinDiagIf_GoToSleepCbFun_t */
  LinDiagIf_ErrorCbFun_t     Error;           /**< @brief Pointer to DIAG Layer 'Error' callback function. (@ref LinDiagIf_ErrorCbFun_t) @copydetails LinDiagIf_ErrorCbFun_t */
  LinDiagIf_TimeoutCbFun_t   Timeout;         /**< @brief Pointer to DIAG Layer 'Timeout' callback function. (@ref LinDiagIf_TimeoutCbFun_t) @copydetails LinDiagIf_TimeoutCbFun_t */
  LinDiagIf_StoreNADCbFun_t  StoreNAD;        /**< @brief Pointer to DIAG Layer 'Store NAD' callback function. (@ref LinDiagIf_StoreNADCbFun_t) @copydetails LinDiagIf_StoreNADCbFun_t */
};

/***************************************************************************//**
 * @brief Object-like This-pointer to connect the DIAG Layer to other layers.
 ******************************************************************************/
struct LinDiagIf_sThis
{
    LinDiagIf_cpInterfaceFunctions_t IfFunsTbl; /**< @brief Pointer to the DIAG Layer interface function struct. (@ref LinDiagIf_sInterfaceFunctions) */
    LinDiagIf_pGenericEnvData_t      EnvData;   /**< @brief Pointer to reserved DIAG Layer environment data. */
};

/***************************************************************************//**
 * @brief LIN UDS Layer interface configuration parameter.
 *
 * Data needed for initialization of the UDS Layer.
 ******************************************************************************/
struct LinDiagIf_sInitParam
{
    LinDiagIf_cpInterfaceFunctions_t       IfFunsTbl;            /**< @brief Pointer to the constant DIAG Layer interface function struct. */
    LinDiagIf_pGenericEnvData_t            EnvData;              /**< @brief Pointer to reserved DIAG Layer environment data. */
    LinDiagIf_Length_t                     EnvDataLen;           /**< @brief Size of the reserved RAM for DIAG Layer environment data. */
    LinProtoIf_pGenericFrmDescLstEnvData_t FrmDescLstEnvData;    /**< TODO: Add description */
    LinProtoIf_EnvDataSze_t                FrmDescLstEnvDataSze; /**< TODO: Add description */
    LinDiagIf_pGenericImpCfgData_t         ImpCfgData;           /**< @brief Pointer to implementation dependent configuration data for the DIAG Layer. (LinDiagImp_sCfgData) */
};

/* ****************************************************************************/
/* **************************** GLOBALE VARIABLES *****************************/
/* ****************************************************************************/

/* ****************************************************************************/
/* ********************* EXTERNAL FUNCTIONS / INTERFACE ***********************/
/* ****************************************************************************/

#endif /* LINDIAG_INTERFACE_H_ */
